<?php
	require "inv.header.php";
	if(isset($maintenance) && $maintenance == true)
	{
		header("HTTP/1.1 503");
		require "public/503.htm";
		exit;
	}
	if(isset($_GET['page']) && $_GET['page'] != "")
	{
		if($_GET['page'] == "about")
			require "includes/about.php";
		else if($_GET['page'] == "account")
			require "includes/account.php";
		else if($_GET['page'] == "atom")
			require "includes/atom.php";
		else if($_GET['page'] == "post")
			require "includes/posts.php";
		else if($_GET['page'] == "history")
			require "includes/history.php";
		else if($_GET['page'] == "comment")
			require "includes/comment.php";
		else if($_GET['page'] == "search")
			require "includes/search.php";
		else if($_GET['page'] == "favorites")
			require "includes/favorites.php";
		else if($_GET['page'] == "alias")
			require "includes/alias.php";
		else if($_GET['page'] == "forum")
			require "includes/forum.php";
		else if($_GET['page'] == "tags")
			require "includes/tags.php";
		else if($_GET['page'] == "print")
			require "includes/print.php";
		else if($_GET['page'] == "help")
			require "includes/help.php";
		/*else if($_GET['page'] == "contact")
		{
			header("Location:mailto:".$site_email);
			exit;
		}*/
		else 
		{
			header("Location:".$site_url);
			exit;
		}
	}
	else
		require "includes/index.php";
?>