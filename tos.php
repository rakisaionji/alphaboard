<?php
	require "inv.header.php";
	require "includes/header.php";
?>
	<div id="content" style="width:800px;margin:auto">
	<h2>Terms of Service</h2><br>
	<div class="section">
		<p>By accessing the <?php print ucfirst($site_url3); ?> website ("Site") you agree to the following terms of service. If you do not agree to these terms, then please do not access the Site.</p>
		<ul>
		<li>The Site reserves the right to change these terms at any time.</li>
		<li>If you are a minor, then you will not use the Site.</li>
		<li>The Site is presented to you AS IS, without any warranty, express or implied. You will not hold the Site or its staff members liable for damages caused by the use of the site.</li>
		<li>The Site reserves the right to delete or modify your account, or any content you have posted to the site.</li>
		<li>You will make a best faith effort to upload only high quality anime-related images.</li>
		<li>You have read the <a href="index.php?page=help&amp;topic=tags">tagging guidelines</a>.</li>
		</ul>
	</div>
	<div class="section">
		<p>In addition, you may not use the Site to upload any of the following:</p>
		<ul>
		<li>Non-anime: Photographs from the real life, photographs of cosplayers, figures, or prominent figures in the industry are prohibited.</li>
		<li>Furry: Any image or movie where a person's skin is made of fur or scales. For example, a girl with cat ears and fur on her body.</li>
		<li>Watermarked: Any image where a person who is not the original copyright owner has placed a watermark on the image.</li>
		<li>Poorly compressed: Any image where compression artifacts are easily visible for human eyes.</li>
		<li>Manga: Uploading entire manga or doujinshi chapters is discouraged. Individual pages can be uploaded if they meet the quality criterion.</li>
		<li>Nude: All kinds of nude and porn are prohibited. We do not allow explicit posts exist on our site.</li>
		<li>Fake translations: Images that have false translations are not allowed in any case.</li>
		<li>Hard translations: Images that have been edited to replace the original language are discouraged.</li>
		</ul>
	</div>
	<div class="section">
		<p>If you believe a post infringes upon your copyright, please send an email to the webmaster with the following pieces of information:</p>
		<ul>
		<li>The URL of the infringing post.</li>
		<li>Proof that you own the copyright.</li>
		<li>An email address that will be provided to the person who uploaded the infringing post to facilitate communication.</li>
		</ul>
	</div><br>
	<h2>Privacy Policy</h2><br>
	<div class="section">
		<p>The Site will not disclose any IP addresses, email addresses, user passwords, or personal messages of any user except to the staff.
		<br>The Site is allowed to make public everything else, including but not limited to: uploaded posts, favorited posts, comments, forum posts and note edits.</p>
	</div><br>
	<div>
		<h5>By using the Site, you have already read all the terms and have agreed to them.
		<br>If not, please stop using the Site.</h5>
	</div> 
	</div>
</body>
</html>
