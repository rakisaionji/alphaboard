<?php
	require "inv.header.php";
	//Authentication Procedure
	$user = new user();
	if(!$user->gotpermission('is_admin'))
	{
		header('Location: index.php');
		exit;
	}
	$path = $import_folder."/";
	$image = new image();
	$folders = scandir($path);
	print "Processing folder for files... Please wait.<br><br>";
	//Scan directory for folders. Exclude . and ..
	foreach($folders as $folder)
	{
		if(is_dir($path.$folder) && $folder !="." && $folder !="..")
		{
			$cur_folder[] = $folder;
			$tags2[] = $folder;	
		}
	}
	$i = 0;
	foreach($cur_folder as $current_folder)
	{
		//Check for images in folder and add them one by one.
		$files = scandir($path.$current_folder);
		foreach($files as $file)
		{
			$extension = explode(".",$file);
			if($extension['1'] == "jpg" || $extension['1'] == "jpeg" || $extension['1'] == "png" || $extension['1'] == "bmp" || $extension['1'] == "gif")
			{
				$uploaded_image = false;
				//Extension looks good, toss it through the image processing section.
				$dl_url = $site_url.$path.rawurlencode($current_folder)."/".rawurlencode($file);
				$iinfo = $image->getremoteimage($dl_url);
				if($iinfo === false)
					$error = $image->geterror()."<br>Could not add the image.";
				else
					$uploaded_image = true;	
				//Ok, download of image was successful! (yay?)
			if($uploaded_image == true)
			{
			$iinfo = explode(":",$iinfo);
			$tclass = new tag();
			$misc = new misc();
			$rating = "Questionable"; $parent = '';
			$ext = strtolower(substr($iinfo[1],-4));
			$tags = mb_strtolower(str_replace('%','',htmlentities(str_replace("\r\n"," ",$tags2[$i]), ENT_QUOTES, 'UTF-8')), 'UTF-8');
			$ttags = array_filter(explode(' ',$tags));
			$tag_count = count($ttags);		
			if($tag_count == 0)
				$ttags[] = "tagme";
			if($tag_count < 5 && strpos(implode(" ",$ttags),"tagme") === false)
				$ttags[] = "tagme";
			$temp_array = array();
			foreach($ttags as $current)
			{
				$params = explode('()', $current);
				//Mac and Windows do not allow dirs or files with vulnerable symbols like punctuation marks so we use () (pussy) instead of : (colon). Pussy !? lol
				if (count($params) > 1){
				if($user->gotpermission('edit_tags'))
				$cmd = $db->real_escape_string($params[0]);
				else
				$cmd = $post->tag_type($current);
				$param = $params[1];
				$alias = $tclass->alias($param);
				if($alias !== false) $param = $alias;
				/* Only accept x:y and use x, y as valid
				If user use x:y:z only x and y is valid */
				switch($cmd){
				case 'tag':
				case 'general':
					$tag_type = "general";
					$current = $param;
					$tclass->updatetag($current, $tag_type);
					break;
				case 'char':
				case 'character':
					$tag_type = "character";
					$current = $param;
					$tclass->updatetag($current, $tag_type);
					break;
				case 'circle':
					$tag_type = "circle";
					$current = $param;
					$tclass->updatetag($current, $tag_type);
					break;
				case 'art':
				case 'artist':
					$tag_type = "artist";
					$current = $param;
					$tclass->updatetag($current, $tag_type);
					break;
				case 'faults':
					$tag_type = "faults";
					$current = $param;
					$tclass->updatetag($current, $tag_type);
					break;
				case 'copy':
				case 'copyright':
					$tag_type = "copyright";
					$current = $param;
					$tclass->updatetag($current, $tag_type);
					break;
/*				case 'pool':
					# Check if sequence was given.
					$sequence = (isset($params[2])) ? $params[2] : null;
					# Insert post into pool.
					$Pool->addPost($post_id, $param, $sequence);
					unset($tags_array[$key]);
					break;
				case '-pool':
					# Remove post from pool.
					if (is_numeric($param)){
					$Pool->removePost($post_id, $param);
					break;
					} else {
					unset($tags_array[$key]);
					break;
					}
*/				case 'parent':
					$parent = $param;
					if(!is_numeric($parent))
					unset($parent);
					unset($current);	
					break;
				case 'rating':
					$rating = strtolower(substr($param, 0, 1));
					if($rating == "e")
						$rating = "Explicit";
					else if($rating == "q")
						$rating = "Questionable";
					else
						$rating = "Safe";
					unset($current);	
					break;
				default:
					break;
				} }
				if(isset($current) && $current != "" && $current != " ")
				{
					$alias = $tclass->alias($current);
					if($alias !== false) $current = $alias;
					$temp_array[] = $current;
				}
			}
			$ttags = $temp_array;
			$tags = implode(" ",$ttags);
			foreach($ttags as $current)
			{
				if($current != "" && $current != " ")
				$ttags = $tclass->filter_tags($tags,$current, $ttags);
			}
			asort($ttags);
			foreach($ttags as $current)
				$tclass->addindextag($current);
			$tags = $db->real_escape_string(implode(" ",$ttags));
			$tags = mb_trim(str_replace("  ","",$tags));			
			if(substr($tags,0,1) != " ")
				$tags = " $tags";
			if(substr($tags,-1,1) != " ")
				$tags = "$tags ";
			$ip = $db->real_escape_string($_SERVER['REMOTE_ADDR']);
			$image_link = "./".$image_folder."/".$iinfo[0]."/".$iinfo[1];
			$isinfo = getimagesize($image_link);
			$query = "INSERT INTO $post_table(title, source, approved, creation_date, hash, crc32, sha1, image, owner, height, width, size, ext, rating, tags, directory,  active_date, ip) VALUES('', '', 1,'".mktime()."', '".md5_file($image_link)."', '".hash_file("crc32b", $image_link)."', '".sha1_file($image_link)."', '".$iinfo[1]."', '$checked_username', '".$isinfo[1]."', '".$isinfo[0]."', '".filesize($image_link)."', '$ext', '$rating', '$tags', '".$iinfo[0]."', '".date("Ymd")."', '$ip')";
					if(!is_dir("./".$thumbnail_folder."/".$iinfo[0]."/"))
						$image->makethumbnailfolder($iinfo[0]);
					if(!$image->thumbnail($iinfo[0]."/".$iinfo[1]))
						print "Thumbnail generation failed. ";
					if(!$db->query($query))
					{
						print "Failed to upload image. ";
						unlink($image_link); unset($image_link);
						$image->folder_index_decrement($iinfo[0]);
						$ttags = explode(" ",$tags);
						foreach($ttags as $current)
							$tclass->deleteindextag($current);
					}
					else
					{
				$query = "SELECT id, tags FROM $post_table WHERE hash='".md5_file('./'.$image_folder.'/'.$iinfo[0]."/".$iinfo[1])."' AND image='".$iinfo[1]."' AND directory='".$iinfo[0]."'  LIMIT 1";
				$result = $db->query($query);
				$row = $result->fetch_assoc();
				$tags = $db->real_escape_string($row['tags']);
				$date = date("Y-m-d H:i:s");
				$query = "INSERT INTO $tag_history_table(id,tags,user_id,updated_at,ip) VALUES('".$row['id']."','$tags','$checked_user_id','$date','$ip')";
				$db->query($query) or die($db->error);				
				if($parent != '' && is_numeric($parent))
				{
					$parent_check = "SELECT COUNT(*) FROM $post_table WHERE id='$parent'";
					$pres = $db->query($parent_check);
					$prow = $pres->fetch_assoc();
					if($prow['COUNT(*)'] > 0)
					{
						$temp = "INSERT INTO $parent_child_table(parent,child) VALUES('$parent','".$row['id']."')";
						$db->query($temp);
						$temp = "UPDATE $post_table SET parent='$parent' WHERE id='".$row['id']."'";
						$db->query($temp);
					}
				}				
				$query = "SELECT id FROM $post_table WHERE id < ".$row['id']." ORDER BY id DESC LIMIT 1";
				$result = $db->query($query);
				$row = $result->fetch_assoc();
				$query = "UPDATE $post_count_table SET last_update='20110101' WHERE access_key='posts'";
				$db->query($query);
				$query = "UPDATE $user_table SET post_count = post_count+1 WHERE id='$checked_user_id'";
				$db->query($query);
						print "Image added. ";
					}
				}
				print "Valid Extension<br>".mb_strtolower(str_replace('%','',htmlentities(str_replace("\r\n"," ",$tags2[$i]), ENT_QUOTES, 'UTF-8')), 'UTF-8')." | ";
				print $file."<br><br>";
			}
		}
		$i++;
	}
?>