<?php

header("Content-Type: text/plain");

$changelog = 'CHANGE LOG

Version 0.9.0
 - This is the first version of Racozsoft.PHP.DanSync.Tag!

Version 0.9.1
 - Some minor bugs fixed.
 - Added support to json_decode().

Version 0.9.3
 - Handle errors with json_last_error() and mysql_error().
 - Added query "CREATE TABLE IF NOT EXISTS ...".
 - Lots of bugs found in this version.

Version 0.9.5
 - Enhance task output and logging system.
 - Prevent 404 error with file_exists() function.
 - Added query "DROP TABLE IF EXISTS ...".
 - Added some variables for custom settings.
 - Some major and minor bugs fixed.

Version 0.9.8
 - Output and Markup validation errors fixed.
 - Added query "ALTER TABLE ... RENAME ...".
 - Database errors are now logged and handled.
 - Errors in htmlentities() and Encoding errors found.

Version 0.9.9
 - Ready for Release Candidate!
 - This is more stable and comfortable to execute!
 - Now uses MySQLi for better security and compatibility!
 - Added formatBytes() from Martin Sweeny.
 - Can be integrated with AlphaBoard!
 - Added mb_detect_encoding().
 - Some major bugs fixed.
 - Uses more resource!

Version 1.0.0
 - Back to text/plain output.
 - Silent mode added and set as default.
 - Less task description during opreration.
 - No more JSON error reporting and caching.
 - Customizable max_execution_time support.
 - Some new customizable vars added.
 - Lower resource usage.';

/*

Copyright (C) 2011 Raki Saionji - Racozsoft Corporation
Made by NSDAP - Under development of AlphaBoard and Rakibooru

*/

echo $changelog;

?>