<?php
// Execution Time Limit.
$time_limit = 1200;
set_time_limit($time_limit);
$start_time = time();

// Content Distribution Type.
header("Content-Type: text/plain");

// Database Information.
$mysql_host = "localhost";
$mysql_user = "root";
$mysql_pass = "raki";
$mysql_db = "blank";

// Tag Index Table Name.
$tag_index_table = "tag_index";
// Tag Index Backup Table Name
$tag_index_backup = "tag_index_backup";

// Tag Index Table Columns.
$col_tag = 'tag';
$col_typ = 'type';
$col_amb = 'ambiguous';
$col_idc = 'index_count';
$col_ver = 'version';

// Tag Data Dump File (.json or .txt)
$tags_api_url = "tag_index.json";

function getTypeName($code, $case = 0)
{
	switch ($code)
	{
	case 0:
		$proper = 'general';
		break;
	case 1:
		$proper = 'artist';
		break;
	case 3:
		$proper = 'copyright';
		break;
	case 4:
		$proper = 'character';
		break;
	case 5:
		$proper = 'circle';
		break;
	case 6:
		$proper = 'faults';
		break;
	default:
		$proper = 'custom';
		break;
	}
	if ($case > 0)
		$proper = ucfirst($proper);
	return $proper;
}

function formatBytes($b,$p = null) {
    $units = array("byte(s)","KB","MB","GB","TB","PB","EB","ZB","YB");
    $c=0;
    if(!$p && $p !== 0) {
        foreach($units as $k => $u) {
            if(($b / pow(1024,$k)) >= 1) {
                $r["bytes"] = $b / pow(1024,$k);
                $r["units"] = $u;
                $c++;
            }
        }
        return number_format($r["bytes"],2) . " " . $r["units"];
    } else {
        return number_format($b / pow(1024,$p)) . " " . $units[$p];
    }
}

function returnBytes($val) {
    $val = trim($val);
    $last = strtolower($val[strlen($val)-1]);
    switch($last) {
        case 't':
            $val *= 1024;
        case 'g':
            $val *= 1024;
        case 'm':
            $val *= 1024;
        case 'k':
            $val *= 1024;
    }
    return $val;
}

	echo 'Last login: '.date("D M j G:i:s").' on '.$_SERVER['SERVER_NAME'].'

--------------------------------------------------
     Danbooru\'s Tag Syncer 1.0.0
              Copyright (C) 2011 Raki Saionji
--------------------------------------------------

> SQL: CONNECT...';

	$db = new mysqli($mysql_host,$mysql_user,$mysql_pass,$mysql_db);

	if (!mysqli_connect_errno())
	{
	echo ' <OK>
> SQL: SET CHARSET...';
	echo ($db->set_charset('utf8')) ? ' <OK>' : ' <ERROR>';
	echo '
> SQL: DROP TABLE...';
	$query = "DROP TABLE IF EXISTS `$tag_index_backup`";
	echo ($db->query($query)) ? ' <OK>' : ' <ERROR>';
	$query = "ALTER TABLE `$tag_index_table` RENAME `$tag_index_backup`";
	echo '
> SQL: ALTER TABLE...';
	if ($db->query($query))
	{ $have_backup = true; }
	else
	{ $have_backup = false; }
	echo ' <OK>';
	echo '
> SQL: CREATE TABLE...';
	$query = "
		CREATE TABLE IF NOT EXISTS `$tag_index_table`
		(
		`$col_tag` VARCHAR(255) NOT NULL,
		`$col_typ` VARCHAR(255) NOT NULL,
		`$col_amb` TINYINT(1) DEFAULT 0, 
		`$col_idc` INT(11) UNSIGNED DEFAULT 0,
		`$col_ver` INT(11) UNSIGNED DEFAULT 0,
		KEY `tag` (`tag`),
		KEY `index_count` (`index_count`)
		)
		ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC
	";
	echo ($db->query($query)) ? ' <OK>' : ' <ERROR>';
	echo '
	
> IO: JSON INPUT...';
	if(file_exists($tags_api_url))
	{
	echo " <OK>\r\n";
	//$jsoned = file_get_contents($tags_api_url);
	//$arrayed = json_decode($jsoned,true);
	/*switch (json_last_error()) {
	case JSON_ERROR_NONE:
		echo '
		<span style="color:#0f0;">No errors';
		break;
	case JSON_ERROR_DEPTH:
		echo '
		<span style="color:#f00;">Maximum stack depth exceeded';
		break;
	case JSON_ERROR_STATE_MISMATCH:
		echo '
		<span style="color:#f00;">Underflow or the modes mismatch';
		break;
	case JSON_ERROR_CTRL_CHAR:
		echo '
		<span style="color:#f00;">Unexpected control character found';
		break;
	case JSON_ERROR_SYNTAX:
		echo '
		<span style="color:#f00;">Syntax error, malformed JSON';
		break;
	case JSON_ERROR_UTF8:
		echo '
		<span style="color:#f00;">Malformed UTF-8 characters, possibly incorrectly encoded';
		break;
	default:
		echo '
		<span style="color:#f00;">Unknown error';
        break;
	}
	echo '!</span></p>';*/

	$current_count = 0; $success_count = 0; $error_count = 0; $ignored_count = 0;

	if (isset($_GET['v']))
	echo '
> PHP: [VERBOSE MODE]';
	else
	echo '
> PHP: [SILENT MODE]';
	echo "\r\n";
	echo '
> SQL: UPDATE TABLE DATA... <..>';
	foreach (json_decode(file_get_contents($tags_api_url), true) as $row)
	{
	$current_count++;
	if
	(
		isset($row['ambiguous']) && is_bool($row['ambiguous']) &&
		isset($row['type']) && isset($row['name']) && $row['name'] != '' && $row['name'] != ' '
	)
	{
	$tag = $db->real_escape_string(htmlentities($row['name'], ENT_QUOTES, "UTF-8"));
	$type = getTypeName($row['type']); $ambiguous = (integer)$row['ambiguous'];
	$query = "INSERT INTO `$tag_index_table` (`$col_tag`, `$col_typ`, `$col_amb`) VALUES ('$tag', '$type', '$ambiguous')";
	$task = $db->query($query); 
	if ($task)
	{
	$success_count++;
	}
	else
	{
	$error_count++;
	}
	if (isset($_GET['v']))
	{
	echo '
  RECORD #'.number_format($current_count).'...'; echo ($task) ? ' <OK>' : ' <ERROR>';
	}
	}
	else
	{
	if (isset($_GET['v'])) echo ' <SKIP>';
	$ignored_count++;
	}
	}
	echo '
	
> SQL: RESTORE TABLE DATA...';
	if ($have_backup)
	{
	echo ' <..>';
	$restore_count = 0;
	$query = "SELECT SQL_NO_CACHE * FROM `$tag_index_backup` WHERE `$col_idc` > 0";
	$result = $db->query($query) or die(' <ERROR>');
	while($record = $result->fetch_assoc())
	{
	$restore_count++;
	$tag = $db->real_escape_string($record['tag']);
	$type = $db->real_escape_string($record['type']);
	$index_count = (integer)$record['index_count'];
	$ambiguous = (integer)$record['ambiguous'];
	$curie = "
		SELECT SQL_NO_CACHE COUNT(*) FROM `$tag_index_table` WHERE `tag` = '$tag'
	";
	$pierre = $db->query($curie);
	$marie = $pierre->fetch_assoc();
	if($marie['COUNT(*)'] > 0)
	$query = "
		UPDATE `$tag_index_table` SET `$col_idc` = '$index_count'
		WHERE `$col_tag` = '$tag'
	";
	else
	$query = "
		INSERT INTO `$tag_index_table` (`$col_tag`, `$col_type`, `$col_amb`, `$col_idc`)
		VALUES ('$tag', '$type', '$ambiguous', '$index_count')
	";
	if (isset($_GET['v']))
	{
	echo '
  RECORD #'.number_format($restore_count).'...';
	echo ($db->query($query)) ? ' <OK>' : ' <ERROR>';
	}
	else
	{
	$db->query($query);
	}
	$pierre->free_result();
	}
	$result->free_result();
	}
	else
	{
        echo ' <SKIP>';
	}

	$end_time = time();
	echo '

> SQL: TOTAL '.number_format($current_count).' RECORD(S)
> SQL: '.number_format($success_count).' SUCCEEDED
> SQL: '.number_format($ignored_count).' IGNORED
> SQL: '.number_format($error_count).' ERROR(S)

> IO: USED  TIME : '.($end_time - $start_time).' SECOND(S)
> IO: TOTAL TIME : '.$time_limit.' SECOND(S)
> IO: USED  MEM  : '.formatBytes(memory_get_peak_usage(true)).'
> IO: TOTAL MEM  : '.formatBytes(returnBytes(ini_get('memory_limit')));
	}
	else
	{
		echo ' <ERROR>';
	}
	}
	else
	{
        echo ' <ERROR>';
	}
	$db->close();

echo '

[Process completed]';
?>