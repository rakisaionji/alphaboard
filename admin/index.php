<?php
	require "header.php";
	if(!defined('_IN_ADMIN_HEADER_'))
		die;
	if($_GET['page'] == "alias")
			require "alias.php";
	else if($_GET['page'] == "pending_posts")
			require "pending_posts.php";
	else if($_GET['page'] == "reported_posts")
			require "reported_posts.php";
	else if($_GET['page'] == "reported_comments")
			require "reported_comments.php";
	else if($_GET['page'] == "server_information")
			require "server_information.php";	
	else if($_GET['page'] == "php_configuration")
			require "php_configuration.php";
	else if($_GET['page'] == "add_group")
			require "add_group.php";
	else if($_GET['page'] == "edit_group")
			require "edit_group_permission.php";
	else if($_GET['page'] == "user_list")
			require "user_list.php";
	else if($_GET['page'] == "edit_user")
			require "edit_user.php";
	else if($_GET['page'] == "ban_user")
			require "ban_user.php";				
?>

	</body>
</html>