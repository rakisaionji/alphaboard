<?php
	if(!defined('_IN_ADMIN_HEADER_'))
	{
		require "401_error.php";
		exit;
	}
	$user = new user();
	if(!$user->gotpermission('approve_posts'))
	{
		require "403_error.php";
		exit;
	}

	$misc = new misc();
	$post = new post();

	print '
	<div class="content">
	<h2>Reported Posts</h2><br>';

	if(isset($_GET['unreport']) && is_numeric($_GET['unreport']))
	{
		$post_id = $db->real_escape_string($_GET['unreport']);
		$query = "UPDATE $post_table SET spam=FALSE, reporter=NULL, reported_date=NULL, rip=NULL, reason=NULL WHERE id='$post_id'";
		if($db->query($query))
			print "
		<div class=\"success-notice\">Unflagged Post #$post_id.</div><br>";
		print '<meta http-equiv="refresh" content="5;url='.$site_url.'admin/?page=reported_posts">';
		exit;
	}

	//number of reports/page
	$limit = 10;
	//number of pages to display. number - 1. ex: for 5 value should be 4
	$page_limit = 4;

	$query = "SELECT COUNT(*) FROM $post_table WHERE spam=TRUE";
	$result = $db->query($query);
	$row = $result->fetch_assoc();
	$numrows = $row['COUNT(*)'];
	if($numrows == 0)
		print "
		<div class=\"status-notice\">No reports found.</div><br>";
	else
	{
		print '
		<table class="highlightable" style="width: 100%; font-size: 12px;">
		<tr>
			<th style="width: 150px;">Post</th>
			<th style="width: 200px;">Summary</th>
			<th>Reason</th>
			<th style="width: 100px;">Tasks</th>
		</tr>';
		if(isset($_GET['pid']) && $_GET['pid'] != "" && is_numeric($_GET['pid']) && $_GET['pid'] >= 0)
			$page = $db->real_escape_string($_GET['pid']);
		else
			$page = 0;
		$query = "SELECT * FROM $post_table WHERE spam=TRUE ORDER BY id DESC LIMIT $page, $limit";
		$result = $db->query($query);
		while($row = $result->fetch_assoc())
		{
		echo '
		<tr>
			<td style="text-align:center">
				<a href="../index.php?page=post&amp;s=view&amp;id='.$row['id'].'">
				<img
				width=128px
				style="margin:12px;"
				src="'.$site_url.'/'.$thumbnail_folder.'/'.$row['directory'].'/'.$thumbnail_prefix.$row['image'].'"
				alt="'.$row['id'].'" title="'.$row['tags'].' Rating:'. $row['rating'].' Score:'.$row['score'].' User:'.$row['owner'].'"
				></a>
			</td>
			<td>
			<br>
				<p>ID: '.$row['id'].'</p>
				<p>Extension: '.strtoupper(substr($row['ext'],1,3)).'
				<br>CRC32: '.strtoupper($row['crc32']).'</p>
				<p>Posted: <a href="'.$site_url.'index.php?page=post&amp;s=list&amp;tags=date:'.date('Y-m-d',$row['creation_date']).'" title="'.date('l, F jS, Y G:i:s T',$row['creation_date']).'">'.$misc->date_words($row['creation_date']).'</a>
				<br>by <a href="'.$site_url.'index.php?page=account&amp;s=profile&amp;uname='.$row['owner'].'">'.$row['owner'].'</a> via <a href="'.$site_url.'index.php?page=post&amp;s=list&amp;tags=ip:'.$row['ip'].'">'.$row['ip'].'</a></p>';
		if (isset($row['approver']) && $row['approver'] != '' && isset($row['approved_date']) && $row['approved_date'] != '' && isset($row['aip']) && $row['aip'] != '' && isset($row['approved']) && $row['approved'] == TRUE) {
echo '
				<p>Approved: <a href="#" title="'.date('l, F jS, Y G:i:s T',$row['approved_date']).'">'.$misc->date_words($row['approved_date']).'</a>
				<br>by <a href="'.$site_url.'index.php?page=account&amp;s=profile&amp;uname='.$row['approver'].'">'.$row['approver'].'</a> via <a href="#">'.$row['aip'].'</a></p>'; }
echo '
				<p>Size: '.$row['width'].'x'.$row['height'].' ('.$post->formatBytes($row['size']).')</p>
			</td>
			<td>
			<br>
				<p>Reported: <a href="#" title="'.date('l, F jS, Y G:i:s T',$row['reported_date']).'">'.$misc->date_words($row['reported_date']).'</a>
				<br>by <a href="'.$site_url.'index.php?page=account&amp;s=profile&amp;uname='.$row['reporter'].'">'.$row['reporter'].'</a> via <a href="#">'.$row['rip'].'</a></p> 
				<p>Reason: '.stripslashes($row['reason']).'</p>
			</td>
			<td>
			<br>
				<p><a href="../index.php?page=post&amp;s=view&amp;id='.$row['id'].'">View</a></p>
				<p><a href="'.$site_url.'/admin/index.php?page=reported_posts&amp;unreport='.$row['id'].'">Unflag</a></p>';
		if($user->gotpermission('delete_posts'))
		echo '
				<p><a href="#" onclick="if(confirm(\'Are you sure you want to delete this post?\')){document.location=\'../public/remove.php?id='.$row['id'].'&amp;removepost=1&amp;token='.base64_encode($site_url.'admin/?page=reported_posts').'\';}; return false;">Delete</a></p>';
		echo '
			</td>
		</tr>';
		}
		$result->free_result();
		
		echo "
		</table>";
		print "
		<div id='paginator'>";
		print '
			'.$misc->pagination($_GET['page'],$_GET['s'],$id,$limit,$page_limit,$numrows,$_GET['pid']);
		print "
		</div>
		<br><br>";
	}
?>

	</div>