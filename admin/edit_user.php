<?php
	if(!defined('_IN_ADMIN_HEADER_'))
	{
		require "401_error.php";
		exit;
	}
?>

	<div class="content">
	<h2>Edit User Information</h2><br>
<?php
	$userc = new user();
	if(!$userc->gotpermission('is_admin'))
	{
		require "403_error.php";
		exit;
	}
	function admin_verify_pass($user, $pass)
	{
		global $db, $userc, $site_url, $user_table;
		$user = $db->real_escape_string($user);
		$pass = $db->real_escape_string($pass);
		$pass = $userc->hashpass($pass);
		$query = "SELECT * FROM $user_table WHERE user='$user' AND pass='$pass'";
		$result = $db->query($query);
		if($result->num_rows == 1)
			return true;
		else
			return false;
	}
	if(isset($_POST['settings']) && $_POST['settings'] == 1 && isset($_POST['group']) && is_numeric($_POST['group']) && isset($_POST['username']) && strlen($_POST['username']) >= 5 && ctype_alnum($_POST['username']))
	{
		$user = $db->real_escape_string($_POST['username']);
		$group = $db->real_escape_string($_POST['group']);
		$email = $db->real_escape_string($_POST['email']);
		$pass = $db->real_escape_string($_POST['password']);
		$admin_password = $db->real_escape_string($_POST['admin_password']);
		
		$domain = explode("@",$email);
		if($domain[1] == "" || $domain[1] == "asdf.com") $email = "";
				
		if($user == $checked_username)
		{
			print '
		<div class="error-notice">Cannot proceed your request! This account is currently being used! Please try again!</div><br>
		<meta http-equiv="refresh" content="5;url='.$site_url.'admin/index.php?page=user_list">';
		exit;
		}

		if(!admin_verify_pass($checked_username, $admin_password))
		{
			print '
		<div class="error-notice">Cannot proceed your request! Admin Password Verification failed! Please try again!</div><br>
		<meta http-equiv="refresh" content="5;url='.$site_url.'admin/index.php?page=edit_user&user='.$user.'">';
		exit;
		}

		$g_pass = ''; $g_email = '';

		if($pass != "" && strlen($_POST['pass']) >= 5)
			$g_pass = ", pass='".$userc->hashpass($pass)."'";
		else
			$g_pass = "";

		if($email != "" && strlen($_POST['email']) >= 5)
			$g_email = ", email='$email'";
		else
			$g_email = "";

		if($userc->user_exists($user))
			$query = "UPDATE $user_table SET ugroup='$group' $g_pass $g_email WHERE user='$user'";
		else
			print "
		<div class=\"error-notice\">User '$user' is not exist in the database.</div><br>";

		if($db->query($query))
			print "
		<div class=\"success-notice\">Record for user '$user' was modified successfully.</div><br>";
		else
			print "
		<div class=\"error-notice\">Error modifying record for user '$user'.</div><br>";
		
		print '<meta http-equiv="refresh" content="5;url='.$site_url.'admin/index.php?page=user_list">';	
		exit;
	}
	else if(isset($_GET['user']) && $_GET['user'] != "")
	{
		$user = $db->real_escape_string($_GET['user']);
		if(!$userc->user_exists($user))
		{
		print '<meta http-equiv="refresh" content="0;url='.$site_url.'admin/index.php?page=user_list">';
		exit;
		}	
		echo '	<style type="text/css">
	.tableinput{width:240px;height:20px;vertical-align:middle;font-size:13px;padding:4px;}
	.tablerowheader{width:192px;height:32px;text-align:center;vertical-align:middle;cursor: pointer;}
	.tablerowdata{width:256px;height:32px;text-align:center;vertical-align:middle;}
	</style>
	<script src="'.$site_url.'script/core.js" type="text/javascript"></script>
	<script type="text/javascript">function showHide(id){$(id).value=\'\';Effect[Element.visible(id)?\'Fade\':\'Appear\'](id);}</script>
	<form method="post" action="" name="edit_user_form">
	<table style="font-size:13px">
		<tr>
			<th class="tablerowheader" onclick="$(\'name\').focus()">Username</th>
			<td class="tablerowdata"><input readonly type="text" id="name" name="username" class="tableinput" value="'.$user.'"></td>
		</tr>
		<tr>
			<th class="tablerowheader" onclick="$(\'group\').focus()">Group</th>
			<td class="tablerowdata">
			<select id="group" name="group" style="width:250px;height:30px;vertical-align:middle;font-size:13px;padding:4px;">';
		$cgroup = false;
		$query = "SELECT t1.ugroup, t2.id, t2.group_name FROM $user_table AS t1 JOIN $group_table AS t2 WHERE t1.user='$user'";
		$result = $db->query($query);
		while($row = $result->fetch_assoc())
		{
			if($cgroup == false)
			{
				echo '
				<option value="'.$row['ugroup'].'">No change</option>';
				$cgroup = true;
			}
			echo '
				<option value="'.$row['id'].'">'.$row['group_name'].'</option>';
		}
		echo '
			</select>
			</td>
		</tr>
		<tr>
			<th class="tablerowheader" onclick="showHide(\'email\')">New email</th>
			<td class="tablerowdata"><input type="text" id="email" name="email" class="tableinput" style="display:none" value=""></td>
		</tr>
		<tr>
			<th class="tablerowheader" onclick="showHide(\'pass\')">New password</th>
			<td class="tablerowdata"><input type="text" id="pass" name="password" class="tableinput" style="display:none" value=""></td>
		</tr>
	</table>
	<table style="font-size:13px">
		<tr>
			<th class="tablerowheader" onclick="$(\'admin_pass\').focus()">Admin password</th>
			<td class="tablerowdata"><input type="password" id="admin_pass" name="admin_password" class="tableinput" value=""></td>
		</tr>
		<tr>
			<td class="tablerowheader">
			<input type="hidden" name="settings" id="settings" value="0">
			</td>
			<td class="tablerowdata">
				<input type="submit" name="submit" style="width:250px;height:32px;vertical-align:middle;font-size:13px;padding:4px;" onclick="verify_information();return false;" value="Save">
			</td>
		</tr>
	</table>
	<script type="text/javascript">
		function is_valid_email(email)
		{
			var atpos=email.indexOf("@");
			var dotpos=email.lastIndexOf(".");
			if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)
				return false;
			else	return true;
		}

		function verify_information()
		{
			var pw = $("pass").value;
			var email = $("email").value;
			var apw = $("admin_pass").value;
			
			if ( $("pass").visible() && pw == "" )
			{
				alert("Please enter your password!");
				$("pass").focus();
				return false;
			}
			else if ( $("pass").visible() && pw.length < 5 )
			{
				alert("Password must be at least 5 characters long!");
				$("pass").focus();
				return false;
			}
			else if ( $("email").visible() && email == "" )
			{
				alert("Please enter your e-mail address!");
				$("email").focus();
				return false;
			}
			else if ( email != "" && !is_valid_email(email) )
			{
				alert("Please enter a valid e-mail address!");
				$("email").focus();
				return false;
			}
			else if ( apw == "" )
			{
				alert("Please enter your admin password!");
				$("admin_pass").focus();
				return false;
			}
			else if ( apw.length < 5 )
			{
				alert("Admin password must be at least 5 characters long!");
				$("admin_pass").focus();
				return false;
			}
			else 
			{
				$("settings").value = 1;
				document.edit_user_form.submit();
			}
		}
	</script>
	</form>';
	}
	else
	{
		echo '	<form method="get" action="">
	<input name="page" value="'.$_GET['page'].'" type="hidden">
	<table>
		<tr style="font-size:13px;">
			<th width="96px" height="32px" style="text-align:center; vertical-align:middle;">User name</th>
			<td width="256px" height="32px" style="text-align:center; vertical-align:middle;">
			<input type="text" name="user" style="width: 250px; height:20px; vertical-align:middle; font-size:13px; padding: 4px;" value="">
			</td>
			<td width="128px" height="32px" style="text-align:center; vertical-align:middle;">
			<input type="submit" style="width: 128px; height:30px; vertical-align:middle; font-size:13px; padding: 4px;" value="Edit">
			</td>
		</tr>
	</table>
	</form>';
	}
?>

	</div>