<?php
	$remote_ip = $db->real_escape_string($_SERVER['REMOTE_ADDR']);	
	$remote_host = htmlentities(gethostbyaddr($_SERVER['REMOTE_ADDR']), ENT_QUOTES, 'UTF-8');	
	$query = "SELECT group_name FROM $group_table WHERE id='$checked_user_group'";
	$result = $db->query($query) or die($db->error);
	$row = $result->fetch_assoc();
	$user_group = $row['group_name'];
	$result->free_result();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<title><?php print $site_url3 ?> - Dashboard<?php
		print (isset($_GET['page'])) ? ' - '.htmlentities(ucwords(str_replace('_', ' ', $_GET['page'])), ENT_QUOTES, 'UTF-8') : '' ?></title>

		<link rel="home" href="<?php print $site_url ?>admin/index.php">
		<link rel="shortcut icon" href="<?php print $site_url ?>favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php print $site_url ?>default.css" title="default">

		<meta http-equiv="content-type" content="text/html; charset=utf-8">
		<meta name="author" content="Racozsoft Corporation">
	</head>
	<body>
	<div id="header">
	<table cellspacing="0" cellpadding="0" style="margin: 0px; padding-top: 12px; width: 100%;">
		<tr>
		<td style="vertical-align: middle; text-align:left;">
			<h2 id="site-title"><a href="<?php print $site_url ?>index.php"><?php print ucfirst($site_url3) ?></a>
			/ <a href="<?php print $site_url ?>admin">Dashboard</a><?php
			print (isset($_GET['page'])) ? '
			/ <a href="'.$site_url.'admin/index.php?page='.urlencode(stripslashes($_GET['page'])).'">'.htmlentities(ucwords(str_replace('_', ' ', $_GET['page'])), ENT_QUOTES, 'UTF-8').'</a>' : ''
			?></h2>
		</td>
		<td style="vertical-align: middle; width: 200px; padding-right: 16px; text-align:right;">
			<h4><a href="<?php print $site_url ?>index.php?page=post&amp;s=list">View Board</a></h4>
		</td></tr>
	</table>
	</div>
	<div id="content">
		<div class="sidebar">
		<br><br>
			<h5>Information:</h5>
			<br>
			<ul>
				<li>Username: <a href="index.php?page=account&amp;s=profile&amp;id=<?php
				print $checked_user_id ?>"><?php print $checked_username ?></a></li>
				<li>Group: <?php print $user_group ?></li>
				<li>Host: <?php
				print ($remote_ip != "127.0.0.1" && $_SERVER['REMOTE_HOST'] != "") ? $remote_host : 'localhost'
				?></li>
				<li>IP: <?php print $remote_ip ?></li>
				<li><a href="<?php print $site_url ?>index.php?page=account&amp;s=profile&amp;id=<?php print $checked_user_id ?>">Profile</a> | <a href="<?php print $site_url ?>index.php?page=account&amp;s=login&amp;code=01">Logout</a></li>
			</ul>
			<br>
			<h5>Moderator Tools:</h5>
			<br>
			<ul>
				<li><a href="index.php?page=pending_posts"<?php
				print ($_GET['page'] == "pending_posts") ? ' style="font-weight:bold"' : ''
				?>>Pending Posts</a></li>
				<li><a href="index.php?page=reported_posts"<?php
				print ($_GET['page'] == "reported_posts") ? ' style="font-weight:bold"' : ''
				?>>Reported Posts</a></li>
				<li><a href="index.php?page=reported_comments"<?php
				print ($_GET['page'] == "reported_comments") ? ' style="font-weight:bold"' : ''
				?>>Reported Comments</a></li>
				<li><a href="index.php?page=alias"<?php
				print ($_GET['page'] == "alias") ? ' style="font-weight:bold"' : ''
				?>>Alias Approve/Reject</a></li>
			</ul>
			<br>
<?php
	if($user->gotpermission('is_admin'))
	{
print '			<h5>Admin Tools:</h5>
			<br>
				<h6>Server Tools:</h6>
				<br>
				<ul>
				<li><a href="index.php?page=server_information"';
				print ($_GET['page'] == "server_information") ? ' style="font-weight:bold"' : '';
				print '>Server Information</a></li>
				<li><a href="index.php?page=php_info" target="_blank">PHP Information</a></li>
				</ul>
				<h6>Group Tools:</h6>
				<br>
				<ul>
				<li><a href="index.php?page=add_group"';
				print ($_GET['page'] == "add_group") ? ' style="font-weight:bold"' : '';
				print '>Create New Group</a></li>
				<li><a href="index.php?page=edit_group"';
				print ($_GET['page'] == "edit_group") ? ' style="font-weight:bold"' : '';
				print '>Edit Group Permissions</a></li>
				</ul>
				<h6>User Tools:</h6>
				<br>
				<ul>
				<li><a href="index.php?page=edit_user"';
				print ($_GET['page'] == "edit_user") ? ' style="font-weight:bold"' : '';
				print '>Edit User Information</a></li>
				<li><a href="index.php?page=user_list"';
				print ($_GET['page'] == "user_list") ? ' style="font-weight:bold"' : '';
				print '>User Listing/Searching</a></li>
				</ul>';
	}
?>

		</div>
	</div>