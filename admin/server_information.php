<?php
	if(!defined('_IN_ADMIN_HEADER_'))
	{
		require "401_error.php";
		exit;
	}
	$user = new user();
	if(!$user->gotpermission('is_admin'))
	{
		require "403_error.php";
		exit;
	}

	print '
	<div class="content">
	<h2>Server Information</h2><br>';
	$type = explode(';',$_SERVER['HTTP_ACCEPT']);
	$lang = explode(';',$_SERVER['HTTP_ACCEPT_LANGUAGE']);
	$char = explode(';',$_SERVER['HTTP_ACCEPT_CHARSET']);
        echo '
		<h4>Server Information</h4><br>
		<table style="width: 100%; font-size: 12px;">
			<tr><th style="width:128px">Name</th><td>'.$_SERVER['SERVER_NAME'].'</td></tr>
			<tr><th>Software</th><td>'.$_SERVER['SERVER_SOFTWARE'].'</td></tr>
			<tr><th>Address</th><td>'.$_SERVER['SERVER_ADDR'].'</td></tr>
			<tr><th>Port</th><td>'.$_SERVER['SERVER_PORT'].'</td></tr>
			<tr><th>Protocol</th><td>'.$_SERVER['SERVER_PROTOCOL'].'</td></tr>
			<tr><th>Gateway</th><td>'.$_SERVER['GATEWAY_INTERFACE'].'</td></tr>
		</table>
		<h4>HTTP Information</h4><br>
		<table style="width: 100%; font-size: 12px;">
			<tr><th style="width:128px">Hostname</th><td>'.$_SERVER['HTTP_HOST'].'</td></tr>
			<tr><th>Type</th><td>'.strtolower(str_replace(',',', ',$type[0])).'</td></tr>
			<tr><th>Language</th><td>'.strtolower(str_replace(',',', ',$lang[0])).'</td></tr>
			<tr><th>Encoding</th><td>'.$_SERVER['HTTP_ACCEPT_ENCODING'].'</td></tr>
			<tr><th>Charset</th><td>'.strtolower(str_replace(',',', ',$char[0])).'</td></tr>
			<tr><th>Connection</th><td>'.strtolower(str_replace('-',' ',$_SERVER['HTTP_CONNECTION'])).' '.$_SERVER['HTTP_KEEP_ALIVE'].'</td></tr>
		</table>
		<h4>MySQL Information</h4><br>
		<table style="width: 100%; font-size: 12px;">
			<tr><th style="width:128px">Server</th><td>'.mysql_get_server_info().'</td></tr>
			<tr><th>Host</th><td>'.mysql_get_host_info().'</td></tr>
			<tr><th>Client</th><td>'.mysql_get_client_info().'</td></tr>
			<tr><th>Protocol</th><td>'.mysql_get_proto_info().'</td></tr>
		</table>
		<h4>Admin Information</h4><br>
		<table style="width: 100%; font-size: 12px;">
			<tr><th style="width:128px">Address</th><td>'.$_SERVER['REMOTE_ADDR'].'</td></tr>
			<tr><th>Port</th><td>'.$_SERVER['REMOTE_PORT'].'</td></tr>
		</table>';
?>

	</div>