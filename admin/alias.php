<?php
	if(!defined('_IN_ADMIN_HEADER_'))
	{
		require "401_error.php";
		exit;
	}

	print '
	<div class="content">
	<h2>Alias Approve/Reject</h2><br>';

	if(isset($_GET['tag']) && $_GET['tag'] != "" && isset($_GET['alias']) && $_GET['alias'] != "")
	{
		if(isset($_GET['action']) && $_GET['action'] != "")
		{
			$tag = $db->real_escape_string(htmlentities(stripslashes(urldecode($_GET['tag'])), ENT_QUOTES, 'UTF-8'));
			$alias = $db->real_escape_string(htmlentities(stripslashes(urldecode($_GET['alias'])), ENT_QUOTES, 'UTF-8'));
			echo '
		<div class="success-notice">Processing your request:<br>Alias '.$alias.' to '.$tag.'.<br>';
			if($_GET['action'] == "accept")
			{
				print 'Action: Accept Alias Submission.</div></br>';
				$tagc = new tag();
				//tag boot, alias boots singular is better.
				$query = "
					UPDATE $alias_table
					SET status='1', approver='$checked_username', approved_date='".mktime()."'
					WHERE tag='$tag' AND alias='$alias'";
				$db->query($query);
				//Convert all current posts from the AKA to the tag.
				$query = "SELECT * FROM $post_table WHERE tags LIKE '% $alias %'";
				$result = $db->query($query) or die($db->error);
				while($row = $result->fetch_assoc())
				{
					$tags = explode(" ",$row['tags']);
					foreach($tags as $current)
						$tagc->deleteindextag($current);
					$tmp = str_replace(' '.$alias.' ',' '.$tag.' ',$row['tags']);
					$tags = implode(" ",$tagc->filter_tags($tmp,$tag,explode(" ",$tmp)));
					$tags = mb_trim(str_replace("  ","",$tags));
					$tags2 = explode(" ",$tags);
					foreach($tags2 as $current)
						$tagc->addindextag($current);						
					$tags = " $tags ";
					$query = "UPDATE $post_table SET tags='$tags' WHERE id='".$row['id']."'";
					$db->query($query);
				}
			}
			else if($_GET['action'] == "reject")
			{
				print 'Action: Reject Alias Submission.</div></br>';
				$query = "UPDATE $alias_table SET status='2', approver=NULL, approved_date=NULL WHERE tag='$tag' AND alias='$alias'";
				$db->query($query);
			}
			else
				print 'Action: Unknown Action.</div></br>';
		}
		print '
		<div class="status-notice">Now redirecting you to:<br><a href="'.$site_url.'admin/?page=alias">'.ucfirst($site_url3).'/Dashboard/Alias</a></div><br></div>';
		print '<meta http-equiv="refresh" content="5;url='.$site_url.'admin/?page=alias">';
		exit;
	}
	else
	{
	$misc = new misc();

	//number of reports/page
	$limit = 25;
	//number of pages to display. number - 1. ex: for 5 value should be 4
	$page_limit = 4;

	$query = "SELECT COUNT(*) FROM $alias_table WHERE status='0'";
	$result = $db->query($query);
	$row = $result->fetch_assoc();
	$numrows = $row['COUNT(*)'];

	if($numrows == 0)
		print "
		<div class=\"status-notice\">No aliases has been requested.</div><br>";
	else
	{
		print '
		<table width="100%" border="0" class="highlightable" style="width: 100%; font-size: 12px;">
		<tr>
			<th width="128px">Submitted</th>
			<th width="20%">Alias</th>
			<th width="20%">To</th>
			<th>Reason</th>
			<th width="128px">Tasks</th>
		</tr>';

		if(isset($_GET['pid']) && $_GET['pid'] != "" && is_numeric($_GET['pid']) && $_GET['pid'] >= 0)
			$page = $db->real_escape_string($_GET['pid']);
		else
			$page = 0;

		$query = "SELECT * FROM $alias_table WHERE status='0' ORDER BY submitted_date DESC LIMIT $page, $limit";
		$result = $db->query($query);

		$tag_url_encode = ''; $alias_url_encode = '';
		while($row = $result->fetch_assoc())
		{
		$tag_url_encode = urlencode(html_entity_decode($row['tag'], ENT_QUOTES, 'UTF-8'));
		$alias_url_encode = urlencode(html_entity_decode($row['alias'], ENT_QUOTES, 'UTF-8'));
		print '
		<tr>
			<td><span title="'.date('l, F jS, Y G:i:s T',$row['submitted_date']).'">'.$misc->date_words($row['submitted_date']).'</span></td>
			<td><a href="'.$site_url.'index.php?page=post&amp;s=list&amp;tags='.$alias_url_encode.'">'.$row['alias'].'</a></td>
			<td><a href="'.$site_url.'index.php?page=post&amp;s=list&amp;tags='.$tag_url_encode.'">'.$row['tag'].'</a></td>
			<td>'.stripslashes($row['reason']).'</td>
			<td>
			<a href="index.php?page=alias&amp;alias='.$alias_url_encode.'&amp;tag='.$tag_url_encode.'&amp;action=accept">Accept</a> |
			<a href="index.php?page=alias&amp;alias='.$alias_url_encode.'&amp;tag='.$tag_url_encode.'&amp;action=reject">Reject</a>
			</td>
		</tr>';
		}
		$result->free_result();

		echo "
		</table>";

		print "
		<div id='paginator'>";
		print '
			'.$misc->pagination($_GET['page'],$_GET['s'],$id,$limit,$page_limit,$numrows,$_GET['pid']);
		print "
		</div>
		<br><br>";
	}
	}
	$db->close();
?>

	</div>