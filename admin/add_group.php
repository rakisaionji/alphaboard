<?php
	if(!defined('_IN_ADMIN_HEADER_'))
	{
		require "401_error.php";
		exit;
	}
?>

	<div class="content">
	<h2>Create New Group</h2><br>
<?php
	$user = new user();
	if(!$user->gotpermission('is_admin'))
	{
		require "403_error.php";
		exit;
	}

	if(isset($_POST['gname']) && $_POST['gname'] != "")
	if (ctype_alnum($_POST['gname']))
	{
		$name = $db->real_escape_string($_POST['gname']);
		$query = "SELECT COUNT(*) FROM $group_table WHERE group_name='$name'";
		$result = $db->query($query);
		$row = $result->fetch_assoc();
		if($row['COUNT(*)'] > 0)
			print "
		<div class=\"info-notice\">Group '$name' is already exist in the database.</div><br>";
		else
		{
			if(isset($_POST['default']) && $_POST['default'] == true)
			{
				$query = "UPDATE $group_table SET default_group=FALSE";
				$db->query($query);
				$query = "INSERT INTO $group_table(group_name, default_group) VALUES('$name', TRUE)";
			}
			else
			{
				$query = "INSERT INTO $group_table(group_name, default_group) VALUES('$name', FALSE)";
			}
			if($db->query($query))
			print "
		<div class=\"success-notice\">Group '$name' was added successfully.</div><br>";
			else
			print "
		<div class=\"error-notice\">Group '$name' cannot be added to the database.</div><br>";
		}
		print '<meta http-equiv="refresh" content="5;url='.$site_url.'admin/?page=add_group">';
		exit;
	}
	else
	{		print "
		<div class=\"error-notice\">Group name must consists of all letters or digits only.<br>Allowed characters: A-Z a-z 0-9.</div><br>";
		print '<meta http-equiv="refresh" content="5;url='.$site_url.'admin/?page=add_group">';
		exit;
	}
?>
		<form method="post" action="">
		<table style="font-size:13px;">
			<tr>
				<th width="96px" height="32px" style="text-align:center; vertical-align:middle;">Group name</th>
				<td width="256px" height="32px" style="text-align:center; vertical-align:middle;">
				<input type="text" name="gname" style="width: 250px; height:20px; vertical-align:middle; font-size:13px; padding: 4px;">
				</td>
				<td width="128px" height="32px" style="text-align:center; vertical-align:middle;">
				<input type="submit" name="submit" style="width: 128px; height:30px; vertical-align:middle; font-size:13px; padding: 4px;" value="Create group">
				</td>
			</tr>
			<tr>
				<td>
				</td>
				<td height="32px" style="vertical-align:middle; font-size:14px;">
				<input type="checkbox" name="default"> Default group
				</td>
				<td>
				</td>
			</tr>
		</table>
		</form>
	</div>