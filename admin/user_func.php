<?php function prepare_users($search)
{
	global $db, $user_table, $group_table;
	$search = html_entity_decode($db->real_escape_string($search), ENT_QUOTES, "UTF-8");
	$operators_list = '(<|<=|=|>=|>)';
	$ttags = explode(" ",$search);
	$g_order = '';
	$g_id = '';
	$g_user = '';
	$g_email = '';		
	$g_date = '';	
	$g_ip = '';
	$g_group = '';	
	foreach($ttags as $current)
	{
		$metatag = explode(':', $current);		if (count($metatag) > 1)
		{			$meta = $db->real_escape_string($metatag[0]);			$param = $metatag[1];			$subparam = isset($metatag[2]) ? $db->real_escape_string($metatag[2]) : null;
			$wildcard = strpos($param,"*");
			switch ($meta)
			{			case 'id':
				if (preg_match('~'.$operators_list.'([0-9]+)~', $param, $op))
				{					$operator = $op[1];					$g_id = $op[2];				}
				else
				{					$operator = '=';					if (!is_numeric($param))
					{						unset($current);						break;
					}					$g_id = $param;				}				$g_id = " AND t1.id".$operator."'$g_id' ";
				unset($current);				break;			case 'email':
				$g_email = str_replace("?","_",str_replace("*","%",$param));
				if($wildcard == false)
					$g_crc32 = " AND t1.email='$g_email' ";
				else
					$g_crc32 = " AND t1.email LIKE '$g_email' ";
				unset($current);				break;			case 'date': //Copyright (C) 2010 Raki Saionji - Racozsoft Corporation.				$stamp = explode("-", $param); //Support: j n Y. Japanese Format: 2000-01-01.
				if (preg_match('~'.$operators_list.'([0-9]+)~', $stamp[0], $op))
				{					if (count($stamp) <= 3 && strlen($op[2]) == 4 && strlen($stamp[1]) == 2  && strlen($stamp[2]) == 2)
					$date = mktime(0, 0, 0, (int)$stamp[1], (int)$stamp[2], (int)$op[2]);
					else $date = mktime(0, 0, 0);
					$operator = $op[1];					$g_date = " AND t1.signup_date".$operator."'$date' ";
				}
				else
				{					if (count($stamp) <= 3 && strlen($stamp[0]) == 4 && strlen($stamp[1]) == 2  && strlen($stamp[2]) == 2)
					$date = mktime(0, 0, 0, (int)$stamp[1], (int)$stamp[2], (int)$stamp[0]); //From midnight {lol}
					else $date = mktime(0, 0, 0); //Returns current date if invalid format entered.
					$g_date = " AND (t1.signup_date > $date AND t1.signup_date < ($date + 24*60*60))";				}
				unset($date);				unset($current);  				break;			case 'order':
				$search = str_replace($current,"",$search);
				$param = str_replace('_asc','',$param);				switch ($param)
				{				case 'id':					$g_order = 't1.id ASC';					break;				case 'id_desc':					$g_order = 't1.id DESC';					break;				case 'user':					$g_order = 't1.user ASC';					break;				case 'user_desc':					$g_order = 't1.user DESC';
					break;				case 'email':					$g_order = 't1.email ASC';					break;				case 'email_desc':					$g_order = 't1.email DESC';					break;				case 'date':					$g_order = 't1.signup_date ASC';					break;				case 'date_desc':					$g_order = 't1.signup_date DESC';					break;				case 'group':					$g_order = 't2.group_name ASC';					break;				case 'group_desc':					$g_order = 't2.group_name DESC';					break;				case 'ip':					$g_order = 't1.ip ASC';					break;				case 'ip_desc':					$g_order = 't1.ip DESC';					break;
				default:
					$g_order = 't1.id DESC';					break;				}				unset($current);				break;			case 'ip':
				$g_ip = str_replace("?","_",str_replace("*","%",str_replace("\\","",$param)));
				if($wildcard == false)
					$g_ip = " AND t1.ip='$g_ip' ";
				else
					$g_ip = " AND t1.ip LIKE '$g_ip' ";	
				unset($current);				break;			case 'group':				$param = str_replace("?","_",str_replace("*","%",$param));
				if($wildcard == false)
					$g_group = " AND t2.group_name = '$param' ";
				else
					$g_group = " AND t2.group_name LIKE '$param' ";
				unset($current);				break;			case '-group':				$param = str_replace("?","_",str_replace("*","%",$param));
				if($wildcard == false)
					$g_group = " AND t2.group_name != '$param' ";
				else
					$g_group = " AND t2.group_name NOT LIKE '$param' ";
				unset($current);				break;			default:				break;			}		}
		else
		{			$current = strtolower($metatag[0]);		}
		$current = $db->real_escape_string(htmlentities($current, ENT_QUOTES, "UTF-8"));
		if($current != "" && $current != " ")
		{
			$len = strlen($current);
			$count = substr_count($current, '*', 0, $len);
			$current = str_replace("?","_",str_replace("*","%",$current));
			if(($len - $count) >= 2)
			{
				if(substr($current,0,1) == "-")
				{
					$current = substr($current,1,strlen($current)-1);
					$g_user .= " AND t1.user NOT LIKE '%$current%' ";
				}
				else if(substr($current,0,1) == "~")
				{
					$current = substr($current,1,strlen($current)-1);
					$g_user .= " OR t1.user LIKE '%$current%' ";
				}
				else
					$g_user .= " AND t1.user LIKE '%$current%' ";
			}
		}
	}
	if (!isset($g_order) || $g_order == '') $g_order = "t1.id DESC";
	if($g_user != "")
	{
		$g_user = substr($g_user, 4);
		$query = "SELECT t1.id, t1.user, t1.email, t1.signup_date, t1.ip, t2.group_name FROM $user_table AS t1 JOIN $group_table AS t2 ON t2.id=t1.ugroup WHERE ($g_user) $g_id $g_email $g_date $g_ip $g_group ORDER BY $g_order";
	}
	else if($g_id != "" || $g_email != "" || $g_date != "" || $g_ip != "" || $g_group != "")
	{
		if($g_date != "")
			$g_date = str_replace('AND',"",$g_date);
		else if($g_id != "")
			$g_id = str_replace('AND',"",$g_id);
		else if($g_ip != "")
			$g_ip = str_replace('AND',"",$g_ip);
		else if($g_group != "")
			$g_group = str_replace('AND',"",$g_group);
		else if($g_email != "")
			$g_email = str_replace('AND',"",$g_email);
		$query = "SELECT t1.id, t1.user, t1.email, t1.signup_date, t1.ip, t2.group_name FROM $user_table AS t1 JOIN $group_table AS t2 ON t2.id=t1.ugroup WHERE $g_id $g_email $g_date $g_ip $g_group ORDER BY $g_order";			
	}
	else
	{
		$count = substr_count($search, '*', 0, strlen($search));
		if(strlen($search)-$count > 0)
		{
			$res = str_replace("*","%",$search);
			$query = "SELECT t1.id, t1.user, t1.email, t1.signup_date, t1.ip, t2.group_name FROM $user_table AS t1 JOIN $group_table AS t2 ON t2.id=t1.ugroup WHERE user LIKE '%$res%' ORDER BY $g_order";
		}
		else
			$query = "SELECT t1.id, t1.user, t1.email, t1.signup_date, t1.ip, t2.group_name FROM $user_table AS t1 JOIN $group_table AS t2 ON t2.id=t1.ugroup ORDER BY $g_order";
	}			
	return $query;
} ?>