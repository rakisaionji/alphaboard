<?php
	if(!defined('_IN_ADMIN_HEADER_'))
	{
		require "401_error.php";
		exit;
	}
	$user = new user();
	if(!$user->gotpermission('is_admin'))
	{
		print '
	<div class="content">';
		require "403_error.php";
		exit;
	}
?>

	<style type="text/css">
		.freecell
		{
			padding:5px;
			text-align:center;
			vertical-align:middle;
		}
	</style>
	<div class="content">
	<h2>PHP Configuration</h2><br>
	<p>These PHP configuration settings are customizable by the server administrator. They are listed for reference only.</p><br>
	<table width="100%" class="highlightable">
		<tr>
			<th class="freecell">Sub Section</th>
			<th class="freecell">Directive</th>
 			<th class="freecell">Info</th>
			<th class="freecell">Value</th>
		</tr>
		<tbody>
		<tr>
			<td class="freecell">Language Options</td> 
			<td class="freecell">asp_tags</td> 
			<td class="freecell">Allow ASP-style &lt;% %&gt; tags.</td> 
			<td class="freecell"><?php echo (ini_get('asp_tags') != '') ? 'On' : 'Off' ?></td> 
		</tr>

		<tr>
			<td class="freecell">File Uploads</td> 
			<td class="freecell">file_uploads</td> 
			<td class="freecell">Whether to allow HTTP file uploads.</td> 
			<td class="freecell"><?php echo (ini_get('file_uploads') != '') ? 'On' : 'Off' ?></td> 
		</tr>
		<tr>
			<td class="freecell">Paths and Directories</td> 
			<td class="freecell">include_path</td> 
			<td class="freecell">UNIX: &quot;/path1:/path2&quot;<br>Windows: &quot;\path1;\path2&quot;</td> 
			<td class="freecell"><?php echo ini_get('include_path') ?></td> 
		</tr>
		<tr>
			<td class="freecell">Resource Limits</td> 
			<td class="freecell">max_execution_time</td> 
			<td class="freecell">Maximum execution time of each script, in seconds.</td> 
			<td class="freecell"><?php echo ini_get('max_execution_time') ?></td> 
		</tr>
		<tr>
			<td class="freecell">Resource Limits</td> 
			<td class="freecell">max_input_time</td> 
			<td class="freecell">Maximum amount of time each script may spend parsing request data.</td> 
			<td class="freecell"><?php echo ini_get('max_input_time') ?></td> 
		</tr>
		<tr>
			<td class="freecell">Resource Limits</td> 
			<td class="freecell">memory_limit</td> 
			<td class="freecell">Maximum amount of memory a script may consume.</td> 
			<td class="freecell"><?php echo ini_get('memory_limit') ?></td> 
		</tr>
		<tr>
			<td class="freecell">Data Handling</td> 
			<td class="freecell">register_globals</td> 
			<td class="freecell">You should do your best to write your scripts so that they do not require register_globals to be on. Using form variables as globals can easily lead to possible security problems, if the code is not very well thought of.</td> 
			<td class="freecell"><?php echo (ini_get('register_globals') != '') ? 'On' : 'Off' ?></td> 
		</tr>
		<tr>
			<td class="freecell">Language Options</td> 
			<td class="freecell">safe_mode</td> 
			<td class="freecell"></td> 
			<td class="freecell"><?php echo (ini_get('safe_mode') != '') ? 'On' : 'Off' ?></td> 
		</tr>
		<tr>
			<td class="freecell">Main</td> 
			<td class="freecell">session.save_path</td> 
			<td class="freecell">Where N is an integer. Instead of storing all the session files in /path, what this will do is use subdirectories N-levels deep, and store the session data in those directories. This is useful if you or your OS have problems with lots of files in one directory, and is a more efficient layout for servers that handle lots of sessions.<br><br>NOTE 1: PHP will not create this directory structure automatically.<br>You can use the script in the ext/session dir for that purpose.<br>NOTE 2: See the section on garbage collection below if you choose to use subdirectories for session storage.</td> 
			<td class="freecell"><?php echo ini_get('session.save_path') ?></td> 
		</tr>
		<tr>
			<td class="freecell">File Uploads</td> 
			<td class="freecell">upload_max_filesize</td> 
			<td class="freecell">Maximum allowed size for uploaded files.</td> 
			<td class="freecell"><?php echo ini_get('upload_max_filesize') ?></td> 
		</tr>
		<tr>
			<td class="freecell">Main</td> 
			<td class="freecell">zend_optimizer.version</td> 
			<td class="freecell"></td> 
			<td class="freecell"><?php echo ini_get('zend_optimizer.version') ?></td> 
		</tr>
		</tbody>
	</table>
	</div>