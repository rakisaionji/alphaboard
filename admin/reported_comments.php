<?php
	if(!defined('_IN_ADMIN_HEADER_'))
	{
		require "401_error.php";
		exit;
	}

	$misc = new misc();

	print '
	<div class="content">
	<h2>Reported Comments</h2><br>';

	if(isset($_GET['unreport']) && is_numeric($_GET['unreport']))
	{
		$comment_id = $db->real_escape_string($_GET['unreport']);
		$query = "UPDATE $comment_table SET spam=FALSE, reporter=NULL, reported_date=NULL, rip=NULL, reason=NULL WHERE id='$comment_id'";
		if($db->query($query))
			echo '
		<div class="success-notice">Unflagged comment #'.$comment_id.'</div></br>';
		print '<meta http-equiv="refresh" content="5;url='.$site_url.'admin/?page=reported_comments">';
		exit;
	}

	//number of reports/page
	$limit = 20;
	//number of pages to display. number - 1. ex: for 5 value should be 4
	$page_limit = 4;
	
	$query = "SELECT COUNT(*) FROM $comment_table WHERE spam='1' ORDER BY id ASC";
	$result = $db->query($query);
	$row = $result->fetch_assoc();
	$numrows = $row['COUNT(*)'];
	if($numrows == 0)
		print "
		<div class=\"status-notice\">No reports found.</div><br>";
	else
	{
	echo '
		<table class="highlightable" style="font-size: 12px; width: 100%;">
		<tr>
			<th style="width: 200px;">Summary</th>
			<th>Comment</th>
			<th style="width: 100px;">Tasks</th>
		</tr>';
		if(isset($_GET['pid']) && $_GET['pid'] != "" && is_numeric($_GET['pid']) && $_GET['pid'] >= 0)
			$page = $db->real_escape_string($_GET['pid']);
		else
			$page = 0;
		$query = "SELECT * FROM $comment_table WHERE spam='1' ORDER BY id LIMIT $page, $limit";
		$result = $db->query($query);
		while($row = $result->fetch_assoc())
		{
			echo '
		<tr>
			<td>
			<br>
				<p>Post ID: <a href="../index.php?page=post&amp;s=view&amp;id='.$row['post_id'].'">'.$row['post_id'].'</a></p>
				<p>Comment ID: <a href="#">'.$row['id'].'</a>
				<br>Comment Score: <a href="#">'.$row['score'].'</a></p>
				<p>Commented: <a href="#" title="'.date('l, F jS, Y G:i:s T',$row['posted_at']).'">'.$misc->date_words($row['posted_at']).'</a>
				<br>by <a href="'.$site_url.'index.php?page=account&amp;s=profile&amp;uname='.$row['user'].'">'.$row['user'].'</a> via <a href="#">'.$row['ip'].'</a></p>
				<p>Reported: <a href="#" title="'.date('l, F jS, Y G:i:s T',$row['reported_date']).'">'.$misc->date_words($row['reported_date']).'</a>
				<br>by <a href="'.$site_url.'index.php?page=account&amp;s=profile&amp;uname='.$row['reporter'].'">'.$row['reporter'].'</a> via <a href="#">'.$row['rip'].'</a></p> 
				<p>Reason: '.stripslashes($row['reason']).'</p>
			</td>
			<td style="padding:16px">
			'.stripslashes($misc->swap_bbs_tags($misc->short_url($misc->linebreaks($row['comment'])))).'
			</td>
			<td>
			<br>
				<p><a href="'.$site_url.'admin/?page=reported_comments&amp;unreport='.$row['id'].'">Unflag</a></p>
				<p><a href="#" onclick="if(confirm(\'Are you sure you want to delete this comment?\')){document.location=\'../public/remove.php?id='.$row['id'].'&amp;removecomment=1&amp;post_id='.$row['post_id'].'&amp;token='.base64_encode($site_url.'admin/?page=reported_comments').'\';}; return false;">Remove</a></p>
			</td>
		</tr>';
		}
		$result->free_result();
		
		echo "
		</table>";
		print "
		<div id='paginator'>";
		print '
			'.$misc->pagination($_GET['page'],$_GET['s'],$id,$limit,$page_limit,$numrows,$_GET['pid']);
		print "
		</div>
		<br><br>";
	}
?>

	</div>