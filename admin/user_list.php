<?php
	if(!defined('_IN_ADMIN_HEADER_'))
	{
		require "401_error.php";
		exit;
	}
?>

	<div class="content">
	<h2>User Listing</h2><br>
<?php
	//number of topics/page
	$limit = 25;
	//number of pages to display. number - 1. ex: for 5 value should be 4
	$page_limit = 4;

	$misc = new misc();
	$user = new user();

	if(!$user->gotpermission('is_admin'))
	{
		require "403_error.php";
		exit;
	}
	
	require "user_func.php";

	echo '		<div style="width: 500px; padding: 0px; margin-bottom: 10px; text-align: left;">
			<form method="get" action="'.$site_url.'admin/index.php?page=user_list">
				<input name="page" value="'.$_GET['page'].'" type="hidden">
				<input type="text" name="search" size="30" class="main_search_text" value="'.stripslashes($_GET['search']).'">
				<input type="submit" class="main_search" value="Search">
			</form>
		</div>
		<table class="highlightable" width="100%">
		<tr>
			<th width="20%">User</th>
			<th width="20%">Group</th>
			<th width="20%">Email</th>
			<th width="15%">Signup</th>
			<th width="15%">IP</th>
			<th width="10%">Edit</th>
		</tr>';

	if(isset($_GET['pid']) && $_GET['pid'] != "" && is_numeric($_GET['pid']) && $_GET['pid'] >= 0)
		$page = $db->real_escape_string($_GET['pid']);
	else
		$page = 0;

	if (!isset($_GET['search']) || isset($_GET['search']) && $_GET['search'] == "")
	{
		$query = "SELECT COUNT(*) FROM $user_table";
		$result = $db->query($query);
		$row = $result->fetch_assoc();
		$numrows = $row['COUNT(*)'];
		$query = "SELECT t1.id, t1.user, t1.email, t1.signup_date, t1.ip, t2.group_name FROM $user_table AS t1 JOIN $group_table AS t2 ON t2.id=t1.ugroup ORDER BY id DESC LIMIT $page, $limit";
	}
	else
	{
		$tags = stripslashes(str_replace("%",'',mb_trim(htmlentities($_GET['search'], ENT_QUOTES, 'UTF-8'))));		
		$query = prepare_users($tags);
		$result = $db->query($query);
		$numrows = $result->num_rows;
		$result->free_result();
		if($numrows != 0)
		$query = $query." LIMIT $page, $limit";			
	}

	$result = $db->query($query);

	while($row = $result->fetch_assoc())
	{
		if ($row['signup_date'] != 0 && $row['signup_date'] != '')
		{
			$date_now = $misc->date_words($row['signup_date']);
			$signup_date = date('l, F jS, Y G:i:s T',$row['signup_date']);
		}
		else
		{
			$date_now = 'N/A'; $signup_date = 'Not Available';
		}

		print '
		<tr>
			<td><a href="'.$site_url.'index.php?page=account&amp;s=profile&amp;id='.$row['id'].'">'.$row['user'].'</a></td>
			<td>'.stripslashes($row['group_name']).'</td>
			<td>';
			if ($row['email'] != '')
				print '<a href="mailto:'.stripslashes($row['email']).'">'.stripslashes($row['email']).'</a>';
			else
				print '<span title="Not Available">N/A</span>';
			print '</td>
			<td><span title="'.$signup_date.'">'.$date_now.'</span></td>
			<td>'.stripslashes($row['ip']).'</td>
			<td><a href="'.$site_url.'admin/index.php?page=edit_user&amp;user='.$row['user'].'">Edit</a></td>
		</tr>';
	}

	echo '
		</table>
		<div id="paginator">
			'.$misc->pagination($_GET['page'],$_GET['s'],$row['id'],$limit,$page_limit,$numrows,$_GET['pid'],$_GET['tags'],$_GET['search']);
?>	
		</div>
		<br>
	</div>