<?php
	if(!defined('_IN_ADMIN_HEADER_'))
	{
		require "401_error.php";
		exit;
	}
?>

	<div class="content">
	<h2>Ban User</h2><br>
<?php
	set_time_limit(0);
	$user = new user();
	if(!$user->gotpermission('is_admin'))
	{
		require "403_error.php";
		exit;
	}

	print '	<div class="status-notice">';

	$ip = $db->real_escape_string($_SERVER['REMOTE_ADDR']);	
	if(isset($_POST['settings']) && is_numeric($_POST['settings']) && $_POST['settings'] == 1)
	{
		//Are you an idiot who decided to ban themselves? Let's hope not... ;)
		if((isset($_POST['user_id']) && $_POST['user_id'] == 0) || (isset($_POST['username']) && mb_strtolower($_POST['username'], 'UTF-8') == "anonymous") || (isset($_POST['user_id']) && mb_strtolower($_POST['user_id'], 'UTF-8') == mb_strtolower($checked_user_id, 'UTF-8')) || (isset($_POST['username']) && mb_strtolower($_POST['username'], 'UTF-8') == mb_strtolower($checked_username, 'UTF-8')))
		{
			print "
		<h4>You REALLY don't want to do that! Trust me!</h4>";
			exit;
		}

		if(isset($_POST['user_id']) && is_numeric($_POST['user_id']))
		{
		$ban_id = $db->real_escape_string($_POST['user_id']);
		$query = "SELECT id, user, ip FROM $user_table WHERE id = '$ban_id' LIMIT 1";
		$result = $db->query($query);
		$row = $result->fetch_assoc();	
		}
		else if(isset($_POST['username']) && $user->user_exists($_POST['username']))
		{
		$ban_username = $db->real_escape_string($_POST['username']);
		$query = "SELECT id, user, ip, login_ip FROM $user_table WHERE user = '$ban_username' LIMIT 1";
		$result = $db->query($query);
		$row = $result->fetch_assoc();	
		}
		else
		{
		print '
		<p>Some information have been lost! Please supply an username or user_id to continue!</p>
		<meta http-equiv="refresh" content="5;url='.$site_url.'admin/?page=ban_user">';
		exit;
		}
	
		//Let's grab the database values of all three since they are already there.
		$ban_id = $db->real_escape_string($row['id']);
		$ban_username = $db->real_escape_string($row['user']);
		$ban_ip = $db->real_escape_string($row['ip']);
		$ban_login = $db->real_escape_string($row['login_ip']);
		$ban_reason = htmlentities(stripslashes($_POST['ban_reason']), ENT_QUOTES, 'UTF-8');
		$ban_about = '
[br]
[c][b]THIS USER HAVE BEEN BANNED[/b][/c][br]
[br]
Reported by: [i]'.$checked_username.'[/i][br]
Date: [i]'.date('l, F jS, Y G:i:s T').'[/i][br]
Reason: [i]'.$_POST['ban_reason'].'[/i][br]
[br]
[c][b]THIS USER HAVE BEEN BANNED[/b][/c]
[br]
';

		//These queries could be done with a single join, but why bother making it complicated? 
		//Multiple simple queries shouldn't be that bad on the server... Right?
		print "
		<br>
		<p>Banning user <i>$ban_username</i> for the following reason:
		<br>$ban_reason</p>
		<p>Is it okay now? Let's ban this user!</p>
		<br>
		<hr>
		<br>
		<p>Starting up procedures...
		<span style=\"color:#0f0\">Success</span>";

		flush(); //To save memory!? *lol*

		if ($ban_ip != $ip)
		{
		print "</p>
		<br>
		<p>Adding the user's registration IP to the database for blocking access in the future...";
		$query = "INSERT INTO $banned_ip_table(ip,user,reason,date_added) VALUES('$ban_ip','$checked_username','$ban_reason','".time()."')";
		if ($db->query($query))
		print '
		<span style="color:#0f0">Success</span>';
		else
		print '
		<span style="color:#f00">Error</span>';
		}

		if ($ban_login != $ip)
		{
		print "</p>
		<br>
		<p>Adding the user's IP from the last recorded session to the database too...";
		$query = "INSERT INTO $banned_ip_table(ip,user,reason,date_added) VALUES('$ban_login','$checked_username','$ban_reason','".time()."')";
		if ($db->query($query))
		print '
		<span style="color:#0f0">Success</span>';
		else
		print '
		<span style="color:#f00">Error</span>';
		}

		//Banning lots of IP addresses have some problems!
		//Especially for users with same ISP and Dynamic IP!
		//Let's disable these procedures temporarily in Alphaboard now!

		/*
		Skip! Skip! Skip!

		print "Now attempting to ban IP addresses in comment_vote table... <br>";
		flush();
		$query = "SELECT * FROM $comment_vote_table WHERE user_id = '$ban_id' GROUP BY ip";
		$result = $db->query($query);
		while($row = $result->fetch_assoc())
		{
			$ban_ip = $db->real_escape_string($row['ip']);
			$query = "INSERT INTO $banned_ip_table(ip,user,reason,date_added) VALUES('$ban_ip','$checked_username','$ban_reason','".time()."')";
			$db->query($query);
		}
		
		print "Now attempting to ban IP addresses in comment table... <br>";	
		flush();		
		$query = "SELECT * FROM $comment_table WHERE user = '$ban_username' GROUP BY ip";
		$result = $db->query($query);
		while($row = $result->fetch_assoc())
		{
			$ban_ip = $db->real_escape_string($row['ip']);
			$query = "INSERT INTO $banned_ip_table(ip,user,reason,date_added) VALUES('$ban_ip','$checked_username','$ban_reason','".time()."')";
			$db->query($query);
		}	
		
		print "Now attempting to ban IP addresses in note table... <br>";
		flush();		
		$query = "SELECT * FROM $note_table WHERE user_id = '$ban_id' GROUP BY ip";
		$result = $db->query($query);
		while($row = $result->fetch_assoc())
		{
			$ban_ip = $db->real_escape_string($row['ip']);
			$query = "INSERT INTO $banned_ip_table(ip,user,reason,date_added) VALUES('$ban_ip','$checked_username','$ban_reason','".time()."')";
			$db->query($query);
		}
		
		print "Now attempting to ban IP addresses in post vote table... <br>";
		flush();		
		$query = "SELECT * FROM $post_vote_table WHERE user_id = '$ban_id' GROUP BY ip";
		$result = $db->query($query);
		while($row = $result->fetch_assoc())
		{
			$ban_ip = $db->real_escape_string($row['ip']);
			$query = "INSERT INTO $banned_ip_table(ip,user,reason,date_added) VALUES('$ban_ip','$checked_username','$ban_reason','".time()."')";
			$db->query($query);
		}		
		
		print "Now attempting to ban IP addresses in tag history table... <br>";
		flush();		
		$query = "SELECT * FROM $tag_history_table WHERE user_id = '$ban_id' GROUP BY ip";
		$result = $db->query($query);
		while($row = $result->fetch_assoc())
		{
			$ban_ip = $db->real_escape_string($row['ip']);
			$query = "INSERT INTO $banned_ip_table(ip,user,reason,date_added) VALUES('$ban_ip','$checked_username','$ban_reason','".time()."')";
			$db->query($query);
		}	
		
		print "Now attempting to ban IP addresses in post table... <br>";		
		flush();
		$query = "SELECT * FROM $post_table WHERE owner = '$ban_username' ORDER BY id DESC";
		$result = $db->query($query);
		while($row = $result->fetch_assoc())
		{
			$ban_ip = $db->real_escape_string($row['ip']);
			$query = "INSERT INTO $banned_ip_table(ip,user,reason,date_added) VALUES('$ban_ip','$checked_username','$ban_reason','".time()."')";
			$db->query($query);
		}

		Skip! Skip! Skip!
		*/

		print "</p>
		<br>
		<p>Removing the accessibility of the user's account from the database...";
		$ban_about = base64_encode(htmlentities($ban_about, ENT_QUOTES, 'UTF-8'));
		$query = "UPDATE $user_table SET pass=NULL, about='$ban_about' WHERE id='$ban_id'";
		if ($db->query($query))
		print '
		<span style="color:#0f0">Success</span>';
		else
		print '
		<span style="color:#f00">Error</span>';

		print "</p>
		<br>
		<hr>
		<br>
		<p>See no errors up there? The process was completed successfully!
		<br>The banned user will not able to login and cannot make you worried anymore!
		<br><br>Have a nice day!</p>
		<br>";
	}
	else if(isset($_GET['user_id']) && is_numeric($_GET['user_id']) && $_GET['user_id'] != 0)
	{
		$ban_id = $db->real_escape_string($_GET['user_id']);
		$query = "SELECT id, user, ip FROM $user_table WHERE id = '$ban_id' LIMIT 1";
		$result = $db->query($query);
		$row = $result->fetch_assoc();	
	
		//Are you an idiot who decided to ban themselves? Let's hope not... ;)
		if(mb_strtolower($row['user'], 'UTF-8') == "anonymous" || mb_strtolower($row['user'], 'UTF-8') == mb_strtolower($checked_username, 'UTF-8'))
		{
			print "
		<h4>You REALLY don't want to do that! Trust me!</h4>";
			exit;
		}
		//Reason
	}
	else
	{
		print '
		<h6>Make note that banning using this tool will check the whole database for all IP addresses associated with this account.
		<br>This will take a while if you have a <i>really</i> big database. Even then this will take forever. Just sit back.</h6>
		<br><br>
		<center>
			<form method="post" action="">
			<table style="font-size:13px">
			<tr>
				<th width="96px" height="32px" style="text-align:center; vertical-align:middle;">User</th>
				<td width="256px" height="32px" style="text-align:center; vertical-align:middle;">
				<input type="text" name="username" style="width: 250px; height:20px; vertical-align:middle; font-size:13px; padding: 4px;" value="">
			</td>
			</tr>
			<tr>
				<th style="text-align:center; vertical-align:middle;">Reason</th>
				<td style="text-align:center; vertical-align:middle;">
				<input type="text" name="ban_reason" style="width: 250px; height:20px; vertical-align:middle; font-size:13px; padding: 4px;" value="">
				</td>
			</tr>
			<tr>
				<td>
				<input type="hidden" name="settings" id="settings" value="1">
				</td>
				<td style="text-align:left; vertical-align:middle;">
				<input type="submit" style="width: 128px; height:30px; vertical-align:middle; font-size:13px; padding: 4px;" value="Ban User">
			</td>
			<tr>
			</table>
			</form>
		</center>';
	}
?>

	</div>
	</div>