<?php
	header("HTTP/1.1 401");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<title>401 Unauthorized</title>
	</head>
	<body>
		<h1>Authorization Required</h1>
		<p>Please log in to the ADMIN PANEL using your account.
		<br>An ADMIN or a MODERATOR account may be required.</p>
		<hr>
		<address>AlphaBoard Server - Admin Page - Version 1.1</address>
	</body>
</html>