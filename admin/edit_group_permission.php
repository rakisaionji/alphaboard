<?php
	if(!defined('_IN_ADMIN_HEADER_'))
	{
		require "401_error.php";
		exit;
	}
?>

	<div class="content">
	<h2>Edit Group Permissions</h2><br>
<?php
	$user = new user();
	if(!$user->gotpermission('is_admin'))
	{
		require "403_error.php";
		exit;
	}

	if(isset($_GET['delete']) && is_numeric($_GET['delete']))
	{
		$del_id = $db->real_escape_string($_GET['delete']);
		$query = "DELETE FROM $group_table WHERE id ='$del_id'";
		$db->query($query);
		
	}
	if(isset($_POST['check']) && $_POST['check'] == 1)
	{
		(isset($_POST['edit_posts']) && $_POST['edit_posts'] == true) ? $eposts = "TRUE" : $eposts = "FALSE";
		(isset($_POST['approve_posts']) && $_POST['approve_posts'] == true) ? $aposts = "TRUE" : $aposts = "FALSE";
		(isset($_POST['approve_comments']) && $_POST['approve_comments'] == true) ? $acomments = "TRUE" : $acomments = "FALSE";
		(isset($_POST['etags']) && $_POST['etags'] == true) ? $etags = "TRUE" : $etags = "FALSE";
		(isset($_POST['aalias']) && $_POST['aalias'] == true) ? $aalias = "TRUE" : $aalias = "FALSE";
		(isset($_POST['delete_posts']) && $_POST['delete_posts'] == true) ? $dposts = "TRUE" : $dposts = "FALSE";
		(isset($_POST['delete_comments']) && $_POST['delete_comments'] == true) ? $dcomments = "TRUE" : $dcomments = "FALSE";
		(isset($_POST['admin_panel']) && $_POST['admin_panel'] == true) ? $apanel = "TRUE" : $apanel = "FALSE";
		(isset($_POST['is_default']) && $_POST['is_default'] == true) ? $is_default = "TRUE" : $is_default = "FALSE";
		(isset($_POST['rnotes']) && $_POST['rnotes'] == true) ? $rnotes = "TRUE" : $rnotes = "FALSE";
		(isset($_POST['rtags']) && $_POST['rtags'] == true) ? $rtags = "TRUE" : $rtags = "FALSE";
		(isset($_POST['fadd']) && $_POST['fadd'] == true) ? $fadd = "TRUE" : $fadd = "FALSE";
		(isset($_POST['faposts']) && $_POST['faposts'] == true) ? $faposts = "TRUE" : $faposts = "FALSE";
		(isset($_POST['fposts']) && $_POST['fposts'] == true) ? $fposts = "TRUE" : $fposts = "FALSE";
		(isset($_POST['ftopics']) && $_POST['ftopics'] == true) ? $ftopics = "TRUE" : $ftopics = "FALSE";
		(isset($_POST['flock']) && $_POST['flock'] == true) ? $flock = "TRUE" : $flock = "FALSE";
		(isset($_POST['fedit']) && $_POST['fedit'] == true) ? $fedit = "TRUE" : $fedit = "FALSE";
		(isset($_POST['fpin']) && $_POST['fpin'] == true) ? $fpin = "TRUE" : $fpin = "FALSE";
		(isset($_POST['anotes']) && $_POST['anotes'] == true) ? $anotes = "TRUE" : $anotes = "FALSE";
		(isset($_POST['cupload']) && $_POST['cupload'] == true) ? $cupload = "TRUE" : $cupload = "FALSE";
		(isset($_POST['iadmin']) && $_POST['iadmin'] == true) ? $iadmin = "TRUE" : $iadmin = "FALSE";
		if($is_default == "TRUE")
		{
			$query = "UPDATE $group_table SET default_group=FALSE";
			$db->query($query);
		}
		$query = "UPDATE $group_table SET add_aliases=$aalias, edit_posts=$eposts, edit_tags=$etags , approve_posts=$aposts, approve_comments=$acomments, delete_posts=$dposts, delete_comments=$dcomments, admin_panel=$apanel, default_group=$is_default, reverse_notes=$rnotes, reverse_tags=$rtags, new_forum_topics=$fadd, new_forum_posts=$faposts, delete_forum_posts=$fposts, delete_forum_topics=$ftopics, lock_forum_topics=$flock, edit_forum_posts=$fedit, pin_forum_topics=$fpin, alter_notes=$anotes, can_upload=$cupload, is_admin=$iadmin WHERE id='".$db->real_escape_string($_POST['group'])."'";
		
		if($db->query($query))
			print "
		<div class=\"success-notice\">Permissions for group '".$_POST['groupname']."' was modified successfully.</div><br>";
		else
			print "
		<div class=\"error-notice\">Failed to edit permissions for group '".$_POST['groupname']."'.</div><br>";
		print '<meta http-equiv="refresh" content="5;url='.$site_url.'admin/?page=edit_group">';
		exit;
	}
	if(isset($_POST['group_name'])  && $_POST['group_name'] != "")
	{
		$gname = $db->real_escape_string($_POST['group_name']);
		$query = "SELECT * FROM $group_table WHERE id='$gname'";
		$result = $db->query($query);
		$row = $result->fetch_assoc();
?>
	<form method="post" action="">
	<h4>Group: <input readonly name="groupname" style="background-color:transparent;color:white;border:0;font-family:tahoma;font-size:1em;font-weight:bold;" value="<?php print $row['group_name']; ?>"></h4>
	<table style="font-size: 12px; width: 100%;"><tr><td>
	</td></tr>
	<tr><td>
	</td></tr>
	<tr><td>
	<input type="checkbox" name="is_default" <?php $row['default_group'] == true ? print 'checked' : print ''; ?>>
	<span style="color: #81F781;">Default group?</span>
	</td></tr>
	<tr><td>
	</td></tr>
	<tr><td>
	</td></tr>
	<tr><td>
	<input type="checkbox" name="iadmin" <?php $row['is_admin'] == true ? print 'checked' : print ''; ?>>
	<span style="color: #F78181;">Members of this group is an admin?</span>
	</td></tr>
	<tr><td>
	<input type="checkbox" name="admin_panel" <?php $row['admin_panel'] == true ? print 'checked' : print ''; ?>>
	<span style="color: #F3F781;">Members of this group can access admin panel?</span>
	</td></tr>
	<tr><td>
	</td></tr>
	<tr><td>
	</td></tr>
	<tr><td>
	<input type="checkbox" name="cupload" <?php $row['can_upload'] == true ? print 'checked' : print ''; ?>>
	Members of this group can upload new posts?
	</td></tr>
	<tr><td>
	<input type="checkbox" name="edit_posts" <?php $row['edit_posts'] == true ? print 'checked' : print ''; ?>>
	Members of this group can edit posts?
	</td></tr>
	<tr><td>
	</td></tr>
	<tr><td>
	<input type="checkbox" name="delete_posts" <?php $row['delete_posts'] == true ? print 'checked' : print ''; ?>>
	Members of this group can delete posts?
	</td></tr>
	<tr><td>
	<input type="checkbox" name="delete_comments" <?php $row['delete_comments'] == true ? print 'checked' : print ''; ?>>
	Members of this group can delete comments?
	</td></tr>
	<tr><td>
	</td></tr>
	<tr><td>
	<input type="checkbox" name="approve_posts" <?php $row['approve_posts'] == true ? print 'checked' : print ''; ?>>
	Members of this group can approve posts?
	</td></tr>
	<tr><td>
	<input type="checkbox" name="approve_comments" <?php $row['approve_comments'] == true ? print 'checked' : print ''; ?>>
	Members of this group can approve comments?
	</td></tr>
	<tr><td>
	</td></tr>
	<tr><td>
	</td></tr>
	<tr><td>
	<input type="checkbox" name="aalias" <?php $row['add_aliases'] == true ? print 'checked' : print ''; ?>>
	Members of this group can add aliases?
	</td></tr>
	<tr><td>
	<input type="checkbox" name="etags" <?php $row['edit_tags'] == true ? print 'checked' : print ''; ?>>
	Members of this group can edit tags?
	</td></tr>
	<tr><td>
	<input type="checkbox" name="anotes" <?php $row['alter_notes'] == true ? print 'checked' : print ''; ?>>
	Members of this group can edit notes?
	</td></tr>
	<tr><td>
	</td></tr>
	<tr><td>
	<input type="checkbox" name="rnotes" <?php $row['reverse_notes'] == true ? print 'checked' : print ''; ?>>
	Members of this group can revert notes?
	</td></tr>
	<tr><td>
	<input type="checkbox" name="rtags" <?php $row['reverse_tags'] == true ? print 'checked' : print ''; ?>>
	Members of this group can revert tags?
	</td></tr>
	<tr><td>
	</td></tr>
	<tr><td>
	</td></tr>
	<tr><td>
	<input type="checkbox" name="fadd" <?php $row['new_forum_topics'] == true ? print 'checked' : print ''; ?>>
	Members of this group can add forum topics?
	</td></tr>
	<tr><td>
	<input type="checkbox" name="faposts" <?php $row['new_forum_posts'] == true ? print 'checked' : print ''; ?>>
	Members of this group can add forum posts?
	</td></tr>
	<tr><td>
	<input type="checkbox" name="fedit" <?php $row['edit_forum_posts'] == true ? print 'checked' : print ''; ?>>
	Members of this group can edit all forum posts?
	</td></tr>
	<tr><td>
	</td></tr>
	<tr><td>
	<input type="checkbox" name="ftopics" <?php $row['delete_forum_topics'] == true ? print 'checked' : print ''; ?>>
	Members of this group can delete forum topics?
	</td></tr>
	<tr><td>
	<input type="checkbox" name="fposts" <?php $row['delete_forum_posts'] == true ? print 'checked' : print ''; ?>>
	Members of this group can delete forum posts?
	</td></tr>
	<tr><td>
	</td></tr>
	<tr><td>
	<input type="checkbox" name="fpin" <?php $row['pin_forum_topics'] == true ? print 'checked' : print ''; ?>>
	Members of this group can pin forum topics?
	</td></tr>
	<tr><td>
	<input type="checkbox" name="flock" <?php $row['lock_forum_topics'] == true ? print 'checked' : print ''; ?>>
	Members of this group can lock forum topics?
	</td></tr>
	</table>
	<input type="submit" name="submit" style="width: 128px; height: 24px; margin-bottom:12px;" value="Save Permissions">
	<input type="hidden" name="check" value="1">
	<input type="hidden" name="group" value="<?php print $gname; ?>">
	</form>
	<form method="get" action="">
	<input type="hidden" name="page" value="edit_group">
	<input type="hidden" name="delete" value="<?php print $row['id']; ?>">
	<input type="submit" style="width: 128px; height: 24px; margin-bottom:12px;" value="Delete Group">
	</form>
	</div><?php
	}
	else
	{
		echo '	<form method="post" action="">
		<table>
		<tr style="font-size:13px;">
		<td width="256px" height="32px" style="text-align:center; vertical-align:middle;">
		<select name="group_name" style="width: 250px; height:30px; vertical-align:middle; font-size:13px; padding: 4px;">
			<option>Select a group</option>';
		
		$uid = $db->real_escape_string($_COOKIE['user_id']); 
		$query = "SELECT group_name, id, (SELECT t1.is_admin FROM $group_table AS t1 JOIN $user_table AS t2 ON t2.id='$uid'WHERE t1.id=t2.ugroup) AS admin FROM $group_table ORDER BY id ASC";
		$result = $db->query($query);
		while($row = $result->fetch_assoc())
		{
			if($row['admin'] == true && $row['id'] == 1)
			{
				echo "
			<option value=\"".$row['id']."\">".$row['group_name']."</option>";
			}
			else if($row['id'] != 1 && $row['id'] > 1)
			{
				echo "
			<option value=\"".$row['id']."\">".$row['group_name']."</option>";
			}
		}
		echo '
		</select>
		</td>
		<td width="128px" height="32px" style="text-align:center; vertical-align:middle;">
		<input type="submit" name="submit" style="width: 128px; height:30px; vertical-align:middle; font-size:13px; padding: 4px;" value="Submit">
		</td>
		</tr>
		</table>
	</form>
	</div>';
	}
?>