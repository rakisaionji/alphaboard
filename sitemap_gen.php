<?php
	//Execution time limit.
	//Recommended value: 600 (10mins).
	set_time_limit(1200);
	require "inv.header.php";

	$user = new user();
	$sitemap = new sitemap(); 
	
	if (!$user->gotpermission("is_admin"))
	{
		header("Location: index.php");
		exit;
	}
	
	header("Content-Type: text/plain");
	
	echo "Last login: ".date("D M j G:i:s")." on {$_SERVER['SERVER_NAME']}

--------------------------------------------------
     AlphaBoard Sitemap Generator
              Copyright (C) 2011 Raki Saionji
--------------------------------------------------

> Creating sitemap for HOME... <OK>";
	$created = time();
	$sitemap->url("toc.php", $created, "yearly"); 
	$sitemap->url("tos.php", $created, "yearly"); 
	$sitemap->url("index.php", $created, "daily"); 
	$sitemap->url("license.php", $created, "yearly"); 
	$sitemap->url("index.php?page=help", $created, "yearly"); 
	$sitemap->url("index.php?page=about", $created, "yearly"); 
	$sitemap->url("index.php?page=post&s=list", $created, "daily");
	$sitemap->url("index.php?page=tags&s=list", $created, "weekly"); 
	$sitemap->url("index.php?page=forum&s=list", $created, "weekly"); 
	$sitemap->url("index.php?page=alias&s=list", $created, "weekly");
	$sitemap->url("index.php?page=account&s=home", $created, "monthly");
	$sitemap->url("index.php?page=comment&s=list", $created, "daily");
	echo "
> Creating sitemap for POSTS...";
	$sitemap->page("posts");
	echo ($result = $db->query("SELECT id, creation_date FROM $post_table")) ? " <OK>" : " <ERROR>";
	while (list($id, $created) = $result->fetch_row())
	{
		$sitemap->url("index.php?page=post&s=view&id=$id", $created, "weekly"); 
	}
	echo "
> Creating sitemap for COMMENTS...";
	$sitemap->page("comments");
	echo ($result = $db->query("SELECT post_id, posted_at FROM $comment_table")) ? " <OK>" : " <ERROR>";
	while (list($id, $created) = $result->fetch_row())
	{
		$sitemap->url("index.php?page=post&s=view&id=$id", $created, "weekly"); 
	}
	echo "
> Creating sitemap for TAGS...";
	$sitemap->page("tags");
	echo ($result = $db->query("SELECT tag FROM $tag_index_table WHERE index_count > 0")) ? " <OK>" : " <ERROR>";
	while (list($id) = $result->fetch_row())
	{
		$sitemap->url("index.php?page=post&s=list&tags=$id", "", "daily"); 
	}
	echo "
> Creating sitemap for FORUM THREADS...";
	$sitemap->page("fthreads");
	echo ($result = $db->query("SELECT id, last_updated FROM $forum_topic_table")) ? " <OK>" : " <ERROR>";
	while (list($id, $created) = $result->fetch_row())
	{
		$sitemap->url("index.php?page=forum&s=view&id=$id", $created, "daily"); 
	}
	echo "
> Creating sitemap for FORUM POSTS...";
	$sitemap->page("fposts");
	echo ($result = $db->query("SELECT id, topic_id, creation_date FROM $forum_post_table")) ? " <OK>" : " <ERROR>";
	while (list($id, $pid, $created) = $result->fetch_row())
	{
		$sitemap->url("index.php?page=forum&s=view&id=$id&post=$pid", $created, "weekly"); 
	}
	echo "
> Pinging search engines... (3)";
	$sitemap->close(); 
	unset ($sitemap);
	echo '
> Closing connection... <OK>

[Process completed]';
?>