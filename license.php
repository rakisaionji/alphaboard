<?php
	require "inv.header.php";
	require "includes/header.php";
?>	<div id="content" style="width:800px;margin:auto">
	<h2>Licensing Information</h2><br>
	<p>AlphaBoard is an <a href="http://en.wikipedia.org/wiki/Imageboard" target="_blank">imageboard</a> based on <a href="http://gelbooru.com" target="_blank">Gelbooru</a>'s original source (version 0.1.11).
	<br>Bundled with some new features, bug fixes and more.</p>
	<p>Available licenses:
	<a href="#prototype">[JS] Prototype</a> |
	<a href="#scriptaculous">[JS] Scriptaculous</a> |
	<a href="#danbooru">[JS] Notes.JS</a> |
	<a href="#gelbooru">[PHP] Gelbooru</a> |
	<a href="#alphaboard">[PHP] AlphaBoard</a></p>
	<br>
	<a name="prototype"></a><br>
	<h4>[JavaScript] Prototype</h4><br>
	<div class="license">
		<p>Copyright (C) 2005-2010 Sam Stephenson.</p>
		<p>Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:</p>
		<p>THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</p>
	</div><br>
	<a name="scriptaculous"></a><br>
	<h4>[JavaScript] Script.aculo.us</h4><br>
	<div class="license">
		<p>Copyright (C) 2005-2010 Thomas Fuchs (http://script.aculo.us, http://mir.aculo.us)</p>
		<p>Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:</p>
		<p>The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.</p>
		<p>THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</p>
	</div><br>
	<a name="danbooru"></a><br>
	<h4>[JavaScript] Danbooru.Notes</h4><br>
	<div class="license">
		<p>Copyright (C) 2005 Albert Yi. All rights reserved.</p>
		<p>Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:</p>
		<p>1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.</p>
		<p>2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.</p>
		<p>THIS SOFTWARE IS PROVIDED BY THE DANBOORU PROJECT "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE FREEBSD PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>
	</div><br>
	<a name="gelbooru"></a><br>
	<h4>[PHP] Gelbooru Original Source</h4><br>
	<div class="license">
		<p>Copyright (C) 2008 Gelbooru.com All rights reserved.</p>
		<p>Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:</p>
		<p>1. Redistributions of source code in whole or part must retain the above copyright notice, this list of conditions and the following disclaimer.</p>
		<p>2. Redistributions in binary form in whole or part must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.</p>
		<p>3. Use of this software for commercial purposes or profit is prohibited without written permission from Gelbooru.com</p>
		<p>THIS SOFTWARE IS PROVIDED BY GELBOORU.COM "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL GELBOORU.COM BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>
	</div><br>
	<a name="alphaboard"></a><br>
	<h4>[PHP+JS+CSS+HTML] AlphaBoard Project Source</h4><br>
	<p>AlphaBoard is licensed under MIT License and CC BY-NC-SA License.</p><br>
	<h6>MIT License is applied to the source code of AlphaBoard (PHP+JS+CSS)</h6><br>
	<div class="license">
	<p>Copyright (C) 2010-2011 Raki Saionji</p>
	<p>Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:</p>
	<p>The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.</p>
	<p>THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</p>
	</div><br>
	<h6>CC BY-NC-CA License is applied to the markup and the documentation of AlphaBoard (HTML+DOC+TXT)</h6><br>
	<div class="license">
	<p><strong>You are free:</strong>
	<br><strong>to Share</strong> &ndash; to copy, distribute and transmit the work
	<br><strong>to Remix</strong> &ndash; adapt the work</p>
	<p>Under the following conditions:
	<br><strong>Attribution</strong> &ndash; You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
	<br><strong>Noncommercial</strong> &ndash; You may not use this work for commercial purposes.
	<br><strong>Share Alike</strong> &ndash; If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.</p>
	<p>Summary of the license can be found at:
	<br>http://creativecommons.org/licenses/by-nc-sa/3.0/</p>
	</div><br>
	</div>
</body>
</html>