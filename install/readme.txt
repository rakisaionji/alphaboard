
#Installation Instructions#

1. Make sure to fill in the information in config.php.
   You need to set the host which MySQL is listening on, the username, password and database name.
   Set your site URL as well.

2. Please navigate to the install folder using your web browser.
   Ex: http://sitename.com/install/

3. When you are done installing, delete the install folder as well as the upgrade folder.

4. Give images, tmp, and thumbnails folders writable permissions.
   Every other directory will not be written to.

5. Check to make sure these are enabled in your php.ini, located at the installation path of PHP.
   - extension=php_mbstring.dll
   - extension=php_gd2.dll
   - extension=php_mysql.dll
   - extension=php_mysqli.dll
   - gd.jpeg_ignore_warning = 1

#Extra Information#

   If you are upgrading or installing for the first time
   and having issues searching for tags with less than 3 characters:

   - Disable the stopword file for MySQL and also set the minchar length to 1.
	Example:
	 - ft_min_word_len=1
	 - ft_stopword_file=

   - After that is done, please run "repair table posts;"
   and also "repair table forum_topics; repair table forum_posts;" in the MySQL command prompt.
   You can also run a repair using phpmyadmin on all the tables.
   Please read here for more information: http://dev.mysql.com/doc/refman/5.1/en/fulltext-fine-tuning.html

   >> DELETE THE UPGRADES AND INSTALL DIRECTORY AFTER YOU ARE DONE <<

#Php.ini Configuration#

  These are the recommended minimum values for some directives:
  - memory_limit = 512M
  - post_max_size = 128M
  - upload_max_filesize = 128M
  Search for php_fileinfo.dll and remove the semicolon to activate it.
  Search for date.timezone and make sure it has a correct value.

??? Seeing Errors ???

  Search for error_reporting in your php.ini. Set it to this:
  - error_reporting = E_ALL & ~(E_NOTICE | E_USER_NOTICE) ; display all errors and warnings
  - display_errors = off (Not recommended, unless you have a error_log set)


Modified 20110728 1210 +0700 by Raki Saionji