<?php
$query = "CREATE TABLE IF NOT EXISTS `$alias_table`
(
	`tag` VARCHAR(255) DEFAULT NULL,
	`alias` VARCHAR(255) DEFAULT NULL,
	`creator` VARCHAR(255) DEFAULT NULL,
	`reason` TEXT DEFAULT NULL,
	`submitted_date` INT(11) DEFAULT NULL,
	`approver` VARCHAR(255) DEFAULT NULL,
	`approved_date` INT(11) DEFAULT NULL,
	`status` TINYINT(1) DEFAULT '0',
	KEY `status` (`status`),
	KEY `alias` (`alias`),
	KEY `tag` (`tag`)
)
	ENGINE=InnoDB
	DEFAULT CHARSET=utf8
	ROW_FORMAT=DYNAMIC
";
$db->query($query);

$query = "CREATE TABLE IF NOT EXISTS `$comment_vote_table` (
  `rated` VARCHAR(4) NOT NULL,
  `ip` VARCHAR(20) DEFAULT NULL,
  `post_id` INT(11) UNSIGNED DEFAULT NULL,
  `comment_id` INT(11) UNSIGNED DEFAULT NULL,
  `user_id` INT(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC
";
$db->query($query);

$query =
"CREATE TABLE IF NOT EXISTS `$comment_table`
(
	`id` INT(11) UNSIGNED NOT NULL auto_increment,
	`post_id` INT(11) UNSIGNED NOT NULL,
	`comment` TEXT,
	`user` VARCHAR(255) DEFAULT NULL,
	`posted_at` INT(11) DEFAULT NULL,
	`edited_at` INT(11) DEFAULT '0',
	`score` INT(11) DEFAULT '0',
	`ip` VARCHAR(20) DEFAULT NULL,
	`approved` TINYINT(1) DEFAULT '0',
	`approver` VARCHAR(255) DEFAULT NULL,
	`approved_date` INT(11) DEFAULT NULL,
	`aip` VARCHAR(20) DEFAULT NULL,
	`spam` TINYINT(1) DEFAULT '0',
	`reporter` VARCHAR(255) DEFAULT NULL,
	`reported_date` INT(11) DEFAULT NULL,
	`rip` VARCHAR(20) DEFAULT NULL,
	`reason` VARCHAR(255) DEFAULT NULL,
	PRIMARY KEY  (`id`),
	KEY `post_id` (`post_id`),
	KEY `posted_at` (`posted_at`)
)
	ENGINE=InnoDB
	DEFAULT	CHARSET=utf8
	ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1
";
$db->query($query);

$query = "CREATE TABLE IF NOT EXISTS `$deleted_image_table` (
  `hash` TEXT
) ENGINE=InnoDB DEFAULT CHARSET=utf8
";
$db->query($query);

$query = "CREATE TABLE IF NOT EXISTS `$favorites_table` (
  `user_id` INT(11) UNSIGNED NOT NULL,
  `user_name` VARCHAR(255) DEFAULT NULL,
  `favorite` INT(11) UNSIGNED DEFAULT NULL,
  `added` INT(11) UNSIGNED DEFAULT NULL,  
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC
";
$db->query($query);

$query = "CREATE TABLE IF NOT EXISTS `$folder_index_table` (
  `id` INT(11) UNSIGNED NOT NULL auto_increment,
  `name` TEXT,
  `count` INT(11) UNSIGNED DEFAULT '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1
";
$db->query($query);

$query = "CREATE TABLE IF NOT EXISTS `$forum_post_table` (
	`id` INT(11) UNSIGNED NOT NULL auto_increment,
	`topic_id` INT(11) UNSIGNED NOT NULL,
	`title` TEXT,
	`post` TEXT NOT NULL,
	`spam` TINYINT(1) DEFAULT '0',
	`author` VARCHAR(255) DEFAULT NULL,
	`creation_date` INT(11) DEFAULT NULL,
	`ip` VARCHAR(20) DEFAULT NULL,
	`updator` VARCHAR(255) DEFAULT NULL,
	`updated_date` INT(11) DEFAULT NULL,
	`uip` VARCHAR(20) DEFAULT NULL,
	`reporter` VARCHAR(255) DEFAULT NULL,
	`reported_date` INT(11) DEFAULT NULL,
	`rip` VARCHAR(20) DEFAULT NULL,
	PRIMARY KEY  (`id`),
	FULLTEXT KEY `post` (`post`)
)
	ENGINE=MyISAM
	DEFAULT
	CHARSET=utf8
	AUTO_INCREMENT=1
";
$db->query($query);

$query = "CREATE TABLE IF NOT EXISTS `$forum_topic_table` (
  `id` INT(11) UNSIGNED NOT NULL auto_increment,
  `topic` TEXT,
  `author` VARCHAR(255) DEFAULT NULL,
  `last_updated` INT(11) DEFAULT NULL,
  `updated_by` VARCHAR(255) DEFAULT NULL,
  `creation_post` INT(11) UNSIGNED NOT NULL,
  `priority` INT(11) UNSIGNED DEFAULT '0',
  `locked` TINYINT(1) DEFAULT '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1
";
$db->query($query);

$query = "CREATE TABLE IF NOT EXISTS `$group_table` (
	`id` INT(11) UNSIGNED NOT NULL auto_increment,
	`group_name` TEXT,
	`default_group` TINYINT(1) DEFAULT '0',
	`is_admin` TINYINT(1) DEFAULT '0',
	`admin_panel` TINYINT(1) DEFAULT '0',
	`can_upload` TINYINT(1) DEFAULT '0',  
	`approve_posts` TINYINT(1) DEFAULT '0',
	`approve_comments` TINYINT(1) DEFAULT '0',
	`edit_posts` TINYINT(1) DEFAULT '0',
	`edit_tags` TINYINT(1) DEFAULT '0',
	`delete_posts` TINYINT(1) DEFAULT '0',
	`delete_comments` TINYINT(1) DEFAULT '0',
	`add_aliases` TINYINT(1) DEFAULT '0',
	`alter_notes` TINYINT(1) DEFAULT '0',
	`reverse_notes` TINYINT(1) DEFAULT '0',
	`reverse_tags` TINYINT(1) DEFAULT '0',
	`new_forum_topics` TINYINT(1) DEFAULT '0',
	`new_forum_posts` TINYINT(1) DEFAULT '0',
	`edit_forum_posts` TINYINT(1) DEFAULT '0',
	`delete_forum_topics` TINYINT(1) DEFAULT '0',
	`delete_forum_posts` TINYINT(1) DEFAULT '0',
	`pin_forum_topics` TINYINT(1) DEFAULT '0',
	`lock_forum_topics` TINYINT(1) DEFAULT '0',
	PRIMARY KEY  (`id`)
)
	ENGINE=InnoDB
	DEFAULT
	CHARSET=utf8
	ROW_FORMAT=DYNAMIC
	AUTO_INCREMENT=3
";
$db->query($query);
$query = "SELECT * FROM $group_table";
$result = $db->query($query);
if($result->num_rows == "0")
{
$query = "INSERT INTO `$group_table` (`id`, `group_name`, `delete_posts`, `delete_comments`, `admin_panel`, `reverse_notes`, `reverse_tags`, `default_group`, `is_admin`, `delete_forum_posts`, `delete_forum_topics`, `lock_forum_topics`, `edit_forum_posts`, `pin_forum_topics`, `alter_notes`, `can_upload`, `add_aliases`, `approve_posts`, `approve_comments`, `new_forum_topics`, `edit_posts`, `edit_tags`, `new_forum_posts`) VALUES 
(1, 'Administrator'	, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(2, 'Moderator'		, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(3, 'Contributor'	, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1),
(4, 'Observator'	, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1),
(5, 'Privileged'	, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1),
(9, 'Regular Member'	, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1),
(10, 'Limited'		, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
";
$db->query($query);
}

$query = "CREATE TABLE IF NOT EXISTS `$hit_counter_table` (
  `count` INT(11) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC
";
$db->query($query);

$query = "CREATE TABLE IF NOT EXISTS `$note_table` (
  `id` INT(11) UNSIGNED DEFAULT '0',
  `post_id` INT(11) UNSIGNED NOT NULL,
  `x` INT(11) DEFAULT NULL,
  `y` INT(11) DEFAULT NULL,
  `width` INT(11) UNSIGNED DEFAULT NULL,
  `height` INT(11) UNSIGNED DEFAULT NULL,
  `body` TEXT,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `ip` VARCHAR(20) DEFAULT NULL,
  `version` INT(11) UNSIGNED DEFAULT '1',
  `user_id` INT(11) UNSIGNED DEFAULT NULL,
  KEY `post_id` (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC
";
$db->query($query);

$query = "CREATE TABLE IF NOT EXISTS `$note_history_table` (
  `id` INT(11) UNSIGNED NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `x` INT(11) DEFAULT NULL,
  `y` INT(11) DEFAULT NULL,
  `width` INT(11) UNSIGNED DEFAULT NULL,
  `height` INT(11) UNSIGNED DEFAULT NULL,
  `body` TEXT,
  `version` INT(11) UNSIGNED DEFAULT NULL,
  `ip` VARCHAR(20) DEFAULT NULL,
  `post_id` INT(11) UNSIGNED NOT NULL,
  `user_id` INT(11) UNSIGNED DEFAULT NULL,
  KEY `post_id` (`post_id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC
";
$db->query($query);

$query = "CREATE TABLE IF NOT EXISTS `$parent_child_table` (
  `id` INT(11) NOT NULL auto_increment,
  `parent` INT(11) NOT NULL,
  `child` INT(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `parent` (`parent`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1
";
$db->query($query);

$query = "CREATE TABLE IF NOT EXISTS `$post_count_table` (
  `access_key` VARCHAR(255) DEFAULT NULL,
  `pcount` INT(11) UNSIGNED DEFAULT '0',
  `last_update` VARCHAR(255) DEFAULT NULL,
  KEY `access_key` (`access_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC
";
$db->query($query);

$query = "CREATE TABLE IF NOT EXISTS `$post_vote_table` (
  `rated` VARCHAR(4) NOT NULL,
  `ip` VARCHAR(20) DEFAULT NULL,
  `post_id` INT(11) UNSIGNED DEFAULT NULL,
  `user_id` INT(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC
";
$db->query($query);

$query =
"CREATE TABLE IF NOT EXISTS `$post_table` (
	`id` INT(11) UNSIGNED NOT NULL auto_increment,
	`directory` VARCHAR(11) DEFAULT NULL,
	`image` VARCHAR(255) DEFAULT NULL,
	`ext` VARCHAR(5) DEFAULT NULL,
	`active_date` INT(11) NOT NULL DEFAULT '0',
	`hash` VARCHAR(32) DEFAULT NULL,
	`sha1` VARCHAR(40) DEFAULT NULL,
	`crc32` VARCHAR(8) DEFAULT NULL,
	`width` INT(11) UNSIGNED DEFAULT '0',
	`height` INT(11) UNSIGNED DEFAULT '0',
	`size` INT(11) UNSIGNED DEFAULT '0',
	`title` VARCHAR(255) DEFAULT NULL,
	`source` VARCHAR(255) DEFAULT NULL,
	`parent` INT(11) NOT NULL DEFAULT '0',
	`rating` VARCHAR(20) DEFAULT NULL,
	`score` INT(11) DEFAULT '0',
	`owner` VARCHAR(255) DEFAULT NULL,
	`creation_date` INT(11) DEFAULT NULL,
	`ip` VARCHAR(20) DEFAULT NULL,
	`approved` TINYINT(1) DEFAULT '0',
	`approver` VARCHAR(255) DEFAULT NULL,
	`approved_date` INT(11) DEFAULT NULL,
	`aip` VARCHAR(20) DEFAULT NULL,
	`spam` TINYINT(1) DEFAULT '0',
	`reporter` VARCHAR(255) DEFAULT NULL,
	`reported_date` INT(11) DEFAULT NULL,
	`rip` VARCHAR(20) DEFAULT NULL,
	`reason` VARCHAR(255) DEFAULT NULL,
	`hit_count` INT(11) UNSIGNED DEFAULT '0',
	`tags_version` INT(11) UNSIGNED DEFAULT '1',
	`tags` TEXT NOT NULL,
	`recent_tags` TEXT,
	`last_comment` INT(11) DEFAULT NULL,
	PRIMARY KEY  (`id`),
	KEY `creation_date` (`creation_date`),
	KEY `parent` (`parent`),
	FULLTEXT KEY `tags` (`tags`)
)
	ENGINE=MyISAM
	DEFAULT
	CHARSET=utf8
	ROW_FORMAT=DYNAMIC
	AUTO_INCREMENT=1
";
$db->query($query);

$query = "CREATE TABLE IF NOT EXISTS `$tag_history_table` (
  `total_amount` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id` INT(11) UNSIGNED NOT NULL,
  `tags` TEXT,
  `active` TINYINT(1) DEFAULT 1, 
  `version` INT(11) UNSIGNED DEFAULT 1,
  `user_id` INT(11) UNSIGNED DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `ip` VARCHAR(20) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `version` (`version`),
  PRIMARY KEY (`total_amount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC
";
$db->query($query);

$query = "CREATE TABLE IF NOT EXISTS `$tag_index_table` (
  `tag` VARCHAR(255) NOT NULL,
  `index_count` INT(11) UNSIGNED DEFAULT 0,
  `version` INT(11) UNSIGNED DEFAULT 0,
  `type` VARCHAR(255) NOT NULL,
  `ambiguous` TINYINT(1) DEFAULT 0, 
  KEY `tag` (`tag`),
  KEY `index_count` (`index_count`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC
";
$db->query($query);

$query = "CREATE TABLE IF NOT EXISTS `$user_table` (
	`id` INT(11) UNSIGNED NOT NULL auto_increment,
	`ugroup` INT(11) UNSIGNED DEFAULT NULL,
	`user` VARCHAR(255) DEFAULT NULL,
	`pass` VARCHAR(255) DEFAULT NULL,
	`email` VARCHAR(255) DEFAULT NULL,
	`ip` VARCHAR(20) DEFAULT NULL,
	`signup_date` INT(11) DEFAULT NULL,
	`login_date` INT(11) DEFAULT NULL,
	`login_ip` VARCHAR(20) DEFAULT NULL,
	`post_count` INTEGER(11) UNSIGNED NOT NULL DEFAULT 0,
	`deleted_count` INTEGER(11) UNSIGNED NOT NULL DEFAULT 0,
	`favorites_count` INTEGER(11) UNSIGNED NOT NULL DEFAULT 0,  
	`comment_count` INTEGER(11) UNSIGNED NOT NULL DEFAULT 0,
	`tag_edit_count` INTEGER(11) UNSIGNED NOT NULL DEFAULT 0,
	`note_edit_count` INTEGER(11) UNSIGNED NOT NULL DEFAULT 0,
	`record_score` INTEGER(11) UNSIGNED NOT NULL DEFAULT 0,
	`forum_post_count` INTEGER(11) UNSIGNED NOT NULL DEFAULT 0,  
	`my_tags` TEXT DEFAULT NULL,
	`mail_reset_code` TEXT NULL,
	`about` TEXT,  
	PRIMARY KEY  (`id`)
)
	ENGINE=InnoDB
	DEFAULT CHARSET=utf8
	ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1
";
$db->query($query);
$query = "CREATE TABLE IF NOT EXISTS `$banned_ip_table` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `ip` VARCHAR(20) DEFAULT NULL,
  `user` TEXT,
  `reason` TEXT,
  `date_added` INT(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ip` (`ip`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8";
$db->query($query);
$query = "CREATE FUNCTION notes_next_id(post INT)
	RETURNS INTEGER
	NOT DETERMINISTIC
	BEGIN
	DECLARE iv1 INTEGER;
	DECLARE iv2 INTEGER;
	SELECT id INTO iv1 FROM $note_table WHERE post_id=post ORDER BY id DESC LIMIT 1;
	SET iv2 = (iv1+1);
	RETURN iv2;
	END";

$db->query($query);
$query = "SELECT * FROM $post_count_table WHERE access_key='posts'";
$result = $db->query($query);
if($result->num_rows =="0")
{
	$query = "INSERT INTO $post_count_table(access_key, last_update) VALUES('posts','20070101')";
	$db->query($query);
}
$query = "SELECT * FROM $post_count_table WHERE access_key='comment_count'";
$result = $db->query($query);
if($result->num_rows =="0")
{
	$query = "INSERT INTO $post_count_table(access_key, pcount) VALUES('comment_count','0')";
	$db->query($query);
}
$query = "SELECT * FROM $hit_counter_table";
$result = $db->query($query);
if($result->num_rows =="0")
{
	$query = "INSERT INTO $hit_counter_table(count) VALUES('0')";
	$db->query($query);
}
echo 'class="good">Successful';
?>