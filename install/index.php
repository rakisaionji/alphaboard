<?php
	require "../config.php";
	require "../functions.global.php";
	function formatBytes($b,$p = null)
	{
		$units = array("byte(s)","KB","MB","GB","TB","PB","EB","ZB","YB");
		$c=0;
		if(!$p && $p !== 0)
		{
			foreach($units as $k => $u)
			{
				if(($b / pow(1024,$k)) >= 1)
				{
					$r["bytes"] = $b / pow(1024,$k);
 					$r["units"] = $u;
					$c++;
				}
			}
			return number_format($r["bytes"],2) . " " . $r["units"];
		}
		else
		{
			return number_format($b / pow(1024,$p)) . " " . $units[$p];
		}
	}
	function returnBytes($val)
	{
		$val = trim($val);
		$last = strtolower($val[strlen($val)-1]);
		switch($last)
		{
			case 't': $val *= 1024;
			case 'g': $val *= 1024;
			case 'm': $val *= 1024;
			case 'k': $val *= 1024;
		}
		return $val;
	}
	$footer = '
	<div style="margin-left:auto;margin-right:auto;width:550px;text-align:center">
	<br><br><br>Running <a href="'.$site_url.'">AlphaBoard</a> 1.0.6<br><br>
	Copyright (C) 2011 Raki Saionji<br>All rights reserved
	</div>
</body>
</html>';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head><?php
echo '
		<title>'.$site_url3.' - AlphaBoard Installer</title>

		<link rel="shortcut icon" href="'.$site_url.'favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" media="screen" href="'.$site_url.'default.css" title="default">

		<meta http-equiv="content-type" content="text/html; charset=utf-8">
		<meta name="author" content="Racozsoft Corporation">

		<script src="'.$site_url.'script/core.js" type="text/javascript"></script>

		<style type="text/css">
		.center_box{width:550px;margin-left:auto;margin-right:auto;margin-bottom:10px}
		.good{color:#0f0}.okay{color:orange}.bad{color:red}
		table{margin-bottom:2em} table.form{width:70em}
		table.form p{font-size:.8em;font-weight:normal;margin:0;padding:0} table.form td {vertical-align:middle}
		table.form th{height:24px;vertical-align:middle;color:white;white-space:normal;padding-right:1em;text-align:right;background:#333}
		</style>
	</head>
	<body>
	<div id="static-index">
		<div style="margin-left:auto;margin-right:auto;width:550px;text-align:center">
		<h1 style="font-size:52px;margin-top:1em"><a href="'.$site_url.'">'.$site_url3.'</a></h1>
		<h3 style="margin-bottom:1em">First-time Installation Wizard</h3>
		<noscript>
			<center>
				<div class="error-notice" style="width:500px;margin-bottom:2em">
					JavaScript must be enabled to use this site.<br>
					Please enable JavaScript in your browser and refresh the page.
				</div>
			</center>
		</noscript>
		</div>';
	if(!function_exists('hash')) exit;
	if(!isset($_POST['settings']) || $_POST['settings'] == 0 || $_POST['settings'] == "")
	{
	if (function_exists('finfo_open'))
	{
		$finfo =  "Enabled";
		$finfo_class = "good";
		$finfo_notice = '';
	}
	else
	{
		$finfo =  "Not enabled";
		$finfo_class = "bad";
		$finfo_notice = '
		<p class="center_box" style="margin-top:-15px;font-weight:700;">
		<a href="#">Please enable php_fileinfo.dll library in php.ini and restart your server.</a></p>';
	}
?>

		<div id="installer" style="display:none">
		<h3 style="margin-bottom:2em">Step 1 of 2: Gathering necessary information</h3>
		<div class="center_box"><h5>PHP.ini Directives</h5></div>
		<table class="form" style="margin-left:auto;margin-right:auto;width:550px;text-align:center">
		<tr>
			<th width="40%" style="text-align:center;background-color:#555">Name</th>
			<th width="30%" style="text-align:center;background-color:#555">Current</th>
			<th width="30%" style="text-align:center;background-color:#555">Recommended</th>
		</tr>
		<tr>
			<th>memory_limit</th>
			<td class="<?php echo (returnBytes(ini_get('memory_limit')) >= returnBytes("512M")) ? 'good' : 'bad' ?>"><?php echo ini_get('memory_limit') ?></td>
			<td>512M</td>
		</tr>
		<tr>
			<th>post_max_size</th>
			<td class="<?php echo (returnBytes(ini_get('post_max_size')) >= returnBytes("128M")) ? 'good' : 'bad' ?>"><?php echo ini_get('post_max_size') ?></td>
			<td>128M</td>
		</tr>
		<tr>
			<th>upload_max_filesize</th>
			<td class="<?php echo (returnBytes(ini_get('upload_max_filesize')) >= returnBytes("128M")) ? 'good' : 'bad' ?>"><?php echo ini_get('upload_max_filesize') ?></td>
			<td>128M</td>
		</tr>
		<tr>
			<th>php_fileinfo.dll</th>
			<td class="<?php echo $finfo_class ?>" id="finfo_info"><?php echo $finfo ?></td>
			<td>Enabled</td>
		</tr>
		</table><?php echo $finfo_notice ?>

		<br>
		<br>
		<div class="center_box"><h5>Administrator Account</h5></div>
		<form method="post" action="index.php" name="install_form">
		<table class="form" style="margin-left:auto;margin-right:auto;width:550px;text-align:center">
		<tr>
			<th width="40%">Username</th>
			<td width="60%"><input type="text" name="user" id="name" style="width:100%;border:0;background:none" value=""></td>
		</tr>
		<tr>
			<th>Password</th>
			<td><input type="text" name="pass" id="pass" style="width:100%;border:0;background:none" value=""></td>
		</tr>
		<tr>
			<th>Email</th>
			<td><input type="text" name="email" id="email" style="width:100%;border:0;background:none" value=""></td>
		</tr>
		</table>
		<br>
		<br>
		<input type="hidden" name="settings" id="settings" value="0">
		<input type="submit" name="submit" style="font-size:18px;font-weight:bold;text-align:center;width:550px;height:32px;border:0;background:none" value="Install AlphaBoard Now!" onclick="install();return false;">
		</form>
		</div>
	</div>
	<script type="text/javascript">
		$("installer").show();

		var finfo = $("finfo_info").innerHTML;

		function is_valid_email(email)
		{
			var atpos=email.indexOf("@");
			var dotpos=email.lastIndexOf(".");
			if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)
				return false;
			else	return true;
		}

		function install()
		{
			if ( finfo != "Enabled" )
			{
				alert("FileInfo library must be enabled!");
				return false;
			}
			var name = $("name").value;
			var pw = $("pass").value;
			var email = $("email").value;
			if ( name == "" )
			{
				alert("Please enter your username!");
				$("name").focus();
				return false;
			}
			else if ( name.length < 5 )
			{
				alert("Username must be at least 5 characters long!");
				$("name").focus();
				return false;
			}
			else if ( pw == "" )
			{
				alert("Please enter your password!");
				$("pass").focus();
				return false;
			}
			else if ( pw.length < 5 )
			{
				alert("Password must be at least 5 characters long!");
				$("pass").focus();
				return false;
			}
			else if ( email != "" && !is_valid_email(email) )
			{
				alert("Please enter a valid e-mail address!");
				$("email").focus();
				return false;
			}
			else 
			{
				$("settings").value = 1;
				document.install_form.submit();
			}
		}
	</script><?php
		echo $footer; 
		exit;
	}
	else if (isset($_POST['settings']) && $_POST['settings'] == 1 && isset($_POST['user']) && isset($_POST['pass']) && $_POST['user'] != "" && $_POST['pass'] != "" && strlen($_POST['user']) >= 5 && strlen($_POST['pass']) >= 5 && ctype_alnum($_POST['user']))
	{
		echo '
		<h3 style="margin-bottom:2em">Step 2 of 2: Installing AlphaBoard on your server</h3>';
?>

		<div class="center_box"><h5>Installation Tasks</h5></div>
		<table class="form" style="margin-left:auto;margin-right:auto;width:550px;text-align:center">
		<tr>
			<th width="70%" style="text-align:center;background-color:#555">Task</th>
			<th width="30%" style="text-align:center;background-color:#555">Status</th>
		</tr>
		<tr>
			<th>Connecting to MySQL Database</th>
			<td <?php
			$db = new mysqli($mysql_host,$mysql_user,$mysql_pass,$mysql_db);
			if (!mysqli_connect_errno())
			echo 'class="good">Successful';
			else
			echo 'class="bad">Error'; ?></td>
		</tr>
		<tr>
			<th>Setup MySQL Connection Collation</th>
			<td <?php
			if ($db->set_charset('utf8'))
			echo 'class="good">Successful';
			else
			echo 'class="bad">Error'; ?></td>
		</tr>
		<tr>
			<th>Setup New Tables and Functions</th>
			<td <?php require "mysql.php" ?></td>
		</tr>
		<tr>
			<th>Setup the Administrator Account</th>
			<td <?php

			require "../classes/user.class.php";
			$class_user = new user();

			$user = $db->real_escape_string($_POST['user']);
			$pass = $db->real_escape_string($_POST['pass']);
			$email = $db->real_escape_string($_POST['email']);
			$ip = $db->real_escape_string($_SERVER['REMOTE_ADDR']);	

			$pass = $class_user->hashpass($pass);
			$domain = explode("@",$email);
			if ($domain[1] == "" || $domain[1] == "asdf.com") $email = "";

			$query = "SELECT * FROM $user_table";
			$result = $db->query($query);

			if ($result->num_rows == "0")
			{
			$query = "INSERT INTO $user_table(user,pass,ugroup,email,signup_date,ip) VALUES('$user','$pass','1','$email','".mktime()."','$ip')";
			if($db->query($query))
			echo 'class="good">Successful';
			else
			echo 'class="bad">Error';
			}
			else
			echo 'class="bad">Error'; ?></td>
		</tr>
		<tr>
			<th>Log in Procedure for <?php echo ucfirst($site_url3) ?></th>
			<td <?php
			if ($class_user->login($user,$db->real_escape_string($_POST['pass'])))
			echo 'class="good">Successful';
			else
			echo 'class="bad">Error'; ?></td>
		</tr>
		<tr>
			<th>Closing Current MySQL Connection</th>
			<td <?php
			if ($db->close())
			echo 'class="good">Successful';
			else
			echo 'class="bad">Error'; ?></td>
		</tr>
		</table>
		<div class="center_box"><h5>Installation Report</h5></div>
		<table class="form" style="margin-left:auto;margin-right:auto;width:550px;text-align:center">
		<tr>
			<th width="70%" style="text-align:center;background-color:#555">Title</th>
			<th width="30%" style="text-align:center;background-color:#555">Data</th>
		</tr>
		<tr>
			<th>Finished Time (Server Time)</th>
			<td><?php echo date('G:i:s T') ?></td>
		</tr>
		<tr>
			<th>Total Memory Used for the Installer</th>
			<td><?php echo formatBytes(memory_get_peak_usage(true)) ?></td>
		</tr>
		<tr>
			<th>Total Associated Memory for the PHP Server</th>
			<td><?php echo formatBytes(returnBytes(ini_get('memory_limit'))) ?></td>
		</tr>
		</table>
		<div style="margin-left:auto;margin-right:auto;font-size:14px;font-weight:bold;text-align:center;width:550px;border:0;background:none">
			<a href="<?php print $site_url; ?>index.php">Go to the Main Page</a> or <a href="<?php print $site_url; ?>index.php?page=post&amp;s=add">Start Posting Now!</a>
		</div>
	</div><?php
		echo $footer; 
		exit;
	}
	else
		echo '
		<h3 style="margin-bottom:2em">Necessary information is not gathered successfully.<br>Please try again later.</h3>';
?>