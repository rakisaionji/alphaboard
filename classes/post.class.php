<?php
	class post
	{
		function show($id)
		{
			global $db, $post_table;
			$id = $db->real_escape_string($id);
			$query = "SELECT * FROM $post_table WHERE id = '$id' LIMIT 1";
			$result = $db->query($query);
			if($result->num_rows == "0")
				return false;
			$row = $result->fetch_assoc();
			return $row;
		}
		
		function get_notes($id)
		{
			global $db, $note_table;
			$id = $db->real_escape_string($id);
			$query = "SELECT * FROM $note_table WHERE post_id='$id'";
			$result = $db->query($query);
			return $result;
		}
		
		function prev_next($id)
		{
			global $db, $post_table;
			$query = "SELECT SQL_NO_CACHE id FROM $post_table WHERE id < $id ORDER BY id DESC LIMIT 1";
			$result = $db->query($query);
			$row = $result->fetch_assoc();
			$prev_next[] = $row['id'];
			$query = "SELECT SQL_NO_CACHE id FROM $post_table WHERE id > $id ORDER BY id ASC LIMIT 1";
			$result = $db->query($query);
			$row = $result->fetch_assoc();
			$prev_next[] = $row['id'];
			return $prev_next;
		}
		
		function has_children($id)
		{
			global $db, $parent_child_table;
			$query = "SELECT * FROM $parent_child_table WHERE parent = '$id' LIMIT 1";
			$result = $db->query($query);
			if($result->num_rows == "0")
				return false;
			else
				return true;
		}
		
		function has_parent($id)
		{
			global $db, $post_table;
			$id = $db->real_escape_string($id);
			$query = "SELECT parent FROM $post_table WHERE id = '$id' LIMIT 1";
			$result = $db->query($query);
			$row = $result->fetch_assoc();
			if(!isset($row['parent']) || $row['parent'] == "" || $row['parent'] == " " || $row['parent'] == "0" || $row['parent'] == 0)
				return false;
			else
				return true;
		}

		function is_spam($id)
		{
			global $db, $post_table;
			$id = $db->real_escape_string($id);
			$query = "SELECT spam FROM $post_table WHERE id = '$id' LIMIT 1";
			$result = $db->query($query);
			$row = $result->fetch_assoc();
			return $row['spam'];
		}

		function is_approved($id)
		{
			global $db, $post_table;
			$id = $db->real_escape_string($id);
			$query = "SELECT approved FROM $post_table WHERE id = '$id' LIMIT 1";
			$result = $db->query($query);
			$row = $result->fetch_assoc();
			return $row['approved'];
		}

		function index_count($current)
		{
			global $db, $tag_index_table;
			$current = $db->real_escape_string(htmlentities($current, ENT_QUOTES, "UTF-8"));
			$query = "SELECT index_count FROM $tag_index_table WHERE tag='$current' LIMIT 1";
			$result = $db->query($query);
			$row = $result->fetch_assoc();
			return $row;
		}

		function tag_type($current)
		{
			global $db, $tag_index_table;
			$current = $db->real_escape_string(htmlentities($current, ENT_QUOTES, "UTF-8"));
			$query = "SELECT type FROM $tag_index_table WHERE tag='$current'";
			$result = $db->query($query);
			$row = $result->fetch_assoc();
			return $row['type'];
		}

		function formatBytes($b,$p = null) {
		/**
		*
		* @author Martin Sweeny
		* @version 2010.0617
		*
		* returns formatted number of bytes.
		* two parameters: the bytes and the precision (optional).
		* if no precision is set, function will determine clean
		* result automatically.
		*
		**/
			$units = array("B","KB","MB","GB","TB","PB","EB","ZB","YB");
			$c=0;
			if(!$p && $p !== 0) {foreach($units as $k => $u) {
			if(($b / pow(1024,$k)) >= 1)
			{$r["bytes"] = $b / pow(1024,$k); $r["units"] = $u; $c++;}}
			return number_format($r["bytes"],2) . " " . $r["units"];}
			else {return number_format($b / pow(1024,$p)) . " " . $units[$p];}
		}
	}
?>