<?php
	class search
	{
		function __construct()
		{
		
		}

		function returnBytes($val)
		{
			$val = trim($val);
			$last = strtolower($val[strlen($val)-1]);
			switch($last)
			{
			case 't':
				$val *= 1024;
			case 'g':
				$val *= 1024;
			case 'm':
				$val *= 1024;
			case 'k':
				$val *= 1024;
			}
			return $val;
		}
		
		function getProperRating($rating)
		{
			$rating = strtolower(substr($rating, 0, 1));
			switch($rating)
			{
			case 'e':
				$rating = 'Explicit';
				break;
			case 'q':
				$rating = 'Questionable';
				break;
			case 's':
				$rating = 'Safe';
				break;
			default:
				return;
			}
			return $rating;
		}

		function prepare_tags($search)
		{
			global $db, $post_table;
			$search = html_entity_decode($db->real_escape_string($search), ENT_QUOTES, "UTF-8");
			$operators_list = '(<|<=|=|>=|>)';
			$tags = '';
			$aliased_tags = '';
			$original_tags = '';
			$parent = '';
			$ttags = explode(" ",$search);
			$g_rating = '';
			$g_order = '';
			$g_owner = '';
			$g_tags = '';
			$g_parent = '';
			$g_score = '';
			$g_date = '';	
			$g_crc32 = '';
			$g_sha1 = '';
			$g_md5 = '';
			$g_id = '';
			$g_ip = '';
			$g_ext = '';
			$g_width = '';	
			$g_height = '';		
			$g_status = '';
			$g_approver = '';
			$g_mpixels = '';	
			$g_filesize = '';		
			foreach($ttags as $current)
			{
			$metatag = explode(':', $current);
			if (count($metatag) > 1)
			{
			$meta = $db->real_escape_string($metatag[0]);
			$param = $metatag[1];
			$subparam = isset($metatag[2]) ? $db->real_escape_string($metatag[2]) : null;
			$wildcard = strpos($param,"*");
			switch ($meta)
			{
			/*
			case 'pool':
				if(is_numeric($param))
				{
					$extra_join[] = '`pools_posts` pp ON p.id = pp.post_id';
					$conditions[] = $not." pp.pool_id = '$param' ";
					$g_order = " CAST(pp.sequence AS UNSIGNED) ";
					$group_by = "pp.id";
				}
				unset($current);
				break;
			*/
			case 'rating':
				$rating = $this->getProperRating($param);
				$g_rating .= " AND rating = '$rating' ";
				unset($current);
				break;
			case '-rating':
				$rating = $this->getProperRating($param);
				$g_rating .= " AND rating != '$rating' ";
				unset($current);
				break;
			case 'user':
				$g_owner = str_replace("?","_",str_replace("*","%",$param));
				if($wildcard == false)
					$g_owner = " AND owner = '$g_owner' ";
				else
					$g_owner = " AND owner LIKE '$g_owner' ";
				unset($current);
				break;
			case '-user':
				$g_owner = str_replace("?","_",str_replace("*","%",$param));
				if($wildcard == false)
					$g_owner = " AND owner != '$g_owner' ";
				else
					$g_owner = " AND owner NOT LIKE '$g_owner' ";
				unset($current);
				break;
			case 'source':
				$g_source = str_replace("?","_",str_replace("*","%",$param));
				$g_source = " AND source LIKE '%$g_source%' ";
				unset($current);
				break;
			case 'id':
				if (preg_match('~'.$operators_list.'([0-9]+)~', $param, $op))
				{
					$operator = $op[1];
					$g_id = $op[2];
				}
				else
				{
					$operator = '=';
					if (!is_numeric($param))
					{
						unset($current);
						break;
					}
					$g_id = $param;
				}
				$g_id = " AND id".$operator."'$g_id' ";
				unset($current);
				break;
			case 'ext':
				$g_ext = str_replace(".","",str_replace("?","_",str_replace("*","%",$param)));
				if($wildcard == false)
					$g_ext = " AND ext='.$g_ext' ";
				else
					$g_ext = " AND ext LIKE '.$g_ext' ";
				unset($current);
				break;
			case '-ext':
				$g_ext = str_replace(".","",str_replace("?","_",str_replace("*","%",$param)));
				if($wildcard == false)
					$g_ext = " AND ext != '.$g_ext' ";
				else
					$g_ext = " AND ext NOT LIKE '.$g_ext' ";
				unset($current);
				break;
			case 'md5':
				$g_md5 = str_replace("?","_",str_replace("*","%",$param));
				if($wildcard == false)
					$g_md5 = " AND hash='$g_md5' ";
				else
					$g_md5 = " AND hash LIKE '$g_md5' ";					
				unset($current);
				break;
			case 'crc32':
				$g_crc32 = str_replace("?","_",str_replace("*","%",$param));
				if($wildcard == false)
					$g_crc32 = " AND crc32='$g_crc32' ";
				else
					$g_crc32 = " AND crc32 LIKE '$g_crc32' ";
				unset($current);
				break;
			case 'sha1':
				$g_sha1 = str_replace("?","_",str_replace("*","%",$param));
				if($wildcard == false)
					$g_sha1 = " AND sha1='$g_sha1' ";
				else
					$g_sha1 = " AND sha1 LIKE '$g_sha1' ";					
				unset($current);
				break;
			case 'parent':
				$parent = " AND id='$param' ";
 				if(!is_numeric($param))
					$g_parent = '';					
				else
					$g_parent = " AND parent='$param' ";
				unset($current);
				break;
			case 'date':
				$param = str_replace("\\","",str_replace("-","",$param));
				if($wildcard == false)
				{
				if (preg_match('~'.$operators_list.'([0-9]+)~', $param, $op))
				{
					$operator = $op[1];
					$g_date = $op[2];
				}
				else
				{
					$operator = '=';
					if (!is_numeric($param))
					{
						unset($current);
						break;
					}
					$g_date = $param;
				}
				$g_date = " AND active_date".$operator."'$g_date' ";
				}
				else
				{
				$g_date = str_replace("?","_",str_replace("*","%",$param));
				$g_date = " AND active_date LIKE '$g_date' ";
				}				
				unset($current);
  				break;
			case 'order':
				$search = str_replace($current,"",$search);
				$param = str_replace('_asc','',$param);
				switch ($param)
				{
				case 'id':
					$g_order = 'id ASC';
					break;
				case 'id_desc':
					$g_order = 'id DESC';
					break;
				case 'score':
					$g_order = 'score ASC';
					break;
				case 'score_desc':
					$g_order = 'score DESC';
					break;
				case 'rating':
					$g_order = 'rating ASC';
					break;
				case 'rating_desc':
					$g_order = 'rating DESC';
					break;
				case 'user':
					$g_order = 'owner ASC';
					break;
				case 'user_desc':
					$g_order = 'owner DESC';
					break;
				case 'width':
					$g_order = 'width ASC';
					break;
				case 'width_desc':
					$g_order = 'width DESC';
					break;
				case 'height':
					$g_order = 'height ASC';
					break;
				case 'height_desc':
					$g_order = 'height DESC';
					break;
				case 'parent':
					$g_order = 'parent ASC';
					break;
				case 'parent_desc':
					$g_order = 'parent DESC';
					break;
				case 'source':
					$g_order = 'source ASC';
					break;
				case 'source_desc':
					$g_order = 'source DESC';
					break;
				case 'filesize':
					$g_order = 'size ASC';
					break;
				case 'filesize_desc':
					$g_order = 'size DESC';
					break;
				case 'mpixels':
					$g_order = "(width * height) ASC";
					break;
				case 'mpixels_desc':
					$g_order = "(width * height) DESC";
					break;
				case 'landscape':
					$g_order = "(width < height) ASC";
					break;
				case 'portrait':
					$g_order = "(width > height) ASC";
  					break;
				case 'hits':
					$g_order = 'hit_count ASC';
					break;
				case 'hits_desc':
					$g_order = 'hit_count DESC';
					break;
				case 'ip':
					$g_order = 'ip ASC';
					break;
				case 'ip_desc':
					$g_order = 'ip DESC';
					break;
				default:
					$g_order = 'id DESC';
					break;
				}
				unset($current);
				break;
			case 'width':
				if(preg_match('~'.$operators_list.'([0-9]+)~', $param, $op))
				{
					$operator = $op[1];
					$g_width = $op[2];
				}
				else
				{
					$operator = '=';
					if (!is_numeric($param))
					{
						unset($current);
						break;
					}
					$g_width = $param;
				}
				$g_width = " AND width".$operator."'$g_width' ";
				unset($current);
				break;
			case 'height':
				if (preg_match('~'.$operators_list.'([0-9]+)~', $param, $op))
				{
					$operator = $op[1];
					$g_height = $op[2];
				}
				else
				{
					$operator = '=';
					if (!is_numeric($param))
					{
						unset($current);
						break;
					}
					$g_height = $param;
				}
				$g_height = " AND height".$operator."'$g_height' ";
				unset($current);
				break;
			case 'score':
				if (preg_match('~'.$operators_list.'([0-9]+)~', $param, $op))
				{
					$operator = $op[1];
					$g_score = $op[2];
				}
				else
				{
					$operator = '=';
					if (!is_numeric($param))
					{
						unset($current);
						break;
					}
					$g_score = $param;
				}
				$g_score = " AND score".$operator."'$g_score' ";
				$current = '';
				unset($current);
				break;
			case 'ip':
				$g_ip = str_replace("?","_",str_replace("*","%",str_replace("\\","",$param)));
				if($wildcard == false)
					$g_ip = " AND ip='$g_ip' ";
				else
					$g_ip = " AND ip LIKE '$g_ip' ";	
				unset($current);
				break;
			case 'mpixels':
				if(preg_match('~'.$operators_list.'([.0-9]+)~', $param, $op))
				{
					$operator = $op[1];
					$g_mpixels = $op[2];
				}
				else
				{
					$operator = '=';
					if (!is_numeric($param))
					{
						unset($current);
						break;
					}
					$g_mpixels = $param;
				}
				$g_mpixels = " AND ((width * height)/1000000)".$operator."'$g_mpixels' ";
				unset($current);
				break;
			case 'filesize':
				$param = strtolower($param);
				$param = str_replace("tb", "t", $param);
				$param = str_replace("gb", "g", $param);
				$param = str_replace("mb", "m", $param);
				$param = str_replace("kb", "k", $param);
				$g_filesize = substr($param, 0, strlen($param)-1);
				if (preg_match('~'.$operators_list.'([.0-9]+)~', $g_filesize, $op))
				{
					$operator = $op[1];
					$g_filesize = substr($param, 1, strlen($param));
				}
				else
				{
					$operator = '=';
					if (!is_numeric($g_filesize))
					{
						unset($current);
						break;
					}
					$g_filesize = $param;
				}
				$g_filesize = $this->returnBytes($g_filesize);
				$g_filesize = " AND size".$operator."'$g_filesize' ";
				$current = '';
				unset($current);
				break;
			case 'approver':
				$g_approver = str_replace("?","_",str_replace("*","%",$param));
				if($wildcard == false)
					$g_approver = " AND approver = '$g_approver' ";
				else
					$g_approver = " AND approver LIKE '$g_approver' ";
				unset($current);
				break;
			case '-approver':
				$g_approver = str_replace("?","_",str_replace("*","%",$param));
				if($wildcard == false)
					$g_approver = " AND approver != '$g_approver' ";
				else
					$g_approver = " AND approver NOT LIKE '$g_approver' ";
				unset($current);
				break;
			case 'status':
				switch ($param)
				{
				case 'active':
					$g_status = " AND approved = 1"; break;
				case 'flagged':
					$g_status = " AND spam = 1"; break;
				case 'pending':
					$g_status = " AND approved = 0"; break;
				case 'deleted':
					$g_status = " AND spam = 2"; break;
				case 'any': default:
					$g_status = " AND active_date != 0"; break;
				}
				unset($current);
				break;
			default:
				break;
			}
			}
			else
			{
			$current = strtolower($metatag[0]);
			}
			$current = $db->real_escape_string(htmlentities($current, ENT_QUOTES, "UTF-8"));
			if($current != "" && $current != " ")
			{
				$len = strlen($current);
				$count = substr_count($current, '*', 0, $len);
				$current = str_replace("?","_",str_replace("*","%",$current));
				if(($len - $count) >= 2)
				{
						$tclass = new tag();
						if(substr($current,0,1) == "-")
						{
							$current = substr($current,1,strlen($current)-1);
							$alias = $tclass->alias($current);
							if($alias !== false)
							{
								$g_tags .= " AND tags NOT LIKE '% $alias %' ";
								$g_tags .= " AND tags NOT LIKE '% $current %' ";
							}
							else
							{
								$g_tags .= " AND tags NOT LIKE '% $current %' ";
							}
						}
						else if(substr($current,0,1) == "~")
						{
							$current = substr($current,1,strlen($current)-1);
							$alias = $tclass->alias($current);
							if($alias !== false)
							{
								$g_tags .= " OR tags LIKE '% $alias %' ";
								$g_tags .= " OR tags LIKE '% $current %' ";
							}
							else
							{
								$g_tags .= " OR tags LIKE '% $current %' ";
							}
						}
						else
						{
							$alias = $tclass->alias($current);
							if($alias !== false)
							{
								$g_tags .= " AND tags LIKE '% $alias %' ";
								$g_tags .= " AND tags LIKE '% $current %' ";
							}
							else
							{
								$g_tags .= " AND tags LIKE '% $current %' ";
							}
						}
					}
				}
			}
			if (!isset($g_order) || $g_order == '') $g_order = "id DESC";
			if($g_tags != "")
			{
				$g_tags = substr($g_tags, 4);
				if($g_parent != "")
					$parent_patch = " OR ($g_tags) $g_parent $g_score $g_date $g_width $g_height $g_id $g_ip $g_ext $g_owner $g_rating $g_md5 $g_sha1 $g_crc32 $g_source $g_status $g_approver $g_mpixels $g_filesize";
				else
					$parent_patch = " AND parent='0' ";
				$query = "SELECT * FROM $post_table WHERE ($g_tags) $g_parent $g_score $g_date $g_width $g_height $g_id $g_ip $g_ext $g_owner $g_rating $g_md5 $g_sha1 $g_crc32 $g_source $g_status $g_approver $g_mpixels $g_filesize $parent_patch ORDER BY $g_order";
			}
			else if($g_parent != "" || $g_width != "" || $g_height != "" || $g_score != ""  || $g_date != "" || $g_id != "" || $g_ip != "" || $g_ext != "" || $g_owner != "" || $g_rating != "" || $g_sha1 != "" || $g_crc32 != "" || $g_md5 != "" || $g_source != "" || $g_status != "" || $g_approver != "" || $g_mpixels != "" || $g_filesize != "")
			{
				if($g_parent != "")
				{
					$g_parent = str_replace('AND',"",$g_parent);
					$parent = substr($parent,4,strlen($parent));
					$parent_patch = " OR $parent $g_score $g_date $g_width $g_height $g_id $g_ip $g_ext $g_owner $g_rating $g_md5 $g_sha1 $g_crc32 $g_source $g_status $g_approver $g_mpixels $g_filesize";
				}				
				else if($g_owner != "")
					$g_owner = str_replace('AND',"",$g_owner);
				else if($g_width != "")
					$g_width = str_replace('AND',"",$g_width);
				else if($g_height != "")
					$g_height = str_replace('AND',"",$g_height);
				else if($g_score != "")
					$g_score = str_replace('AND',"",$g_score);
				else if($g_date != "")
					$g_date = str_replace('AND',"",$g_date);
				else if($g_source != "")
					$g_source = str_replace('AND',"",$g_source);
				else if($g_id != "")
					$g_id = str_replace('AND',"",$g_id);
				else if($g_ip != "")
					$g_ip = str_replace('AND',"",$g_ip);
				else if($g_ext != "")
					$g_ext = str_replace('AND',"",$g_ext);
				else if($g_crc32 != "")
					$g_crc32 = str_replace('AND',"",$g_crc32);
				else if($g_md5 != "")
					$g_md5 = str_replace('AND',"",$g_md5);
				else if($g_sha1 != "")
					$g_sha1 = str_replace('AND',"",$g_sha1);
				else if($g_filesize != "")
					$g_filesize = str_replace('AND',"",$g_filesize);
				else if($g_mpixels != "")
					$g_mpixels = str_replace('AND',"",$g_mpixels);
				else if($g_approver != "")
					$g_approver = str_replace('AND',"",$g_approver);
				else if($g_status != "")
					$g_status = str_replace('AND',"",$g_status);
				else if($g_rating != "")
					$g_rating = substr($g_rating,4,strlen($g_rating));
				if($g_parent == "")
					$parent_patch = " AND parent='0' ";
				$query = "SELECT * FROM $post_table WHERE $g_parent $g_score $g_date $g_width $g_height $g_id $g_ip $g_ext $g_owner $g_rating $g_md5 $g_sha1 $g_crc32 $g_source $g_status $g_approver $g_mpixels $g_filesize $parent_patch ORDER BY $g_order";			
			}
			else
			{
				$count = substr_count($search, '*', 0, strlen($search));
				if(strlen($search)-$count > 0)
				{
					$res = str_replace("*","%",$search);
					$query = "SELECT * FROM $post_table WHERE tags LIKE '% $res %' ORDER BY $g_order";
				}
				else
					$query = "SELECT * FROM $post_table ORDER BY $g_order";
			}			
			return $query;
		}
		
		function prepare_forum_posts($search)
		{
			global $db, $forum_topic_table, $forum_post_table;
			$search = html_entity_decode($db->real_escape_string($search), ENT_QUOTES, "UTF-8");
			$operators_list = '(<|<=|=|>=|>)';
			$ttags = explode(" ",$search);
			$g_id = '';
			$g_ip = '';
			$g_tags = '';
			$g_date = '';
			$g_order = '';
			$g_topic = '';
			$g_author = '';
			foreach($ttags as $current)
			{
			$metatag = explode(':', $current);
			if (count($metatag) > 1)
			{
			$meta = $db->real_escape_string($metatag[0]);
			$param = $metatag[1];
			$wildcard = strpos($param,"*");
			switch ($meta)
			{
			case 'user':
				$g_author = str_replace("?","_",str_replace("*","%",$param));
				if($wildcard == false)
					$g_author = " AND t1.author = '$g_author' ";
				else
					$g_author = " AND t1.author LIKE '$g_author' ";
				unset($current);
				break;
			case '-user':
				$g_author = str_replace("?","_",str_replace("*","%",$param));
				if($wildcard == false)
					$g_author = " AND t1.author != '$g_author' ";
				else
					$g_author = " AND t1.author NOT LIKE '$g_author' ";
				unset($current);
				break;
			case 'id':
				if (preg_match('~'.$operators_list.'([0-9]+)~', $param, $op))
				{
					$operator = $op[1];
					$g_id = $op[2];
				}
				else
				{
					$operator = '=';
					if (!is_numeric($param))
					{
						unset($current);
						break;
					}
					$g_id = $param;
				}
				$g_id = " AND t1.id".$operator."'$g_id' ";
				unset($current);
				break;
			case 'topic':
 				if(!is_numeric($param))
					$g_topic = '';					
				else
					$g_topic = " AND t1.topic_id='$param' ";
				unset($current);
				break;
			case 'date': //Copyright (C) 2010 Raki Saionji - Racozsoft Corporation.
				$stamp = explode("-", $param); //Support: j n Y. Japanese Format: 2000-01-01.
				if (preg_match('~'.$operators_list.'([0-9]+)~', $stamp[0], $op))
				{
					if (count($stamp) <= 3 && strlen($op[2]) == 4 && strlen($stamp[1]) == 2  && strlen($stamp[2]) == 2)
					$date = mktime(0, 0, 0, (int)$stamp[1], (int)$stamp[2], (int)$op[2]);
					else $date = mktime(0, 0, 0);
					$operator = $op[1];
					$g_date = " AND t1.creation_date".$operator."'$date' ";
				}
				else
				{
					if (count($stamp) <= 3 && strlen($stamp[0]) == 4 && strlen($stamp[1]) == 2  && strlen($stamp[2]) == 2)
					$date = mktime(0, 0, 0, (int)$stamp[1], (int)$stamp[2], (int)$stamp[0]); //From midnight {lol}
					else $date = mktime(0, 0, 0); //Returns current date if invalid format entered.
					$g_date = " AND (t1.creation_date > $date AND t1.creation_date < ($date + 24*60*60))";
				}
				unset($date);
				unset($current);
  				break;
			case 'order':
				$search = str_replace($current,"",$search);
				$param = str_replace('_asc','',$param);
				switch ($param)
				{
				case 'id':
					$g_order = 't1.id ASC';
					break;
				case 'id_desc':
					$g_order = 't1.id DESC';
					break;
				case 'title':
					$g_order = 't1.title ASC';
					break;
				case 'title_desc':
					$g_order = 't1.title DESC';
					break;
				case 'user':
					$g_order = 't1.author ASC';
					break;
				case 'user_desc':
					$g_order = 't1.author DESC';
					break;
				case 'ip':
					$g_order = 't1.ip ASC';
					break;
				case 'ip_desc':
					$g_order = 't1.ip DESC';
					break;
				case 'topic':
					$g_order = 't2.topic ASC';
					break;
				case 'topic_desc':
					$g_order = 't2.topic DESC';
					break;
				case 'priority':
					$g_order = 't2.priority ASC';
					break;
				case 'priority_desc':
					$g_order = 't2.priority DESC';
					break;
				default:
					$g_order = 't2.priority DESC';
					break;
				}
				unset($current);
				break;
			case 'ip':
				$g_ip = str_replace("?","_",str_replace("*","%",str_replace("\\","",$param)));
				if($wildcard == false)
					$g_ip = " AND t1.ip = '$g_ip' ";
				else
					$g_ip = " AND t1.ip LIKE '$g_ip' ";	
				unset($current);
				break;
			default:
				break;
			}
			}
			else
			{
			$current = strtolower($metatag[0]);
			}
			$current = $db->real_escape_string(htmlentities($current, ENT_QUOTES, "UTF-8"));
			if($current != "" && $current != " ")
			{
				$len = strlen($current);
				$count = substr_count($current, '*', 0, $len);
				$current = str_replace("?","_",str_replace("*","%",$current));
				if(($len - $count) >= 2)
				{
					if(substr($current,0,1) == "-")
					{
						$current = substr($current,1,strlen($current)-1);
						$g_tags .= " AND (t1.title NOT LIKE '%$current%' OR t1.post NOT LIKE '%$current%') ";
					}
					else if(substr($current,0,1) == "~")
					{
						$current = substr($current,1,strlen($current)-1);
						$g_tags .= " OR (t1.title LIKE '%$current%' OR t1.post LIKE '%$current%') ";
					}
					else
					{
						$g_tags .= " AND (t1.title LIKE '%$current%' OR t1.post LIKE '%$current%') ";
					}
				}
			}
			}
			if (!isset($g_order) || $g_order == '') $g_order = " t2.priority DESC";
			if($g_tags != "")
			{
				$g_tags = substr($g_tags, 4);
				$query = "SELECT t1.topic_id, t1.id, t1.title, t1.post, t1.author, t1.creation_date, t2.topic, t2.priority, t2.locked FROM $forum_post_table AS t1 JOIN $forum_topic_table AS t2 ON t2.id=t1.topic_id WHERE ($g_tags) $g_id $g_ip $g_date $g_topic $g_author ORDER BY $g_order";
			}
			else if($g_id != "" || $g_ip != "" || $g_date != "" || $g_topic != "" || $g_author != "")
			{
				if($g_id != "")
					$g_id = str_replace('AND',"",$g_id);
				else if($g_ip != "")
					$g_ip = str_replace('AND',"",$g_ip);
				else if($g_date != "")
					$g_date = substr($g_date, 4);
				else if($g_topic != "")
					$g_topic = str_replace('AND',"",$g_topic);
				else if($g_author != "")
					$g_author = str_replace('AND',"",$g_author);
				$query = "SELECT t1.topic_id, t1.id, t1.title, t1.post, t1.author, t1.creation_date, t2.topic, t2.priority, t2.locked FROM $forum_post_table AS t1 JOIN $forum_topic_table AS t2 ON t2.id=t1.topic_id WHERE $g_id $g_ip $g_date $g_topic $g_author ORDER BY $g_order";
			}
			else
			{
				$count = substr_count($search, '*', 0, strlen($search));
				if(strlen($search)-$count > 0)
				{
					$res = str_replace("*","%",$search);
					$query = "SELECT t1.topic_id, t1.id, t1.title, t1.post, t1.author, t1.creation_date, t2.topic, t2.priority, t2.locked FROM $forum_post_table AS t1 JOIN $forum_topic_table AS t2 ON t2.id=t1.topic_id WHERE (t1.title LIKE '% $res %' OR t1.post LIKE '%$res%') ORDER BY $g_order";
				}
				else
					$query = "SELECT t1.topic_id, t1.id, t1.title, t1.post, t1.author, t1.creation_date, t2.topic, t2.priority, t2.locked FROM $forum_post_table AS t1 JOIN $forum_topic_table AS t2 ON t2.id=t1.topic_id ORDER BY $g_order";
			}			
			return $query;
		}

		function search_tags_count($search)
		{
			global $post_table;
			$date = date("Ymd");
			$query = "SELECT COUNT(*) FROM $post_table".$search;
			return $query;
		}
		
		function search_tags($search,$condition)
		{
			global $post_table;
			$date = date("Ymd");
			$query = "SELECT * FROM $post_table".$search.$condition;
			return $query;
		}
	}
?>