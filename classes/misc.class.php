<?php
	class misc
	{
		function short_url($text)
		{
			$text = preg_replace('`(([[:alpha:]]+://)[-a-zA-Z0-9@:%_\+.~#?&amp;//=]+)`is', '<a href="$1" target="_blank">$1</a>', $text);
			$text = preg_replace('`([[:space:]()[{}])(www.[-a-zA-Z0-9@:%_\+.~#?&amp;//=]+)`is', '$1<a href="http://$2" target="_blank">$2</a>', $text);
			$text = preg_replace('`([_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,3})`is', '<a href="mailto:$1">$1</a>', $text);
			return $text;
		}

		function linebreaks($text)
		{
			if(strpos($text,"\r\n") !== false)
				return str_replace("\r\n","<br>",$text);
			if(strpos($text,"[br]") !== false)
				return str_replace("[br]","<br>",$text);
			return $text;
		}

		function send_mail($reciver, $subject, $body, $ctype = "plain", $sender = NULL, $etype = "base64")
		{
			require "config.php";
			global $site_email, $site_url3;
			$headers = "";
			$eol = "\r\n";
			$headers .= "From: ".$site_url3.@$sender." <$site_email>".$eol; 
			$headers .= "X-Mailer: ".$site_url3." PHP Mail Module".$eol;
			$headers .= "MIME-Version: 1.0".$eol;
			$headers .= "Content-Type: text/$ctype; charset=\"UTF-8\"".$eol;
			$headers .= "Content-Disposition: inline".$eol;
			$headers .= "Content-Transfer-Encoding: $etype".$eol.$eol;
			if(substr($body,-8,strlen($body)) != $eol.$eol)
				$body = $body.$eol.$eol;
			if($etype == "base64")
				$body = base64_encode($body);
			if(@mail($reciver,$subject,$body,$headers))
				return true;
			else
				return false;
		}

		function windows_filename_fix($new_tag_cache)
		{
			if(strpos($new_tag_cache,";") !== false)
				$new_tag_cache = str_replace(";","&#059;",$new_tag_cache);
			if(strpos($new_tag_cache,".") !== false)
				$new_tag_cache = str_replace(".","&#046;",$new_tag_cache);
			if(strpos($new_tag_cache,"*") !== false)
				$new_tag_cache = str_replace("*","&#042;",$new_tag_cache);
			if(strpos($new_tag_cache,"|") !== false)
				$new_tag_cache = str_replace("|","&#124;",$new_tag_cache);
			if(strpos($new_tag_cache,"\\") !== false)
				$new_tag_cache = str_replace("\\","&#092;",$new_tag_cache);
			if(strpos($new_tag_cache,"/") !== false)
				$new_tag_cache = str_replace("/","&#047;",$new_tag_cache);
			if(strpos($new_tag_cache,":") !== false)
				$new_tag_cache = str_replace(":","&#058;",$new_tag_cache);
			if(strpos($new_tag_cache,'"') !== false)
				$new_tag_cache = str_replace('"',"&quot;",$new_tag_cache);
			if(strpos($new_tag_cache,"<") !== false)
				$new_tag_cache = str_replace("<","&lt;",$new_tag_cache);
			if(strpos($new_tag_cache,">") !== false)
				$new_tag_cache = str_replace(">","&gt;",$new_tag_cache);
			if(strpos($new_tag_cache,"?") !== false)
				$new_tag_cache = str_replace("?","&#063;",$new_tag_cache);
			return $new_tag_cache;	
		}
		
		function ReadHeader($socket)
		{
			$i=0;
			$header = "";
			while( true && $i<20 && !feof($socket))
			{
			   $s = fgets( $socket, 4096 );
			   $header .= $s;
			   if( strcmp( $s, "\r\n" ) == 0 || strcmp( $s, "\n" ) == 0 )
				   break;
			   $i++;
			}
			if( $i >= 20 )
			   return false;
			return $header;
		}

		function getRemoteFileSize($header)
		{
			if(strpos($header,"Content-Length:") === false)
				return 0;
			$count = preg_match($header,'/Content-Length:\s([0-9].+?)\s/',$matches);
			if($count > 0)
			{
				if(is_numeric($matches[1]))
					return $matches[1];
				else
					return 0;
			}
			else
				return 0;
		}
		
		function swap_bbs_tags($data)
		{
			$post = new post();
			$pattern = array();
			$replace = array();
			$pattern[] = '/\[spoiler\](.*?)\[\/spoiler\]/i';
			$replace[] = '<span class="spoiler">$1</span>';
			$pattern[] = '/\[quote\](.*?)\[\/quote\]/i';
			$replace[] = '<blockquote>$1</blockquote>';
			$pattern[] = '/\[code\](.*?)\[\/code\]/i';
			$replace[] = '<code>$1</code>';
			$pattern[] = '/\[pre\](.*?)\[\/pre\]/i';
			$replace[] = '<pre>$1</pre>';
			$pattern[] = '/\[ul\](.*?)\[\/ul\]/i';
			$replace[] = '<ul>$1</ul>';
			$pattern[] = '/\[ol\](.*?)\[\/ol\]/i';
			$replace[] = '<ol>$1</ol>';
			$pattern[] = '/\[li\](.*?)\[\/li\]/i';
			$replace[] = '<li>$1</li>';
			$pattern[] = '/\[\](.*?)\[\/\]/i';
			$replace[] = '<p>$1</p>';
			$pattern[] = '/\[p\](.*?)\[\/p\]/i';
			$replace[] = '<p>$1</p>';
			$pattern[] = '/\[s\](.*?)\[\/s\]/i';
			$replace[] = '<s>$1</s>';
			$pattern[] = '/\[b\](.*?)\[\/b\]/i';
			$replace[] = '<b>$1</b>';
			$pattern[] = '/\[i\](.*?)\[\/i\]/i';
			$replace[] = '<i>$1</i>';
			$pattern[] = '/\[u\](.*?)\[\/u\]/i';
			$replace[] = '<u>$1</u>';
			$pattern[] = '/\[c\](.*?)\[\/c\]/i';
			$replace[] = '<span style="display:block; text-align: center;">$1</span>';
			$pattern[] = '/\[l\](.*?)\[\/l\]/i';
			$replace[] = '<span style="display:block; text-align: left;">$1</span>';
			$pattern[] = '/\[r\](.*?)\[\/r\]/i';
			$replace[] = '<span style="display:block; text-align: right;">$1</span>';
			$pattern[] = '/\[color\=(.*?)\](.*?)\[\/color\]/i';
			$replace[] = '<span style="color:$1">$2</span>';
			$pattern[] = '/\[img\](.*?)\[\/img\]/i';
			$replace[] = '<img alt="embedded_id_'.rand().'" src="http://$1" style="display:block; max-width:100%;">';
			$pattern[] = '/\[url\](.*?)\[\/url\]/i';
			$replace[] = '<a href="http://$1" target="_blank">http://$1</a>';
			$pattern[] = '/\[url\=(.*?)\](.*?)\[\/url\]/i';
			$replace[] = '<a href="http://$1" target="_blank">$2</a>';
			$pattern[] = '/\[t\](.*?)\[\/t\]/i';
			$replace[] = '<a href="index.php?page=post&amp;s=list&amp;tags=$1" target="_blank">$1</a>';
			$pattern[] = '/\[tag\](.*?)\[\/tag\]/i';
			$replace[] = '<a href="index.php?page=post&amp;s=list&amp;tags=$1" target="_blank">tag #$1</a>';
			$pattern[] = '/\[spoiler\](.*?)\[\/spoiler\]/i';
			$replace[] = '<span class="spoiler">$1</span>';
			$pattern[] = '/\[post\](.*?)\[\/post\]/i';
			$replace[] = '<a href="index.php?page=post&amp;s=view&amp;id=$1" target="_blank">post #$1</a>';
			$pattern[] = '/\[forum\](.*?)\[\/forum\]/i';
			$replace[] = '<a href="index.php?page=forum&amp;s=view&amp;id=$1" target="_blank">forum #$1</a>';
			$count = count($pattern)-1;
			for($i=0;$i<=$count;$i++)
			{
				while(preg_match($pattern[$i],$data) == 1)
					$data = preg_replace($pattern[$i], $replace[$i], $data);
			}
			return $data;
		}
		
		function date_words ($tm,$rcs = 0) {
			$x =='';
			$cur_tm = time(); $dif = $cur_tm-$tm;
			$pds = array('second','minute','hour','day','week','month','year','decade');
			$lngh = array(1,60,3600,86400,604800,2630880,31570560,315705600);
			for($v = sizeof($lngh)-1; ($v >= 0)&&(($no = $dif/$lngh[$v])<=1); $v--); if($v < 0) $v = 0; $_tm = $cur_tm-($dif%$lngh[$v]);
			$no = floor($no); if($no <> 0 && $no <> 1) $pds[$v] .='s'; $x=sprintf("%d %s ",$no,$pds[$v]);
			if(($rcs == 1)&&($v >= 1)&&(($cur_tm-$_tm) > 0)) $x .= time_ago($_tm);
			$x .= ' ago'; return $x;
		}	
	
		public function is_html($data)
		{
			if(preg_match("#<script|<html|<head|<title|<body|<pre|<table|<a\s+href|<img|<plaintext|<div|<frame|<iframe|<li|type=#si", $data) == 1)
				return true;
			else
				return false;
		}
		
		function pagination($page_type,$sub = false,$id = false,$limit = false,$page_limit = false,$count = false,$page = false,$tags = false, $query = false)
		{
			$has_id = "";
			if(isset($id) && $id > 0)
				$has_id = '&amp;id='.$id.'';
			if(isset($tags) && $tags !="" && $tags)
				$has_tags = '&amp;tags='.str_replace(" ","+",urlencode($tags)).'';
			if(isset($sub) && $sub !="" && $sub)
				$sub = '&amp;s='.$sub.'';
			if(isset($query) && $query != "" && $query)
				$query = '&amp;query='.urlencode($query).'';
			$pages = intval($count/$limit);
			if ($count%$limit)
				$pages++;
			$current = ($page/$limit) + 1;
			$total = $pages;
			if ($pages < 1 || $pages == 0 || $pages == "")
				$total = 1;
				
			$first = $page + 1;
			$last = $count;
			if (!((($page + $limit) / $limit) >= $pages) && $pages != 1)
				$last = $page + $limit;
			$output = "";
			if($page == 0)
				$start = 1;
			else
				$start = ($page/$limit) + 1;
			$tmp_limit = $start + $page_limit;
			if($tmp_limit > $pages)
				$tmp_limit = $pages;
			if($pages > $page_limit)
				$lowerlimit = $pages - $page_limit;
			if($start > $lowerlimit)
				$start = $lowerlimit;
			$lastpage = $limit*($pages - 1);
			if($page != 0 && !((($page+$limit) / $limit) > $pages)) 
			{ 
				$back_page = $page - $limit;
				$output .=  '<a href="?page='.$page_type.''.$sub.''.$query.''.$has_id.''.$has_tags.'&amp;pid=0">&lt;&lt;</a><a href="?page='.$page_type.''.$sub.''.$query.''.$has_id.''.$has_tags.'&amp;pid='.$back_page.'">&lt;</a>';
			}
			for($i=$start; $i <= $tmp_limit; $i++)
			{
				$ppage = $limit*($i - 1);
				if($ppage >= 0)
				{
					if ($ppage == $page)
						$output .=  '<span class="current">'.$i.'</span>';
					else
						$output .=  '<a href="?page='.$page_type.''.$sub.''.$query.''.$has_id.''.$has_tags.'&amp;pid='.$ppage.'">'.$i.'</a>';
				}
			}
			if (!((($page+$limit) / $limit) >= $pages) && $pages != 1) 
			{ 
				// If last page don't give next link.
				$next_page = $page + $limit;
				$output .= '<a href="?page='.$page_type.''.$sub.''.$query.''.$has_id.''.$has_tags.'&amp;pid='.$next_page.'">&gt;</a><a href="?page='.$page_type.''.$sub.''.$query.''.$has_id.''.$has_tags.'&amp;pid='.$lastpage.'">&gt;&gt;</a>';
			}
			return $output;
		}
	}
?>