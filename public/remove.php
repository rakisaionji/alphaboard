<?php
	require "../inv.header.php";
	$user = new user();
	if(!$user->check_log())
	{
		header('Location: ../index.php?page=account&s=home');
		exit;
	}		
	if(isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] != "")
	{
		$id = $db->real_escape_string($_GET['id']);
		if(isset($_GET['note_id']) && is_numeric($_GET['note_id']) && $_GET['note_id'] != "")
		{
			if(!$user->gotpermission('alter_notes'))
				exit;
			$note_id = $db->real_escape_string($_GET['note_id']);
			$query = "SELECT COUNT(*) FROM $note_table WHERE post_id='$id' AND id='$note_id'";
			$result = $db->query($query);
			$row = $result->fetch_assoc();
			if($row['COUNT(*)'] == 1)
			{
				$result->free_result();
				$query = "DELETE FROM $note_table WHERE post_id='$id' AND id='$note_id'";
				$db->query($query);
				$query = "DELETE FROM $note_history_table WHERE post_id='$id' AND id='$note_id'";
				$db->query($query);
				print $note_id;
			}
		}
		else if(isset($_GET['removepost']) && $_GET['removepost'] == 1)
		{
			$image = new image();
			if($image->removeimage($id) == true)
			{
				if(isset($_GET['token']) && $_GET['token'] != "" && base64_decode($_GET['token'], true))
				header("Location:".base64_decode($_GET['token'])."");
				else
				header("Location:../index.php?page=post&s=list");
			}
			else
				if(isset($_GET['token']) && $_GET['token'] != "" && base64_decode($_GET['token'], true))
				header("Location:".base64_decode($_GET['token'])."");
				else
				header("Location:../index.php?page=post&s=view&id=$id");
		}
		else if(isset($_GET['removecomment']) && $_GET['removecomment'] == 1)
		{
			$permission = $user->gotpermission('delete_comments');
			if($permission == true)
			{
				$post_id = $db->real_escape_string($_GET['post_id']);
				$query = "SELECT * FROM $comment_table WHERE id='$id' LIMIT 1";
				$result = $db->query($query);
				if($result->num_rows =="1")
				{
					$query = "DELETE FROM $comment_table WHERE id='$id'";
					$db->query($query);
					$query = "DELETE FROM $comment_vote_table WHERE comment_id='$id'";
					$db->query($query);
					$query = "UPDATE $post_count_table SET pcount=pcount-1 WHERE access_key = 'comment_count'";
					$db->query($query);
				}
			}
			if(isset($_GET['token']) && $_GET['token'] != "" && base64_decode($_GET['token'], true))
			header("Location:".base64_decode($_GET['token'])."");
			else
			header("Location:../index.php?page=post&s=view&id=$post_id");
		}
	}
?>