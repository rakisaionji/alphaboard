<?php
	require "../inv.header.php";
	$user = new user();
	$ip = $db->real_escape_string($_SERVER['REMOTE_ADDR']);	
	if($user->banned_ip($ip))
		exit;
	if(!$user->check_log() || !$user->gotpermission('approve_posts'))
	{
		header('Location: index.php?page=account&s=home');
		exit;
	}
	if(isset($_GET['action']) && $_GET['action'] != "" && isset($_GET['id']) && is_numeric($_GET['id']))
	{
		$type = $db->real_escape_string($_GET['action']);
		$rid = $db->real_escape_string($_GET['id']);
		/*if($type == "comment")
		{
			$user = new user();
			if(!$user->check_log())
			{
				header("Location: ../index.php?page=post&s=view&id=$rid");
				exit;
			}
			$query = "UPDATE $comment_table SET approved=TRUE WHERE id='$rid'";
			if($db->query($query))
			{
				$query = "SELECT post_id FROM $comment_table where id='$rid'";
				$result = $db->query($query);
				$row = $result->fetch_assoc();
				print "pass";
			}
			else
				print "fail";	
		}
		else */if($type == "approve_post")
		{
			$user = new user();
			if(!$user->check_log())
			{
				header("Location: ../index.php?page=post&s=view&id=$rid");
				exit;
			}
			$query = "UPDATE $post_table SET approved=TRUE, approver='$checked_username', approved_date='".mktime()."', aip='$ip' WHERE id='$rid'";
			$db->query($query);
			header("Location:../index.php?page=post&s=view&id=$rid");
		}
		else if($type == "unapprove_post")
		{
			$user = new user();
			if(!$user->check_log())
			{
				header("Location: ../index.php?page=post&s=view&id=$rid");
				exit;
			}
			$query = "UPDATE $post_table SET approved=FALSE, approver=NULL, approved_date=NULL, aip=NULL WHERE id='$rid'";
			$db->query($query);
			header("Location:../index.php?page=post&s=view&id=$rid");
		}
		else
			header("Location:../index.php");
		exit;
	}
?>