<?php
	require "../inv.header.php";
	if($_POST['pconf'] !="1")
		die;
	$post = new post();
	$misc = new misc();
	$user = new user();
	$ip = $db->real_escape_string($_SERVER['REMOTE_ADDR']);	
	$id = $db->real_escape_string($_POST['id']);
	if($user->banned_ip($ip))
	{
		print "Action failed: ".$row['reason'];
		exit;
	}
	if(!$user->gotpermission('edit_posts'))
	{
		if(!$anon_edit)
		{
			header("Location: ../index.php?page=post&s=view&id=$id");
			exit;
		}
	}
	$user_id = $checked_user_id;
	$tags = stripslashes(mb_strtolower(str_replace('%','',htmlentities(str_replace("\r\n"," ",$_POST['tags']), ENT_QUOTES, 'UTF-8')), 'UTF-8'));
	$ttags = array_filter(explode(' ',$tags));
	asort($ttags);
	$tags = implode(" ",$ttags);
	$tags = mb_trim(str_replace("  ","",$tags));
	$parent = '';
	$query = "SELECT tags FROM $post_table WHERE id='$id'";
	$result = $db->query($query);
	$row = $result->fetch_assoc();
	if($tags != $row['tags'])
	{
		$tclass = new tag();
		$mtags = explode(" ",$row['tags']);
		$misc = new misc();
		foreach($mtags as $current)
		{
			if($current != "")
			{
				$tclass->deleteindextag($current);
			}
		}
		$temp_array = array();
		foreach($ttags as $current)
		{
			if($misc->is_html(html_entity_decode($current,ENT_QUOTES,'UTF-8')))
			{
				header('Location: ../index.php');
				exit;
			}
			$params = explode(':', $current);
			if (count($params) > 1){
			if($user->gotpermission('edit_tags'))
			$cmd = $db->real_escape_string($params[0]);
			else
			$cmd = $post->tag_type($current);
			$param = $params[1];
			$alias = $tclass->alias($param);
			if($alias !== false) $param = $alias;
			/* Only accept x:y and use x, y as valid
			If user use x:y:z only x and y is valid */
			switch($cmd){
			case 'tag':
			case 'general':
				$tag_type = "general";
				$current = $param;
				$tclass->updatetag($current, $tag_type);
				break;
			case 'char':
			case 'character':
				$tag_type = "character";
				$current = $param;
				$tclass->updatetag($current, $tag_type);
				break;
			case 'circle':
				$tag_type = "circle";
				$current = $param;
				$tclass->updatetag($current, $tag_type);
				break;
			case 'art':
			case 'artist':
				$tag_type = "artist";
				$current = $param;
				$tclass->updatetag($current, $tag_type);
				break;
			case 'faults':
				$tag_type = "faults";
				$current = $param;
				$tclass->updatetag($current, $tag_type);
				break;
			case 'copy':
			case 'copyright':
				$tag_type = "copyright";
				$current = $param;
				$tclass->updatetag($current, $tag_type);
				break;
/*			case 'pool':
				# Check if sequence was given.
				$sequence = (isset($params[2])) ? $params[2] : null;
				# Insert post into pool.
				$Pool->addPost($post_id, $param, $sequence);
				unset($tags_array[$key]);
				break;
			case '-pool':
				# Remove post from pool.
				if (is_numeric($param)){
				$Pool->removePost($post_id, $param);
				break;
				} else {
				unset($tags_array[$key]);
				break;
				}
*/			case 'parent':
				$parent = $param;
				if(!is_numeric($parent))
				$parent = '';
				$query = "SELECT COUNT(*) FROM $post_table WHERE id='$parent'";
				$result = $db->query($query);
				$row = $result->fetch_assoc();
				if($row['COUNT(*)'] < 1)
					$parent = '';
				unset($current);	
				break;
			case 'rating':
				$rating = strtolower(substr($param, 0, 1));
				if($rating == "e")
					$rating = "Explicit";
				else if($rating == "q")
					$rating = "Questionable";
				else
					$rating = "Safe";
				unset($current);	
				break;
			default:
				break;
			} }
			if($current != "" && $current != " ")
			{
				$alias = $tclass->alias($current);
				if($alias !== false) $current = $alias;
				$temp_array[] = $current;
			}
		}
		$ttags = $temp_array;
		$tags = implode(" ",$ttags);
		foreach($ttags as $current)
		{
			if($current != "" && $current != " ")
			$ttags = $tclass->filter_tags($tags,$current, $ttags);
		}
		asort($ttags);
		foreach($ttags as $current)
			$tclass->addindextag($current);
		$tags = $db->real_escape_string(implode(" ",$ttags));
		if(substr($tags,0,1) != " ")
			$tags = " $tags";
		if(substr($tags,-1,1) != " ")
			$tags = "$tags ";
		$date = date("Y-m-d H:i:s");
		$new_tags = str_replace($row['tags'],"",$tags);
		$ret = "SELECT tags_version FROM $post_table WHERE id='$id'";
		$set = $db->query($ret);
		$retme = $set->fetch_assoc();
		$version = $retme['tags_version'];
		$version = $version+1;
		$set->free_result();
		$result->free_result();
		$ret = "INSERT INTO $tag_history_table(id, tags, version, user_id, updated_at, ip) VALUES('$id', '$tags', '$version', '$user_id', '$date', '$ip')";
		$db->query($ret) or die($db->error);
		$ret = "UPDATE $post_table SET tags_version=tags_version+1 WHERE id='$id'";
		$db->query($ret) or die($db->error);
		$query = "UPDATE $user_table SET tag_edit_count = tag_edit_count+1 WHERE id='$user_id'";
		$db->query($query);
	}
	else
		$new_tags = '';
	$title = $db->real_escape_string($_POST['title']);
	if (!isset($rating) || $rating == '')
	{
	$rating = $db->real_escape_string($_POST['rating']);
	if($rating == "e")
		$rating = "Explicit";
	else if($rating == "q")
		$rating = "Questionable";
	else
		$rating = "Safe";
	}
	$source = $db->real_escape_string(htmlentities($_POST['source'], ENT_QUOTES, "UTF-8"));
	$tmp_parent = $db->real_escape_string($_POST['parent']);
	if($parent == $id)
		$parent = '';
	if($tmp_parent == $id)
		$tmp_parent = '';	
	if(!isset($parent) && $parent == '' && $tmp_parent != '' && is_numeric($tmp_parent))
	{
		$query = "SELECT COUNT(*) FROM $post_table WHERE id='$tmp_parent'";
		$result = $db->query($query);
		$row = $result->fetch_assoc();
		if($row['COUNT(*)'] > 0)
			$parent = $tmp_parent;
	}
	$query = "SELECT parent FROM $parent_child_table WHERE child='$id'";
	$result = $db->query($query);
	$row = $result->fetch_assoc();
	
	$tmp_parent = $row['parent'];
	if(is_numeric($row['parent']))
	{
		if($tmp_parent != $parent && $parent == '' || $tmp_parent != $parent && $parent == '0')
		{
			$query = "DELETE FROM $parent_child_table WHERE child='$id' AND parent='".$row['parent']."'";
			$db->query($query) or die($db->error);
		}
		else if($tmp_parent != $parent)
		{
			$query = "UPDATE $parent_child_table SET parent='$parent' WHERE child='$id' AND parent='".$row['parent']."'";
			$db->query($query);
		}
	}
	else
	{
		if(is_numeric($parent))
		{
			$query = "INSERT INTO $parent_child_table(parent,child) VALUES('$parent','$id')";
			$db->query($query);
		}
	}
	
	$misc = new misc();
	$query = "SELECT rating, tags FROM $post_table WHERE id='$id' LIMIT 1";
	$result = $db->query($query) or die($db->error);
	$row = $result->fetch_assoc();

	if($parent == '')
		$parent = 0;
	if($parent != $tmp_parent)
	{
		$query = "UPDATE $post_count_table SET last_update='20110101' WHERE access_key='posts'";
		$db->query($query);
	}
	$query = "UPDATE $post_table SET title='$title', tags='$tags', recent_tags='$new_tags', rating='$rating', source='$source', parent='$parent' WHERE id='$id'";
	$db->query($query);
	@header("Location:../index.php?page=post&s=view&id=$id");
?>