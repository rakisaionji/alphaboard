<?php
	/*
	LoliRaki Production MyImgBoard Project
	Software: AlphaBoard EN-US 1.0.6 Build 20110915
	Coded by Raki Saionji - Racozsoft Corporation
	Engine: Gelbooru 0.1.11 by Geltas
	Platform: Win / Linux / Mac
	Language: PHP / MySQL / JS
	Server: Apache / IIS
	*/

	//PHP Error Reporting Level and Settings
	error_reporting(E_ALL^E_NOTICE);	//ini_set("display_errors", 0);

	//Is your site running in Maintenace Mode?
	$maintenance = false;
	
	/*
	Use your custom logo instead of the default text link?
	|      Main page       |        Header        |     */
	  $csslogomain = true;   $csslogohead = true;

	//MySQL Login Information.
	$mysql_host = "localhost";
	$mysql_user = "root";
	$mysql_pass = "raki";
	$mysql_db = "rinnechan";
	
	//Site Source URL, with Trailing Slash.
	$site_url = "http://rinnechan/";
	//Site Image URL 1, with Trailing Slash.
	//$site_url1 = "http://rinnechan/";
	//Site Image URL 2, with Trailing Slash.
	//$site_url2 = "http://rinnechan/";
	//Site Name. Displays in header and being used globally.
	$site_url3 = "Rinnechan";
	//Engine Name. Used for Firefox Search Integration.
	$site_url4 = "rinnechan";
	//Site Notice. Displays in Header.
	$site_notice = "<br><div style=\"font:bold 16px Monaco,Monospace\">
			＿人人人人人人人人人人人人人人人人人人人人人＿<br>
			＞　リンネチャンネルアニメ壁紙にようこそ！　＜<br>
			￣ＹＹＹＹＹＹＹＹＹＹＹＹＹＹＹＹＹＹＹＹＹ￣</div>";

	//Site Description, put on meta tags. As STRING, no LINE BREAKS and ENTITIES.
	$site_desc = ".:Rinnechan:. An imageboard repository for anime, manga and Japanese games iPhone wallpapers. No registration required & unlimited downloads.";
	//Site Keywords, put on meta tags. Necessary for Search engines. As ARRAY.
	$site_kwrd = array(strtolower($site_url3),'imageboard','iphone','wallpaper','alphaboard','japanese','anime','manga','repo','album');
	asort($site_kwrd);

	//folder containing the images...
	$image_folder = "images";
	//folder quota for images...
	$image_folder_quota = 15;
	//thumbnails dimension...
	$dimension = 240;
	//thumbnails folder.
	$thumbnail_folder = "thumbnails";
	//thumbnails prefix.
	$thumbnail_prefix = "[T] ";
	//imports folder.
	$import_folder = "import";
	
	//user database table...
	$user_table = "users";
	//post table..
	$post_table = "posts";
	//tag index table
	$tag_index_table = "tag_index";
	//folder index table
	$folder_index_table = "folder_index";
	//favorites table
	$favorites_table = "favorites";
	//note table
	$note_table = "notes";
	//note history table
	$note_history_table = "notes_history";
	//comment table
	$comment_table = "comments";
	//comments vote table
	$comment_vote_table = "comment_votes";
	//user_comment table
	$user_comment_table = "user_comments";
	//user_comments vote table
	$user_comment_vote_table = "user_comment_votes";
	//posts vote table
	$post_vote_table = "post_votes";
	//group table
	$group_table = "groups";
	//tag historys
	$tag_history_table = "tag_history";
	//comment count table
	$comment_count_table = "comment_count";
	//cache table post count, updates once a day. (part of speed optimization)
	$post_count_table = "post_count";
	//hit counter table
	$hit_counter_table = "hit_counter";
	//alias table
	$alias_table = "alias";
	//parent/child table
	$parent_child_table = "parent_child";
	//forum_index_table
	$forum_topic_table = "forum_topics";
	//forum_post_table
	$forum_post_table = "forum_posts";
	//deleted image table
	$deleted_image_table = "delete_images";
	//banned ip table
	$banned_ip_table = "banned_ip";

	//Domains for Images. Used for Cross-Domain Function (EXPERIMENTAL).
	//Ex: http://img1.domain.com/, http://img2.domain.com/folder/folder/
	$domains = array($site_url);
	
	//max image width for upload (0 for no limit)
	$max_upload_width = 640;
	//max image height for upload (0 for no limit)
	$max_upload_height = 960;
	//min image width for upload (0 for no limit)
	$min_upload_width = 640;
	//min image height for upload (0 for no limit)
	$min_upload_height = 960;
	
	//registration allowed?
	$registration_allowed = true;

	//mail settings for admin contact and password recovery.
	$site_email = "admin@rakisaionji.co.cc";
	$email_recovery_subject = "Password Reset Confirmation for ".ucfirst($site_url3);

	//global limitations for POST (used for comments and others)
	$global_max_chars = 1000;
	$global_min_words = 5;

	//enable or disable anonymous reports set false to disable
	$anon_report = false;
	//enable or disable anonymous edits set false to disable
	$anon_edit = false;
	//enable or disable anonymous comments set false to disable
	$anon_comment = true;
	//enable or disable anonymous voting set false to disable
	$anon_vote = false;
	//enable or disable anonymous post adding
	$anon_can_upload = false;
	//Edit limit in minutes. If time is over this, edit will not happen.
	$edit_limit = 15; // <Currently DEPRECATED>

	//cache dir, all cache will be stored in subdirs to this. Put it on RAM or FAST Raid drives.
	$main_cache_dir = "cache//";
?>