<?php
	//Execution time limit.
	//Recommended value: 600 (10mins).
	set_time_limit(1200);
	
	require "inv.header.php";
	
	$user = new user();
	
	if (!$user->gotpermission('is_admin'))
	{
		header('Location: index.php');
		exit;
	}
	
	$start_time = time();
	
	function formatBytes($b,$p = null)
	{
    $units = array("byte(s)","KB","MB","GB","TB","PB","EB","ZB","YB");
    $c=0;
    if(!$p && $p !== 0) {
        foreach($units as $k => $u) {
            if(($b / pow(1024,$k)) >= 1) {
                $r["bytes"] = $b / pow(1024,$k);
                $r["units"] = $u;
                $c++;
            }
        }
        return number_format($r["bytes"],2) . " " . $r["units"];
    } else {
        return number_format($b / pow(1024,$p)) . " " . $units[$p];
    }
	}

	function returnBytes($val)
	{
    $val = trim($val);
    $last = strtolower($val[strlen($val)-1]);
    switch($last) {
        case 't':
            $val *= 1024;
        case 'g':
            $val *= 1024;
        case 'm':
            $val *= 1024;
        case 'k':
            $val *= 1024;
    }
    return $val;
	}

	// Database Information
	$mysql_host = "localhost";
	$mysql_user = "root";
	$mysql_pass = "raki";
	$mysql_db = "rinnechan";

	//Posts Table
	$post_table = "posts";
	//Tag Index Table
	$tag_index_table = "tag_index";

	header("Content-Type: text/plain");
	
	echo 'Last login: '.date("D M j G:i:s").' on '.$_SERVER['SERVER_NAME'].'

--------------------------------------------------
     AlphaBoard Tag Index Repair Tool
              Copyright (C) 2011 Raki Saionji
--------------------------------------------------

> Connecting to the database...';

	$db = new mysqli($mysql_host,$mysql_user,$mysql_pass,$mysql_db);

	if (!mysqli_connect_errno())
	{
	echo ' <OK>
> Analyzing current database connection...';
	echo ($db->set_charset('utf8')) ? ' <OK>' : ' <ERROR>';
	echo '
> Loading records from the database...';
	$query = "SELECT `tag` FROM `$tag_index_table` ORDER BY `tag` ASC";
	echo ($result = $db->query($query)) ? ' <OK>' : ' <ERROR>';
	echo "\r\n";
	$current_count = 0; $success_count = 0; $error_count = 0; $ignored_count = 0;
	if (isset($_GET['v']))
	echo '
> Task is running in VERBOSE MODE.
> All processes will be displayed.';
	else if (isset($_GET['d']))
	echo '
> Task is running in DEBUG MODE.
> Errors and results will be displayed.';
	else
	echo '
> Task is running in SILENT MODE.
> Only results will be displayed.';
	while ($record = $result->fetch_assoc())
	{
	$current_count++;
	$tag = $db->real_escape_string($record['tag']);
	$query = "UPDATE `$tag_index_table` SET `index_count` = (SELECT COUNT(*) FROM `$post_table` WHERE `tags` LIKE '% $tag %') WHERE `tag` = '$tag'";
	$task = $db->query($query); if ($task) $success_count++; else $error_count++;
	if (isset($_GET['v']))
	{
	echo '
> Processing Record #'.$current_count.'...'; echo ($task) ? ' <OK>' : ' <ERROR>';
	}
	else if (isset($_GET['d']) && !$task)
	{
	echo '
> Errors in Record #'.$current_count;
	}
	}
	$result->free_result();
	$end_time = time();
	echo '

> Processed '.$current_count.' record(s)
> '.$success_count.' succeeded, '.$ignored_count.' ignored, '.$error_count.' error
> Total time consuming: '.($end_time - $start_time).' second(s)
> Total memory used: '.formatBytes(memory_get_peak_usage(true)).' / '.formatBytes(returnBytes(ini_get('memory_limit')));
	}
	else
	{
        echo ' <ERROR>';
	}
	$db->close();

echo '

[Process completed]';
?>