<?php
header("Content-Type: text/html");
echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
	<title>'.$site_url3.''; if(isset($lozerisdumb)) print " ".$lozerisdumb; echo '</title>
	<link rel="stylesheet" type="text/css" media="all" href="'.$site_url.'default.css" title="default">
	<link rel="search" type="application/opensearchdescription+xml" title="'.$site_url3.'" href="'.$site_url.'rinnechan.xml">
	<link rel="shortcut icon" href="'.$site_url.'favicon.ico" type="image/x-icon">
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<meta name="author" content="Racozsoft Corporation">
	<meta name="keywords" content="'.implode(', ',$site_kwrd).'">
	<meta name="description" content="'.$site_desc.'">
	<meta name="generator" content="AlphaBoard 1.0.6e">
	<link rel="home" href="'.$site_url.'">
	<link rel="index" href="'.$site_url.'toc.php">
	<link rel="alternate" type="application/rss+xml" title="RSS" href="'.$site_url.'index.php?page=atom">
	<script src="'.$site_url.'script/core.js" type="text/javascript"></script>
	<script src="'.$site_url.'script/notes.js" type="text/javascript"></script>
	<script src="'.$site_url.'script/global.js" type="text/javascript"></script>
	<!--[if lt IE 7]><script defer type="text/javascript" src="'.$site_url.'script/pngfix.js?2"></script><![endif]-->
</head>
<body>
	<div id="header">
	<table cellspacing="0" cellpadding="0" style="margin: 0px; padding: ';
	echo ($csslogohead == true) ? '0px 0px 0px 0px' : '15px 0px 0px 15px';
	echo '; width: 100%;">
		<tr>
		<td style="vertical-align: middle; width: 300px;">
			';
		echo ($csslogohead == true) ? '<h2 id="site-title"><a id="ruu"' : '<h2><a';
		echo ' href="#" onclick="Finder.Show(\'quick-menu\'); return false;">'.$site_url3.'</a></h2>
		</td>
		<td style="vertical-align: middle; padding-right: 50px;">
			'.$site_notice.'
		</td>
		<!--
		<td style="vertical-align: middle; width: 200px; padding: 16px; text-align:right;">
			<a href="http://validator.w3.org/check?uri=referer" target="_blank">
			<img src="'.$site_url.'public/valid-html401" title="Valid HTML 4.01" alt="Valid HTML 4.01">
			</a>
			<a href="http://jigsaw.w3.org/css-validator/check/referer" target="_blank">
			<img src="'.$site_url.'public/valid-css2" title="Valid CSS Level 2.1" alt="Valid CSS 2.1">
			</a>
		</td>
		-->
		</tr>
	</table>
';
require "menu.php";
echo '	<noscript>
	<div style="padding: 0 20px 15px 20px;">
		<div class="error-notice">
			JavaScript must be enabled to use this site.<br>
			Please enable JavaScript in your browser and refresh the page.
		</div>
	</div>
	</noscript>
';
?>