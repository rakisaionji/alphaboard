<?php
	if(isset($_GET['s']) && $_GET['s'] == "view")
	{
		//List how many images per page that should be here.
		//number of images/page
		$limit = 50;
		//number of pages to display. number - 1. ex: for 5 value should be 4
		$page_limit = 6;
		$user = new user();
		$cache = new cache();
		$domain = $cache->select_domain();
		header("Cache-Control: store, cache");
		header("Pragma: cache");
		require "includes/header.php";
		?>
	<div id="content">
		<script type="text/javascript">
			var posts = {}; var pignored = {};
		</script><?php
		$id = $db->real_escape_string($_GET['id']);
		if ($username = $user->get_user_name($id))
		print '
		<div style="clear: both">
			<h1>'.$username.'\'s favorites</h1>
		</div>
		<br>';
		else die('
		<h1>Index not found!</h1>
	</div>');
		if(isset($_GET['pid']) && $_GET['pid'] != "" && is_numeric($_GET['pid']) && $_GET['pid'] >= 0)
			$page = $db->real_escape_string($_GET['pid']);
		else
			$page = 0;
		$query = "SELECT favorites_count FROM $user_table WHERE id='$id'";
		$result = $db->query($query);
		$row = $result->fetch_assoc();
		$numrows = $row['favorites_count'];
		$result->free_result();
		if($numrows < 1)
			die('
		<h2>Nobody here but us chickens!</h2>
	</div>');
		$images = '';
		$query = "SELECT t2.id, t2.image, t2.directory as dir, t2.tags, t2.owner, t2.score, t2.rating FROM $favorites_table as t1 JOIN $post_table AS t2 ON t2.id=t1.favorite WHERE t1.user_id='$id' LIMIT $page, $limit";
		$result = $db->query($query);
		while($row = $result->fetch_assoc())
		{
			$tags = $row['tags'];
			$tags = substr($tags,1,strlen($tags));
			$tags = substr($tags,0,strlen($tags)-1);
			$images .= '
		<span class="thumb" id="p'.$row['id'].'" style="margin: 10px;">
			<a href="index.php?page=post&amp;s=view&amp;id='.$row['id'].'"onclick="document.location=\'index.php?page=post&amp;s=view&amp;id='.$row['id'].'\'; return false;">
			<img src="'.$domain.$thumbnail_folder.'/'.$row['dir'].'/'.$thumbnail_prefix.$row['image'].'" title="'.$tags.'" border="0" alt="image_thumb">
			</a>'; (isset($_COOKIE['user_id']) && $_COOKIE['user_id'] == $id) ? $images .= '<br>
			<a href="#" onclick="document.location=\'index.php?page=favorites&amp;s=delete&amp;id='.$row['id'].'&amp;pid='.$_GET['pid'].'\'; return false;"><b>Remove</b></a>
		</span>' : $images .= '
		</span>';
			$images .= '
		<script type="text/javascript">
			posts['.$row['id'].'] = {\'tags\':\''.str_replace('\\',"&#92;",str_replace(' ','%20',str_replace("'","&#039;",$tags))).'\'.split(/ /g), \'rating\':\''.$row['rating'].'\', \'score\':'.$row['score'].', \'user\':\''.str_replace('\\',"&#92;",str_replace(' ','%20',str_replace("'","&#039;",$row['owner']))).'\'}
		</script>';
		}
		$images .= '
	</div>
	<div style="clear: both; margin-top: 16px; margin-right: 50px; float: right; font-weight:bold;">
		<a id="pi" href="#" onclick="showHideIgnored(\'0\',\'pi\'); return false;">Hidden Posts</a>
		<span id="blacklist-count" class="post-count"></span>
	</div>
	<script type="text/javascript">
		filterPosts(posts)
	</script>
	<div id="paginator">';
	print $images;
	$result->free_result();
	$misc = new misc();
	print '
		'.$misc->pagination($_GET['page'],$_GET['s'],$id,$limit,$page_limit,$numrows,$_GET['pid']);
	print '
		<br><br>';
	}
	else if(isset($_GET['s']) && $_GET['s'] == "list")
	{
		//List how many users per page that should be here.
		$limit = 50;
		//number of pages to display. number - 1. ex: for 5 value should be 4
		$page_limit = 6;
		header("Cache-Control: store, cache");
		header("Pragma: cache");
		require "includes/header.php";
		print '	<div id="content">
		<div style="clear: both">
			<h1>Everyone\'s favorites</h1><br>';
		if(isset($_GET['pid']) && $_GET['pid'] != "" && is_numeric($_GET['pid']) && $_GET['pid'] >= 0)
			$page = $db->real_escape_string($_GET['pid']);
		else
			$page = 0;
		$query = "SELECT COUNT(*) FROM $user_table WHERE favorites_count > 0 ORDER BY id";
		$result = $db->query($query);
		$row = $result->fetch_assoc();
		$numrows = $row['COUNT(*)'];
		$result->free_result();
		if($numrows < 1)
			die('
			<h1>No favorites exists.</h1>
		</div>
	</div>
</body>
</html>');
		$uid = '';
		$query = "SELECT t2.user, t1.id, t1.favorites_count FROM $user_table AS t1 JOIN $user_table AS t2 ON t2.id=t1.id ORDER BY t2.user ASC LIMIT $page, $limit";
		$result = $db->query($query);
		print '
		</div>
		<div id="account-favorites-list">
			<table width="100%">
			<tr>
			<th width="30%">User</th>
			<th width="70%">Count</th>
			</tr>';
		while($row = $result->fetch_assoc())
			print '
			<tr>
			<td><a href="index.php?page=favorites&amp;s=view&amp;id='.$row['id'].'">'.$row['user'].'</a></td>
			<td>'.$row['favorites_count'].'</td>
			</tr>';
		$result->free_result();
		print '
			</table>
		</div>
		<div id="paginator">';
		$misc = new misc();
		print '
			'.$misc->pagination($_GET['page'],$_GET['s'],$id,$limit,$page_limit,$numrows,$_GET['pid']).'
		</div>';
	}
	else if(isset($_GET['s']) && $_GET['s'] == "delete" && isset($_GET['id']) && is_numeric($_GET['id']))
	{
		$pid = $_GET['pid'];
		$id = $db->real_escape_string($_GET['id']);
		$user_id = $db->real_escape_string($_COOKIE['user_id']);
		$query = "SELECT favorites_count FROM $user_table WHERE id='$user_id'";
		$result = $db->query($query) or die(mysql_error());
		$row = $result->fetch_assoc();
		$count = $row['favorites_count'];
		$result->free_result();
		if($count > 0)
		{
			$query = "DELETE FROM $favorites_table WHERE user_id='$user_id' and favorite='$id'";
			$db->query($query) or die(mysql_error());
			$query = "UPDATE $user_table SET favorites_count = favorites_count - 1 WHERE id='$user_id'";
			$db->query($query) or die(mysql_error());
		}
		header("Location:index.php?page=favorites&s=view&id=$user_id&pid=".$pid."");
		exit;
	}
?>

	</div>
</body>
</html>