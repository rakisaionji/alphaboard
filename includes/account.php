<?php
	$s = $_GET['s'];
	if($s == "home" || $s == "")
		require "account/account_home.php";
	else if($s == "options")
		require "account/account_options.php";
	else if($s == "profile")
		require "account/account_profile.php";
	else if($s == "tag_edits")
		require "account/account_tag_edits.php";
	else if($s == "edit")
		require "account/account_edit.php";
	else if($s == "reg")
		require "account/signup.php";
	else if($s == "login")
		require "account/login.php";
	else if($s == "reset_password")
		require "account/reset_password.php";
	else if($s == "change_password")
		require "account/change_password.php";
	else 	header("Location:index.php");
?>