<?php
	//number of topics/page
	$limit = 40;
	//number of pages to display. number - 1. ex: for 5 value should be 4
	$page_limit = 6;
	
	header("Cache-Control: store, cache");
	header("Pragma: cache");
	$misc = new misc();
	$user = new user();
	$search = new search();
	if(!isset($_GET['query']) || isset($_GET['query']) && $_GET['query'] == "")
		$lozerisdumb = "- Forum - Main Listing";
	else
		$lozerisdumb = "- Forum - Search for ".stripslashes(str_replace("%",'',mb_trim(htmlentities($_GET['query'], ENT_QUOTES, 'UTF-8'))));
	require "includes/header.php";
	print '	<div id="content">';
	
	echo '
		<div style="width: 500px; padding: 0px; margin-bottom: 10px; text-align: left;">
			<form method="post" action="?page=forum&amp;s=search">
				<input type="text" name="search" size="30" class="main_search_text" value="'.stripslashes($_GET['query']).'">
				<input type="submit" name="submit" class="main_search" value="Search">
			</form>
		</div>
		<table class="highlightable" width="100%">';
	if(isset($_GET['pid']) && $_GET['pid'] != "" && is_numeric($_GET['pid']) && $_GET['pid'] >= 0)
		$page = $db->real_escape_string($_GET['pid']);
	else
		$page = 0;
	if(!isset($_GET['query']) || isset($_GET['query']) && $_GET['query'] == "")
	{
		echo '
			<thead>
			<tr>
			<th width="50%">Title</th>
			<th width="10%">Created by</th>
			<th width="10%">Updated by</th>
			<th width="10%">Updated</th>
			<th width="5%">Responses</th>';
		$query = "SELECT COUNT(*) FROM $forum_topic_table";
		$result = $db->query($query);
		$row = $result->fetch_assoc();
		$numrows = $row['COUNT(*)'];
		$query = "SELECT * FROM $forum_topic_table ORDER BY priority DESC, last_updated DESC LIMIT $page, $limit";
		$result = $db->query($query);
		if($user->gotpermission('delete_forum_topics') || $user->gotpermission('pin_forum_topics'))
		print '
			<th width="10%">Tools</th>';
		echo '
			</tr>
			</thead>
			<tbody>';
		while($row = $result->fetch_assoc())
		{
		$que = "SELECT COUNT(*) FROM $forum_post_table WHERE topic_id='".$row['id']."'";
		$res = $db->query($que) or die(mysql_error());
		$ret = $res->fetch_assoc();
		$replies = $ret['COUNT(*)']-1;
		$date_now = $misc->date_words($row['last_updated']);
		$sticky = "";
		$locked = "";
		$unread = "";
		if($row['priority'] =="1")
			$sticky = "Sticky: ";
		if($row['locked']=="1")
			$locked =' <span class="locked-topic">(locked)</span>';
		if($row['last_updated']+60*60*24 >= time() || $row['last_updated']+60*60*48 >= time()) 
			$unread = ' class="unread-topic"';
		print '
			<tr>';
		print '
			<td><span'.$unread.'>'.$sticky.'<a href="?page=forum&amp;s=view&amp;id='.$row['id'].'">'.stripslashes($row['topic']).'</a></span>'.$locked.'</td>
			<td><a href="index.php?page=account&amp;s=profile&amp;uname='.$row['author'].'">'.$row['author'].'</a></td>
			<td><a href="index.php?page=account&amp;s=profile&amp;uname='.$row['updated_by'].'">'.$row['updated_by'].'</a></td>
			<td>'.$date_now.'</td>
			<td>'.$replies.'</td>'; 
		if($row['priority'] == 0)
		{
			if($user->gotpermission('pin_forum_topics'))
				print '
			<td><a href="index.php?page=forum&amp;s=edit&amp;pin=1&amp;id='.$row['id'].'&amp;pid='.$page.'">Pin</a> |';
		}
		else
		{
			if($user->gotpermission('pin_forum_topics')) 
				print '
			<td><a href="index.php?page=forum&amp;s=edit&amp;pin=0&amp;id='.$row['id'].'&amp;pid='.$page.'">Unpin</a> |';
		}
		if($user->gotpermission('delete_forum_topics'))
			print '
			<a href="index.php?page=forum&amp;s=remove&amp;fid='.$row['id'].'&amp;pid='.$page.'">Delete</a></td>'; 
		echo '
			</tr>';
		}
	}
	else
	{
		echo '
		<thead>
		<tr>
			<th width="30%">Topic</th>
			<th width="40%">Message</th>
			<th width="10%">Author</th>
			<th width="10%">Created</th>';
		$tags = stripslashes(str_replace("%",'',mb_trim(htmlentities($_GET['query'], ENT_QUOTES, 'UTF-8'))));		
		$query = $search->prepare_forum_posts($tags);
		$result = $db->query($query);
		$numrows = $result->num_rows;
		$result->free_result();
		if($numrows != 0)
			$query = $query." LIMIT $page, $limit";			
		$result = $db->query($query);
		if($user->gotpermission('delete_forum_posts'))
		print '
			<th width="10%">Tools</th>';
		echo '
			</tr>
		</thead>
		<tbody>';
		while($row = $result->fetch_assoc())
		{
		$date_now = $misc->date_words($row['creation_date']);
		$sticky = "";
		$locked = "";
		$unread = "";
		if($row['priority'] == "1")
			$sticky = "Sticky: ";
		if($row['locked']=="1")
			$locked =' <span class="locked-topic">(locked)</span>';
		if($row['creation_date']+60*60*24 >= time() || $row['creation_date']+60*60*48 >= time()) 
			$unread = ' class="unread-topic"';
		print '
			<tr>';
		print '
			<td><span'.$unread.'>'.$sticky.'<a href="?page=forum&amp;s=view&amp;id='.$row['topic_id'].'">'.stripslashes($row['topic']).'</a></span>'.$locked.'</td>
			<td><a href="?page=forum&amp;s=view&amp;id='.$row['topic_id'].'&amp;post='.$row['id'].'">'.htmlentities(substr(str_replace("\r\n"," ",html_entity_decode(stripslashes($row['post']), ENT_QUOTES, 'UTF-8')),0,50), ENT_QUOTES, 'UTF-8').'...</a></td>
			<td><a href="index.php?page=account&amp;s=profile&amp;uname='.$row['author'].'">'.$row['author'].'</a></td>
			<td>'.$date_now.'</td>';
		if($user->gotpermission('delete_forum_posts'))
			print '
			<td><a href="index.php?page=forum&amp;s=remove&amp;pid='.$row['topic_id'].'&amp;cid='.$row['id'].'">Delete</a></td>'; 
		echo '
			</tr>';
		}
	}
	echo '
			</tbody>
		</table>
		<div id="paginator">';
	print '
			'.$misc->pagination($_GET['page'],$_GET['s'],$row['id'],$limit,$page_limit,$numrows,$_GET['pid'],$_GET['tags'],$_GET['query']);
	echo '
		</div>
		<br>
		<div id="footer">
			<a href="#" onclick="showHide(\'new_topic\'); return false;">New Topic</a> | <a href="'.$site_url.'index.php?page=help&amp;topic=forum">Help</a>';
?>	
		</div>
		<br>
		<center>
		<form method="post" action="index.php?page=forum&amp;s=add" id="new_topic" style="display:none">
			<table>
			<tr><td>
			<h5>New Topic</h5><br>
			</td></tr>
			<tr><td>Topic:<br>	
			<input type="text" name="topic" style="width: 512px; height: 24px; margin-bottom:12px;" value="">
			</td></tr>
			<tr><td>Post:<br>
			<textarea name="post" id="post" rows="4" cols="6" style="width: 512px; height: 256px; margin-bottom:12px;" onKeyDown="counter_comment('post', 'count', <?php print $global_max_chars ?>)" onKeyUp="counter_comment('post', 'count', <?php print $global_max_chars ?>)"></textarea>
			</td></tr>
			<tr><td>
			<input type="hidden" name="conf" id='conf' value="0">
			</td></tr>
			<tr><td>
			<input id="count" class="comment-counter" value="<?php print $global_max_chars ?>" disabled="disabled">
			<input type="submit" name="submit" style="width: 96px; height: 24px; margin-bottom:12px;" onclick="return validate_comment('post', <?php print $global_min_words ?>)" value="Create topic">
			</td></tr>
			</table>
		</form>
		</center>
		<script type="text/javascript">
			document.getElementById('conf').value = 1
		</script>
	</div>
</body>
</html>