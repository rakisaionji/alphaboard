<?php
	$user = new user();
	$ip = $db->real_escape_string($_SERVER['REMOTE_ADDR']);	
	if($user->banned_ip($ip))
	{
		print "Action failed: ".$row['reason'];
		exit;
	}	
	if(!$user->check_log())
	{
		header("Location:index.php?page=account&s=login&code=00");
		exit;
	}
	$add_forum_count = "UPDATE $user_table SET forum_post_count = forum_post_count+1 WHERE id='$checked_user_id'";	
	if(isset($_GET['t']) && $_GET['t'] == "post")
	{
		if(!$user->gotpermission('new_forum_posts'))
		{
			header("Location:index.php?page=forum&s=list");
			exit;
		}
		if(isset($_GET['pid']) && is_numeric($_GET['pid']) && isset($_POST['conf']) && $_POST['conf'] == 1)
		{
			$title = $db->real_escape_string(htmlentities($_POST['title'], ENT_QUOTES, 'UTF-8'));
			$post = $db->real_escape_string(htmlentities($_POST['post'], ENT_QUOTES, 'UTF-8'));
			$pid = $db->real_escape_string($_GET['pid']);
			$query = "SELECT locked FROM $forum_topic_table WHERE id='$pid'";
			$result = $db->query($query) or die($db->error);
			$row = $result->fetch_assoc();
			if($row['locked'] == true || strlen($_POST['post']) > $global_max_chars)
			{
				header("Location:index.php?page=forum&s=list");
				exit;
			}
			$user = $checked_username;
			$query = "INSERT INTO $forum_post_table(title, post, author, creation_date, topic_id, ip) VALUES('$title', '$post', '$user', '".mktime()."', '$pid', '$ip')";
			$db->query($query) or die($db->error);
			$query = "SELECT LAST_INSERT_ID() as id FROM $forum_post_table";
			$result = $db->query($query) or die($db->error);
			$row = $result->fetch_assoc();
			$id = $row['id'];
			$result->free_result();
			$query = "UPDATE $forum_topic_table SET last_updated='".mktime()."' WHERE id='$pid'";
			$db->query($query) or die($db->error);
			$query = "UPDATE $forum_topic_table SET updated_by='".$user."' WHERE id='$pid'";
			$db->query($query) or die($db->error);
			$db->query($add_forum_count);			
			header("Location:index.php?page=forum&s=view&id=$pid&post=$id");
			exit;
		}	
	}
	else
	{
		if(!$user->gotpermission('new_forum_topics'))
		{
			header("Location:index.php?page=forum&s=list");
			exit;
		}
		if(isset($_POST['topic']) && $_POST['topic'] != "" && isset($_POST['post']) && $_POST['post'] != "" && isset($_POST['conf']) && $_POST['conf'] == 1)
		{
			$topic = $db->real_escape_string(htmlentities($_POST['topic'], ENT_QUOTES, 'UTF-8'));
			$query = "SELECT COUNT(*) FROM $forum_topic_table WHERE topic='$topic'";
			$result = $db->query($query) or die($db->error);
			$row = $result->fetch_assoc();
			if ($row['COUNT(*)'] > 0 || strlen($_POST['post']) > $global_max_chars)
			{
				header("Location:index.php?page=forum&s=list");
				exit;
			}
			$post = $db->real_escape_string(htmlentities($_POST['post'], ENT_QUOTES, 'UTF-8'));
			$result->free_result();
			$user = $checked_username;
			$query = "INSERT INTO $forum_topic_table(topic, author, creation_post, last_updated, updated_by) VALUES('$topic', '$user', '0', '".mktime()."', '$user')";
			$db->query($query) or die($db->error);
			$query = "SELECT LAST_INSERT_ID() as id FROM $forum_topic_table";
			$result = $db->query($query) or die($db->error);
			$row = $result->fetch_assoc();
			$pid = $row['id'];
			$query = "INSERT INTO $forum_post_table(title, post, author, creation_date, topic_id, ip) VALUES('$topic', '$post', '$user', '".mktime()."', '$pid', '$ip')";
			$db->query($query) or die($db->error);
			$db->query($add_forum_count);			
			$query = "SELECT LAST_INSERT_ID() as id FROM $forum_post_table";
			$result = $db->query($query) or die($db->error);
			$row = $result->fetch_assoc();
			$id = $row['id'];
			$query = "UPDATE $forum_topic_table SET creation_post='$id' WHERE id='$pid'";
			$db->query($query) or die($db->error);
			header("Location:index.php?page=forum&s=view&id=$pid");
			exit;
		} 
	}
		$lozerisdumb = "- Forum - New Topic";
		require "includes/header.php";
?>
	<div id="content">
	<h2>New Topic</h2><br>
	<form method="post" action="">
		<table>
		<tr><td>Topic:<br>	
			<input type="text" name="topic" style="width: 512px; height: 24px; margin-bottom:12px;" value="">
		</td></tr>
		<tr><td>Post:<br>
			<textarea name="post" id="post" rows="4" cols="6" style="width: 512px; height: 256px; margin-bottom:12px;" onKeyDown="counter_comment('post', 'count', <?php print $global_max_chars ?>)" onKeyUp="counter_comment('post', 'count', <?php print $global_max_chars ?>)"></textarea>
		</td></tr>
		<tr><td>
			<input type="hidden" name="conf" id='conf' value="0">
		</td></tr>
		<tr><td>
			<input id="count" class="comment-counter" value="<?php print $global_max_chars ?>" disabled="disabled">
			<input type="submit" name="submit" style="width: 96px; height: 24px; margin-bottom:12px;" onclick="return validate_comment('post', <?php print $global_min_words ?>)" value="Create topic">
		</td></tr>
		</table>
	</form>
	<script type="text/javascript">
		document.getElementById('conf').value = 1
	</script>
	</div>
</body>
</html>