<?php
	//number of topics/page
	$limit = 20;
	//number of pages to display. number - 1. ex: for 5 value should be 4
	$page_limit = 6;
	$user = new user();
	$misc = new misc();
	header("Cache-Control: store, cache");
	header("Pragma: cache");

	if (!isset($_GET['id']) && !is_numeric($_GET['id']))
	{
		header("Location: index.php?page=forum&s=list");
		exit;
	}
	if(isset($_GET['pid']) && $_GET['pid'] != "" && is_numeric($_GET['pid']) &&  $_GET['pid'] >= 0)
		$page = $db->real_escape_string($_GET['pid']);
	else
		$page = 0;
	$id = $db->real_escape_string($_GET['id']);
	if($user->check_log())
	{
		$uname = $checked_username;
		$uid = checked_user_id;
	}
	if (isset($_GET['post']) && $_GET['post'] != "")
	{
		if (!is_numeric($_GET['post']))
		{
		header("Location: index.php?page=forum&s=view&id=$id");
		exit;
		}
		$post_id = $db->real_escape_string($_GET['post']);
		$lozerisdumb = "- Forum - Topic #$id - Post #$post_id";
		$post_condition = " AND id='$post_id' ";
	}
	else
	{
		$lozerisdumb = "- Forum - Topic #$id";
		$post_condition = "";
	}
	$query = "SELECT COUNT(*) FROM $forum_post_table WHERE topic_id='$id' $post_condition";
	$result = $db->query($query);
	$row = $result->fetch_assoc();
	$numrows = $row['COUNT(*)'];
	$result->free_result();
	if($numrows == 0)
	{
		header("Location: index.php?page=forum&s=list");
		exit;
	}
	require "includes/header.php";
	print '	<div id="content">
	<h1>'.ucwords($site_url3).' Forum</h1><br>';
	if (isset($_GET['post']) && $_GET['post'] != "")
		$post_condition = " AND t1.id='$post_id' ";
	else
		$post_condition = "";
	$query = "SELECT t1.id, t1.title, t1.post, t1.author, t1.creation_date, t2.topic, t2.creation_post FROM $forum_post_table  AS t1 JOIN $forum_topic_table AS t2 ON t2.id=t1.topic_id WHERE t1.topic_id='$id' $post_condition ORDER BY id LIMIT $page, $limit";
	$result = $db->query($query) or die(mysql_error());
	print'
	<div id="forum" class="response-list">';
	while($row = $result->fetch_assoc())
	{
		$date_made = $misc->date_words($row['creation_date']);
		if ($row['id'] == $row['creation_post'])
		{
		if (isset($row['topic']) || $row['topic'] != "")
		print '
		<h3>'.stripslashes($row['topic']).'</h3>';
		print '
		<h5>Started <span title="'.date('l, F jS, Y G:i:s T', $row['creation_date']).'">'.$date_made.'</span> by <a href="index.php?page=account&amp;s=profile&amp;uname='.$row['author'].'">'.$row['author'].'</a></h5><br>';
		}
		else
		if (isset($_GET['post']) && $_GET['post'] != "")
		{
		if (isset($row['topic']) || $row['topic'] != "")
		print '
		<h3>'.stripslashes($row['topic']).'</h3>';
		print '
		<h5>Viewing message #'.$row['id'].' by <a href="index.php?page=account&amp;s=profile&amp;uname='.$row['author'].'">'.$row['author'].'</a></h5><br>';
		}
		print '
		<div class="post">
			<div class="author">
				<h6 class="author">
				<a href="index.php?page=account&amp;s=profile&amp;uname='.$row['author'].'" name="'.$row['id'].'">'.$row['author'].'</a>
				</h6>
				<span class="date">'.$date_made.'</span>
			</div>
			<div class="content">
				<div class="body">
				'.stripslashes($misc->swap_bbs_tags($misc->short_url($misc->linebreaks($row['post'])))).'
				</div>
				<div class="footer">';
    	if($uname == $row['author'] || $user->gotpermission('edit_forum_posts'))
			echo '
					<a href="#" onclick="showHide(\'c'.$row['id'].'\'); return false;">Edit</a> | ';
		else
			echo '
					<a href="">Edit</a> | ';
		echo '<a href="#" onclick="forum_quote(\''.$row['author'].' said:\r\n'.str_replace("'","\'",str_replace("\r\n",'\r\n',str_replace('\&#039;',html_entity_decode("'", ENT_QUOTES, 'UTF-8'),$row['post']))).'\'); return false;">Quote</a>'; 
		if($user->gotpermission('delete_forum_posts') && $row['id'] != $row['creation_post'])
			print ' | <a href="index.php?page=forum&amp;s=remove&amp;pid='.$id.'&amp;cid='.$row['id'].'">Remove</a><br>';
		if($uname == $row['author'] || $user->gotpermission('edit_forum_posts'))
		{	print '
					<form method="post" action="index.php?page=forum&amp;s=edit&amp;pid='.$id.'&amp;cid='.$row['id'].'&amp;ppid='.$page.'" style="margin-top:12px; display:none" id="c'.$row['id'].'">
						<table>';
		if ($row['id'] == $row['creation_post'])
			print '
						<tr><td>
						<input type="text" name="title" style="width: 512px; height: 24px;" value="'.stripslashes($row['title']).'">
						</td></tr>';
			print '
						<tr><td>
						<textarea name="post" id="post_'.$row['id'].'" rows="4" cols="6" style="max-width: 512px; width: 512px; height: 256px; margin-bottom:12px;" onKeyDown="counter_comment(\'post_'.$row['id'].'\', \'count_'.$row['id'].'\', '.$global_max_chars.')" onKeyUp="counter_comment(\'post_'.$row['id'].'\', \'count_'.$row['id'].'\', '.$global_max_chars.')">'.stripslashes($row['post']).'</textarea>
						</td></tr>
						<tr><td>
						<input id="count_'.$row['id'].'" class="comment-counter" value="'.($global_max_chars - strlen(stripslashes($row['post']))).'" disabled="disabled">
						<input type="submit" name="submit" style="width: 96px; height: 24px; margin-bottom:12px;" onclick="return validate_comment(\'post_'.$row['id'].'\', '.$global_min_words.')" value="Edit Post">
						</td></tr>
						</table>
					</form>'; }
		echo '
				</div>
			</div>
		</div>';
	}
	echo '
	</div>
	<div class="paginator">
		<div id="paginator">';
	$misc = new misc();
	print '
			'.$misc->pagination($_GET['page'],$_GET['s'],$row['id'],$limit,$page_limit,$numrows,$_GET['pid'],$_GET['tags']);
	echo '
		</div>
		<br>';
	$query = "SELECT locked FROM $forum_topic_table WHERE id='$id' LIMIT 1";
	$result = $db->query($query) or die(mysql_error());
	$row = $result->fetch_assoc();
	echo '
		<div id="footer">
			';
	print (isset($_GET['post']) && $_GET['post'] != "") ? '<a href="index.php?page=forum&amp;s=view&amp;id='.$id.'">View Topic</a> | ' : '';
	print ($row['locked'] == false) ? '<a href="#" onclick="showHide(\'reply\'); return false;">New Reply</a> | ' : '';
	print '<a href="'.$site_url.'/index.php?page=forum&amp;s=list">List</a> | <a href="index.php?page=forum&amp;s=add">New Topic</a> | <a href="'.$site_url.'index.php?page=help&amp;topic=forum">Help</a>';
	if($row['locked'] == false) 
	{
		if($user->gotpermission('lock_forum_topics'))
			print ' | <a href="index.php?page=forum&amp;s=edit&amp;lock=true&amp;id='.$id.'&amp;pid='.$page.'">Lock topic</a>';
	}
	else
	{	
		if($user->gotpermission('lock_forum_topics'))
			print ' | <a href="index.php?page=forum&amp;s=edit&amp;lock=false&amp;id='.$id.'&amp;pid='.$page.'">Unlock topic</a>';
	}		
	if($row['locked'] == false)
	{
		echo '
		</div>
		<br><br>
		<center>
		<form method="post" action="index.php?page=forum&amp;s=add&amp;t=post&amp;pid='.$id.'" style="display:none" id="reply">
			<table>
			<tr><td>
				<h5>New Reply</h5><br>
				<textarea id="reply_box" name="post" rows="4" cols="6" style="width: 512px; height: 256px; margin-bottom:12px;" onKeyDown="counter_comment(\'reply_box\', \'reply_count\', '.$global_max_chars.')" onKeyUp="counter_comment(\'reply_box\', \'reply_count\', '.$global_max_chars.')"></textarea>
			</td></tr>
			<tr><td>
				<input type="hidden" name="l" value="'.$limit.'">
			</td></tr>
			<tr><td>
				<input type="hidden" name="conf" id="conf" value="0">
			</td></tr>
			<tr><td>
				<input id="reply_count" class="comment-counter" value="'.$global_max_chars.'" disabled="disabled">
				<input type="submit" name="submit" style="width: 96px; height: 24px; margin-bottom:12px;" onclick="return validate_comment(\'reply_box\', '.$global_min_words.')" value="Post Reply">
			</td></tr>
			</table>
		</form>
		</center>
		<script type="text/javascript">
			document.getElementById(\'conf\').value = 1
		</script>';
	}
?>

	</div>
	</div>
</body>
</html>