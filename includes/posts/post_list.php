<?php
	//number of images/page
	$limit = 20;
	//number of pages to display. number - 1. ex: for 5 value should be 4
	$page_limit = 10;
	$cache = new cache();
	$post = new post();
	$domain = $cache->select_domain();
	if(isset($_GET['tags']) && $_GET['tags'] != "all" && $_GET['tags'] != "")
	$lozerisdumb = "/ ".str_replace('_',' ',htmlentities(stripslashes($_GET['tags']), ENT_QUOTES, 'UTF-8'));
	require "includes/header.php";
?>

	<div id="content">
	<div id="post-list">
		<div class="sidebar">
			<div class="sidebar2">
			<h5>Search</h5>
			<center>
			<div class="sidebar3"><br>
			<form action="index.php?page=search" method="post">
			<input id="tags" name="tags" class="search_list" type="text" value="<?php if(isset($_GET['tags']) && $_GET['tags'] != "all"){ echo htmlentities(stripslashes($_GET['tags']), ENT_QUOTES, 'UTF-8');} ?>"><br>
			<input name="commit" class="secondary_search" type="submit" value="Search">
			</form>
			</div>
			</center>
			</div>
			<br>
			<div class="space" id="blacklisted-sidebar" style="display: none;">
			<h5>Filter</h5>
			<br>
			<ul>
			<li>
			<b>
			<a href="#" onclick="showHideIgnored('0','pi'); return false;">Hidden Posts</a>
			<span id="blacklist-count" class="post-count"></span>
			</b>
			</li>
			</ul>
			<br>
			</div>
			<div id="tag_list">
			<h5>Tags</h5>
			<br>
<?php
$chicken_error_message =
'			</div>
		</div>
		<div class="content">
		<div>
		<h1>Nobody here but us chickens!</h1>';
	if(isset($_GET['pid']) && $_GET['pid'] != "" && is_numeric($_GET['pid']) && $_GET['pid'] >= 0)
		$page = $db->real_escape_string($_GET['pid']);
	else
		$page = 0;
	$search = new search();
	if(!isset($_GET['tags']) || isset($_GET['tags']) && $_GET['tags'] == "all" || isset($_GET['tags']) && $_GET['tags'] == "")
	{
		$query = "SELECT pcount, last_update FROM $post_count_table WHERE access_key='posts'";
		$result = $db->query($query);
		$row = $result->fetch_assoc();
		$numrows = $row['pcount'];
		$date = date("Ymd");		
		if($row['last_update'] < $date)
		{
		$query = "SELECT COUNT(id) FROM $post_table WHERE parent = '0'";
		$result = $db->query($query);
		$row = $result->fetch_assoc();
		$numrows = $row['COUNT(id)'];
		$query = "UPDATE $post_count_table SET pcount='".$row['COUNT(id)']."', last_update='$date' WHERE access_key='posts'";
		$db->query($query);			
		}
	}
	else
	{
		$tags = stripslashes(mb_trim(htmlentities($_GET['tags'], ENT_QUOTES, 'UTF-8')));		
		$query = $search->prepare_tags($tags);
		$result = $db->query($query) or die($chicken_error_message);
		$numrows = $result->num_rows;
		$result->free_result();
	}
	if($numrows == 0)
		echo $chicken_error_message;
	else
	{
		echo '			<ul id="tag-sidebar">';
		if(!isset($_GET['tags']) || isset($_GET['tags']) && $_GET['tags'] == "all" || isset($_GET['tags']) && $_GET['tags'] == "")
			$query = "SELECT * FROM $post_table WHERE parent = '0' ORDER BY id DESC LIMIT $page, $limit";
		else
		{
			$query = $query." LIMIT $page, $limit";			
		}
		$gtags = array();
		$images = '';
		$filter = '';
		$tcount = 0;
		$result = $db->query($query);
		//Limit main tag listing to 40 tags. Keep the loop down to the minimum really.
		while($row = $result->fetch_assoc())
		{
			$tags = mb_trim($row['tags']);
			$imagep = "normal";
			if($tcount <= 40)
			{	
				$ttags = explode(" ",$tags);
				foreach($ttags as $current)
				{
					if($current != "" && $current != " ")
					{
						$gtags[$current] = $current;
						++$tcount;
					}
				}
			}
			if($row['approved'] == false) $imagep = "pending";
			if($post->has_children($row['id'])) $imagep = "has-children";
			if(isset($row['parent']) && $row['parent'] != '' && $row['parent'] != ' ' && $row['parent'] != '0') $imagep = "has-parent";
			if($row['spam'] == true) $imagep = "flagged";
				$images .= '
				<span id="p'.$row['id'].'" class="thumb">
				<a href="index.php?page=post&amp;s=view&amp;id='.$row['id'].'">
				<img class="'.$imagep.'" src="'.$site_url.$thumbnail_folder.'/'.$row['directory'].'/'.$thumbnail_prefix.$row['image'].'" alt="Post #'.$row['id'].'" border="0" title="'.$row['tags'].' Rating:'. $row['rating'].' Score:'.$row['score'].' User:'.$row['owner'].'">
				</a>
				</span>';
				$filter .= "{$row['id']}:{'tags':['".str_replace(" ","','",$tags)."'],'rating':'{$row['rating']}','score':'{$row['score']}','user':'".htmlentities($row['owner'], ENT_QUOTES, 'UTF-8')."'},";
			++$tcount;
		}
		$result->free_result();	
		if(isset($_GET['tags']) && $_GET['tags'] != "" && $_GET['tags'] != "all")
			$ttags = stripslashes(htmlentities($_GET['tags'],ENT_QUOTES,"UTF-8"));
		else
			$ttags = "";
		asort($gtags);
		/*Tags have been sorted in ascending order
		Let's now grab the index count from database
		Needs to be escaped before query is sent!
		URL Decode and entity decode for the links
		*/
		foreach($gtags as $current)
		{
			$query = "SELECT index_count, type FROM $tag_index_table WHERE tag='".$db->real_escape_string($current)."'";
			$result = $db->query($query);
			$row = $result->fetch_assoc();
			$t_decode = urlencode(html_entity_decode($ttags,ENT_QUOTES,"UTF-8"));
			$c_decode = urlencode(html_entity_decode($current,ENT_QUOTES,"UTF-8"));
			echo '
				<li class="tag-type-'.$row['type'].'">
				<a href="index.php?page=post&amp;s=list&amp;tags='.$t_decode."+".$c_decode.'">+</a>
				<a href="index.php?page=post&amp;s=list&amp;tags='.$t_decode."+-".$c_decode.'">&ndash;</a>
				<a href="index.php?page=post&amp;s=list&amp;tags='.$c_decode.'">'.str_replace('_',' ',$current).'</a>
				<span class="post-count">'.$row['index_count'].'</span>
				</li>';
		}
		echo '
			</ul>
			<br><br>
			</div>
		</div>
		<div class="content">
			<div>';
			$images .= '
			</div>
			<div id="paginator">
				';
		//echo out image results and filter javascript
		echo $images;

		//Pagination function. This should work for the whole site... Maybe.
		$misc = new misc();
		$tags = stripslashes(mb_trim(htmlentities($_GET['tags'], ENT_QUOTES, 'UTF-8')));		
		echo $misc->pagination($_GET['page'],$_GET['s'],$id,$limit,$page_limit,$numrows,$_GET['pid'],$tags);
	}
?>

			</div>
			<script type="text/javascript">
				var pignored = {};
				filterPosts({<?php echo substr($filter, 0, -1) ?>});
			</script>
			<div id="footer">
				<a href="index.php?page=post&amp;s=list">List</a> | <?php $user = new user(); if($user->gotpermission('admin_panel')) { echo '<a href="index.php?page=post&amp;s=mass_edit">Edit</a> | '; } ?><a href="index.php?page=post&amp;s=add">Upload</a> | <a href="index.php?page=post&amp;s=random">Random</a> | <a href="index.php?page=help&amp;topic=posts">Help</a>
			</div>
			<br><br>
		</div>
	</div>
	</div>
</body>
</html>