<?php
	header("Cache-Control: store, cache");
	header("Pragma: cache");
	require "includes/header.php";
	print '	<div id="content">';
	//die("Maintenance mode. please try again in 1 hour.");
	ignore_user_abort(1);
	$post = new post();
	$misc = new misc();
	$userc = new user();
	$ip = $db->real_escape_string($_SERVER['REMOTE_ADDR']);	
	if($userc->banned_ip($ip))
	{
		print '<div class="error-notice">Action failed: '.$row['reason'].'</div><br>';
		exit;
	}	
	if(!$userc->check_log())
	{
		if(!$anon_can_upload)
			$no_upload = true;
	}
	else
	{
		if(!$userc->gotpermission('can_upload'))
			$no_upload = true;
	}
	if($no_upload)
	{
		print '<div class="error-notice">You do not have permission to upload.</div><br>';
		exit;
	}
	if(isset($_POST['submit']))
	{
		$image = new image();
		$uploaded_image = false;
		$parent = '';
		$error = '';
		if(empty($_FILES['upload']) && isset($_POST['source']) && $_POST['source'] != "" && substr($_POST['source'],0,4) == "http" || $_FILES['upload']['error'] != 0 && isset($_POST['source']) && $_POST['source'] != "" && substr($_POST['source'],0,4) == "http")
		{
			$iinfo = $image->getremoteimage($_POST['source']);
			if($iinfo === false)
				$error = $image->geterror();
			else
				$uploaded_image = true;
		}
		else if(!empty($_FILES['upload']) && $_FILES['upload']['error'] == 0)
		{
			$iinfo = $image->process_upload($_FILES['upload']);
			if($iinfo === false)
				$error = $image->geterror();
			else
				$uploaded_image = true;
		}
		else
			print '<div class="error-notice">Generic error. This error will be given if not image was specified, or a required field did not exist, or the required data was not included.</div><br>';
		if($uploaded_image == true)
		{
			$iinfo = explode(":",$iinfo);
			$tclass = new tag();
			$misc = new misc();
			$ext = strtolower(substr($iinfo[1],-4));
			$source = $db->real_escape_string(htmlentities($_POST['source'],ENT_QUOTES,'UTF-8'));
			$title = $db->real_escape_string(htmlentities($_POST['title'],ENT_QUOTES,'UTF-8'));
			$tags = stripslashes(mb_strtolower(str_replace('%','',htmlentities(str_replace("\r\n"," ",$_POST['tags']), ENT_QUOTES, 'UTF-8')), 'UTF-8'));
			$ttags = array_filter(explode(' ',$tags));
			$tag_count = count($ttags);		
			if($tag_count == 0)
				$ttags[] = "tagme";
			if($tag_count < 5 && strpos(implode(" ",$ttags),"tagme") === false)
				$ttags[] = "tagme";
			$temp_array = array();
			foreach($ttags as $current)
			{
				$params = explode(':', $current);
				if (count($params) > 1){
				if($user->gotpermission('edit_tags'))
				$cmd = $db->real_escape_string($params[0]);
				else
				$cmd = $post->tag_type($current);
				$param = $params[1];
				$alias = $tclass->alias($param);
				if($alias !== false) $param = $alias;
				/* Only accept x:y and use x, y as valid
				If user use x:y:z only x and y is valid */
				switch($cmd){
				case 'tag':
				case 'general':
					$tag_type = "general";
					$current = $param;
					$tclass->updatetag($current, $tag_type);
					break;
				case 'char':
				case 'character':
					$tag_type = "character";
					$current = $param;
					$tclass->updatetag($current, $tag_type);
					break;
				case 'circle':
					$tag_type = "circle";
					$current = $param;
					$tclass->updatetag($current, $tag_type);
					break;
				case 'art':
				case 'artist':
					$tag_type = "artist";
					$current = $param;
					$tclass->updatetag($current, $tag_type);
					break;
				case 'faults':
					$tag_type = "faults";
					$current = $param;
					$tclass->updatetag($current, $tag_type);
					break;
				case 'copy':
				case 'copyright':
					$tag_type = "copyright";
					$current = $param;
					$tclass->updatetag($current, $tag_type);
					break;
/*				case 'pool':
					# Check if sequence was given.
					$sequence = (isset($params[2])) ? $params[2] : null;
					# Insert post into pool.
					$Pool->addPost($post_id, $param, $sequence);
					unset($tags_array[$key]);
					break;
				case '-pool':
					# Remove post from pool.
					if (is_numeric($param)){
					$Pool->removePost($post_id, $param);
					break;
					} else {
					unset($tags_array[$key]);
					break;
					}
*/				case 'parent':
					$parent = $param;
					if(!is_numeric($parent))
					unset($parent);
					unset($current);	
					break;
				case 'rating':
					unset($current);	
					break;
				default:
					break;
				} }
				if($current != "" && $current != " ")
				{
					$alias = $tclass->alias($current);
					if($alias !== false) $current = $alias;
					$temp_array[] = $current;
				}
			}
			$ttags = $temp_array;
			$tags = implode(" ",$ttags);
			foreach($ttags as $current)
			{
				if($current != "" && $current != " ")
				$ttags = $tclass->filter_tags($tags,$current, $ttags);
			}
			asort($ttags);
			foreach($ttags as $current)
				$tclass->addindextag($current);
			$tags = $db->real_escape_string(implode(" ",$ttags));
			$tags = mb_trim(str_replace("  ","",$tags));			
			if(substr($tags,0,1) != " ")
				$tags = " $tags";
			if(substr($tags,-1,1) != " ")
				$tags = "$tags ";
			$rating = $db->real_escape_string($_POST['rating']);
			if($rating == "e")
				$rating = "Explicit";
			else if($rating == "q")
				$rating = "Questionable";
			else
				$rating = "Safe";
			if($userc->check_log())
				$user = $checked_username;
			else
				$user = "Anonymous";
			$approved = $userc->gotpermission('approve_posts');
			$ip = $db->real_escape_string($_SERVER['REMOTE_ADDR']);
			$image_link = "./".$image_folder."/".$iinfo[0]."/".$iinfo[1];
			$isinfo = getimagesize($image_link);
			$query = "INSERT INTO $post_table(approved, creation_date, hash, crc32, sha1, image, title, owner, height, width, size, ext, rating, tags, directory, source, active_date, ip) VALUES('$approved','".mktime()."', '".md5_file($image_link)."', '".hash_file("crc32b", $image_link)."', '".sha1_file($image_link)."', '".$iinfo[1]."', '$title', '$user', '".$isinfo[1]."', '".$isinfo[0]."', '".filesize($image_link)."', '$ext', '$rating', '$tags', '".$iinfo[0]."', '$source', '".date("Ymd")."', '$ip')";
			if(!is_dir("./".$thumbnail_folder."/".$iinfo[0]."/"))
				$image->makethumbnailfolder($iinfo[0]);
			if(!$image->thumbnail($iinfo[0]."/".$iinfo[1]))
				print '<div class="error-notice">Thumbnail generation failed! A serious error occured and the image could not be resized.</div><br>';
			if(!$db->query($query))
			{
				print '<div class="error-notice">Failed to upload image: '; print $query; print '</div><br>';
				unlink($image_link); unset($image_link);
				$image->folder_index_decrement($iinfo[0]);
				$ttags = explode(" ",$tags);
				foreach($ttags as $current)
					$tclass->deleteindextag($current);
			}
			else
			{
				$query = "SELECT id, tags FROM $post_table WHERE hash='".md5_file('./'.$image_folder.'/'.$iinfo[0]."/".$iinfo[1])."' AND image='".$iinfo[1]."' AND directory='".$iinfo[0]."'  LIMIT 1";
				$result = $db->query($query);
				$row = $result->fetch_assoc();
				$tags = $db->real_escape_string($row['tags']);
				$date = date("Y-m-d H:i:s");
				$query = "INSERT INTO $tag_history_table(id,tags,user_id,updated_at,ip) VALUES('".$row['id']."','$tags','$checked_user_id','$date','$ip')";
				$db->query($query) or die($db->error);				
				if($parent != '' && is_numeric($parent))
				{
					$parent_check = "SELECT COUNT(*) FROM $post_table WHERE id='$parent'";
					$pres = $db->query($parent_check);
					$prow = $pres->fetch_assoc();
					if($prow['COUNT(*)'] > 0)
					{
						$temp = "INSERT INTO $parent_child_table(parent,child) VALUES('$parent','".$row['id']."')";
						$db->query($temp);
						$temp = "UPDATE $post_table SET parent='$parent' WHERE id='".$row['id']."'";
						$db->query($temp);
					}
				}				
				$query = "SELECT id FROM $post_table WHERE id < ".$row['id']." ORDER BY id DESC LIMIT 1";
				$result = $db->query($query);
				$row = $result->fetch_assoc();
				$query = "UPDATE $post_count_table SET last_update='20110101' WHERE access_key='posts'";
				$db->query($query);
				$query = "UPDATE $user_table SET post_count = post_count+1 WHERE id='$checked_user_id'";
				$db->query($query);
				print '<div class="success-notice">Image added.</div><br>';
			}
		}
	}
	if (isset($error) && $error != '' && $error != ' ')
	print '<div class="error-notice">'.$error.'</div><br>';
?>

	<div style="margin-bottom: 2em;">
	<h3>Upload Guidelines</h3>
	<p>Please keep the following guidelines in mind when uploading something. Violating these rules will result in a ban.</p>
	<h4>Read <a href="tos.php">Terms of Service</a> under prohibited content!</h4>
	<br>
	<ul>
	<li>Read our terms of service before continuing. This has been updated to reflect recent changes in our policy.</li>
	<li>Rate images appropriately. If you wouldn't look at it in front of your family, then it is probably not safe.</li>
	<li>Our site is mainly anime related. <b>Stick to the status quo.</b></li>
	<li>Do not upload toons, doujinshi, images with compression artifacts or obnoxious watermarks.</li>
	<li>Tag your images that you are going to upload with <b>at least <i>5</i> tags!</b> Tag correctly or you will be banned.</li>
	</ul>
	<b><a href="http://iqdb.org" target="_BLANK">Check your image before you upload.</a> If it exists under Danbooru, it likely exists here!</b>
	</div>
	<form method="post" action="index.php?page=post&amp;s=add" enctype="multipart/form-data">
	<table class="form">
        <tfoot>
	<tr>
	<td></td>
	<td>
	<input type="submit" name="submit" style="width: 64px; height: 24px; margin-top:8px;" value="Upload">
	</td>
	</tr>
	</tfoot>
	<tbody>
	<tr style="height: 32px;">
	<th width="64px"><label>Rating</label></th>
	<td width="85%">
	<input type="radio" name="rating" value="e"> Explicit
	<input type="radio" name="rating" value="q" checked> Questionable
	<input type="radio" name="rating" value="s"> Safe
	</td>
	</tr>
	<tr>
	<th><label for="post_file">File</label></th>
	<td><input id="post_file"type="file" name="upload" size="64" style="margin-bottom:8px;"></td>
	<tr>
	<th><label for="post_source">Source</label></th>
	<td><input id="post_source" type="text" name="source" style="width: 512px; height: 24px; margin-bottom:8px;" value=""></td>
	</tr>
	<tr>
	<th><label for="tags">Title</label></th>
	<td><input id="title" type="text" name="title" style="width: 512px; height: 24px; margin-bottom:8px;" value=""></td>
	</tr>
	<tr>
	<th><label for="tags">Tags</label></th>
	<td><textarea id="tags" name="tags" cols="60" rows="2" tabindex="3" style="max-width: 512px; width: 512px; height: 64px; margin-bottom:8px;"></textarea>Separate tags with spaces. (Ex: green_eyes purple green_hair)</td>
	</tr>
	<tr>
	<th><label>My Tags</label></th>
	<td>
	<div id="my-tags" style="width:512px; margin-bottom:12px;">
	<a href="index.php?page=account&amp;s=options">Edit</a>
	</div>
	</td>
	</tr>
	</table>
	</form>
	<script type="text/javascript">
	tempMyTags()
	</script>
</div>
</body>
</html>