<?php

	$user = new user();
	$post = new post();
	$misc = new misc();
	
	$id = (int)$db->real_escape_string($_GET['id']);
	$ip = $db->real_escape_string($_SERVER['REMOTE_ADDR']);	

	$post_data = $post->show($id);
	
	if($post_data == "" || is_null($post_data))
	{
		header('Location: index.php?page=post&s=list');
		exit;
	}
	
	$prev_next = $post->prev_next($id);
	
	$lozerisdumb = "- Checksum - Post #".$id;
	
	$pnext = $prev_next['1'];
	$pback = $prev_next['0'];

	require "includes/header.php";	

	print '	<div id="content">
	<div id="report">
	<div class="report_header">
		<table style="width: 1000px; text-align:center; margin:auto;">
			<tr>
				<td height="72px" style="vertical-align: middle">
					<h2>Checksum Verification Report for Post #'.$post_data['id'].'</h2>
				</td>
				<td width="256px" style="text-align:right; vertical-align: middle;">
					<form action="index.php?page=search" method="post">
						<input id="stags" name="tags" class="search_list" type="text" style="width:200px; height: 16px; font-size: 13px; padding:8px; vertical-align: middle; background: #333; border: 1px solid #666;">
						<input name="commit" class="secondary_search" type="submit" style="width:64px; display:none;" value="Search">
					</form>
				</td>
			</tr>
		</table>
	</div>
	<div class="report_body" style="overflow:hidden"><br>
		<div style="width:200px; text-align:center; float:left;"><br>
			<p style="margin-bottom:48px;">
			<a href="index.php?page=post&amp;s=view&amp;id='.$post_data['id'].'">
				<img src="'.$site_url.$thumbnail_folder.'/'.$post_data['directory'].'/'.$thumbnail_prefix.$post_data['image'].'" alt="Post #'.$row['id'].'" border="0" title="'.$post_data['tags'].' Rating:'. $post_data['rating'].' Score:'.$post_data['score'].' User:'.$post_data['owner'].'" style="max-width:192px">
			</a>
			</p>
			<p style="margin-bottom:48px;">
			<a href="index.php?page=post&amp;s=checksum&amp;id='.$post_data['id'].'">
				<img src="http://chart.apis.google.com/chart?cht=qr&amp;chs=128x128&amp;choe=UTF-8&amp;chld=|0&amp;chl='.urlencode($site_url."index.php?page=post&s=checksum&id=".$post_data['id']).'" alt="qrcode" title="QRLink to this report page.">
			</a>
			</p>
		</div>
		<div style="width:824px; text-align:center; float:left; font-size:14px;">';
		
		if($user->banned_ip($ip) || !$user->check_log())
			print '
			<div style="width:800px; margin:auto; margin-bottom:32px;">
				<!-- Received a HTTP/1.1 401 error -->
				<br><h1>Authorization Required</h1><br><hr>
			</div>';
		else
		{
			print '
			<table width="800px" class="highlightable align_center" style="margin:auto; margin-bottom:32px;">
				<tr>
					<th width="24px">ID</th>
					<th width="128px">Hash Algorithm</th>
					<th>Hash Result</th>
					<th width="64px">Check</th>
				</tr>';
			$hash_id = 0;
			$hash_data = "./".$image_folder."/".$post_data['directory']."/".$post_data['image'];
			foreach (hash_algos() as $hash_algo)
			{
				$hash_id++;
				$hash_result = hash_file($hash_algo, $hash_data, false);
				if (strlen($hash_result) < 10)
					$hash_result = strtoupper($hash_result);
				print '
				<tr>
					<th>'.$hash_id.'</th>
					<td>'.$hash_algo.'</td>
					<td title="'.$hash_result.'">';
				if (strlen($hash_result) > 50)
					$hash_result = substr($hash_result, 0, 50).'...';
				print $hash_result.'</td>';
				switch ($hash_algo)
				{
				case 'md5':
					if ($hash_result == $post_data['hash'])
						print '
					<td style="color:#0F0">OK</td>';
					else
						print '
					<td style="color:#F00">ERR</td>';
					break;
				case 'crc32b':
					if (strtolower($hash_result) == $post_data['crc32'])
						print '
					<td style="color:#0F0">OK</td>';
					else
						print '
					<td style="color:#F00">ERR</td>';
					break;
				case 'sha1':
					if ($hash_result == $post_data['sha1'])
						print '
					<td style="color:#0F0">OK</td>';
					else
						print '
					<td style="color:#F00">ERR</td>';
					break;
				default:
					print '
					<td style="color:#00F">N/A</td>';
					break;
				}
				print '
				</tr>';
			}
			print '
			</table>';
		}
		print '
		</div>
		<div style="text-align:center; font-family: Monaco, Monotype; font-size:14px; color: #AAA; margin-top:32px; margin-bottom:24px;" id="copyright_notice">
			File Checksum Reporter 1.0.1 build 2011-09<br>Copyright (C) 2010-2011 &ndash; Raki Saionji.<br>All rights reserved.
		</div>
	</div>
	</div>
	<div id="footer">
		<a href="#" onclick="Report.show(\'report\'); return false;">View Report</a> | <a href="#" onclick="Report.print(\'report\', true); return false;">Print Report</a> | <a href="index.php?page=post&amp;s=view&amp;id='.$post_data['id'].'">View Post</a> | <a href="index.php?page=post&amp;s=list">List</a> | <a href="index.php?page=post&amp;s=add">Upload</a> | <a href="index.php?page=post&amp;s=random">Random</a> | <a href="index.php?page=help&amp;topic=posts">Help</a>
	</div>
	</div>
</body>
</html>';
?>