<?php
	//number of comments/page
	$limit = 10;
	//number of pages to display. number - 1. ex: for 5 value should be 4
	$page_limit = 6;
	//Load required class files. post.class and cache.class
	$user = new user();
	$post = new post();
	$cache = new cache();
	$misc = new misc();
	$domain = $cache->select_domain();
	$id = (int)$db->real_escape_string($_GET['id']);
	$date = date("Ymd");
	//Load post_table data and the previous next values in array. 0 previous, 1 next.
	$post_data = $post->show($id);
	//Check if data exists in array, if so, kinda ignore it.
	if($post_data == "" || is_null($post_data))
	{
		header('Location: index.php?page=post&s=list');
		exit;
	}
	$prev_next = $post->prev_next($id);
	$tags = mb_trim(html_entity_decode($post_data['tags'], ENT_QUOTES, 'UTF-8'));
	$ttags = explode(" ",$tags);
	$rating = $post_data['rating'];
	$lozerisdumb = "- ".str_replace('_',' ',htmlentities($tags, ENT_QUOTES, 'UTF-8'));
	$pnext = $prev_next['1'];
	$pback = $prev_next['0'];
	$query = "UPDATE $post_table SET hit_count=hit_count+1 WHERE id=$id";
	$db->query($query);
	require "includes/header.php";	

print '	<div id="content">
	<div id="post-view">';

	if($post_data['approved'] == false)
		print '
		<div class="status-notice">This post is pending moderator approval.</div><br>';

	if($post_data['spam'] == true)
		print '
		<div class="status-notice">This post was flagged for deletion. Reason: '.$post_data['reason'].'.<br>MD5: '.$post_data['hash'].'</div><br>';

	if($post->has_children($id))
		print '
		<div class="status-notice">This post has <a href="index.php?page=post&amp;s=list&amp;tags=parent:'.$id.'">child posts</a>. Child posts are often minor variations of the parent post (<a href="index.php?page=help&amp;topic=post_relationships">learn more</a>).</div><br>';

	if(isset($post_data['parent']) && $post_data['parent'] != '' && $post_data['parent'] != ' ' && $post_data['parent'] != '0')
		print '
		<div class="status-notice">This post belongs to a <a href="index.php?page=post&amp;s=view&amp;id='.$post_data['parent'].'">parent post</a>. Child posts are often minor variations of the parent post (<a href="/index.php?page=help&amp;topic=post_relationships">learn more</a>).</div><br>';
	print '
		<div class="sidebar">
			<div class="sidebar2">
				<h5>Search</h5>
				<center>
				<div class="sidebar3"><br>
					<form action="index.php?page=search" method="post">
						<input id="stags" name="tags" class="search_list" type="text"><br>
						<input name="commit" class="secondary_search" type="submit" value="Search">
					</form>
				</div>
				</center>
			</div>
			<br>
			<div id="tag_list">
				<h5>Tags</h5>
				<br><ul id="tag-sidebar">';
		foreach($ttags as $current)
		{
			$count = $post->index_count($current);
			$type = $post->tag_type($current);
			print '
					<li class="tag-type-'.$type.'"><a href="index.php?page=post&amp;s=list&amp;tags='.urlencode($current).'">'.str_replace('_',' ',$current).'</a><span class="post-count"> '.$count['index_count'].'</span></li>';
		}
		print '
				</ul><br><br>
			</div>
			<div class="space">
				<h5>Statistics</h5>
				<br><ul>
					<li>ID: <a href="index.php?page=post&amp;s=list&amp;tags=id:'.$post_data['id'].'">'.$post_data['id'].'</a></li>
					<li>Extension: <a href="index.php?page=post&amp;s=list&amp;tags=ext:'.str_replace('.','',$post_data['ext']).'">'.strtoupper(str_replace('.','',$post_data['ext'])).'</a></li>
					<li>CRC32: <a href="index.php?page=post&amp;s=checksum&amp;id='.$post_data['id'].'">'.strtoupper($post_data['crc32']).'</a></li>
					<li>Posted: <a href="index.php?page=post&amp;s=list&amp;tags=date:'.date('Y-m-d',$post_data['creation_date']).'" title="'.date('l, F jS, Y G:i:s T',$post_data['creation_date']).'">'.$misc->date_words($post_data['creation_date']).'</a>
					<br>by <a href="index.php?page=account&amp;s=profile&amp;uname='.$post_data['owner'].'">'.$post_data['owner'].'</a>';
		if($user->gotpermission('is_admin'))
		{ print ' via <a href="index.php?page=post&amp;s=list&amp;tags=ip:'.$post_data['ip'].'">'.$post_data['ip'].'</a>'; }
print '</li>';
if ($user->gotpermission('approve_posts') && isset($post_data['approver']) && $post_data['approver'] != '' && isset($post_data['approved_date']) && $post_data['approved_date'] != '' && isset($post_data['aip']) && $post_data['aip'] != '' && isset($post_data['approved']) && $post_data['approved'] == TRUE) {
print '
					<li>Approved: <a href="#" title="'.date('l, F jS, Y G:i:s T',$post_data['approved_date']).'">'.$misc->date_words($post_data['approved_date']).'</a>
					<br>by <a href="index.php?page=account&amp;s=profile&amp;uname='.$post_data['approver'].'">'.$post_data['approver'].'</a>';
		if($user->gotpermission('is_admin'))
		{ print ' via <a href="#">'.$post_data['aip'].'</a>'; }
		print '</li>'; }
print '
					<li>Size: <a href="index.php?page=post&amp;s=list&amp;tags=width:'.$post_data['width'].'+height:'.$post_data['height'].'">'.$post_data['width'].'x'.$post_data['height'].'</a> (<a href="index.php?page=post&amp;s=list&amp;tags=filesize:'.str_replace(" ", "",$post_data['size']).'">'.$post->formatBytes($post_data['size']).'</a>)';
		if($post_data['source'] == "" or $post_data['source'] == " ")
			print '';
		else if(substr($post_data['source'],0,7) == "http://" || substr($post_data['source'],0,6) == "ftp://")
		{
		$source = explode('://',$post_data['source']);
		print '
					<li>Source: <a href="'.stripslashes($post_data['source']).'" target="_blank">'.substr(stripslashes($source[1]),0,20).'...</a></li>';
		}
		else print '
					<li>Source: <a href="'.$domain.$image_folder.'/'.$post_data['directory'].'/'.$post_data['image'].'">'.stripslashes($post_data['source']).'</a></li>';
		print '
					<li>Rating: <a href="index.php?page=post&amp;s=list&amp;tags=rating:'.strtolower($post_data['rating']).'">'.$post_data['rating'].'</a></li>
					<li>Score: <span id="psc'.$id.'">'.$post_data['score'].'</span> (Vote <a href="#" onclick="post_vote(\''.$id.'\', \'up\'); return false;">Up</a>/<a href="#" onclick="post_vote(\''.$id.'\', \'down\'); return false;">Down</a>)</li>';
		$query = 'SELECT COUNT(*) FROM '.$favorites_table.' WHERE favorite=\''.$post_data['id'].'\'';
		$result = $db->query($query);
		$row = $result->fetch_assoc();
		print '
					<li>Favorited by: <span id="fav'.$id.'">'.$row['COUNT(*)'].' user';
		if($row['COUNT(*)'] > 1) print 's'; print '</span></li>';
print '
					<li>Total views: '.number_format($post_data['hit_count']).'
				</ul><br>
			</div>
			<div class="space">
				<h5>Options</h5>
				<br><ul>
					<li><a href="#" onclick="showHide(\'edit_form\'); return false;">Edit</a></li>
';
		if($user->gotpermission('delete_posts'))
print '					<li><a href="#" onclick="if(confirm(\'Are you sure you want to delete this post?\')){document.location=\'public/remove.php?id='.$id.'&amp;removepost=1\';}; return false;">Delete</a></li>
';
		if($user->gotpermission('approve_posts'))
		if($post_data['approved'] == false)
print '					<li id="pad'.$id.'"><a href="#" onclick="papprv(\''.$id.'\'); return false;">Approve</a></li>';
else print '					<li id="pad'.$id.'"><a href="#" onclick="punapprv(\''.$id.'\'); return false;">Unapprove</a></li>';
print '
					<li><a href="'.$site_url.$image_folder.'/'.$post_data['directory'].'/'.$post_data['image'].'">Original Image</a></li>
';
		if($post_data['spam'] == false)
print '					<li id="pfd'.$id.'"><a href="#" onclick="pflag(\''.$id.'\'); return false;">Flag for deletion</a></li>';
		/*<form id="report_form" method="post" action="./public/report.php?type=post&amp;rid='.$id.'" style="display: none;">Reason for deletion:<br><input type="text" name="reason" value=""><input type="submit" name="submit" value="" style="display: none;"></form></li>
		else
			print '<li><b>Post reported</b></li>'; */
		if($user->gotpermission('alter_notes'))
		print '
					<li><a href="#" onclick="Note.create('.$id.'); return false;">Add note</a></li>';
print '
					<li><a href="#" onclick="addFav(\''.$id.'\'); return false;">Add to favorites</a></li>
				</ul><br>
			</div>
			<div class="space">
				<h5>History</h5>
				<br><ul>
					<li><a href="index.php?page=history&amp;type=tag_history&amp;id='.$id.'">Tags</a></li>
					<li><a href="index.php?page=history&amp;type=page_notes&amp;id='.$id.'">Notes</a></li>
				</ul><br>
			</div>
			<div class="space">
				<h5>Related</h5>
				<br><ul>';
		if (isset($pback) && $pback != '') print '
					<li><a href="index.php?page=post&amp;s=view&amp;id='.$pback.'">Previous</a></li>';
		if (isset($pnext) && $pnext != '') print '
					<li><a href="index.php?page=post&amp;s=view&amp;id='.$pnext.'">Next</a></li>';
print '
					<li><a href="index.php?page=post&amp;s=random">Random</a></li>
					<li><a href="http://iqdb.org/?url='.urlencode($site_url.$thumbnail_folder.'/'.$post_data['directory'].'/'.$thumbnail_prefix.$post_data['image']).'">Similar</a>
				</ul><br>
			</div>
		</div>';
print '
		<div class="content" id="right-col">
			<div>
				<div id="note-container">';
		$note_data = $post->get_notes($id);
		while($retme = $note_data->fetch_assoc())
		{
print '
				<div class="note-box" style="width: '.$retme['width'].'px; height: '.$retme['height'].'px; top: '.$retme['y'].'px; left: '.$retme['x'].'px; display: block;" id="note-box-'.$retme['id'].'">
					<div class="note-corner" id="note-corner-'.$retme['id'].'"></div>
				</div>
				<div style="display: none; top: '.($retme['width']+$retme['y']+5).'px; left: '.$retme['x'].'px;" class="note-body" id="note-body-'.$retme['id'].'" title="Click to edit">'.stripslashes($retme['body']).'</div>';
		}
print "
				<script type=\"text/javascript\">
					image = {'domain':'$domain', 'width':'{$post_data['width']}', 'height':'{$post_data['height']}','dir':'{$post_data['directory']}', 'img':'{$post_data['image']}', 'base_dir':'$image_folder', 'sample_dir':'samples', 'sample_width':'0', 'sample_height':'0'};	
				</script>";
		print '
				<img id="image" alt="'.$post_data['tags'].'" src="'.$domain.$image_folder.'/'.$post_data['directory'].'/'.$post_data['image'].'" onclick="Note.toggle();"><br>
				<p id="note-count"></p>
				<script type="text/javascript">
					Note.post_id = '.$id.';';
		$notes = '';
		$note_data = $post->get_notes($id);
		while($retme = $note_data->fetch_assoc())		
		print '
					Note.all.push(new Note('.$retme['id'].', false));';
		print '
					Note.updateNoteCount();
					Note.show();
				</script>';
		print '
				</div>
				<br>
				<h4>
				<a href="#" onclick="showHide(\'share_form\'); return false;">Share</a> | <a href="#" onclick="showHide(\'edit_form\'); return false;">Edit</a> | <a href="#" onclick="showHide(\'comment_form\'); return false;">Respond</a>
				</h4>
				<br><br>';
		print '
					<table id="share_form" width="800px" style="display:none">
						<tr>
						<td width="140px" height="150px" style="vertical-align: middle">
						<a href="'.$site_url.'index.php?page=post&amp;s=view&amp;id='.$post_data['id'].'">
						<img src="http://chart.apis.google.com/chart?cht=qr&amp;chs=128x128&amp;choe=UTF-8&amp;chld=|0&amp;chl='.urlencode($site_url.'index.php?page=post&s=view&id='.$post_data['id']).'" alt="qrcode">
						</a>
						</td>
						<td style="vertical-align: middle">
						<p>
						Permalink:<br>
						<input readonly value="'.htmlentities($domain.$image_folder.'/'.$post_data['directory'].'/'.$post_data['image'], ENT_QUOTES, 'UTF-8').'" style="width: 100%; height: 24px; margin-bottom:12px;" type="text">
						</p>
						<p>
						Code (XHTML 1.0 Transitional):<br>
						<input readonly value="'.htmlentities('<a href="'.$site_url.'index.php?page=post&s=view&id='.$post_data['id'].'"><img src="'.$domain.$image_folder.'/'.$post_data['directory'].'/'.$post_data['image'].'" alt="'.$site_url3.' - Post #'.$post_data['id'].'" /></a>', ENT_QUOTES, 'UTF-8').'" style="width: 100%; height: 24px; margin-bottom:12px;" type="text">
						</p>
						</td>
						</tr>
					</table>
				'; ?>

				<form method="post" action="./public/edit_post.php" id="edit_form" name="edit_form" style="display:none">
					<table width="800px">
					<tr><td>
					<div style="margin-bottom:12px;">Rating<br>
						<input type="radio" name="rating" <?php if($post_data['rating'] == "Explicit"){ print 'checked'; } ?> value="e">Explicit
						<input type="radio" name="rating" <?php if($post_data['rating'] == "Questionable"){ print 'checked'; } ?> value="q">Questionable
						<input type="radio" name="rating" <?php if($post_data['rating'] == "Safe"){ print 'checked'; } ?> value="s">Safe
					</div>
					</td></tr>
<?php		if($post_data['parent'] == "0")
			$post_data['parent'] = "";
		print '
					<tr><td>Title<br>
						<input type="text" name="title" style="width: 100%; height: 24px; margin-bottom:12px;" id="title" value="'.stripslashes($post_data['title']).'">
					</td></tr>
					<tr><td>Parent<br>
						<input type="text" name="parent" style="width: 100%; height: 24px; margin-bottom:12px;" value="'.$post_data['parent'].'">';
		/* </td></tr><tr><td>Next Post<br>
		<input type="text" name="next_post" style="width: 256px; height: 24px;" id="next_post" value="'.$prev_next['1'].'">
		</td></tr><tr><td>Previous Post<br>
		<input type="text" name="previous_post" style="width: 256px; height: 24px;" id="previous_post" value="'.$prev_next['0'].'"> */
		print '
					</td></tr>
					<tr><td>Source<br>
						<input type="text" name="source" style="width: 100%; height: 24px; margin-bottom:12px;" id="source" value="'.stripslashes($post_data['source']).'">
					</td></tr>
					<tr><td>Tags<br>
						<textarea id="tags" name="tags" style="resize:vertical; width: 100%; height: 128px; margin-bottom:12px;" cols="40" rows="5">'.$tags.'</textarea>
					</td></tr>
					<tr><td>My Tags<br>
						<div id="my-tags" style="width:100%; margin-bottom:12px;">
							<a href="index.php?page=account&amp;s=options">Edit</a>
						</div>
					</td></tr>
					<tr><td>
						<input type="hidden" name="pconf" id="pconf" value="0">
						<div style="width:100%; margin-bottom:12px;">Recent Tags<br>
							'.$post_data['recent_tags'].'
						</div>
					</td></tr>
					<tr><td>
						<input type="hidden" name="id" value="'.$id.'">
					</td></tr>
					<tr><td>
						<input type="submit" name="submit" value="Save changes">
					</td></tr>
					</table>
				</form>
				<script type="text/javascript">
					tempMyTags();
					$(\'pconf\').value=1;			
					var posts = {}; posts['.$id.'] = {}; posts['.$id.'].comments = {}; posts['.$id.'].ignored = {}; var cthreshold = parseInt(Cookie.get(\'comment_threshold\')) || 0; var users = Cookie.get(\'user_blacklist\').split(/[, ]|%20+/g);
				</script>
				<br><br>';

	$got_permission = $user->gotpermission('delete_comments');
	if(isset($_GET['pid']) && is_numeric($_GET['pid']) && $_GET['pid']> "0")
	{
		$pid = ceil($_GET['pid']);
		$page = $pid;
	}
	else
	{
		$page = 0;
		$pid = 0;
	}
		
		//Pagination starts here with dependencies. No need to change this usually... :3
		$comment = new comment();
		$count = $comment->count($id,$_GET['page'],$_GET['s']);
		print "$count ";
		if($count> 1)
			print "comments";
		else
			print "comment";
		print '
				<a href="#" id="ci" onclick="showHideIgnored('.$id.',\'ci\'); return false;">(0 hidden)</a>
				<br>';
		//List comments... Could this be better off as a function to use here and on the comment list page? :S
		$query = "SELECT SQL_NO_CACHE id, comment, user, posted_at, score, spam FROM $comment_table WHERE post_id='$id' ORDER BY posted_at ASC LIMIT $page, $limit";
		$result = $db->query($query);
		$ccount = 0;
		while($row = $result->fetch_assoc())
		{
			print '
				<div id="c'.$row['id'].'" style="display:inline;">
					<br>
					<b><a href="index.php?page=account&amp;s=profile&amp;id='.$row['user'].'">'.$row['user'].'</a></b> <span style="color:#aaa;">&raquo; #'.$row['id'].'</span>
					<br>
					<b>Posted on '.date('Y-m-d H:i:s',$row['posted_at']).' Score: <a id="sc'.$row['id'].'">'.$row['score'].'</a>
					(Vote <a href="#" onclick="vote(\''.$id.'\', \''.$row['id'].'\', \'up\'); return false;">Up</a>/<a href="#" onclick="vote(\''.$id.'\', \''.$row['id'].'\', \'down\'); return false;">Down</a>)
';
?>
					(<?php $row['spam'] == false ? print "<a id=\"rc".$row['id']."\"></a><a href=\"#\" id=\"rcl".$row['id']."\" onclick=\"cflag('".$row['id']."')\">Report as spam</a>)" : print "<b>Reported</b>)"; $got_permission == true ? print '
					(<a href="#" onclick="if(confirm(\'Are you sure you want to delete this comment?\')){document.location=\'public/remove.php?id='.$row['id'].'&amp;removecomment=1&amp;post_id='.$id.'\';}; return false;">Remove</a>)' : print ''; print "
					</b>
					<br>
					<div class=\"comment-body\">
					".stripslashes($misc->swap_bbs_tags($misc->short_url($misc->linebreaks($row['comment']))));?>

					</div>
					<br>
				</div>
				<script type="text/javascript">
					posts[<?php print $id;?>].comments[<?php print $row['id'];?>] = {'score':<?php print $row['score'];?>, 'user':'<?php print str_replace('\\',"&#92;",str_replace(' ','%20',str_replace("'","&#039;",$row['user'])));?>'}
				</script>
<?php
			++$ccount;
		}
		print "
				<br><br>
				<div id='paginator'>";
		
		//Functionized the paginator... Let's see how well this works in practice.
		$misc = new misc();
		print $misc->pagination($_GET['page'],$_GET['s'],$id,$limit,$page_limit,$count,$pid);
		print '
				</div>
				<script type="text/javascript">
					filterComments(\''.$id.'\', \''.$ccount.'\')
				</script>
				<form method="post" action="index.php?page=comment&amp;id='.$id.'&amp;s=save" name="comment_form" id="comment_form" style="display:none">
				<table width="800px">
					<tr><td>
						<textarea name="comment" id="comment" rows="0" cols="0" style="resize:vertical; width: 100%; height: 256px; margin-bottom:12px;" onKeyDown="counter_comment(\'comment\', \'chars_count\', '.$global_max_chars.')" onKeyUp="counter_comment(\'comment\', \'chars_count\', '.$global_max_chars.')"></textarea>
					</td></tr>
					<tr><td style="display:none">
						<input type="checkbox" name="post_anonymous"> Post as anonymous.
					</td></tr>
					<tr><td>
					<div style="margin-top:12px;">
						<input id="chars_count" class="comment-counter" value="'.$global_max_chars.'" disabled="disabled">
						<input class="comment-button" type="submit" name="submit" onclick="return validate_comment(\'comment\', '.$global_min_words.')" value="Post comment">
					</div>
					</td></tr>
					<tr><td>
						<input type="hidden" name="conf" id="conf" value="0">
					</td></tr>
				</table>
				</form>
				<script type="text/javascript">
					document.getElementById(\'conf\').value = 1		
				</script>
';

?>				<br><br>
				<br><br>
				<div id="footer">
					<a href="index.php?page=post&amp;s=list">List</a> | <a href="index.php?page=post&amp;s=add">Upload</a> | <a href="index.php?page=post&amp;s=random">Random</a> | <a href="index.php?page=help&amp;topic=posts">Help</a>
				</div>
				<br><br>
			</div>
		</div>
	</div>
	</div>
</body>
</html>