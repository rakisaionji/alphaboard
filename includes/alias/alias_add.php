<?php
	$user = new user();
	$ip = $db->real_escape_string($_SERVER['REMOTE_ADDR']);	
	if($user->banned_ip($ip))
	{
		print "Action failed: ".$row['reason'];
		exit;
	}	
	if(!$user->gotpermission('add_aliases'))
	{
		header("Location:index.php?page=account&s=login&code=00");
		exit;
	}

	require "includes/header.php";
	print '	<div id="content">';

	if(isset($_POST['tag']) && $_POST['tag'] != "" && isset($_POST['alias']) && $_POST['alias'] != "")
	{
		$tag = str_replace(" ","_",mb_trim(htmlentities($_POST['tag'], ENT_QUOTES, 'UTF-8')));
		$alias = str_replace(" ","_",mb_trim(htmlentities($_POST['alias'], ENT_QUOTES, 'UTF-8')));
		$reason = $db->real_escape_string(mb_trim(htmlentities($_POST['reason'], ENT_QUOTES, 'UTF-8')));
		$query = "SELECT COUNT(*) FROM $alias_table WHERE tag='$tag' AND alias='$alias'";
		$result = $db->query($query);
		$row = $result->fetch_assoc();
		if($row['COUNT(*)'] > 0)
			echo '<div class="info-notice">Tag/Alias combination has already been requested.</div><br>';
		else
		{
			$query = "INSERT INTO $alias_table(tag, alias, reason, status, creator, submitted_date) VALUES('$tag', '$alias', '$reason', '0','$checked_username', '".mktime()."')";
			$db->query($query);
			echo '<div class="success-notice">Tag/Alias combination has been requested.</div><br>';
		}
	}

	echo '
	<form method="post" action="">
	<h1>Add Alias</h1>
	<p>You can suggest a new alias, but it must be approved by an administrator before it is activated.</p>
	<table>
	<tr>
	<th><label for="tag_alias_name">Name</label></th>
	<td><input id="tag_alias_name" type="text" name="alias" style="width: 512px; height: 20px;" value=""></td>
	</tr>
	<tr>
	<th><label for="tag_alias_alias">Alias to</label></th>
	<td><input id="tag_alias_alias" type="text" name="tag" style="width: 512px; height: 20px;" value=""></td>
	</tr>
	<tr>
	<th><label for="tag_alias_reason">Reason</label></th>
	<td><textarea cols="40" style="width: 512px; height: 128px; resize: none;" id="tag_alias_reason" name="reason" rows="2"></textarea></td>
	</tr>
	<tr>
	<td></td>
	<td><input type="submit" name="submit" style="font-size:14px; width: 96px; height: 32px;" value="Submit"></td>
	</tr>
	</table>
	</form>';
?>

	</div>
</body>
</html>