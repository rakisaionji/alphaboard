<?php
	//number of tags/page
	$limit = 40;
	//number of pages to display. number - 1. ex: for 5 value should be 4
	$page_limit = 6;
	$query == '';
	$query_ == '';

	require "includes/header.php";
	print '	<div id="content">';
	
/*	<div>You can suggest a new alias, but they must be approved by an administrator before they are activated.</div>
	<div style="color: #AAA;">An example of how to use this: (Evangelion is the tag and Neon_Genesis_Evangelion is the alias.)</div><br>	*/

	if(isset($_GET['pid']) && $_GET['pid'] != "" && is_numeric($_GET['pid']) && $_GET['pid'] >= 0)
		$page = $db->real_escape_string($_GET['pid']);
	else
		$page = 0;
	$query = "SELECT COUNT(*) FROM $alias_table WHERE status !='2'";
	$result = $db->query($query);
	$row = $result->fetch_assoc();
	$count = $row['COUNT(*)'];	
	$numrows = $count;
	$result->free_result();
	if(isset($_GET['tags']) && $_GET['tags'] != "")
	{	$search = strtolower($db->real_escape_string(str_replace("*",'%',str_replace("?",'_',mb_trim(htmlentities($_GET['tags'], ENT_QUOTES, 'UTF-8'))))));
	if (strpos($search,'%') === false && strpos($search,'_') === false)
		$query_ = "(tag LIKE '%$search%' OR alias LIKE '%$search%')";
	else	$query_ = "(tag LIKE '$search' OR alias LIKE '$search')";
	}
	else	$query_ = "";
	if ($query_ != "") $query_ .= " AND";
	$query = "SELECT * FROM $alias_table WHERE $query_ status != '2' ORDER BY alias ASC LIMIT $page, $limit";
	$result = $db->query($query) or die($db->error);
	print '
	<div style="width: 500px; padding: 0px; margin-bottom: 10px; text-align: left;">
	<form action="" method="get">
		<input name="page" value="alias" type="hidden">
		<input name="s" value="list" type="hidden">
		<input type="text" name="tags" size="30" class="main_search_text" value="'.stripslashes($_GET['tags']).'">
		<input type="submit" class="main_search" value="Search">
	</form>
	</div>
	<table class="highlightable" style="width: 100%;">
		<thead>
		<tr>
		<th width="25%"><b>Alias</b></th>
		<th width="25%"><b>To</b></th>
		<th>Reason</th>
		</tr>
		</thead>
		<tbody>';
	while($row = $result->fetch_assoc())
	{
		if($row['status']=="0")
			$status = "pending-tag";
		else
			$status = "approved-tag";
		echo '
		<tr class="'.$status.'">
		<td><a href="index.php?page=post&amp;s=list&amp;tags='.urlencode(html_entity_decode($row['alias'], ENT_QUOTES, 'UTF-8')).'">'.$row['alias'].'</a></td>
		<td><a href="index.php?page=post&amp;s=list&amp;tags='.urlencode(html_entity_decode($row['tag'], ENT_QUOTES, 'UTF-8')).'">'.$row['tag'].'</a></td>
		<td>'.stripslashes($row['reason']).'</td>
		</tr>';
	}
	echo '
		</tbody>
	</table>
	<br><br>
	<div id="paginator">';
	$misc = new misc();
	print '
		'.$misc->pagination($_GET['page'],$_GET['s'],$id,$limit,$page_limit,$numrows,$_GET['pid'],stripslashes($_GET['tags']));
?>

	</div>
	</div>
</body>
</html>