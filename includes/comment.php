<?php
	$userc = new user();
	$cache = new cache();
	$comment = new comment();
	$ip = $db->real_escape_string($_SERVER['REMOTE_ADDR']);
	if($userc->check_log())
	{
		$user_id = $checked_user_id;
		$user = $checked_username;
	}
	else
	{
		$user = "Anonymous";
		$user_nolog = "Anonymous";
		$query = "SELECT id FROM $user_table WHERE user='$user' LIMIT 1";
		$result = $db->query($query);
		$row = $result->fetch_assoc();
		$user_id = $row['id'];
		$anon_id = $row['id'];
	}
	if(isset($_GET['id']) && is_numeric($_GET['id']))
	{
		$id = $db->real_escape_string($_GET['id']);
		if(isset($_GET['s']) && $_GET['s'] == "edit" && isset($_POST['comment']))
		{
			if($_POST['conf'] != 1)
			{
				header("Location:index.php?page=post&s=view&id=$id");
				exit;
			}
			if($user == "Anonymous")
			{
				header("Location:index.php?page=post&s=view&id=$id");
				exit;	
			}
			$comment->edit($id,$_POST['comment'],$user);
			header("Location:index.php?page=post&s=view&id=$id");
		}
		if(isset($_GET['s']) && $_GET['s'] == "save" && isset($_POST['comment']))
		{
			if($_POST['conf'] != 1 || strlen($_POST['comment']) > $global_max_chars)
			{
				header("Location:index.php?page=post&s=view&id=$id");
				exit;
			}

			if(!$anon_comment && $user_nolog == "Anonymous")
			{
				header('Location: index.php?page=account&s=home');
				exit;
			}
			if(isset($_POST['post_anonymous']) || $_POST['post_anonymous'] == true)
			{
				$user = "Anonymous";
				$query = "SELECT id FROM $user_table WHERE user='$user' LIMIT 1";
				$result = $db->query($query);
				$row = $result->fetch_assoc();
				$user_id = $row['id'];
				$anon_id = $row['id'];
			}
			$comment->add($_POST['comment'],$user,$id,$ip,$user_id);
			header("Location:index.php?page=post&s=view&id=$id");
		}
		else if(isset($_GET['s']) && isset($_GET['cid']) && is_numeric($_GET['cid']) && isset($_GET['vote']))
		{
			$vote = $_GET['vote'];
			$id = $_GET['id'];
			$cid = $_GET['cid'];
			if($user == "Anonymous" && !$anon_vote)
			{
				header('Location: index.php?page=account&s=home');
				exit;
			}
			$comment->vote($cid,$vote,$user,$id,$user_id,$ip);
		}
		else if(isset($_GET['s']) && $_GET['s'] === "view" && isset($_GET['cid']) && is_numeric($_GET['cid']))
		{
			header("Cache-Control: store, cache");
			header("Pragma: cache");
			require "includes/header.php";
			$cid = $db->real_escape_string($_GET['cid']);
			$query = "SELECT post_id, comment, user, posted_at, score FROM $comment_table WHERE id='$cid'";
			$result = $db->query($query);
			$row = $result->fetch_assoc();
			$misc = new misc();
			echo '<a href="index.php?page=post&s=view&id='.$row['post_id'].'">'.$row['post_id'].'</a> '.$misc->swap_bbs_tags($misc->short_url($misc->linebreaks(htmlentities($row['comment'],ENT_QUOTES,"UTF-8")))).' '.$row['user'].' '.$row['posted_at'].' '.$row['score'];
			$result->free_result();
		}
	}
	else if(isset($_GET['s']) && $_GET['s'] == "list")
	{
		//number of comments/page
		$limit = 15;
		//number of pages to display. number - 1. ex: for 5 value should be 4
		$page_limit = 6;
		
		header("Cache-Control: store, cache");
		header("Pragma: cache");
		$post = new post();
		$cache = new cache();
		$domain = $cache->select_domain();
		$misc = new misc();
		require "includes/header.php";
		$misc = new misc();
		?>
	<div id="content">
	<div id="comment-list">
		<div style="margin-bottom: 1em;">
		<!--<h2>Before commenting, read the <a href="">how to comment guide</a>.</h2>-->
		</div>
		<script type="text/javascript">
			var posts = {}; posts.comments = {}; posts.ignored = {}; posts.totalcount = {}; posts.tags = {}; posts.rating = {}; posts.score = {}; posts.rating[0] = ''; var phidden = {}; var cthreshold = parseInt(Cookie.get('comment_threshold')) || 0; var users = Cookie.get('user_blacklist').split(/[, ]|%20+/g);
		</script>
		<?php
		if(isset($_GET['pid']) && $_GET['pid'] != "" && is_numeric($_GET['pid']) && $_GET['pid'] >= 0)
			$page = $db->real_escape_string($_GET['pid']);
		else
			$page = 0;
		$query = "SELECT pcount FROM $post_count_table WHERE access_key = 'comment_count'";
		$result = $db->query($query);
		$row = $result->fetch_assoc();
		if($row['pcount'] == 0 || $limit*$page > $row['pcount'])
		{
			echo '<h1>Nobody here but us chickens!</h1>';
		}
		else
		{
		$numrows = $row['pcount'];
		$result->free_result();
		$row_count = 0;
		$img = '';
		$ccount = 0;
		$ptcount = 0;
		$lastpid = '';
		$previd = '';
		$tcount = 0;
		$images = '';
		$query = "SELECT t1.id, t1.comment, t1.user, t1.posted_at, t1.score, t1.post_id, t1.spam, t2.image, t2.directory as dir, t2.tags, t2.rating, t2.score as p_score, t2.owner, t2.creation_date FROM $comment_table AS t1 JOIN $post_table AS t2 ON t2.id=t1.post_id ORDER BY t2.last_comment DESC,t1.id ASC LIMIT $page, $limit";
		$result = $db->query($query) or die($db->error);
		while($row = $result->fetch_assoc())
		{
			$previd = $row['post_id'];
			/* DebugInfo: print 'prev '.$previd; print ' last '.$lastpid; print ' -route- '; */
			$posted_at = $row['posted_at'];
			$date_now = $misc->date_words($posted_at);
			$posted_at = date('Y-m-d H:i:s',$posted_at);
			if($previd != $lastpid && $tcount > 0)
			{
			if($img != "")
			$images .= '
					<script type="text/javascript">
						posts.totalcount['.$lastpid.'] = \''.$ptcount.'\'
					</script>';
			$images .= '
				</div>
			</div>
		</div>
		';
			}
			if($img != $row['image'])
			{
				$images .= '<!-- Post #'.$row['post_id'].' -->
		<div class="post" id="p'.$row['post_id'].'">';
				$pat = $row['creation_date'];
				$rating = $row['rating'];
				$user = $row['owner'];
				$tags = mb_trim($row['tags']);
				$images .= '
			<script type="text/javascript">
				posts.tags['.$row['post_id'].'] = \''.str_replace('\\',"&#92;",str_replace("'","&#039;",$tags)).'\'; posts.rating['.$row['post_id'].'] = \''.$row['rating'].'\'; posts.score['.$row['post_id'].'] = \''.$row['p_score'].'\';
			</script>';
				$ttags = explode(" ",$tags);
				$images .= '
			<div class="col1">
				<span class="thumb">
					<a href="index.php?page=post&amp;s=view&amp;id='.$row['post_id'].'">
					<img src="'.$site_url.$thumbnail_folder.'/'.$row['dir'].'/'.$thumbnail_prefix.$row['image'].'" border="0" class="preview" title="'.$tags.'" alt="thumbnail_'.$row['post_id'].'">
					</a>
				</span>
			</div>
			<div class="col2" id="comments-for-p'.$row['post_id'].'">';
				if($tcount >= 0)
				{
					$images .= '
				<div class="header">
				<div>
					<span class="info"><strong>Date </strong>'.date( 'M d, Y', $pat).'</span>
					<span class="info"><strong>User </strong><a href="index.php?page=account&amp;s=profile&amp;uname='.$user.'">'.$user.'</a></span>
					<span class="info"><strong>Rating </strong>'.$rating.'</span>
					<span class="info"><strong>Score </strong><span id="psc'.$row['post_id'].'">'.$row['p_score'].'</span> (Vote <a href="#" onclick="Javascript:post_vote(\''.$row['post_id'].'\', \'up\')">Up</a>/<a href="#" onclick="Javascript:post_vote(\''.$row['post_id'].'\', \'down\')">Down</a>)</span>
				</div>
				<div class="tags">
				<strong>Tags</strong>';
					$ttcount = 0;
					foreach($ttags as $current)
					{
						if($ttcount < 15)
						{
							$type = $post->tag_type($current);
							$images .= "
					<span class=\"tag-type-$type\"><a href=\"index.php?page=post&amp;s=list&amp;tags=$current\">$current</a></span>";
							++$ttcount;
						}
					}
					$images .='
				</div>
				</div>
				<div class="response-list" id="response-list-for-'.$row['post_id'].'">';
				}
				$ptcount = 0;
				$img = $row['image'];
			}
			$images .= "
					<script type=\"text/javascript\">
						posts.comments[".$row['id']."] = {'score':".$row['score'].", 'user':'".str_replace('\\',"&#92;",str_replace(' ','%20',str_replace("'","&#039;",$row['user'])))."', 'post_id':'".$row['post_id']."'}
					</script>";	
			$images .= '
					<div class="comment" id="comment-'.$row['id'].'">
						<div class="author">
						<h4><a href="index.php?page=account_profile&amp;uname='.$row['user'].'">'.$row['user'].'</a></h4>
						<span class="date" title="Posted at '.$posted_at.'">'.$date_now.'</span>';
			$images .= "
						</div>
						<div class=\"content\">
						<div class=\"body\">
						".stripslashes($misc->swap_bbs_tags($misc->short_url($misc->linebreaks($row['comment']))))."
						</div>
						<div class=\"footer\">";
			$row['spam'] == false ? $images .= '<a href="#" id="rcl'.$row['id'].'" onclick="cflag(\''.$row['id'].'\')">Report</a>' : $images .= "Reported";
			$images .= "
						</div>
				      		</div>
					</div>";	
			++$ccount;
			++$ptcount;
			++$tcount;
			$lastpid = $row['post_id'];
		}
		$images .= '
					<script type="text/javascript">
						posts.totalcount['.$lastpid.'] = \''.$ptcount.'\'
					</script>
				</div>
			</div>
		</div>
		<div style="clear: both;">
			<br>
			<a href="#" id="ci" onclick="showHideCommentListIgnored(); return false;">(0 hidden)</a>
			<br><br>
		</div>
		<script type="text/javascript">
			filterCommentList(\''.$ccount.'\')
		</script>';
		$result->free_result();
		echo $images;
		//Pagination. Nothing really needs to be changed at this point.
		print '
		<div id="paginator">
			';
		print $misc->pagination($_GET['page'],$_GET['s'],$id,$limit,$page_limit,$numrows,$_GET['pid']);
		print '
		</div>';
		}
	}
?>

	</div>
	</div>
</body>
</html>