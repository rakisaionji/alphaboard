<?php
	$lozerisdumb = "- Help";
	require "includes/header.php";
?>
<div id="content">
<div class="help">
  <h1>Help</h1>
  <ul class="link-page">
    <li><a href="index.php?page=help&amp;topic=about">&raquo; About <?php print ucfirst($site_url3) ?></a></li>
    <li><a href="index.php?page=help&amp;topic=users">&raquo; Accounts</a></li>
    <li><a href="index.php?page=help&amp;topic=bbcode">&raquo; BBCode</a></li>
    <li><a href="index.php?page=help&amp;topic=trac">&raquo; Bugs &amp; Suggestions</a></li>
    <li><a href="index.php?page=help&amp;topic=cheatsheet">&raquo; Cheat Sheet</a></li>
    <li><a href="index.php?page=help&amp;topic=comments">&raquo; Comments</a></li>
    <li><a href="index.php?page=help&amp;topic=faq">&raquo; Frequently Asked Questions</a></li>
    <li><a href="index.php?page=help&amp;topic=favorites">&raquo; Favorites</a></li>
    <li><a href="index.php?page=help&amp;topic=forum">&raquo; Forum</a></li>
    <li><a href="index.php?page=help&amp;topic=start">&raquo; Getting Started</a></li>
    <li><a href="index.php?page=help&amp;topic=hidden_tags">&raquo; Hidden Tags</a></li>
    <li><a href="index.php?page=help&amp;topic=image_sampling">&raquo; Image Sampling</a></li>
    <li><a href="index.php?page=help&amp;topic=mass_edit">&raquo; Mass Edit</a></li>
    <li><a href="index.php?page=help&amp;topic=notes">&raquo; Notes</a></li>
    <li><a href="index.php?page=help&amp;topic=plugin">&raquo; Firefox Plugin</a></li>
    <li><a href="index.php?page=help&amp;topic=posts">&raquo; Posts</a></li>
    <li><a href="index.php?page=help&amp;topic=post_relationships">&raquo; Post Relationships</a></li>
    <li><a href="index.php?page=help&amp;topic=ratings">&raquo; Ratings</a></li>
    <li><a href="index.php?page=help&amp;topic=tags">&raquo; Tags</a></li>
    <li><a href="index.php?page=help&amp;topic=tag_aliases">&raquo; Tag Aliases</a></li>
    <li><a href="index.php?page=help&amp;topic=voting">&raquo; Voting</a></li>
  </ul>
  <p>Revision 1.1 - 2011-09-20</p>
</div>
</div>
</body>
</html>