<?php
	require "includes/header.php";
?>
<div id="content">
<div class="help">
  <h1>Help: Favorites</h1>

  <div class="section">
    <p>You can save individual posts to a personal list of favorites. You need an <a href="index.php?page=help&amp;topic=users">account</a> in order to use this feature, and you must have Javascript enabled.</p>
    <p>To add a post to your favorites, simply click on the <strong>Add to Favorites</strong> link.</p>
    <p>You can view your favorites by clicking on the <strong>My Favorites</strong> link or others favorites by clicking on the <strong>Everyone's Favorites</strong> link from <strong><a href="index.php?page=account">My Account</a></strong>.</p>
  </div>
</div>
</div>
</body>
</html>