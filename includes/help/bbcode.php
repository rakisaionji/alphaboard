<?php
	$lozerisdumb = "- Help - BBCode";
	require "includes/header.php";
?>
<div id="content">
<div class="help">
  <h1>Help: BBCode</h1>
  <div class="section">
    <h4>Introduction</h4>
    <p><a href="http://en.wikipedia.org/wiki/BBCode" title="BBCode">BBCode</a> or <a href="http://en.wikipedia.org/wiki/BBCode" title="BBCode">Bulletin Board Code</a> is a <a href="http://en.wikipedia.org/wiki/Lightweight_markup_language" title="Lightweight markup language">lightweight markup language</a> used to format posts in many <a href="http://en.wikipedia.org/wiki/Message_board" title="Message board">message boards</a>. The available tags are usually indicated by <a href="http://en.wikipedia.org/wiki/Bracket" title="Bracket">square brackets</a> surrounding a keyword, and they are <a href="http://en.wikipedia.org/wiki/Parsing" title="Parsing">parsed</a> by the message board system before being translated into a <a href="http://en.wikipedia.org/wiki/Markup_language" title="Markup language">markup language</a> that <a href="http://en.wikipedia.org/wiki/Web_browser" title="Web browser">web browsers</a> understand &ndash; usually <a href="http://en.wikipedia.org/wiki/HTML" title="HTML">HTML</a> or <a href="http://en.wikipedia.org/wiki/XHTML" title="XHTML">XHTML</a>.</p>
    <p><a href="http://en.wikipedia.org/wiki/BBCode" title="BBCode">BBCode</a> was first introduced in 1998 by the messageboard software <a href="http://en.wikipedia.org/wiki/UBB.classic" title="UBB.classic">Ultimate Bulletin Board</a> (UBB).</p>
    <p>And now, with its comfortability and easy to use method, <a href=""http://en.wikipedia.org/wiki/BBCode" title="BBCode">BBCode</a> is going to be the main text formatting language of AlphaBoard. It is used for formatting <a href="index.php?page=help&amp;topic=comments">comments</a> and <a href="index.php?page=help&amp;topic=forum">forum</a> posts.</p>
  </div>

  <div class="section">
    <h4>Formatting</h4>
    <dl>
      <dt><?php print substr($site_url, 0, -1) ?></dt>
      <dd>URLs are automatically linked.</dd>
      
      <dt>[b]strong text[/b]</dt>
      <dd>Makes text bold.</dd>
      
      <dt>[i]emphasized text[/i]</dt>
      <dd>Makes text italicized.</dd>
      
      <dt>[u]underlined text[/u]</dt>
      <dd>Makes text underlined.</dd>
<?php /*      
      <dt>[[wiki page]]</dt>
      <dd>Links to the wiki.</dd>
      
      <dt>{{touhou monochrome}}</dt>
      <dd>Links to a post search.</dd>
      
      <dt>post #1234</dt>
      <dd>Links to post #1234.</dd>
      
      <dt>forum #1234</dt>
      <dd>Links to forum #1234.</dd>
      
      <dt>comment #1234</dt>
      <dd>Links to comment #1234.</dd>
      
      <dt>pool #1234</dt>
      <dd>Links to pool #1234.</dd>*/ ?>
      
      <dt>[post]1234[/post]</dt>
      <dd>Links to post #1234.</dd>
      
      <dt>[forum]1234[/forum]</dt>
      <dd>Links to forum #1234.</dd>
      
      <dt>[tag]abcd[/tag]</dt>
      <dd>Links to tag search for 'abcd'.</dd>
      
      <dt>[spoiler]Some spoiler text[/spoiler]</dt>
      <dd>Marks a section of text as spoilers.</dd>
      
      <dt>[quote]Some quoted text[/quote]</dt>
      <dd>Marks a blockquote for quoted text.</dd>
    </dl>
  </div>
<?php /*  
  <div class="section">
    <h4>Block</h4>
    <pre>
      A paragraph.
      
      Another paragraph
      that continues on multiple lines.
      
      
      h1. An Important Header
      
      h2. A Less Important Header
      
      h6. The Smallest Header
      
      
      [quote]
      bob said:
      
      When you are quoting someone.
      [/quote]
    </pre>
  </div>
  
  <div class="section">
    <h4>Lists</h4>
    <pre>
      * Item 1
      * Item 2
      ** Item 2.a
      ** Item 2.b
      * Item 3
    </pre>
  </div> */ ?>
</div>
</div>
</body>
</html>