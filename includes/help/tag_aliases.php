<?php
	require "includes/header.php";
?>
<div id="content">
<div class="help">
  <h1>Help: Tag Aliases</h1>
  <p>Sometimes, two tags can mean the same thing. For example, <code>pantsu</code> and <code>panties</code> have identical meanings. It makes sense that if you search for one, you should also get the results for the other.</p>
  <p>AlphaBoard tries to fix this issue by using tag aliases. You can alias one or more tags to one reference tag. For example, if we aliased <code>pantsu</code> to <code>panties</code>, then whenever someone searched for <code>pantsu</code> or tagged a post with <code>pantsu</code>, it would be internally replaced with <code>panties</code>.</p>
  <p>When a tag is aliased to another tag, that means that the two tags are equivalent. You would not generally alias <code>rectangle</code> to <code>square</code>, for example, because while all squares are rectangles, not all rectangles are squares.</p>
  <p>While anyone can <a href="index.php?page=alias&amp;s=add">suggest</a> an alias, only an administrator can approve it.</p>
</div>
</div>
</body>
</html>