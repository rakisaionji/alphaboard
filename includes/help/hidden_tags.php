<?php
	require "includes/header.php";
?>
<div id="content">
<div class="help">
  <h1>Help: Hidden Tags</h1>

  <div class="section">
    <p>AlphaBoard's filters help you hiding tags that you blacklisted in your Account Settings panel. These posts will be hidden from your view after the page loaded successfully.</p>
    <p>Hidden tags become visible when you click on the 'Hidden Posts' link in the 'Filter' section of the Sidebar. The 'Filter' section is appeared only when there is a post hidden in the current page.</p>
    <p>The filters is written in JavaScript which is originally used in Gelbooru by Geltas and modified by Raki Saionji for the better use.</p>
  </div>
</div>
</div>
</body>
</html>