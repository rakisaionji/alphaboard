<?php
	require "includes/header.php";
?>
<div id="content">
<div class="help">
  <h1>Help: Voting</h1>

  <div class="section">
    <p>If you have an account, you can vote on posts. When you click on the vote up or vote down link, your browser queries in the background and records your vote. You can only vote once per post, and once you've voted you can't change your vote.</p>
    <p>In order to vote, you must have Javascript enabled.</p>
  </div>
</div>
</div>
</body>
</html>