<?php
	require "includes/header.php";
?>
<div id="content">
<div class="help">
  <h1>Help: About <?php print ucfirst($site_url3) ?></h1>

  <div class="section">
    <p><?php print ucfirst($site_url3) ?> is running Raki Saionji's AlphaBoard software which is a web application that allows you to upload, share, and tag images. It was made to be more comfortable use and maintain Danbooru's user interface for easier posting and downloading images. Not all of Danbooru's features are included, just some of these features are noticeable:</p>
    <ul>
      <li>Posts never expire unless deleted by Administrators and Moderators</li>
      <li>Tag and comment on posts with max_chars and min_words filter</li>
      <li>Search for tags via intersection, union, negation, or pattern</li>
      <li>Annotate images with notes using JavaScript</li>
      <li>Input a URL and AlphaBoard will automatically downloads the file</li>
      <li>Duplicate post detection (via MD5, SHA1 and CRC32 hashes)</li>
      <li>Atom feeds for Posts and User Subscription - Compatible with all readers</li>
      <li>Syncing with Danbooru's database using Raki's DanSync tool</li>
      <li>Advanced User Profile page from Gelbooru 0.2.5 (Modified)</li>
      <li>Easy to install with Raki's EasyPHPInstaller for Gelbooru</li>
      <li>Fully using PHP and MySQL for easy hosting and bug searching</li>
    </ul>
  </div>
</div>
</div>
</body>
</html>