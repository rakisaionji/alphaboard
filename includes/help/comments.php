<?php
	require "includes/header.php";
?>
<div id="content">
<div class="help">
  <h1>Help: Comments</h1>

  <div class="section">
    <h4>Formatting</h4>
    <p>All comments are formatted using <a href="index.php?page=help&amp;topic=bbcode">Modified BBCode</a>.</p>
  </div>

  <div class="section">
    <h4>Limiting</h4>
    <p>We allow posting comments with an unlimit rate, you can comment as much as you'd like now. However, to save our memory usage and lower POST package size, we only allow a hard limit of <?php print $global_max_chars ?> characters an at least <?php print $global_min_words ?> words per comment when commenting via our web interface.</p>
  </div>

  <div class="section">
    <h4>Search</h4>
    <p>If you want to only search for a specific user, prefix your query with user. For example: "user:<?php print str_replace(' ','_',strtolower($site_url3)) ?>_user".</p>
  </div>
</div>
</div>
</body>
</html>