<?php
	$lozerisdumb = "- Help - Accounts";
	require "includes/header.php";
?>
<div id="content">
<div class="help">
  <h1>Help: Accounts</h1>

  <div class="section">
    <p>There are three basic account types in AlphaBoard's default configuration: basic, privileged, and contributor.</p>
    <table width="100%">
      <thead>
        <tr>
          <th width="20%">Level</th>
          <th width="20%">Cost</th>
          <th width="60%">Description</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Basic</td>
          <td>Free<br>(Default)</td>
          <td>By default, you cannot upload posts but you can comment, new forum posts, vote posts and comments.</td>
        </tr>
        <tr>
          <td>Privileged</td>
          <td><b>500</b><br>forum posts</td>
          <td>You can edit posts, tags, aliases, post new forum threads and topics but you cannot upload posts.</td>
        </tr>
        <tr>
          <td>Contributor</td>
          <td>Invite only</td>
          <td>You can upload and edit posts, notes, tags, aliases. Your posts are subject to moderation.</td>
        </tr>
      </tbody>
    </table>
    <p>Registration for basic accounts is free and open.</p>
    <p>Privileges in each account type depends on admin's configuration.</p>
  </div>
</div>
</div>
</body>
</html>