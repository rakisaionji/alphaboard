<?php
	$lozerisdumb = "- Help - Getting Started";
	require "includes/header.php";
?>
<div id="content">
<div class="help">
  <h1>Help: Getting Started</h1>
  <p><?php print ucfirst($site_url3) ?> is running AlphaBoard software which is written by Raki Saionji (loliwebdev) from the core of Gelbooru with a lot of modifications and optimizations to be more stable and comfortable use.</p>
  <p>If you are already familiar with AlphaBoard, you may want to consult the <a href="index.php?page=help&amp;topic=cheatsheet">cheat sheet</a> for a quick overview of the site.</p>
  <p>The core of AlphaBoard is represented by <a href="index.php?page=help&amp;topic=posts">posts</a> and <a href="index.php?page=help&amp;topic=tags">tags</a>. Posts are the content, and tags are how you find the posts.</p>
</div>
</div>
</body>
</html>