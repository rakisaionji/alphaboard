<?php
	require "includes/header.php";
?>
<div id="content">
<div class="help">
  <h1>Help: Mass Edit</h1>
  <p><em>Note: This function is only available to moderators.</em></p>
  <p>Mass edit allows you to make sweeping changes to posts. It allows you to add tags, remove tags, or change tags to posts in each page at once.</p>
  <p>The interface is very similar to the 'Post Listing' page. There are four text fields and a button next to the thumbnail for each post. The first text field is the ID of the post. The second and third text fields are 'Title' and 'Source'. The fourth text field is where you enter the tags you want to tag the selected post with. This will overwrite the current information of the affected post.</p>
  <p>Click on the 'Save Changes' button to save your tags. Note that this will process your request through AJAX request. The process will be taken place like a normal post edit task. All changes will be logged to the 'Tag History' table as usually. Please do not empty tags.</p>
</div>
</div>
</body>
</html>