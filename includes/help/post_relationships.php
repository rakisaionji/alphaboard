<?php
	require "includes/header.php";
?>
<div id="content">
<div class="help">
  <h1>Help: Post Relationships</h1>

  <p>Every post can have a parent. Any post that has a parent will not show up in the <a href="index.php?page=post&amp;s=list&amp;tags=all">main listing</a>. However, the post will appear again sometimes if a user does any weird kind of tag search. This makes it useful for things like duplicates.</p>
  <p>Please do not use parent/children relationship for pages of a manga or doujinshi.</p>
  <p>To use this field, simply enter the id number of the parent post when you upload or edit a post. To search for all the children of a parent post, you can do a tag search for <code>parent:1234</code>, where <code>1234</code> is the id number of the parent post.</p>
</div>
</div>
</body>
</html>