<?php
	require "includes/header.php";
?>
<div id="content">
<div class="help">
  <h1>Help: Image Sampling</h1>

  <div class="section">
    <p>While high resolution images are nice for archival purposes, beyond a certain resolution they become impractical to view and time consuming to download.</p>
<?php /* <p><?php print ucfirst($site_url3) ?> will automatically resize any image larger than <%= CONFIG["sample_width"] %>x<%= CONFIG["sample_height"] %> to a more manageable size, in addition to the thumbnail. It will also store the original, unresized image.</p>
    <% unless CONFIG["force_image_samples"] %>
      <p>You can toggle this behavior by changing the Show Image Samples setting in your <%= link_to "user settings", :controller => "user", :action => "edit" %>.</p>
    <% end %> */ ?>
    <p>To save our storage space, we currently disable and never use Image Sampling for our server. The AlphaBoard is also did not support this function (originally) according to author's notice:<br><code><br>... To save the space AlphaBoard used, I will not write the Image Sampling module for it. However, some hosting service provider restrict using much of their resources for the imageboards so posting high quality images is discouraged, unless you have a powerful dedicated server or VPS server to host the site. ... <br><br>Jun 01 2011, RAKI SAIONJI.</code></p>
  </div>
</div>
</div>
</body>
</html>