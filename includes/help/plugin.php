<?php
	require "includes/header.php";
?>
<div id="content">
<div class="help">
  <h1>Help: Firefox Plugin</h1>

  <div class="section">
    <p>There is a Firefox search plugin available to search posts in <?php print ucfirst($site_url3) ?>.</p>
    <p>Note that you need Firefox 2.0.x and above to use this plugin.</p>
    <p><a href="#" onclick="addEngine('<?php print strtolower($site_url4) ?>', 'png', '0', '<?php print $site_url ?>')">Click here</a> to add the plugin to your Firefox browser!</p>
  </div>
</div>
</div>
</body>
</html>