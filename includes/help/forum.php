<?php
	require "includes/header.php";
?>
<div id="content">
<div class="help">
  <h1>Help: Forum</h1>

  <div class="section">
    <p>All forum posts are formatted using <a href="index.php?page=help&amp;topic=bbcode">Modified BBCode</a>.</p>
  </div>
  
  <div class="section">
    <h4>Search</h4>
    <p>If you prepend your query with user you will search for forum posts from that user. Example: <code>user:albert</code> searches for forum posts from albert.</p>
  </div>
</div>
</div>
</body>
</html>