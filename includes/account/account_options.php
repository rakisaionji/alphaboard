<?php
	if(isset($_POST['submit']))
	{
		if(isset($_POST['users']) && $_POST['users'] != "")
		{
			setcookie("user_blacklist",htmlentities(mb_strtolower(stripslashes($_POST['users']), 'UTF-8'), ENT_QUOTES, "UTF-8"),time()+60*60*24*365);
			$new_user_list = stripslashes($_POST['users']);
		}
		else
		{
			setcookie("user_blacklist",'',time()-60*60*24*365);
			$new_user_list = " ";
		}
		if(isset($_POST['tags']) && $_POST['tags'])
		{
			setcookie("tag_blacklist",htmlentities(mb_strtolower(stripslashes($_POST['tags']), 'UTF-8'), ENT_QUOTES, "UTF-8"),time()+60*60*24*365);
			$new_tag_list = stripslashes($_POST['tags']);
		}
		else
		{
			setcookie("tag_blacklist","",time()-60*60*24*365);
			$new_tag_list = " ";
		}
		if(isset($_POST['cthreshold']) && $_POST['cthreshold'] != "")
		{
			if(!is_numeric($_POST['cthreshold']))
			{
				setcookie('comment_threshold',0,time()+60*60*24*365);
				$new_cthreshold = 0;
			}
			else
			{
				setcookie('comment_threshold',$_POST['cthreshold'],time()+60*60*24*365);
				$new_cthreshold = $_POST['cthreshold'];
			}
		}
		else
		{
			setcookie('comment_threshold',"",time()-60*60*24*365);
			$new_cthreshold = 0;
		}
		if(isset($_POST['pthreshold']) && $_POST['pthreshold'] != "")
		{
			if(!is_numeric($_POST['pthreshold']))
			{
				setcookie('post_threshold',0,time()+60*60*24*365);
				$new_pthreshold = 0;
			}
			else
			{
				setcookie('post_threshold',$_POST['pthreshold'],time()+60*60*24*365);
				$new_pthreshold = $_POST['pthreshold'];
			}
		}
		else
		{
			setcookie('post_threshold',"",time()-60*60*24*365);
			$new_pthreshold = 0;
		}
		if(isset($_POST['my_tags']) && $_POST['my_tags'] != "")
		{
			$user = new user();
			$my_tags = htmlentities($_POST['my_tags'], ENT_QUOTES, "UTF-8");
			setcookie("tags",stripslashes($my_tags),time()+60*60*24*365);
			if($user->check_log())
			{
				$query = "UPDATE $user_table SET my_tags = '$my_tags' WHERE id = '$checked_user_id'";
				$db->query($query);
			}
			$new_my_tags = stripslashes($my_tags);
		}
		else
		{
			setcookie("tags",'',time()-60*60*24*365);
			$new_my_tags = " ";
		}
	}
	header("Cache-Control: store, cache");
	header("Pragma: cache");
	require "includes/header.php";
?>
	<div id="content">
	<h2>Account Options</h2><br>
	<p><b>Separate individual tags and users with spaces.</b><br>You must have cookies and JavaScript enabled in order for filtering to work.<br>Note that the user blacklist is case sensitive.</p>
	<form action="" method="post">
	<div class="option">
	<table cellpadding="0" cellspacing="4">
	<tr>
		<td>
			<label class="block">Tag Blacklist</label>
			<p>Any post containing a blacklisted tag will be ignored. Note that you can also blacklist ratings.</p>
		</td>
		<td>
			<textarea name="tags" style="width: 512px; height: 128px; margin-bottom:12px;" rows="20" cols="50"><?php $new_tag_list != "" ? print $new_tag_list : print html_entity_decode($_COOKIE['tag_blacklist'], ENT_QUOTES, "UTF-8"); ?></textarea>
		</td>
	</tr>
	<tr>
		<td>
			<label class="block">User Blacklist</label>
			<p>Any post or comment from a blacklisted user will be ignored.</p>
		</td>
		<td>
			<input type="text" name="users" style="width: 512px; height: 24px; margin-bottom:12px;" value="<?php $new_user_list != "" ? print $new_user_list : print html_entity_decode($_COOKIE['user_blacklist'], ENT_QUOTES, "UTF-8"); ?>">
		</td>
	</tr>
	<tr>
		<td>
			<label class="block">Comment Threshold</label>
			<p>Any comment with a score below this will be ignored.</p>
		</td>
		<td>
			<input type="text" name="cthreshold" style="width: 512px; height: 24px; margin-bottom:12px;" value="<?php ($new_cthreshold == "" && !isset($_COOKIE['comment_threshold'])) ? print 0 : $new_cthreshold != "" ? print $new_cthreshold : print $_COOKIE['comment_threshold']; ?>">
		</td>
	</tr>
	<tr>
		<td>
			<label class="block">Post Threshold</label>
			<p>Any post with a score below this will be ignored.</p>
		</td>
		<td>
			<input type="text" name="pthreshold" style="width: 512px; height: 24px; margin-bottom:12px;" value="<?php ($new_pthreshold == "" && !isset($_COOKIE['post_threshold'])) ? print 0 : $new_pthreshold != "" ? print $new_pthreshold : print $_COOKIE['post_threshold']; ?>">
		</td>
	</tr>
	<tr>
		<td>
			<label class="block">My Tags</label>
			<p>These will be accessible when you add or edit a post.</p>
		</td>
		<td>
			<textarea name="my_tags" style="width: 512px; height: 128px; margin-bottom:12px;" rows="30" cols="50"><?php $new_my_tags != "" ? print $new_my_tags : print stripslashes(html_entity_decode($_COOKIE['tags'], ENT_QUOTES, "UTF-8"));?></textarea>	
		</td>
	</tr>
	<tr>
		<td>
			<label class="block">Safe Only Listing</label>
			<p>You will only see images that are marked as safe on the main index listing.</p>
		</td>
		<td>
			<input disabled name="safe_only" type="checkbox">
		</td>
	</tr>
	<tr>
		<td>
		</td>
		<td style="text-align: left;">
			<input type="submit" style="width: 128px; height: 24px; margin-bottom:12px;" name="submit" value="Save Settings">
		</td>
	</tr>
	</table>
	</div>
	</form>
	</div>
</body>
</html>