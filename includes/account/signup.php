<?php
	if($registration_allowed != true)
		die('	<div id="content"><br><b>Registration is closed.</b>');
	$user = new user();
	$ip = $db->real_escape_string($_SERVER['REMOTE_ADDR']);	
	if($user->banned_ip($ip))
	{
		print '	<div id="content">Action failed: '.$row['reason'];
		exit;
	}	
	if($user->check_log())
	{
		header("Location:index.php?page=account");
		exit;
	}
	if(isset($_POST['settings']) && $_POST['settings'] == 1 && isset($_POST['user']) && $_POST['user'] != "" && isset($_POST['pass']) && $_POST['pass'] != "" && isset($_POST['conf_pass']) && $_POST['conf_pass'] != "" && strlen($_POST['user']) >= 5 && strlen($_POST['pass']) >= 5 && ctype_alnum($_POST['user']))
	{
		$username = $db->real_escape_string(str_replace(" ",'_',htmlentities($_POST['user'], ENT_QUOTES, 'UTF-8')));
		$password = $db->real_escape_string($_POST['pass']);
		$conf_password = $db->real_escape_string($_POST['conf_pass']);
		$email = $db->real_escape_string($_POST['email']);
		if($password == $conf_password)
		{
			$user = new user();
			if(!$user->signup($username,$password,$email))
			{
				require "includes/header.php";
				print '	<div id="content"><div class="error-notice">This username is already exists.</div><br>';
			}
			else
			{
				$user->login($username,$password);
				header("Location:index.php?page=account");
				exit;
			}
		}
		else
		{
			require "includes/header.php";
			print '	<div id="content"><div class="error-notice">Passwords do not match.</div><br>';
		}
	}
	else
	{	require "includes/header.php"; print '	<div id="content">'; }
?>

	<h2>Signup</h2><br>
	<form method="post" action="index.php?page=account&amp;s=reg" name="signup_form">
	<table>
		<tr>
			<td>
			<p>Sign up for an account by filling in the information below.
			<br>You do not need to supply your email address
			<br>unless you are really bad at remembering your passwords.</p>
			</td>
		</tr>
		<tr>
			<td>Username:<br>
			<input type="text" id="username" name="user" style="width: 400px; height: 24px; margin-bottom:8px;" value="">
			</td>
		</tr>
		<tr>
			<td>Choose password:<br>
			<input type="password" id="password" name="pass" style="width: 400px; height: 24px; margin-bottom:8px;" value="">
			</td>
		</tr>
		<tr>
			<td>Confirm password:<br>
			<input type="password" id="conf_pass" name="conf_pass" style="width: 400px; height: 24px; margin-bottom:8px;" value="">
			</td>
		</tr>
		<tr>
			<td>Email (not required):<br>
			<input type="text" id="email" name="email" style="width: 400px; height: 24px; margin-bottom:8px;" value="">
			</td>
		</tr>
		<tr>
			<td>
			<p>By creating an account, you have agreed to our <a href="<?php print $site_url; ?>tos.php">terms of service</a>.</p>
			</td>
		</tr>
		<tr>
			<td>
			<input type="hidden" name="settings" id="settings" value="0">
			<input type="submit" name="submit" style="width: 100px; height: 24px; margin-bottom:8px;" onclick="verify_information();return false;" value="Register">
			</td>
		</tr>
		</table>
		<script type="text/javascript">
		function is_valid_email(email)
		{
			var atpos=email.indexOf("@");
			var dotpos=email.lastIndexOf(".");
			if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)
				return false;
			else	return true;
		}
		function verify_information()
		{
			var name = $("username").value;
			var pw = $("password").value;
			var pwc = $("conf_pass").value;
			var email = $("email").value;

			if ( name == "" )
			{
				notice("Please enter your username!");
				$("username").focus();
				return false;
			}
			else if ( name.length < 5 )
			{
				notice("Username must be at least 5 characters long!");
				$("username").focus();
				return false;
			}
			else if ( pw == "" )
			{
				notice("Please enter your password!");
				$("password").focus();
				return false;
			}
			else if ( pw.length < 5 )
			{
				notice("Password must be at least 5 characters long!");
				$("password").focus();
				return false;
			}
			else if ( pwc == "" )
			{
				notice("Please confirm your password!");
				$("conf_pass").focus();
				return false;
			}
			else if ( pw != pwc )
			{
				notice("Passwords do not match!");
				$("conf_pass").focus();
				return false;
			}
			else if ( email != "" && !is_valid_email(email) )
			{
				notice("Please enter a valid e-mail address!");
				$("email").focus();
				return false;
			}
			else
			{
				$("settings").value = 1;
				document.signup_form.submit();
			}
		}
		</script>
	</form>
	</div>
</body>
</html>