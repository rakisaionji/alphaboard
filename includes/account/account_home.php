<?php 
require 'includes/header.php';
print '	<div id="content">
	<div id="user-index">';
	$user = new user(); 
	if($user->check_log())
	{
		print '
		<h2>Hello '.$checked_username.'!</h2>
		<p>From here you can access account-specific options and features.</p>
			<h4><a href="index.php?page=account&amp;s=login&amp;code=01">&raquo; Logout</a></h4>
				<p>Make like a tree and get out of here! This will delete the cookies that are required to authenticate your account.</p>
			<h4><a href="index.php?page=account&amp;s=profile&amp;id='.$checked_user_id.'">&raquo; My Profile</a></h4>
				<p>View your profile as others would see it. This page will show your recent uploads, favorites and avatar, if you have one.<p>
			<h4><a href="index.php?page=favorites&amp;s=view&amp;id='.$_COOKIE['user_id'].'">&raquo; My Favorites</a></h4>
				<p>View a condensed list of all your favorites on the site. This should be rather short, unless you like everything you see... Which you probably do.</p>';		
	}
	else
	{
		print '
		<h2>You are not logged in.</h2>
		<p>Please login to continue.</p>
			<h4><a href="index.php?page=account&amp;s=login&amp;code=00">&raquo; Login</a></h4>
				<p>If you already have an account you can login here. Alternatively, accessing features that require an account will automatically log you in if you have enabled cookies.</p>';
		if($registration_allowed == true)
			print '
			<h4><a href="index.php?page=account&amp;s=reg">&raquo; Sign Up</a></h4>
				<p>You can access 90% of '.$site_url3.' without an account, but you can sign up for that extra bit of functionality. Just a login and password, no email required!</p>';
		else
			print '
			<h4>&raquo; Sign Up</h4>
				<p>Sorry! Registration is closed!</p>';
	}
?>

			<h4><a href="index.php?page=favorites&amp;s=list">&raquo; Everyone's Favorites</a></h4>
				<p>If you like to stalk other people and steal their ideas, you can view their favorites and eventually transform into a clone of them. I swear it's true.</p>
			<h4><a href="index.php?page=account&amp;s=options">&raquo; Options</a></h4>
				<p>Account options such as blacklists, safe only searching, and almost anything configurable for your account is on this page. You should not really spend too much time here.</p>
	</div>
	</div>
</body>
</html>