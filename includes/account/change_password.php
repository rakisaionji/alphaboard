<?php
	$user = new user();
	$ip = $db->real_escape_string($_SERVER['REMOTE_ADDR']);	
	if($user->banned_ip($ip))
	{
		print '<div id="content">Action failed: '.$row['reason'];
		exit;
	}	
	if(!$user->check_log())
	{
		header("Location:index.php?page=account&s=home");
		exit;
	}
	if(isset($_POST['pass']) && $_POST['pass'] != "" && isset($_POST['conf_pass']) && $_POST['conf_pass'] != "")
	{
		$id = $db->real_escape_string($checked_user_id);
		$username = $db->real_escape_string(str_replace(" ",'_',htmlentities($checked_username, ENT_QUOTES, 'UTF-8')));
		$password = $db->real_escape_string($_POST['pass']);
		$conf_password = $db->real_escape_string($_POST['conf_pass']);
		if($password == $conf_password)
		{
			$user = new user();
			if(!$user->update_password($id,$password))
			{
				require "includes/header.php";
				print '<div id="content"><div class="error-notice">Database Error.</div><br>';
			}
			else
			{
				$user->login($username,$password);
				header("Location:index.php?page=account&s=home");
				exit;
			}
		}
		else
		{
			require "includes/header.php";
			print '<div id="content"><div class="error-notice">Passwords do not match.</div><br>';
		}
	}
	else
	{	require "includes/header.php";
		print '<div id="content">';
	}
?>
<h2>Change Password</h2><br>
<form method="post" action="index.php?page=account&amp;s=change_password">
<table><tr><td>
New password:<br>
<input type="password" name="pass" style="width: 400px; height: 24px; margin-bottom:8px;" value="">
</td></tr>
<tr><td>
Confirm password:<br>
<input type="password" name="conf_pass" style="width: 400px; height: 24px; margin-bottom:8px;" value="">
</td></tr>
<tr><td>
<input type="submit" name="submit" style="width: 100px; height: 24px; margin-bottom:8px;" value="Save">
<input onclick="history.back();"type="button" style="width: 100px; height: 24px; margin-bottom:8px;" value="Cancel">
</td></tr>
</table>
</form></div></body></html>