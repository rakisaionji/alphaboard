<?php
	session_start();
	$user = new user();
	if($user->check_log())
	{
		header("Location: index.php?page=account&s=home");
		exit;
	}
	header("Cache-Control: store, cache");
	header("Pragma: cache");
	require "includes/header.php";
	echo '	<div id="content">';
	if(isset($_POST['username']) && $_POST['username'] != "")
	{
		if ($user->user_exists($_POST['username']))
		{
		$user = $db->real_escape_string(htmlentities($_POST['username'], ENT_QUOTES, 'UTF-8'));
		$query = "SELECT email, id FROM $user_table WHERE user='$user' LIMIT 1";
		$result = $db->query($query);
		$count = $result->num_rows;
		if($count > 0)
		{	
			$row = $result->fetch_assoc();
			if($row['email'] != "" && $row['email'] != NULL && strpos($row['email'],"@") !== false && strpos($row['email'],".") !== false && strlen($row['email']) > 2)
			{		
				$misc = new misc();
				$code = hash('sha256',rand(132,1004958327747882664857));
				$link = $site_url."/index.php?page=account&s=reset_password&id=".$row['id']."&code=".$code;
				if ($body = file_get_contents("includes/mail-form.rml"))
				{
					$ctype = "html"; $etype = "binary";
					$link = htmlentities($link, ENT_QUOTES, "UTF-8");
					$body = str_replace("[SITE_TITLE]","<a href=\"$site_url\">$site_url3</a>",str_replace("[MSG-TITLE]","Password Reset Confirmation",str_replace("[MSG-BODY]","<p>A password reset has been requested for your account.<br>If you didn't request this, please ignore this email.</p><p>To reset you password, please click on the link below:<br><em><a href=\"$link\">$link</a></em></p><p style=\"margin:0\">This request have been processed on: ".date('l, F jS, Y G:i:s T')."</p>",$body)));
				}
				else
				{
					$ctype = "plain"; $etype = "7bit";
					$body = strtoupper($site_url3." - Password Reset Confirmation")."\r\nA password reset has been requested for your account.\nIf you didn't request this, please ignore this email.\r\nTo reset you password, please follow the link below:\n$link\r\n$site_url3 Support Team";
				}
				$misc->send_mail($row['email'],$email_recovery_subject,$body,$ctype," Support Team", $etype);
				$query = "UPDATE $user_table SET mail_reset_code='$code' WHERE id='".$row['id']."'";				
				$db->query($query);				
				$msg = "An email with a reset link has been sent to your mailbox.<br>";
			}
			else
				$msg = "No email has been added to this account.<br>";
		}
		else
			$msg = "No email has been added to this account.<br>";
		}
		else
			$msg = "This username is not available.<br>";
	}
	if (isset($msg) && $msg != '')
	{
		echo '
		<div class="status-notice">'.$msg.'</div><br>';
	}
	if(isset($_GET['code']) && $_GET['code'] != "" && isset($_GET['id']) && $_GET['id'] != "" && is_numeric($_GET['id']))
	{
		$id = $db->real_escape_string($_GET['id']);
		$code = $db->real_escape_string($_GET['code']);
		$query = "SELECT id FROM $user_table WHERE id='$id' AND mail_reset_code='$code' LIMIT 1";
		$result = $db->query($query) or die($db->error);
		if($result->num_rows > 0)
		{
			$_SESSION['reset_code'] = $code;
			$_SESSION['tmp_id'] = $id;
			echo '
		<div class="status-notice">Your request ticket have been processed successfully. (Password reset ticket)
		<br>Ticket ID: '.$db->real_escape_string(htmlentities($_GET['code'], ENT_QUOTES, 'UTF-8')).'</div><br>
		<h2>Reset Password</h2><br>
		<form method="post" action="index.php?page=account&amp;s=reset_password">
		<table>
		<tr>
		<td>
		<h4>Step 3 of 3</h4><h6>Finishing your request.</h6><br>
		<p>Your request ticket have been processed successfully.
		<br>You\'ll get your new password working after submitting this form.</p>
		</td>
		</tr>
		<tr>
		<td>
		Enter your new password: 
		</td>
		</tr>
		<tr>
		<td>
		<input type="password" name="new_password" style="width: 400px; height: 24px; margin-bottom:8px;" value="">
		</td>
		</tr>
		<tr>
		<td>
		<input type="submit" name="submit" name="submit" style="width: 100px; height: 24px; margin-bottom:8px;" value="Submit">
		</td>
		</tr>
		</table>				
		</form>';
		}
		else
		{
			echo '
		<div class="status-notice">Invalid reset link.</div><br>';
		}
	}
	if(isset($_POST['new_password']) && $_POST['new_password'] != "" && isset($_SESSION['tmp_id']) && $_SESSION['tmp_id'] != "" && is_numeric($_SESSION['tmp_id']) && isset($_SESSION['reset_code']) && $_SESSION['reset_code'] != "")
	{
		$code = $db->real_escape_string($_SESSION['reset_code']);
		$id = $db->real_escape_string($_SESSION['tmp_id']);
		$pass = $db->real_escape_string($_POST['new_password']);		
		$user = new user();
		$query = "SELECT id FROM $user_table WHERE id='$id' AND mail_reset_code='$code'";
		$result = $db->query($query) or die($db->error);
		if($result->num_rows > 0)
		{
			$user->update_password($id,$pass);
			$query = "UPDATE $user_table SET mail_reset_code='' WHERE id='$id' AND mail_reset_code='$code'";
			$db->query($query);
			unset($_SESSION['tmp_id']);
			unset($_SESSION['reset_code']);
			echo '
		<meta http-equiv="refresh" content="5;url='.$site_url.'index.php?page=account&amp;s=login&amp;code=00">
		<div class="status-notice">Your password has been changed.
		<br>You will be redirected to the login page in 5 seconds.</div><br>';
		}	
	}
	if(!isset($_GET['code']) && $_GET['code'] == "")
	{	
		echo '
		<h2>Reset Password</h2><br>
		<form method="post" action="index.php?page=account&amp;s=reset_password">
		<table>
		<tr>
		<td>
		<h4>Step 1 of 3</h4><h6>Gathering required information.</h6><br>
		<p>If you supplied an email address when you signed up, you can have your password reset.<br>
		You\'ll get an email containing your new password reset code after submitting this form.</p>
		</td>
		</tr>
		<tr>
		<td>
		Username:
		</td>
		</tr>
		<tr>
		<td>
		<input type="text" name="username" style="width: 400px; height: 24px; margin-bottom:8px;" value="">
		</td>
		</tr>
		<tr>
		<td>
		<input type="submit" name="submit" style="width: 100px; height: 24px; margin-bottom:8px;" value="Submit">
		</td>
		</tr>
		</table>
		</form>';
	}
?>

	</div>
</body>
</html>