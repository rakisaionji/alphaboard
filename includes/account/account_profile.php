<?php
$userc = new user();
$ip = $db->real_escape_string($_SERVER['REMOTE_ADDR']);
require 'includes/header.php';
print '	<div id="content">
	';
if(!isset($_GET['uname']) && !isset($_GET['id']))
{
	print "<h2>Unknown Parameters</h2>"; exit;
}
if($userc->banned_ip($ip))
{
	print "<h2>Action failed: ".$row['reason']."</h2>"; exit;
}	
if(!$userc->check_log())
{
	print "<h2>Access Denied</h2>"; exit;
}
if(isset($_GET['id']) && is_numeric($_GET['id']) || isset($_GET['uname']) && $_GET['uname'] != "")
{
	if($_GET['id'] == '0' || strtolower($_GET['uname']) == "anonymous")
	{
		print "<h2>Anonymous!?</h2>";
		exit;
	}
	if(isset($_GET['uname']))
	{
		$uname = $db->real_escape_string($_GET['uname']);
		$query = "SELECT user, id FROM $user_table WHERE user='$uname'";
		$result = $db->query($query) or die($db->error);
		$row = $result->fetch_assoc();
		$result->close();
		$id = $row['id'];
	}
	else
		$id = $db->real_escape_string($_GET['id']);
	$query = "SELECT t1.id, t1.user, t1.email, t1.record_score, t1.post_count, t1.favorites_count, t1.comment_count, t1.tag_edit_count, t1.forum_post_count, t1.signup_date, t1.about, t1.deleted_count, t1.note_edit_count, t2.group_name FROM $user_table as t1 JOIN $group_table AS t2 ON t2.id=t1.ugroup WHERE t1.id='$id'";
	$result = $db->query($query) or die($db->error);
	$row = $result->fetch_assoc();
	if($result->num_rows == 0)
	{
		print "<h2>Index not found!</h2>";
		exit;
	}
	$result->close();
	$user = $row['user'];
	$userid = $row['id'];
	$useremail = $row['email'];
?>
<table style="padding: 0px; margin: 0px; width: 100%;">
	<tr>
		<td style="width: 196px;">
			<span style="font-size: 18px; font-weight: bold;"><?php print $row['user']; ?></span><br>
			<span><?php print ucfirst(mb_strtolower($row['group_name'],'UTF-8')); ?></span><br>
		</td>
		<td style="text-align:justify; vertical-align: middle;">
			You may add this user as your friend or leave a message on their comment section. Do not give out any personal information on this area, or any area of the site. There is no need for it. Have fun creating drama! :3
		</td>
	</tr>
	</table>
	<table cellpadding="0" cellspacing="0" style="width: 100%;">
	<tr>
<?php
	if (file_exists('avatars/'.$userid.'.ava'))
	$avatar_url = $site_url.'avatars/'.$userid.'.ava';
	else if (isset($useremail) && $useremail != '' && $useremail != ' ' && $useremail != '	')
	$avatar_url = 'http://www.gravatar.com/avatar.php?gravatar_id='.md5(strtolower($useremail)).'&d=identicon&size=196';
	else $avatar_url = $site_url.'avatars/0.png';
?>
		<td style="width: 196px; text-align: center;">
			<img src="<?php print $avatar_url; ?>" width=192px alt="avatar" style="border: 0px solid #ccc; margin-top: 16px; border-radius: 16px;"><br>
			<div style="display:none">
				<a href="#" id="blacklist-count"></a>
				<a href="#" id="blacklisted-sidebar"></a>
			</div><br>
			<div style="width: 196px; text-align: left;">
				<table cellpadding="0" cellspacing="0" style="width: 100%; padding: 0px; margin: 0px;">
					<tr>
						<td style="text-align:center;">
						<?php print '<b><a href="'.$site_url.'index.php?page=atom&amp;user='.$row['user'].'">Subscribe</a></b>';
		$user2 = new user(); $misc = new misc();
		if($user2->gotpermission('is_admin')){print ' | <b><a href="'.$site_url.'admin/index.php?page=ban_user&amp;user_id='.$id.'">Ban User</a></b>';}
		?>

						<br><br>
						</td>
					</tr>
					<tr>
						<td style="text-align: center;"><b>Account Statistics</b>
						<br><br>
						</td>
					</tr>
					<tr>
						<td>
						<table cellpadding="0" cellspacing="0" class="highlightable" style="width: 100%; padding: 0px; margin: 0px; border-spacing: 1px;">
							<tr>
								<td width="90px">Joined</td>
								<td style="text-align: center;"><?php if(!is_null($row['signup_date']) && $row['signup_date']!='0'){ print date("Y-m-d",$row['signup_date']);} else { print "N/A";} ?></td>
							</tr>
							<tr>
								<td><b>Posts</b></td>
								<td style="text-align: center;"><a href="index.php?page=post&amp;s=list&amp;tags=user:<?php print $row['user']; ?>"><?php print $row['post_count']; ?></a></td>
							</tr>
							<tr>
								<td>Deleted Posts</td>
								<td style="text-align: center;"><?php print $row['deleted_count']; ?></td>
							</tr>
							<tr>
								<td>Favorites</td>
								<td style="text-align: center;"><a href="index.php?page=favorites&amp;s=view&amp;id=<?php print $id;?>"><?php print $row['favorites_count']; ?></a></td>
							</tr>
							<tr>
								<td>Comments</td>
								<td style="text-align: center;"><?php print $row['comment_count']; ?></td>
							</tr>
							<tr>
								<td>Tag Edits</td>
								<td style="text-align: center;"><a href="index.php?page=account&amp;s=tag_edits&amp;id=<?php print $id; ?>"><?php print $row['tag_edit_count']; ?></a></td>
							</tr>
							<tr>
								<td>Note Edits</td>
								<td style="text-align: center;"><?php print $row['note_edit_count']; ?></td>
							</tr>
							<tr>
								<td>Forum Posts</td>
								<td style="text-align: center;"><?php print $row['forum_post_count']; ?></td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
			</div>
		</td>
		<td style="width: 100%; border: 0px solid #ebebeb; padding: 10px;">
			<div style="font-size: 18px; font-weight: bold; padding-bottom: 10px;">Recent Favorites</div>
				<table class="highlightable" style="width: 100%; padding: 0px; margin: 0px;">
<?php
		$cache = new cache();
		$domain = $cache->select_domain();
?>
<?php 
		$query = "SELECT favorite, added FROM $favorites_table WHERE user_id='$id' ORDER BY added DESC LIMIT 5";
		$result = $db->query($query) or die($db->error);
		while($row = $result->fetch_assoc()) {
		$query = "SELECT id, directory as dir, image, tags, owner, rating, score FROM $post_table WHERE id='".$row['favorite']."'";
		$res = $db->query($query) or die($db->error);
		$r = $res->fetch_assoc();
?>
					<tr>
						<td style="width: 72px; padding: 5px; text-align: center;">
							<span id="favorite_p<?php print $r['id']; ?>"><a href="index.php?page=post&amp;s=view&amp;id=<?php print $r['id']; ?>"><img style="max-width: 64px;" src="<?php print $site_url.$thumbnail_folder.'/'.$r['dir']; ?>/<?php print $thumbnail_prefix.$r['image']; ?>" alt="<?php print $r['tags'].' Rating:'.$r['rating'].' Score:'.$r['score'].' User:'.$r['owner']; ?>" class="preview" title="<?php print $r['tags'].' Rating:'.$r['rating'].' Score:'.$r['score'].' User:'.$r['owner']; ?>"></a></span>
						</td>
						<td style="padding: 5px; vertical-align: middle;">
							<span style="font-size: 11px;">Favorited #<a href="http://gelbooru.com/index.php?page=post&amp;s=view&amp;id=<?php print $r['id']; ?>"><?php print $r['id']; ?></a> <?php print $misc->date_words($row['added']); ?></span>
						</td>
					</tr>
<?php
		$res->close();
		}
		if($result->num_rows<1)
		{
print '					<tr>
						<td style="padding: 5px; font-size: 11px;">No favorites have been added by this user yet!</td>
					</tr>
';
		}
?>				</table>
			<br>
			<div style="font-size: 18px; font-weight: bold; padding-bottom: 10px; padding-top: 15px;">Recent Uploads</div>
				<table class="highlightable" style="width: 100%; padding: 0px; margin: 0px;">
<?php
		$query = "SELECT id, creation_date, directory as dir, image, tags, rating, score, owner FROM $post_table WHERE owner='$user' ORDER BY id DESC LIMIT 5";
		$result = $db->query($query) or die($db->error);
		while($row = $result->fetch_assoc())
		{
?>
					<tr>
						<td style="padding: 5px; width: 72px; text-align: center;">
							<span id="upload_p<?php print $row['id']; ?>"><a href="index.php?page=post&amp;s=view&amp;id=<?php print $row['id']; ?>"><img style="max-width: 64px;" src="<?php print $site_url.$thumbnail_folder.'/'.$row['dir']; ?>/<?php print $thumbnail_prefix.$row['image']; ?>" alt="<?php print $row['tags'].' Rating:'.$row['rating'].' Score:'.$row['score'].' User:'.$row['owner']; ?>" class="preview" title="<?php print $row['tags'].' Rating:'.$row['rating'].' Score:'.$row['score'].' User:'.$row['owner']; ?>"></a></span>
						</td>
						<td style="vertical-align: middle;">
							<span style="font-size: 11px;">Uploaded #<a href="index.php?page=post&amp;s=view&amp;id=<?php print $row['id']; ?>"><?php print $row['id']; ?></a> <?php print $misc->date_words($row['creation_date']); ?></span>
						</td>
					</tr>
<?php
		}
		if($result->num_rows<1)
		{
print '					<tr>
						<td style="padding: 5px; font-size: 11px;">No posts have been added by this user yet!</td>
					</tr>
';
		}
		$result->close();
print '				</table>
		</td>
		<td style="width: 100%; border: 0px solid #ebebeb; padding: 10px;">
			<div style="width:480px; font-size: 18px; font-weight: bold; padding-bottom: 10px;">About Me';
	if(strtolower($userid) == strtolower($checked_user_id))
	{
		print ' [<a href="#" onclick="Finder.AboutMe.Show('.$userid.'); return false;">Edit</a>]';
	}
print '</div>
			<div style="width:480px; word-wrap: break-word">'; print $userc->show_about($userid); print'</div>
		</td>
	</tr>
	</table>
	</div>
</body>
</html>';
	}
?>