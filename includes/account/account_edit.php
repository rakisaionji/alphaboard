<?php
	$user = new user();
	$ip = $db->real_escape_string($_SERVER['REMOTE_ADDR']);	
	if (!$user->check_log() || $user->banned_ip($ip))
	{ header("Location:index.php?page=account&s=login&code=00"); exit; }
	if (!isset($_GET['id']) || $_GET['id'] == "" || $_GET['id'] != $checked_user_id)
	{ echo "0"; exit; }
	$id = $db->real_escape_string($_GET['id']);	
	$about = $user->get_about($_GET['id'],0);
	if (isset($_POST['about']) && $_POST['about'] != "")
	if ($user->update_about($_GET['id'], base64_decode($db->real_escape_string($_POST['about'])))) { print "1"; exit; }
	else { print "0"; exit; }
	if (isset($_GET['protocol']) && $_GET['protocol'] == "javascript")
	{
?>
	<form method="post" action="">
	<textarea cols="0" rows="0" id="about" wrap="hard" style="width: 512px; height: 256px; padding:8px; resize:vertical;"><?php print $about; ?></textarea>
    <br>
	<input type="submit" id="toggle" style="width: 128px; height: 24px; margin: 0px 10px 0px 0px;" onclick="Finder.AboutMe.Toggle('toggle', 'about'); return false;" value="Word-Wrap = YES">
	<input type="submit" style="width: 128px; height: 24px;" onclick="Finder.AboutMe.Save('about-me-box', <?php print $checked_user_id ?>, $('about').value); return false;" value="Save Settings">
	</form><?php
	}
	exit; ?>