<?php
$limit = 50;
$page_limit = 6;
$userc = new user();
$ip = $db->real_escape_string($_SERVER['REMOTE_ADDR']);
require 'includes/header.php';
print '	<div id="content">
		';
if(!isset($_GET['uname']) && !isset($_GET['id']))
{
	print "<h2>Unknown Parameters</h2>"; exit;
}
if($userc->banned_ip($ip))
{
	print "<h2>Action failed: ".$row['reason']."</h2>"; exit;
}	
if(!$userc->check_log())
{
	print "<h2>Access Denied</h2>"; exit;
}
if(isset($_GET['pid']) && $_GET['pid'] != "" && is_numeric($_GET['pid']) && $_GET['pid'] >= 0)
	$page = $db->real_escape_string($_GET['pid']);
else
	$page = 0;
if(isset($_GET['id']) && is_numeric($_GET['id']))
{
	$id = $db->real_escape_string($_GET['id']);
	if($id == '0')
	{
		print "<h2>Anonymous!?</h2>";
		exit;
	}
	$query = "SELECT t1.id, t1.user, t2.group_name FROM $user_table as t1 JOIN $group_table AS t2 ON t2.id=t1.ugroup WHERE t1.id='$id'";
	$result = $db->query($query) or die($db->error);
	$row = $result->fetch_assoc();
	if($result->num_rows == 0)
	{
		print "<h2>Index not found!</h2>";
		exit;
	}
	$result->free_result();
	$result->close();
	$user = $row['user'];
	$userid = $row['id'];
?><h2>Tag Edit History</h2><br>
		<h3><a href="index.php?page=account&amp;s=profile&amp;id=<?php print $row['id']; ?>"><?php print $row['user']; ?></a></h3>
		<h6><?php print ucfirst(mb_strtolower($row['group_name'],'UTF-8')); ?></h6><br>
		<table class="highlightable" style="width: 100%">
		<tr>
			<th width="5%">ID</th>
			<th width="85%">Tags</th>
			<th width="10%">Date</th>
		</tr><?php
	$query = "SELECT COUNT(*) FROM $tag_history_table WHERE user_id='$id' AND active='1' ORDER BY total_amount DESC";
	$result = $db->query($query) or die($db->error);
	$row = $result->fetch_assoc();
	$numrows = $row['COUNT(*)'];
	$query = "SELECT tags, id, updated_at FROM $tag_history_table WHERE user_id='$id' AND active='1' ORDER BY total_amount DESC LIMIT $page, $limit";
	$result = $db->query($query) or die($db->error);
	while($row = $result->fetch_assoc())
	{
	print '
		<tr>
			<td><a href="'.$site_url.'index.php?page=post&amp;s=view&amp;id='.$row['id'].'">'.$row['id'].'</a></td>
			<td>';
	$post = new post(); $misc = new misc();
	$ttags = explode(" ", mb_trim(html_entity_decode($row['tags'], ENT_QUOTES, "UTF-8")));
	foreach($ttags as $current)
	{
	$tagtype = $post->tag_type($current);
	print '
			<span class="tag-type-'.$tagtype.'">
			<a href="index.php?page=post&amp;s=list&amp;tags='.urlencode($current).'">'.str_replace('_',' ',$current).'</a>
			</span>';
	}
	print '</td>
			<td>
			<span title="'.$row['updated_at'].'">'.$misc->date_words(strtotime($row['updated_at'])).'</span>
			</td>
		</tr>';
	}
	print '
		</table>';
	$result->free_result();
?>

	</div>
	<div id="paginator">
		<div class="pagination">
			<?php print $misc->pagination($_GET['page'],$_GET['s'].'&amp;id='.$id,$row['id'],$limit,$page_limit,$numrows,$_GET['pid']); ?>

		</div>
	<br>
<?php	}	?>
	</div>
</body>
</html>