<?php
$post = new post();
$now = gmdate('Y-m-d\TH:i:s\Z');

$output == '';

$output .= '<?xml version="1.0" encoding="UTF-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">

<title>'.$site_url3.'</title>
<link href="'.$site_url.'index.php?page=atom" rel="self"/>
<link href="'.$site_url.'index.php?page=post&amp;s=list" rel="alternate"/>
<id>'.$site_url.'index.php?page=atom</id>
<updated>'.$now.'</updated>
<author><name>'.$site_url3.'</name></author>
';

if (isset($_GET['tags']) && $_GET['tags'] != ""){
$qtags = strtolower($db->real_escape_string(mb_trim(htmlentities($_GET['tags'], ENT_QUOTES, 'UTF-8'))));
$qtags_ = "tags LIKE '%$qtags%'";}
else	$qtags_ = '';
if (isset($_GET['user']) && $_GET['user'] != ""){
$quser = strtolower($db->real_escape_string($_GET['user']));
$quser_ = "owner='$quser'";}
else	$quser_ = '';
if (isset($qtags) && $qtags != "" && !isset($quser) && $quser == "")
$qfunc = "WHERE $qtags_";
else if (!isset($qtags) && $qtags == "" && isset($quser) && $quser != "")
$qfunc = "WHERE $quser_";
else if (isset($qtags) && $qtags != "" && isset($quser) && $quser != "")
$qfunc = "WHERE $qtags_ AND $quser_";
else	$qfunc = '';

$query = "SELECT * FROM $post_table $qfunc ORDER BY creation_date DESC LIMIT 0, 20";
$result = $db->query($query) or die($db->error);

while($row = $result->fetch_assoc()) {
$output .= '
<entry>
<title>'.html_entity_decode($row['tags'],ENT_QUOTES,"UTF-8").'</title>
<link href="'.$site_url.'index.php?page=post&amp;s=view&amp;id='.$row['id'].'" rel="alternate"/>
';

if($row['source'] != "" or $row['source'] != " ")
if(substr($post_data['source'],0,4) == "http")
$output .= '<link href="'.$row['source'].'" rel="related"/>';

$output .= '<id>'.$site_url.'index.php?page=post&amp;s=view&amp;id='.$row['id'].'</id>
<updated>'.gmdate('Y-m-d\TH:i:s\Z',$row['creation_date']).'</updated>
<summary>'.html_entity_decode($row['tags'],ENT_QUOTES,"UTF-8").'</summary>
<content type="xhtml">
<div xmlns="http://www.w3.org/1999/xhtml">
<a href="'.$site_url.'index.php?page=post&amp;s=view&amp;id='.$row['id'].'">
<img src="'.$site_url.$thumbnail_folder.'/'.$row['directory'].'/'.$thumbnail_prefix.$row['image'].'"/>
</a>
</div>
</content>
<author>
<name>'.$row['owner'].'</name>
</author>
</entry>
';}

$output .= '
</feed>';
header("Content-Type: application/rss+xml");
print $output;
?>
