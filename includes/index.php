<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
<?php 	print '	<title>'.$site_url3.'</title>
		<link rel="home" href="'.$site_url.'">
		<link rel="index" href="'.$site_url.'toc.php">
		<link rel="shortcut icon" href="'.$site_url.'favicon.ico" type="image/x-icon">
		<link rel="alternate" type="application/rss+xml" title="RSS" href="'.$site_url.'index.php?page=atom">
		<link rel="search" type="application/opensearchdescription+xml" title="'.$site_url3.'" href="'.$site_url.'rinnechan.xml">
		<link rel="stylesheet" type="text/css" media="screen" href="'.$site_url.'default.css" title="default">

		<meta http-equiv="content-type" content="text/html; charset=utf-8">
		<meta name="author" content="Racozsoft Corporation">
		<meta name="keywords" content="'.implode(', ',$site_kwrd).'">
		<meta name="description" content="'.$site_desc.'">
		<meta name="generator" content="AlphaBoard 1.0.6e">

		<script src="'.$site_url.'script/core.js" type="text/javascript"></script>
		<script src="'.$site_url.'script/global.js" type="text/javascript"></script>
	</head>

	<body>
		<div id="static-index">';
		if ($csslogomain == true)
			echo '
			<h1 id="static-index-header">';
		else
			echo '
			<h1 id="header" style="font-size: 52px; margin-top: 1em;">';
		echo '<a href="'.$site_url.'">'.$site_url3.'</a></h1>
			<noscript>
			<br>
			<center>
				<div class="error-notice" style="width: 500px;">
					JavaScript must be enabled to use this site.<br>
					Please enable JavaScript in your browser and refresh the page.
				</div>
			</center>
			<br>
			</noscript>
	';
?>

			<div class="space" id="links" style="display:none; margin-bottom: 24px;">
				<a id="account" href="index.php?page=account&amp;s=home"><?php $user = new user(); echo ($user->check_log()) ? 'My Account' : 'Login/Signup'; ?></a>
				<a href="index.php?page=post&amp;s=list">Posts</a>
				<a href="index.php?page=comment&amp;s=list">Comments</a>
				<a href="index.php?page=tags&amp;s=list">Tags</a>
				<a href="index.php?page=forum&amp;s=list">Forum</a>
				<a href="toc.php">&raquo</a>
			</div>

			<center>
			<div id="searchbox" style="display:none; width: 500px; padding: 0px; margin-bottom: 24px; text-align: left;">
				<form action="index.php?page=search" method="post">
				<input id="tags" name="tags" size="30" type="text" class="main_search_text" value="">
				<input name="searchDefault" type="submit" class="main_search" value="Search">
				</form>
			</div>
			</center>

			<div style="font-size: 80%; margin-bottom: 2em;">
				<p>
<?php
	$query = "UPDATE $hit_counter_table SET count=count+1";
	$db->query($query);
	$query = "SELECT t1.pcount, t2.count FROM $post_count_table AS t1 JOIN $hit_counter_table as t2 WHERE t1.access_key='posts'";
	$result = $db->query($query);
	$row = $result->fetch_assoc();
print '					<a href="tos.php">Terms of service</a> - Total number of visitors so far: '.number_format($row['count']).'<br>
					Running <a href="">AlphaBoard</a> 1.0.6e HTML 4.01 build 20111001<br>
				</p>
				<br>
				<div id="nekocounter" style="margin-bottom:24px;">';
	for ($i=0;$i<strlen($row['pcount']);$i++) 
	{
		$digit=substr($row['pcount'],$i,1);
		print '
					<img src="./counter/'.$digit.'.gif" border="0" alt="'.$digit.'">'; 						
	}
	print '
				</div>

				<div style="display:none; text-align: center;" id="addsearch">
					Have you tried our search plugin available for 
					<a href="#" onclick="addEngine(\''.strtolower($site_url4).'\',\'png\',\'0\',\''.$site_url.'\')">Firefox</a>?
				</div>

				<p>Original concept by <a href="http://danbooru.donmai.us" target="_blank">Danbooru</a>. Coded by <a href="">Raki Saionji</a>.</p>

				<p>Best viewed with <a href="http://www.getfirefox.com" target="_blank">Firefox 4</a> 
				and <a href="https://developer.mozilla.org/en/JavaScript" target="_blank">JavaScript</a> enabled.</p>

				<br>

				<!--
				We claim that our documents are all W3C validated!
				If you find any error in documents, please report!
				-->

				<!--
				Those badges are outdated now, we don\'t need them anymore!
				If you found errors in our markup, please post a ticket on our Trac site! 
				<p>
				<a href="http://validator.w3.org/check?uri=referer" target="_blank">
				<img alt="Valid HTML 4.01" src="'.$site_url.'public/valid-html401" title="Valid HTML 4.01 Transitional">
				</a>
				<a href="http://jigsaw.w3.org/css-validator/check/referer" target="_blank">
				<img alt="Valid CSS 2.1" src="'.$site_url.'public/valid-css2" title="Valid CSS Level 2.1">
				</a>
				<img alt="Valid XML 1.0" src="'.$site_url.'public/valid-xml10" title="Valid XML 1.0">
				</p>

				<p style="display:none">
				<a href="http://feed2.w3.org/check.cgi?url='.$site_url.'index.php?page=atom">
				<img alt="Valid Atom 1.0" src="'.$site_url.'public/valid-atom" title="Valid Atom 1.0 Feed">
				</a>
				</p>
				-->
			</div>
		</div>

		<script type="text/javascript">
			$("links").show();
			$("searchbox").show();
			$("addsearch").show();
		</script>
	</body>
</html>';
?>