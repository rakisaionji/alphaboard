<?php
	//number of tags/page
	$limit = 100;
	//number of pages to display. number - 1. ex: for 5 value should be 4
	$page_limit = 6;
	require "includes/header.php";
	print '	<div id="content">';

	if (isset($_GET['tags']) && $_GET['tags'] != ""){
		$qtags = strtolower($db->real_escape_string(str_replace("*",'%',str_replace("?",'_',mb_trim(htmlentities($_GET['tags'], ENT_QUOTES, 'UTF-8'))))));
		$qtags_ = "tag LIKE '$qtags'";}
	else	$qtags_ = '';

	if (isset($_GET['type']) && $_GET['type'] != ""){
		$qtype = strtolower($db->real_escape_string($_GET['type']));
		$qtype_ = "type='$qtype'";}
	else	$qtype_ = '';

	if (isset($qtags) && $qtags != "" && !isset($qtype) && $qtype == "")
		$qfunc = " AND $qtags_";
	else if (!isset($qtags) && $qtags == "" && isset($qtype) && $qtype != "")
		$qfunc = " AND $qtype_";
	else if (isset($qtags) && $qtags != "" && isset($qtype) && $qtype != "")
		$qfunc = " AND $qtags_ AND $qtype_";
	else	$qfunc = '';

	if (isset($_GET['order_by']) && $_GET['order_by'] != ""){
		$qorder = strtolower($db->real_escape_string($_GET['order_by']));
		$qorder_ = "ORDER BY $qorder";}
	else {
		$qorder = 'version';
		$qorder_ = 'ORDER BY version';
	}

	if (isset($_GET['sort']) && $_GET['sort'] != "")
		$qsort = strtoupper($_GET['sort']);
	else	$qsort = 'DESC';

	echo '
		<form action="index.php?page=tags&amp;s=list" method="get">
		<table class="form">
		<tfoot>
			<tr>
			<td>
			<input name="page" value="tags" type="hidden">
			<input name="s" value="list" type="hidden">
			</td>
			<td>
			<input value="Search" type="submit" style="width: 64px; height: 24px;">
			</td>
			</tr>
		</tfoot>
		<tbody>
			<tr>
			<th width="15%">
			<label for="name">Name</label>
			</th>
			<td width="85%">
			<input id="name" name="tags" style="width: 256px; height: 20px;" value="';
	print stripslashes($_GET['tags']);
	echo '" type="text">
			</td>
        		</tr>
			<tr>
			<th>
			<label for="type">Type</label>
			</th>
			<td>
			<select id="type" name="type" style="width: 264px; height: 20px;">
			<option value=""';
		if($qtype == '') echo 'selected="selected"'; else echo '';
	echo '>Any</option>
			<option value="new"';
		if($qtype == 'new') echo 'selected="selected"'; else echo '';
	echo '>New</option>
			<option value="general"';
		if($qtype == 'general') echo 'selected="selected"'; else echo '';
	echo '>General</option>
			<option value="character"';
		if($qtype == 'character') echo 'selected="selected"'; else echo '';
	echo '>Character</option>
			<option value="copyright"';
		if($qtype == 'copyright') echo 'selected="selected"'; else echo '';
	echo '>Copyright</option>
			<option value="artist"';
		if($qtype == 'artist') echo 'selected="selected"'; else echo '';
	echo '>Artist</option>
			<option value="circle"';
		if($qtype == 'circle') echo 'selected="selected"'; else echo '';
	echo '>Circle</option>
			<option value="faults"';
		if($qtype == 'faults') echo 'selected="selected"'; else echo '';
	echo '>Faults</option>
			</select>
			</td>
			</tr>
			<tr>
			<th>
			<label for="type">Order</label>
			</th>
			<td>
			<select id="sort" name="sort" style="width: 264px; height: 20px;">
			<option value="asc" ';
		if($_GET['sort'] == 'asc') echo 'selected="selected"'; else echo '';
	echo '>Ascending</option>
			<option value="desc" ';
		if($_GET['sort'] == 'desc') echo 'selected="selected"'; else echo '';
	echo '>Descending</option>
			</select>
			</td>
			</tr>
			<tr>
			<th>
			<label for="order">Sort</label>
			</th>
			<td>
			<select id="order" name="order_by" style="width: 264px; height: 20px;">
			<option value="tag"';
		if($qorder == 'tag') echo 'selected="selected"'; else echo '';
	echo '>Name</option>
			<option value="version"';
		if($qorder == 'version') echo 'selected="selected"'; else echo '';
	echo '>Version</option>
			<option value="type"';
		if($qorder == 'type') echo 'selected="selected"'; else echo '';
	echo '>Type</option>
			<option value="index_count"';
		if($qorder == 'index_count') echo 'selected="selected"'; else echo '';
	echo '>Total Count</option>
			</select>
			</td>
			</tr>
		</tbody>
		</table>
		</form>';
	if(isset($_GET['pid']) && $_GET['pid'] != "" && is_numeric($_GET['pid']) && $_GET['pid'] >= 0)
	$page = $db->real_escape_string($_GET['pid']);
else	$page = 0;
	$query = "SELECT COUNT(*) FROM $tag_index_table WHERE index_count > 0 $qfunc $qorder_ $qsort";
	$result = $db->query($query);
	$row = $result->fetch_assoc();
	$count = $row['COUNT(*)'];	
	$numrows = $count;
	$result->free_result();
	print '
		<table class="highlightable" style="width: 100%;">
			<thead>
			<tr>
			<th width="8%">Posts</th>
			<th width="45%">Name</th>
			<th width="47%">Type</th>
			</tr>
			</thead>
			<tbody>';
	$query = "SELECT * FROM $tag_index_table WHERE index_count > 0 $qfunc $qorder_ $qsort LIMIT $page, $limit";
	$result = $db->query($query) or die($db->error);
	while($row = $result->fetch_assoc()) {
	echo '
			<tr>
			<td>'.$row['index_count'].'</td>
			<td>
			<span class="tag-type-'.$row['type'].'">
			<a href="index.php?page=post&amp;s=list&amp;tags='.urlencode(html_entity_decode($row['tag'], ENT_QUOTES, 'UTF-8')).'">'.$row['tag'].'</a>
			</span>
			</td>
			<td>'.$row['type'].' (<a href="index.php?page=tags&amp;s=edit&amp;tag='.urlencode(html_entity_decode($row['tag'], ENT_QUOTES, 'UTF-8')).'">edit</a>)</td>
			</tr>'; }
	echo '
			</tbody>
		</table>
		<div id="paginator">';
	$misc = new misc();
	print '
			'.$misc->pagination($_GET['page'],$_GET['s'].'&amp;type='.$_GET['type'].'&amp;sort='.$_GET['sort'].'&amp;order_by='.$_GET['order_by'],$id,$limit,$page_limit,$numrows,$_GET['pid'],$_GET['tags']);
?>

		</div>
		<br>
	</div>
</body>
</html>