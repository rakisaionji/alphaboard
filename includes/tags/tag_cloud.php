<?php
	function printTagCloud($tags)
	{
	ksort($tags);
	$max_size = 32;
	$min_size = 12;
	$max_qty = max(array_values($tags));
        $min_qty = min(array_values($tags));

	$spread = $max_qty - $min_qty;
	if ($spread == 0) $spread = 1;

	$step = ($max_size - $min_size) / ($spread);

	foreach ($tags as $key => $value)
	{
		$tag = explode(' ',$key);
		$size = round($min_size + (($value - $min_qty) * $step));
		echo '
		<span class="tag-type-'.$tag[1].'">
		<a href="index.php?page=post&amp;s=list&amp;tags='.urlencode(html_entity_decode($tag[0],ENT_QUOTES,'UTF-8')).'" style="font-size: '.$size.'px" title="'.$value.' post';
		if($value > 1) echo 's'; 
		echo '">'.$tag[0].'</a>
		</span>';
	}
	}

	require "includes/header.php";
	print '	<div id="content">';
	$query = "SELECT * FROM $tag_index_table WHERE index_count > 0 ORDER BY index_count DESC LIMIT 0, 250";
	$result = $db->query($query);
	$tags = array();
	while($row = $result->fetch_assoc()) $tags[$row['tag'].' '.$row['type']] = $row['index_count'];
	printTagCloud($tags);
?>

	</div>
</body>
</html>