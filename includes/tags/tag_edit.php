<?php
	$user = new user();
	$ctags = new tag();
	$post = new post();
	$ip = $db->real_escape_string($_SERVER['REMOTE_ADDR']);	
	
	/* <p><b><a href="index.php?page=tags&amp;s=edit&amp;tags='.$qtags.'&amp;type='.$qtype.'">&raquo; Continue Editing</a></b></p> */

	$menu =	'
	<p><b><a href="index.php?page=tags&amp;s=list">&raquo; Back to Tags List</a></b></p>
	</div>
</body>
</html>';

	if(!$user->gotpermission('edit_tags'))
	{
		header("Location:index.php?page=account&s=login&code=00");
		exit;
	}

	require "includes/header.php";
	echo '	<div id="content">
	<h2>Tag Edit</h2><br>';

	if($user->banned_ip($ip))
	{
		print "<h2>Action failed: ".$row['reason']."</h2>";
		exit;
	}

	if (isset($_GET['tag']) && $_GET['tag'] != "")
	{	$qtags = strtolower($db->real_escape_string($_GET['tag']));
		$qtype = $post->tag_type($qtags); }
	else	{ $qtags = ''; $qtype = ''; }

	if (isset($_POST['tag']) && $_POST['tag'] != "" && isset($_POST['type']) && $_POST['type'] != "")
	{
	$tag = stripslashes(strtolower(htmlentities(str_replace(' ','_',$_POST['tag']))));
	$type = strtolower($db->real_escape_string($_POST['type']));
	if ($ctags->updatetag($tag, $type)) {
		print '<h3>Status</h3><br><h4>Tag Updated!</h4><br>'; print $menu;
		exit; }
	else	{ print '<h3>Status</h3><br><h4>Updating Error!</h4><br>'; print $menu;
		exit; }
	}
	echo'
	<form action="index.php?page=tags&amp;s=edit" method="post">
	<table class="form">
		<tbody>
		<tr>
		<th width="15%"><label for="tag_name">Name</label></th>
		<td width="85%"><input id="tag_name" style="width: 256px; height: 20px;" name="tag" ';
	echo 'value="'.stripslashes(stripslashes($qtags)).'"';
	echo ' type="text">
		</td>
		</tr>
		<tr>
		<th><label for="tag_tag_type">Type</label></th>
		<td>
		<select id="tag_tag_type" name="type" style="width: 264px; height: 20px;">
		<option value="general"';
		if($qtype == 'general') echo 'selected="selected"'; else echo '';
	echo '>General</option>
		<option value="character"';
		if($qtype == 'character') echo 'selected="selected"'; else echo '';
	echo '>Character</option>
		<option value="copyright"';
		if($qtype == 'copyright') echo 'selected="selected"'; else echo '';
	echo '>Copyright</option>
		<option value="artist"';
		if($qtype == 'artist') echo 'selected="selected"'; else echo '';
	echo '>Artist</option>
		<option value="circle"';
		if($qtype == 'circle') echo 'selected="selected"'; else echo '';
	echo '>Circle</option>
		<option value="faults"';
		if($qtype == 'faults') echo 'selected="selected"'; else echo '';
	echo '>Faults</option>
		</select>
		</td>
		</tr>
		<tr>
		<td>
		</td>
		<td>
		<input name="commit" value="Save" type="submit" style="width: 64px;">
		<input onclick="history.back();" value="Cancel" type="button" style="width: 64px;">
		</td>
		</tr>
		</tbody>
	</table>
	</form>
	</div>';
?>

</body>
</html>