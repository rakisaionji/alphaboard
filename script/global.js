/*func.js*/
/*
	Core Components: core.js
	+ Prototype 1.7 (prototype.js)
	+ Scriptaculous 1.9.0 Library
	  - Effects (effects.js)
	  - DragDrop (dragdrop.js)
	> Compiled with YUI Compressor 2.4.6
	> Tested with AlphaBoard 1.1.x Branch
	> Best executed on FireFox 4.x Mac & Win
*/
/*
	Some functions can be found on Danbooru's souce.
	cookie.js : Copyright (C) danbooru.donmai.us.
*/
/*
	AlphaBoard Javascript Library build 20110601
	Original by Geltas from Gelbooru.com Network
	Rewrited by Raki Saionji from Racozsoft Corp
	This javascript requires core-min.js library
	Released under NO LICENSE !!!!!!!!!!!!!!!!!!
*/
/*
	Cockie cookie.js
	Original From Danbooru
	Modified by Raki Saionji
*/
//This is essential
/**
*
*  Base64 encode / decode
*  http://www.webtoolkit.info/
*
**/

var Base64 = {

	// private property
	_keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

	// public method for encoding
	encode : function (input) {
		var output = "";
		var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
		var i = 0;

		input = Base64._utf8_encode(input);

		while (i < input.length) {

			chr1 = input.charCodeAt(i++);
			chr2 = input.charCodeAt(i++);
			chr3 = input.charCodeAt(i++);

			enc1 = chr1 >> 2;
			enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
			enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
			enc4 = chr3 & 63;

			if (isNaN(chr2)) {
				enc3 = enc4 = 64;
			} else if (isNaN(chr3)) {
				enc4 = 64;
			}

			output = output +
			this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
			this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

		}

		return output;
	},

	// public method for decoding
	decode : function (input) {
		var output = "";
		var chr1, chr2, chr3;
		var enc1, enc2, enc3, enc4;
		var i = 0;

		input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

		while (i < input.length) {

			enc1 = this._keyStr.indexOf(input.charAt(i++));
			enc2 = this._keyStr.indexOf(input.charAt(i++));
			enc3 = this._keyStr.indexOf(input.charAt(i++));
			enc4 = this._keyStr.indexOf(input.charAt(i++));

			chr1 = (enc1 << 2) | (enc2 >> 4);
			chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
			chr3 = ((enc3 & 3) << 6) | enc4;

			output = output + String.fromCharCode(chr1);

			if (enc3 != 64) {
				output = output + String.fromCharCode(chr2);
			}
			if (enc4 != 64) {
				output = output + String.fromCharCode(chr3);
			}

		}

		output = Base64._utf8_decode(output);

		return output;

	},

	// private method for UTF-8 encoding
	_utf8_encode : function (string) {
		string = string.replace(/\r\n/g,"\n");
		var utftext = "";

		for (var n = 0; n < string.length; n++) {

			var c = string.charCodeAt(n);

			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}

		}

		return utftext;
	},

	// private method for UTF-8 decoding
	_utf8_decode : function (utftext) {
		var string = "";
		var i = 0;
		var c = c1 = c2 = 0;

		while ( i < utftext.length ) {

			c = utftext.charCodeAt(i);

			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			}
			else if((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i+1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			}
			else {
				c2 = utftext.charCodeAt(i+1);
				c3 = utftext.charCodeAt(i+2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}

		}

		return string;
	}

}

//End here!
// Some new functions for my beloved Prototype.js
Element.addMethods({
/* Script by: www.jtricks.com
 * Version: 1.0 (20110615)
 * Latest version:
 * www.jtricks.com/javascript/blocks/ordering.html
 */
getAbsoluteDivs:function ()
{
    var arr = new Array();
    var all_divs = document.body.getElementsByTagName("DIV");
    var j = 0;

    for (i = 0; i < all_divs.length; i++)
    {
        if (all_divs.item(i).currentStyle)
            style = all_divs.item(i).currentStyle.position;
        else
            style = window.getComputedStyle(
                all_divs.item(i), null).position;

        if ('absolute' == style || 'relative' == style || 'fixed' == style)
        {
            arr[j] = all_divs.item(i);
            j++;
        }
    }
    return arr;
}
,
bringToFront:function (id)
{
    if (!document.getElementById ||
        !document.getElementsByTagName)
        return;

    var obj = document.getElementById(id);
    var divs = this.getAbsoluteDivs();
    var max_index = 100;
    var cur_index;

    // Compute the maximal z-index of
    // other absolute-positioned divs
    for (i = 0; i < divs.length; i++)
    {
        var item = divs[i];
        if (item == obj ||
            item.style.zIndex == '')
            continue;

        cur_index = parseInt(item.style.zIndex);
        if (max_index < cur_index)
        {
            max_index = cur_index;
        }
    }

    obj.style.zIndex = max_index + 1;
},

sendToBack:function (id)
{
    if (!document.getElementById ||
        !document.getElementsByTagName)
        return;

    var obj = document.getElementById(id);
    var divs = this.getAbsoluteDivs();
    var min_index = 999999;
    var cur_index;

    if (divs.length < 2)
        return;

    // Compute the minimal z-index of
    // other absolute-positioned divs
    for (i = 0; i < divs.length; i++)
    {
        var item = divs[i];
        if (item == obj)
            continue;

        if (item.style.zIndex == '')
        {
            min_index = 0;
            break;
        }

        cur_index = parseInt(item.style.zIndex);
        if (min_index > cur_index)
        {
            min_index = cur_index;
        }

    }

    if (min_index > parseInt(obj.style.zIndex))
    {
        return;
    }

    obj.style.zIndex = 1;

    if (min_index > 1)
        return;

    var add = min_index == 0 ? 2 : 1;

    for (i = 0; i < divs.length; i++)
    {
        var item = divs[i];
        if (item == obj)
            continue;

        item.style.zIndex += add;
    }
}});
// End here! Now, Danbooru's Cookie.js!
Cookie = {
  put: function(name, value, days) {
    if (days == null) {
      days = 365
    }

    var date = new Date()
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000))
    var expires = "; expires=" + date.toGMTString()
    document.cookie = name + "=" + encodeURIComponent(value) + expires + "; path=/"
  },

  raw_get: function(name) {
    var nameEq = name + "="
    var ca = document.cookie.split(";")

    for (var i = 0; i < ca.length; ++i) {
      var c = ca[i]

      while (c.charAt(0) == " ") {
        c = c.substring(1, c.length)
      }

      if (c.indexOf(nameEq) == 0) {
        return c.substring(nameEq.length, c.length)
      }
    }

    return ""
  },
  
  get: function(name) {
    return this.unescape(this.raw_get(name))
  },
  
  remove: function(name) {
    Cookie.put(name, "", -1)
  },

  unescape: function(val) {
    return decodeURIComponent(val.replace(/\+/g, " "))
  }
}
/*
	app.js
	Original by Geltas
	Remixed by Raki Saionji
*/
/*function updatePost()
{
	try
	{
		HttpRequest = new XMLHttpRequest() 
	}
	catch(e)
	{
		try
		{
			HttpRequest = new ActiveXObject("Msxml2.XMLHTTP")
		}
		catch(e)
		{
			try
			{
				HttpRequest = new ActiveXObject("Microsoft.XMLHTTP")
			}
			catch(e)
			{
				alert("Your browser is too old or does not support Ajax.")
			}
		}
	}
	var tags = encodeURIComponent(encodeURI(document.getElementById("tags").value))
	var rating = document.getElementById("rating").value
	var source = encodeURIComponent(encodeURI(document.getElementById("source").value))
	var title = encodeURIComponent(encodeURI(document.getElementById("title").value))
	HttpRequest.onreadystatechange=function()
	{
		if(HttpRequest.readyState==4)
		{
			//alert("Request Completed.")//document.getElementById("test2").value=HttpRequest.responseText
		}
	}
	HttpRequest.open("GET","public/edit_post.php?id=" + id + "&tags=" + tags + "&rating=" + rating + "&source=" + source + "&title=" + title,true)
	HttpRequest.send(null)
}*/

function showHide(id)
{
	Effect[Element.visible(id) ? 'Fade' : 'Appear'](id)
}

function addFav(id)
{
new Ajax.Request("./public/addfav.php", {
  method: 'get',
      parameters: {
        "id": id
      },
  onSuccess: function(AjaxRequest) {
			if(AjaxRequest.responseText == "1")
			{
				notice("Post already in your favorites")
			}
			else if(AjaxRequest.responseText == "2")
			{
				notice("You are not logged in")
			}
			else
			{
				updateScore("fav"+id,AjaxRequest.responseText)
				notice("Post added to favorites")
			}
  }
});
/*	var HttpRequest
	try
	{
		HttpRequest = new XMLHttpRequest() 
	}
	catch(e)
	{
		try
		{
			HttpRequest = new ActiveXObject("Msxml2.XMLHTTP")
		}
		catch(e)
		{
			try
			{
				HttpRequest = new ActiveXObject("Microsoft.XMLHTTP")
			}
			catch(e)
			{
				alert("Your browser is too old or does not support Ajax.")
			}
		}
	}
	HttpRequest.onreadystatechange=function()
	{
		if(HttpRequest.readyState==4)
		{
			if(HttpRequest.responseText == "1")
			{
				notice("Post already in your favorites")
			}
			else if(HttpRequest.responseText == "2")
			{
				notice("You are not logged in")
			}
			else
			{
				updateScore("fav"+id,HttpRequest.responseText)
				notice("Post added to favorites")
			}
		}
	}
	HttpRequest.open("GET","public/addfav.php?id=" + id,true)
	HttpRequest.send(null)*/
}
/*
Obsolette and Deprecated form of using Notice()
function notice(msg)
{
	$('notice').update(msg).appear()
}
Disable it to use the new Notice() function from Imouto
*/
function vote(pid, comment_id, up_down)
{
	//pid = encodeURI(pid)
new Ajax.Request("index.php?page=comment&s=vote", {
  method: 'get',
      parameters: {
        "id": pid,
		"cid":comment_id,
		"vote":up_down,
      },
  onSuccess: function(AjaxRequest) {
			updateScore("sc" + comment_id,parseInt(AjaxRequest.responseText))
  }
});/*	var HttpRequest
	try
	{
		HttpRequest = new XMLHttpRequest() 
	}
	catch(e)
	{
		try
		{
			HttpRequest = new ActiveXObject("Msxml2.XMLHTTP")
		}
		catch(e)
		{
			try
			{
				HttpRequest = new ActiveXObject("Microsoft.XMLHTTP")
			}
			catch(e)
			{
				alert("Your browser is too old or does not support Ajax.")
			}
		}
	}
	HttpRequest.onreadystatechange=function()
	{
		if(HttpRequest.readyState==4)
		{
			updateScore("sc" + comment_id,parseInt(HttpRequest.responseText))
		}
	}
	pid = encodeURI(pid)
	HttpRequest.open("GET", "index.php?page=comment&id=" + pid + "&s=vote&cid=" + comment_id + "&vote=" + up_down,true)
	HttpRequest.send(null)*/
}
function post_vote(pid, up_down)
{
new Ajax.Request("index.php?page=post&s=vote", {
  method: 'get',
      parameters: {
        "id": pid,
		"type":up_down,
      },
  onSuccess: function(AjaxRequest) {
			updateScore("psc"+pid,parseInt(AjaxRequest.responseText))
  }
});/*	var HttpRequest
	try
	{
		HttpRequest = new XMLHttpRequest() 
	}
	catch(e)
	{
		try
		{
			HttpRequest = new ActiveXObject("Msxml2.XMLHTTP")
		}
		catch(e)
		{
			try
			{
				HttpRequest = new ActiveXObject("Microsoft.XMLHTTP")
			}
			catch(e)
			{
				alert("Your browser is too old or does not support Ajax.")
			}
		}
	}
	HttpRequest.onreadystatechange=function()
	{
		if(HttpRequest.readyState==4)
		{
			updateScore("psc"+pid,parseInt(HttpRequest.responseText))
		}
	}
	pid = encodeURI(pid)
	HttpRequest.open("GET", "index.php?page=post&s=vote&id=" + pid + "&type=" + up_down,true)
	HttpRequest.send(null)*/
}
function updateScore(id, score)
{
	$(id).innerHTML=score
}
function cflag(id)
{
	var r = prompt("Why should this post be flagged for deletion?")
	if(r == "" || r == null)
	{
		return false
	}
	else
	{
new Ajax.Request('./public/report.php?type=comment', {
  method: 'get',
      parameters: {
        "rid": id,
		"reason":r,
      },
  onSuccess: function(AjaxRequest) {
			if(AjaxRequest.responseText == "pass")
			{
				notice("Comment #" + id + " has been reported as spam")
				$('rc' + id).innerHTML='<b>Reported</b>'
				$('rcl' + id).innerHTML= null
			}
  }
});
	}
	/*
	var HttpRequest
	try
	{
		HttpRequest = new XMLHttpRequest() 
	}
	catch(e)
	{
		try
		{
			HttpRequest = new ActiveXObject("Msxml2.XMLHTTP")
		}
		catch(e)
		{
			try
			{
				HttpRequest = new ActiveXObject("Microsoft.XMLHTTP")
			}
			catch(e)
			{
				alert("Your browser is too old or does not support Ajax.")
			}
		}
	}
	
	var r = prompt("Why should this post be flagged for deletion?")
	if(r == "" || r == null)
	{
		return false
	}
	r = encodeURIComponent(encodeURI(r))
	HttpRequest.open('GET','public/report.php?type=comment&rid=' + id + '&reason='+r, true)
	HttpRequest.send(null)
	HttpRequest.onreadystatechange=function()
	{
		if(HttpRequest.readyState==4)
		{
			if(HttpRequest.responseText == "pass")
			{
				notice("Comment #" + id + " has been reported as spam")
				$('rc' + id).innerHTML='<b>Reported</b>'
				$('rcl' + id).innerHTML=''
			}
		}
	}*/
}
function filterComments(post_id, comment_size)
{
	var cignored = []
	
	for (i in posts[post_id].comments) 
	{
		var hidden = false
		
		if (posts[post_id].comments[i].score < cthreshold) 
		{
			hidden = true
		}

		if (!hidden && users.include(posts[post_id].comments[i].user)) 
		{
			hidden = true
		}
		
		if (hidden) 
		{
			showHide('c' + i)
			cignored.push('c' + i)
			posts[post_id].ignored[i] = i
		}
	}	

	if (cignored.length > 0) 
	{
		$('ci').innerHTML = ' (' + cignored.length + ' hidden)'
	}
}
function showHideIgnored(post_id,id)
{
	var j = 0
	if(id == 'ci')
	{	
		for(i in posts[post_id].ignored)
		{
			j++
			showHide('c' + i)
		}
		if($('ci').innerHTML != " (" + j + " hidden)")
		{
			$('ci').innerHTML = " (" + j + " hidden)"
		}
		else
		{
			$('ci').innerHTML = " (0 hidden)"
		}
	}
	else if(id == 'pi')
	{
		for(i in pignored)
		{
			j++
			showHide('p' + i)
		}
		if($('blacklist-count').innerHTML != j && j > 0)
		{
			$('blacklist-count').innerHTML = j
		}
		else if($('blacklist-count').innerHTML == j && j > 0)
		{
			$('blacklist-count').innerHTML = 0
		}
		else
		{
			$('blacklist-count').innerHTML = ""
		}
	}
}
function filterPosts(posts) {
	var users = Cookie.get("user_blacklist").split(" ")
	var tags = Cookie.get("tag_blacklist").split(" ") || ''
	var threshold = parseInt(Cookie.get("post_threshold")) || 0
	var ttags = Array()
	var ignored = []
	var g = 0        
	tags.each(function(j){
		if(j != "" && j != " "){
			ttags[g] = j.toLowerCase()
			++g
		}
	})
	tags = ttags

	for (i in posts)  
	{
		var hidden = false

		if (posts[i].score < threshold || users.include(posts[i].user) || tags.include('user:'+posts[i].user.toLowerCase()) || tags.include('rating:'+posts[i].rating.toLowerCase()) || posts[i].tags.any(function(t){return tags.include(t)}))
		{
			hidden = true
		}

		if (hidden) 
		{
			Effect.Fade('p' + i)
			ignored.push('p' + i)
			pignored[i] = i
		}
	}
	if (ignored.length > 0) 
	{
		$('blacklist-count').innerHTML=ignored.length
		$('blacklisted-sidebar').appear()
	}
}
function filterCommentList(comment_size) 
{
	var tags = Cookie.get("tag_blacklist").split(" ")
	var threshold = parseInt(Cookie.get("post_threshold")) || 0
	var auto_hidden = false
	var ttags = Array()
	var cignored = []
	var lastid = 0
	var g = 0
	var j = 0
	tags.each(function(j){
		if(j != "" && j != " "){
			ttags[g] = j.toLowerCase()
			++g
		}
	})
	tags = ttags
	for (i in posts.comments) 
	{
		var hidden = false
		if(lastid != posts.comments[i].post_id)
		{
			auto_hidden = false
			if(tags.include('rating:'+posts.rating[posts.comments[i].post_id].toLowerCase()) || tags.any(function(t){return posts.tags[posts.comments[i].post_id].include(t.toLowerCase())}) || posts.score[posts.comments[i].post_id] < threshold)
			{
				auto_hidden = true
			}
			if(posts.totalcount[lastid] <= j)
			{
				Effect.Fade('p' + lastid)
				phidden[lastid] = true
			}
			else
			{
				phidden[lastid] = false
			}
			j = 0
			lastid = posts.comments[i].post_id
		}
		if (!auto_hidden && posts.comments[i].score < cthreshold) 
		{
			hidden = true
		}

		if (!auto_hidden && !hidden && users.include(posts.comments[i].user) && tags.include('user:'+posts.comments[i].user.toLowerCase())) 
		{
			hidden = true
		}

		if (hidden || auto_hidden) 
		{
			Effect.Fade('comment-' + i)
			cignored.push('comment-' + i)
			posts.ignored[i] = i
			++j
		}
	}	
	if(posts.totalcount[lastid] <= j)
	{
		Effect.Fade('p' + lastid)
		phidden[lastid] = true
	}
	else
	{
		phidden[lastid] = false
	}
	if (cignored.length > 0) 
	{
		$('ci').innerHTML = '(' + cignored.length + ' hidden)'
	}
}

function showHideCommentListIgnored(id)
{
	var j = 0
	var lastpid = 0
	for(i in posts.ignored)
	{
		++j
		showHide('comment-' + i)
		if(phidden[posts.comments[i].post_id] && lastpid != posts.comments[i].post_id)
		{
			showHide("p" + posts.comments[i].post_id)
			lastpid = posts.comments[i].post_id
		}
	}
	if($('ci').innerHTML != "(" + j + " hidden)")
	{
		$('ci').innerHTML = "(" + j + " hidden)"
	}
	else
	{
		$('ci').innerHTML = "(0 hidden)"
	}
}
function addEngine(name,ext,cat,link)
{
	if ((typeof window.sidebar == "object") && (typeof window.sidebar.addSearchEngine == "function"))
	{
		window.sidebar.addSearchEngine(link+name+".src",link+name+".png",name,cat)
	}
	else
	{
		alert("Could not install the search plugin from "+link+" because this browser is not supported or the function is disabled.")
	}
}
function pflag(id)
{
	var r = prompt("Why should this post be flagged for deletion?")
	if(r == "" || r == null)
	{
		return false
	}
	else
	{
new Ajax.Request('./public/report.php?type=post', {
  method: 'get',
      parameters: {
        "rid": id,
		"reason":r
      },
  onSuccess: function(AjaxRequest) {
		if(AjaxRequest.readyState==4)
		{
			notice("Post has been flagged for deletion")
			Effect.Fade('pfd'+id)
		}
		else
		{
			notice("Failed to report post")
		}
  }
});
	}
/*	r = encodeURIComponent(encodeURI(r))
	var HttpRequest
	try
	{
		HttpRequest = new XMLHttpRequest() 
	}
	catch(e)
	{
		try
		{
			HttpRequest = new ActiveXObject("Msxml2.XMLHTTP")
		}
		catch(e)
		{
			try
			{
				HttpRequest = new ActiveXObject("Microsoft.XMLHTTP")
			}
			catch(e)
			{
				alert("Your browser is too old or does not support Ajax.")
			}
		}
	}
	HttpRequest.open('GET','public/report.php?type=post&rid=' + id + '&reason='+r, true)
	HttpRequest.send(null)
	HttpRequest.onreadystatechange=function()
	{
		if(HttpRequest.readyState==4)
		{
			notice("Post has been flagged for deletion")
			Effect.Fade('pfd'+id)
		}
		else
		{
			notice("Failed to report post")
		}
	}*/
}
function papprv(id)
{
new Ajax.Request('./public/approve.php?action=approve_post', {
  method: 'get',
      parameters: {
        "id": id
      },
  onSuccess: function(AjaxRequest) {
		if(AjaxRequest.readyState==4)
		{
			notice("Post has been approved")
			$('pad'+id).innerHTML="<a href=\"#\" onclick=\"punapprv('"+id+"'); return false\">Unapprove</a>"
		}
		else
		{
			notice("Failed to approve post")
		}
  }
});
/*	var HttpRequest
	try
	{
		HttpRequest = new XMLHttpRequest() 
	}
	catch(e)
	{
		try
		{
			HttpRequest = new ActiveXObject("Msxml2.XMLHTTP")
		}
		catch(e)
		{
			try
			{
				HttpRequest = new ActiveXObject("Microsoft.XMLHTTP")
			}
			catch(e)
			{
				alert("Your browser is too old or does not support Ajax.")
			}
		}
	}
	HttpRequest.open('GET','public/approve.php?type=post_approve&rid=' + id, true)
	HttpRequest.send(null)
	HttpRequest.onreadystatechange=function()
	{
		if(HttpRequest.readyState==4)
		{
			notice("Post has been approved")
			$('pad'+id).innerHTML="<a href=\"#\" onclick=\"punapprv('"+id+"') return false\">Unapprove</a>"
		}
		else
		{
			notice("Failed to approve post")
		}
	}*/
}
function punapprv(id)
{
new Ajax.Request('./public/approve.php?action=unapprove_post', {
  method: 'get',
      parameters: {
        "id": id
      },
  onSuccess: function(AjaxRequest) {
		if(AjaxRequest.readyState==4)
		{
			notice("Post has been unapproved")
			$('pad'+id).innerHTML="<a href=\"#\" onclick=\"papprv('"+id+"'); return false\">Approve</a>"
		}
		else
		{
			notice("Failed to unapprove post")
		}
  }
});
/*	var HttpRequest
	try
	{
		HttpRequest = new XMLHttpRequest() 
	}
	catch(e)
	{
		try
		{
			HttpRequest = new ActiveXObject("Msxml2.XMLHTTP")
		}
		catch(e)
		{
			try
			{
				HttpRequest = new ActiveXObject("Microsoft.XMLHTTP")
			}
			catch(e)
			{
				alert("Your browser is too old or does not support Ajax.")
			}
		}
	}
	HttpRequest.open('GET','public/approve.php?type=post_unapprove&rid=' + id, true)
	HttpRequest.send(null)
	HttpRequest.onreadystatechange=function()
	{
		if(HttpRequest.readyState==4)
		{
			notice("Post has been unapproved")
			$('pad'+id).innerHTML="<a href=\"#\" onclick=\"papprv('"+id+"') return false\">Approve</a>"
		}
		else
		{
			notice("Failed to unapprove post")
		}
	}*/
}
/*function select_image_domain(creation_date,image_path)
{
		try
		{
			HttpRequest = new XMLHttpRequest() 
		}
		catch(e)
		{
			try
			{
				HttpRequest = new ActiveXObject("Msxml2.XMLHTTP")
			}
			catch(e)
			{
				try
				{
					HttpRequest = new ActiveXObject("Microsoft.XMLHTTP")
				}
				catch(e)
				{
					alert("Your browser is too old or does not support Ajax.")
				}
			}
		}
		alert("asdf")
		HttpRequest.onreadystatechange=function()
		{
			if(HttpRequest.readyState==4)
			{	
				if(HttpRequest.responseText != "" && HttpRequest.responseText != null)
				{
					update_image_src(HttpRequest.responseText, creation_date, image_path)
				}
				else
				{
					update_image_src(creation_date, creation_date, image_path)
				}
			}
		}
		HttpRequest.open("GET","public/current_date.php",true)
		HttpRequest.send(null)
}
function update_image_src(current_date,creation_date,image_path)
{
	try
	{
		HttpRequest = new XMLHttpRequest() 
	}
	catch(e)
	{
		try
		{
			HttpRequest = new ActiveXObject("Msxml2.XMLHTTP")
		}
		catch(e)
		{
			try
			{
				HttpRequest = new ActiveXObject("Microsoft.XMLHTTP")
			}
			catch(e)
			{
				alert("Your browser is too old or does not support Ajax.")
			}
		}
	}
	if(current_date == creation_date)
	{
		alert("bsdf")
		document.getElementById('image').src='images/' + image_path
	}
	else
	{
		alert("asdf")
		HttpRequest.onreadystatechange=function()
		{
			if(HttpRequest.readyState == 4)
			{
				var tmp = HttpRequest.reponseText
				var domains = tmp.split('<split-here>')
				var i = Math.floor(Math.random()*domains.length)
				var domain = domains[i]
				document.getElementById('image').src=domain+'/'+image_path
				alert(document.getElementById('image').src)
			}
		}
		HttpRequest.open("GET","public/domains.php",true)
		HttpRequest.send(null)
	}
}*/
MassEdit = {submit:function(id)
{
	new Ajax.Request("./public/edit_post.php", {
  method: 'post',
      parameters: {
	"id":$("id"+id).value,
	"tags":$("tags"+id).value,
	"title":$("title"+id).value,
	"source":$("source"+id).value,
	"pconf":$("pconf").value
      },
  onCreate: function () {
			notice("Saving changes for post #"+id+"...");
  },onFailure: function () {
			notice("Errors occured when saving changes for post #"+id+".");
  },
  onSuccess: function() {
			notice("Changes in post #"+id+" have been saved successfully.");
  }
});

/*	var HttpRequest;
	try
	{
		HttpRequest = new XMLHttpRequest(); 
	}
	catch(e)
	{
		try
		{
			HttpRequest = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch(e)
		{
			try
			{
				HttpRequest = new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch(e)
			{
				alert("Your browser is to old or does not support Ajax.");
			}
		}
	}
	HttpRequest.onreadystatechange=function()
{
		if(HttpRequest.readyState == 1)
		{
			document.getElementById("savestatus"+id).innerHTML = "Working...";
		}
		if(HttpRequest.readyState==4)
		{
			document.getElementById("savestatus"+id).innerHTML = HttpRequest.status+" "+HttpRequest.statusText;
			if(HttpRequest.status == 200) {
				var t = setTimeout("document.getElementById('savestatus'+"+id+").innerHTML = '';", 3000);
			}
		}
	}
	
	var data; data = "";
	data += "id="+document.getElementById("id"+id).value+"&";
	data += "tags="+document.getElementById("tags"+id).value+"&";
	data += "title="+document.getElementById("title"+id).value+"&";
	data += "source="+document.getElementById("source"+id).value+"&";
	data += "parent="+document.getElementById("parent"+id).value+"&";
	data += "pconf="+document.getElementById("pconf").value;
	HttpRequest.open("POST", "./public/edit_post.php", true);
	HttpRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	HttpRequest.setRequestHeader("Content-length", data.length);
	HttpRequest.setRequestHeader("Connection", "close");
	HttpRequest.send(data);*/
},
copy:function (field, idarray) {
	var data = $(field).value;
	for (var i=0; idarray[i]; i++) {
		$(field+idarray[i]).value = data;
	}
},
submit_all:function (idarray) {
	for (var i=0; idarray[i]; i++) {
		this.submit(idarray[i]);
	}
},
tagsplus:function (field, idarray) {
	for (var i=0; idarray[i]; i++) {
		$("tags"+idarray[i]).value += " "+$(field).value;
	}
},
tagsminus:function (field, idarray) {
	var tagarray = $(field).value.split(" ");
	for (var i=0; tagarray[i]; i++) {
		pattern = new RegExp("(^"+tagarray[i]+" | "+tagarray[i]+" | "+tagarray[i]+"$)", "gm");
		for (var j=0; idarray[j]; j++) {
			element = $("tags"+idarray[j]);
			element.value = element.value.replace(pattern, " ");
		}
	}
}}
//http://w3schools.invisionzone.com/index.php?showtopic=20914
window.GET = function(){
    var url = window.location.href
    var array = url.indexOf('#') == -1 ?
                url.substring(url.indexOf('?') + 1).split(/&/):
                url.substring(url.indexOf('?') + 1, url.indexOf('#')).split(/&/)
                            //URLs can be like either "sample.html?test1=hi&test2=bye" or
                                //"sample.html?test1=hitest2=bye"
    window._GET = {}
    for(var i = 0; i < array.length; i++){
        var assign = array[i].indexOf('=')
        if(assign == -1){
            _GET[array[i]] = true//if no value, treat as boolean
        }else{
            _GET[array[i].substring(0, assign)] = array[i].substring(assign + 1)
        }
    }
}
//External Functions
function tempMyTags()
{
	var my_tags = Cookie.get("tags").split(" ")
	var tags = $('tags').value.split(' ')
	var my_tags_length = my_tags.length
	var temp_my_tags = Array()
	var g = 0
	for(i in my_tags)
	{
		if(my_tags[i] != "" && my_tags[i] != " " && i <= my_tags_length)
		{
			temp_my_tags[g] = my_tags[i]				
			g++
		}
	}
	my_tags = temp_my_tags
	var links = ''
	j = 0
	my_tags_length = my_tags.length
	for(i in my_tags)
	{
		if(j < my_tags_length)
		{
			if(!tags.include(my_tags[i]))
			{
				links = links+'<a href="index.php?page=post&amps=list&amptags='+my_tags[i]+'" id="t_'+my_tags[i]+'"' + "onclick=\"javascript:toggleTags('"+my_tags[i]+"','tags','t_"+my_tags[i]+"');" + 'return false">'+my_tags[i]+'</a> '
			}
			else
			{
				links = links+'<a href="index.php?page=post&amps=list&amptags='+my_tags[i]+'" id="t_'+my_tags[i]+'"' + "onclick=\"javascript:toggleTags('"+my_tags[i]+"','tags','t_"+my_tags[i]+"');" + 'return false"><b>'+my_tags[i]+'</b></a> '
			}
		}
		j++
	}
	if(j > 0)
		$('my-tags').innerHTML=links
	else
		$('my-tags').innerHTML='<a href="index.php?page=account&amps=options">Edit</a>'
}
function toggleTags(tag, id, lid)
{
	temp = new Array(1)
	temp[0] = tag
	tags = $('tags').value.split(" ")
	if(tags.include(tag))
	{
		$('tags').value=tags.without(tag).join(" ")
		$(lid).innerHTML=tag+" "
	}
	else
	{
		$('tags').value=tags.concat(temp).join(" ")
		$(lid).innerHTML="<b>"+tag+"</b> "
	}
	return false
}
function validate_comment(CommentBox, MinSize)
{
	var comment = $(CommentBox).value.strip()
	var tmp = comment.split(" ")
	if(tmp.size() < MinSize)
	{
		alert("A few more keystrokes and this may actually be interesting...")
		return false
	}
	return true
}
function counter_comment(field, count, max)
{
	if ($(field).value.length > max)
		$(field).value = $(field).value.substring(0, max)
	else
		$(count).value = max - $(field).value.length
}
function forum_quote(text)
{
	$('reply_box').value=$('reply_box').value+'[quote]'+text+'[/quote]'
	Effect.Appear('reply')
}
function scrollFocus(id)
{
	Element.toggle(id)
	if($(id).style.display != 'none')
	{
		$(id).show().scrollTo()
		$(id).focus()
	}
}
//From Imouto
var ClearNoticeTimer;
/* If initial is true, this is a notice set by the notice cookie and not a
 * realtime notice from user interaction. */
function notice(msg, initial) {
  /* If this is an initial notice, and this screen has a dedicated notice
   * container other than the floating notice, use that and don't disappear
   * it. */
  if(initial) {
    var static_notice = $("static_notice");
    if(static_notice) {
      static_notice.update(msg);
      Effect.Appear(static_notice);
      return;
    }
  }
  start_notice_timer();
  $('notice').update(msg);
  Effect.Appear($('notice-container'),{to:0.9});
}
function start_notice_timer() {
  if(ClearNoticeTimer)
    window.clearTimeout(ClearNoticeTimer);

  ClearNoticeTimer = window.setTimeout(function() {
		  Effect.Fade($('notice-container'));
  }, 10000); // 1 second = 1000 miliseconds
}
//From Danbooru
var ClipRange = Class.create({
  initialize: function(min, max) {
    if (min > max)  {
      throw "paramError"
    }

    this.min = min
    this.max = max
  },

  clip: function(x) {
    if (x < this.min) {
      return this.min
    }
    
    if (x > this.max) {
      return this.max
    }
    
    return x
  }
})
//Quick Search Box:
Finder = {
	Stat:null,
	Load : function(){
		this.QuickMenu.Load()	
		//this.QuickAbout.Load()
this.QuickSearch.Load()
	this.QuickLogin.Load()
	this.Stat = 'Loaded';
		},
	Hide: function(com) {
		var com = com+'-box'

		if ($(com) != null) {
			Event.stopObserving(com, "mousedown", $(com).eventMouseDown)
			Draggables.unregister(com)
			Effect.Fade(com)
		}
	},
	Show: function (com) {
		if (	this.Stat != 'Loaded') {this.Load()}
		var com = com+'-box'
		Effect.Appear(com,{to:0.9})
		Event.observe(com, 'mousedown', function(){ Element.bringToFront(com)}, true)
		new Draggable(com,{snap: function(x, y) {
            return[ (x < (window.innerWidth - $(com).clientWidth - 32)) ? (x > 32 ? x : 32 ) : window.innerWidth - $(com).clientWidth - 32,
                    (y < (window.innerHeight - $(com).clientHeight - 32)) ? (y > 32 ? y : 32 ) : window.innerHeight - $(com).clientHeight - 32];
    }})
	/**/},
	Surf: function(url) {		var surfBox = $('quick-surf-box')
		
		if (surfBox == null) {
			this.Explorer.Load();
		}
			$("surf-frame").src = url; this.Show("quick-surf");},

	Explorer:{Load: function(url) {
		var surfBox = $('quick-surf-box')
		
		if (surfBox != null) {
			return;
		}
		
		var html = 
		/*var left = window.innerWidth / 4 
		//) - 32
		var top = 32//(window.innerHeight / 2) - 64
		if (left < 32)
			left = 32

		/*if (top < 32)
			top = 32*/
		 '<div id="quick-surf-box" class="finder-box" style="display: none; left:32px; top:32px;"><h4><a href="#" id="menu-cancel" onclick="Finder.Hide(\'quick-surf\'); return false;" title="Close">✖</a>&nbsp;<a href="#" onclick="showHide(\'surf-frame\'); return false;" title="Toggle">❖</a>&nbsp;<a href="#" onclick="$(\'surf-frame\').src = $(\'surf-frame\').src; return false;" title="Home">❖</a>&nbsp;The Explorer</h4><iframe id="surf-frame" src="" frameborder="0" width="800px" height="600px" style="margin:16px; background:#ffffff;"></iframe></div>'

		new Insertion.Bottom('header', html) //deprecated
		//Finder.Object.Arrange($('quick-search-box'),x,y)
		//new Draggable('quick-search-box');
		//Effect.Appear('quick-search-box',{to:0.9})
    	Element.bringToFront('quick-surf-box');
		//Event.observe('search-cancel', 'click', Finder.bind("Cancel"), true)
		//Event.observe('note-help-' + this.id, 'click', this.bind("help"), true)
		}},
	AboutMe : {
	Show: function(uid){
		if($('about-me-box') == null)
		{Finder.AboutMe.Load(uid)}
		Finder.Show("about-me")
	},
	Load: function(uid) {
		var aboutBox = $('about-me-box')
		
		if (aboutBox != null) {
			return
		}
		
		var html = 
		 '<div id="about-me-box" class="finder-box" style="display: none; left:224px; top:32px;"><h4><a href="#" id="about-me-cancel" onclick="Finder.Hide(\'about-me\'); return false;">✖</a>&nbsp;Account Information</h4><br><div id="edit_account_info"></div></div>'

		new Ajax.Updater("edit_account_info", "index.php?page=account&s=edit&protocol=javascript&id="+uid, {method:'get'})
		new Insertion.Bottom('header', html) //deprecated
    	Element.bringToFront('about-me-box');
	},
Save: function (holder,uid,text) //Password must be encoded
{
new Ajax.Request("index.php?page=account&s=edit&id="+uid, {
  method: 'post',
      parameters: {
        "about" : Base64.encode(text)
      },
  onSuccess: function(AjaxRequest) {
			if(AjaxRequest.responseText == "1")
			{ // Login OK
				Finder.Hide("about-me")
				//notice("Logged in as " + user)
				window.location.reload()
			}
			else //if(HttpRequest.responseText == "1")
			{ // Login FAIL
				Effect.Shake(holder)
			}
  }
});
},
Toggle: function(holder,id)
{ if($(id).wrap=='hard'){$(id).wrap='off';$(holder).value="Word-Wrap = NO"}else{$(id).wrap='hard';$(holder).value="Word-Wrap = YES"}
	}
},
	/*Object:{
		Arrange : function(obj,x,y) {
if(x < (window.innerWidth - obj.clientWidth - 32)){if(x < 32) {x = 32} }else{ x = window.innerWidth - obj.clientWidth - 32}
if(y < 32){ y = 32}
		obj.style.left = x + 'px'
    	obj.style.top = y + 'px'
/*if (window.innerWidth < 1000 || window.innerHeight < 800)
			{Draggables.unregister(obj.id)
			Element.remove(obj.id)}
			}
		},*/
	/*bind: function(method_name) {
		if (!this.bound_methods) {
			this.bound_methods = new Object()
		}

		if (!this.bound_methods[method_name]) {
			this.bound_methods[method_name] = this[method_name].bindAsEventListener(this)
		}

		return this.bound_methods[method_name]
	}/*,
	Hide: function(e) {
		//var searchBox = $('quick-search-box')
		//if (searchBox != null) {
			//Event.stopObserving('quick-search-box', 'mousedown', this.bind("DragStart"), true)
			//Event.stopObserving('search-cancel', 'click', this.bind("Cancel"), true)
			//Event.stopObserving('note-help-' + this.id, 'click', this.bind("help"), true)

			//Effect.Fade('quick-search-box')
		//}

	},*/
	QuickMenu : {

	Load: function() {
		var menuBox = $('quick-menu-box')
		
		if (menuBox != null) {
			//Element.remove(searchBox)
			return
		}
		
		var html = 
		/*var left = window.innerWidth / 4 
		//) - 32
		var top = 32//(window.innerHeight / 2) - 64
		if (left < 32)
			left = 32

		/*if (top < 32)
			top = 32*/
		 '<div id="quick-menu-box" class="finder-box" style="display: none; left:32px; top:32px; width:160px;"><h4><a href="#" id="menu-cancel" onclick="Finder.Hide(\'quick-menu\'); return false;">✖</a>&nbsp;Main Menu</h4><br><br><div style="font-size:18px; text-align:center;"><p><a href="index.php">Home</a></p><p><a href="#" onclick="Finder.Show(\'quick-search\'); return false;">Search</a></p><p id="login_check">';
			if($("account").textContent == "My Account"){
html+='<a href="index.php?page=account&s=login&code=01">Log Out</a>'}
			else{html+='<a href="#" onclick="Finder.Show(\'quick-login\'); return false;">Log In</a>'}
		//html +=
		//Finder.QuickLogin.Check("login_check")// == true)
		html += '</p><p><a href="#" onclick="Finder.QuickAbout.Show(); return false;">About</a></p></div></div>'

		new Insertion.Bottom('header', html) //deprecated
		//Finder.Object.Arrange($('quick-search-box'),x,y)
		//new Draggable('quick-search-box');
		//Effect.Appear('quick-search-box',{to:0.9})
    	Element.bringToFront('quick-menu-box');
		//Event.observe('search-cancel', 'click', Finder.bind("Cancel"), true)
		//Event.observe('note-help-' + this.id, 'click', this.bind("help"), true)
	}/*,
	Hide: function(e) {
		var menuBox = 'quick-menu-box'

		if ($(menuBox) != null) {
			Event.stopObserving(menuBox, "mousedown", $(menuBox).eventMouseDown)
			Draggables.unregister(menuBox)
			Effect.Fade(menuBox)
		}
	},
	Show: function () {
		var menuBox = 'quick-menu-box'
		Effect.Appear(menuBox,{to:0.9})
		Event.observe(menuBox, 'mousedown', function(e){ Element.bringToFront(menuBox)}, true)
		new Draggable(menuBox,{snap: function(x, y) {
            return[ (x < (window.innerWidth - $(menuBox).clientWidth - 32)) ? (x > 32 ? x : 32 ) : window.innerWidth - $(menuBox).clientWidth - 32,
                    (y > 32 ? y : 32) ];
    }})
	/*}*/},
	QuickAbout : {
	/*Get : function(holder) {
new Ajax.Request("about.php", {
  method: "get",
  onSuccess: function(AjaxRequest) {
DEPRECATED
  }
});	},*/
	Show: function(){
		if($('quick-about-box') == null)
		{Finder.QuickAbout.Load()}
		Finder.Show("quick-about")
	},
	Load: function() {
		var menuBox = $('quick-about-box')
		
		if (menuBox != null) {
			return
		}
		
		var html = '<div id="quick-about-box" class="finder-box" style="display: none; left:224px; top:32px;"><h4><a href="#" id="about-cancel" onclick="Finder.Hide(\'quick-about\'); return false;">✖</a></h4><div id="about_data" style="width:400px; text-align:center;"></div><br></div>';
		
		//this.Get()
		new Ajax.Updater("about_data", "index.php?page=about&protocol=javascript", {method:'get'})
		new Insertion.Bottom('header', html)
    	Element.bringToFront('quick-about-box')
	}
	},
	QuickLogin : {
Login: function (holder,user,pass) //Password must be encoded
{
new Ajax.Request("index.php?page=account&s=login&code=00&protocol=javascript", {
  method: 'post',
      parameters: {
        "user" : user , "pass" : Base64.encode(pass)
      },
  onSuccess: function(AjaxRequest) {
			if(AjaxRequest.responseText == "1")
			{ // Login OK
			notice("Logged in! This page will be reloaded in 5 seconds!")
				Finder.Hide("quick-login")
				//notice("Logged in as " + user)
				window.setTimeout(function(){window.location.reload()
				/*.location = "index.php?page=account&s=home"*/},5000);
			}
			else //if(HttpRequest.responseText == "1")
			{ // Login FAIL
			notice("The username or password you entered is incorrect!");
				Effect.Shake(holder)
			}
  }
});
	/*var HttpRequest
	try
	{
		HttpRequest = new XMLHttpRequest() 
	}
	catch(e)
	{
		try
		{
			HttpRequest = new ActiveXObject("Msxml2.XMLHTTP")
		}
		catch(e)
		{
			try
			{
				HttpRequest = new ActiveXObject("Microsoft.XMLHTTP")
			}
			catch(e)
			{
				alert("Your browser is too old or does not support Ajax.")
			}
		}
	}
	HttpRequest.onreadystatechange=function()
	{
		if(HttpRequest.readyState==4)
		{
			if(HttpRequest.responseText == "1")
			{ // Login OK
				Finder.Hide("quick-login")
				//notice("Logged in as " + user)
				document.location = "index.php?page=account&s=home"
			}
			else //if(HttpRequest.responseText == "1")
			{ // Login FAIL
				Effect.Shake(holder)
			}
			/*else
			{
				updateScore("fav"+id,HttpRequest.responseText)
				notice("Post added to favorites")
			}
		}
	}
	HttpRequest.open("POST","index.php?page=account&s=login&code=00&protocol=javascript",true)
HttpRequest.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	HttpRequest.send("user=" + user + "&pass=" + pass)*/
},
/*Logout: function () //Password must be encoded
{
	var HttpRequest
	try
	{
		HttpRequest = new XMLHttpRequest() 
	}
	catch(e)
	{
		try
		{
			HttpRequest = new ActiveXObject("Msxml2.XMLHTTP")
		}
		catch(e)
		{
			try
			{
				HttpRequest = new ActiveXObject("Microsoft.XMLHTTP")
			}
			catch(e)
			{
				alert("Your browser is too old or does not support Ajax.")
			}
		}
	}
	HttpRequest.onreadystatechange=function()
	{
		if(HttpRequest.readyState==4)
		{
			if(HttpRequest.responseText == "1")
			{ // Login OK
				//Finder.QuickLogin.Hide()
				//notice("Logged in as " + user)
				window.location.reload()
			}
			/*else //if(HttpRequest.responseText == "1")
			{ // Login FAIL
				Effect.Shake(holder)
			}*/
			/*else
			{
				updateScore("fav"+id,HttpRequest.responseText)
				notice("Post added to favorites")
			}//
		}
	}
	HttpRequest.open("GET","index.php?page=account&s=login&code=01&protocol=javascript",true)
//HttpRequest.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	HttpRequest.send(null)
},*/
/*Check: function (id) //Password must be encoded
{
new Ajax.Request("index.php?page=account&s=login&code=02", {
  method: 'get',
  onSuccess: function(AjaxRequest) {
			if(AjaxRequest.responseText == "1"){
$(id).innerHTML='<a href="index.php?page=account&s=login&code=01">Log Out</a>'}
			else{$(id).innerHTML='<a href="#" onclick="Finder.Show(\'quick-login\'); return false;">Log In</a>'}
  }
});
	/*var HttpRequest
	try
	{
		HttpRequest = new XMLHttpRequest() 
	}
	catch(e)
	{
		try
		{
			HttpRequest = new ActiveXObject("Msxml2.XMLHTTP")
		}
		catch(e)
		{
			try
			{
				HttpRequest = new ActiveXObject("Microsoft.XMLHTTP")
			}
			catch(e)
			{
				alert("Your browser is too old or does not support Ajax.")
			}
		}
	}
	HttpRequest.onreadystatechange=function()
	{
		if(HttpRequest.readyState==4)
		{
			if(HttpRequest.responseText == "1"){
$(id).innerHTML='<a href="index.php?page=account&s=login&code=01">Log Out</a>'}
			else{$(id).innerHTML='<a href="#" onclick="Finder.Show(\'quick-login\'); return false;">Log In</a>'}
		}
	}
	HttpRequest.open("GET","index.php?page=account&s=login&code=02",true)
//HttpRequest.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	HttpRequest.send(null)
	//			return 
},*/
	Load: function() {
		var loginBox = $('quick-login-box')
		
		if (loginBox != null) {
			//Element.remove(searchBox)
			return
		}
		
		var html = 
		/*var top = 32//(window.outerWidth / 2) - 480
		var left = window.outerHeight / 4

		if (left < 32)
			left = 32

		/*if (top < 32)
			top = 32*/

		 '<div id="quick-login-box" class="finder-box" style="display: none; top: 32px; left: 224px;"><h4><a href="#" id="login-cancel" onclick="Finder.Hide(\'quick-login\'); return false;">✖</a>&nbsp;Log In</h4><br><form action="" style="padding: 0; margin: 0px 16px 24px 16px;" class="finder-form"><p>Username:<br><input type="text" id="user" name="user" value=""></p><p>Password:<br><input type="password" id="pass" name="pass" value=""></p><!--<p><a href="index.php?page=account&amp;s=reset_password">Forgotten your password?</a></p>--><p class="login-box-submit"><input type="submit" onclick="Finder.QuickLogin.Login(\'quick-login-box\', $(\'user\').value, $(\'pass\').value); return false;" value="Log in"><input type="submit" onclick="document.location=\'index.php?page=account&s=reg\'; return false;" value="Sign up"><input type="submit" style="width:168px; margin:0;" onclick="document.location=\'index.php?page=account&s=reset_password\'; return false;" value="Password recovery"></p></form></div>'

		new Insertion.Bottom('header', html) //deprecated
		//Finder.Object.Arrange($('quick-login-box'),left,top)
		//new Draggable('quick-search-box');
		//Effect.Appear('quick-search-box',{to:0.9})
    	Element.bringToFront('quick-login-box');
		//Event.observe('search-cancel', 'click', Finder.bind("Cancel"), true)
		//Event.observe('note-help-' + this.id, 'click', this.bind("help"), true)
	}/*,
	Hide: function(e) {
		var loginBox = 'quick-login-box'

		if ($(loginBox) != null) {
			Event.stopObserving(loginBox, "mousedown", $(loginBox).eventMouseDown)
			Draggables.unregister(loginBox)
			Effect.Fade(loginBox)
		}
	},
	Show: function () {
		var loginBox='quick-login-box'
		Effect.Appear(loginBox,{to:0.9})
		Event.observe(loginBox, 'mousedown', function(e){ Element.bringToFront(loginBox)}, true)
		new Draggable(loginBox,{snap: function(x, y) {
            return[ (x < (window.innerWidth - $(loginBox).clientWidth - 32)) ? (x > 32 ? x : 32 ) : window.innerWidth - $(loginBox).clientWidth - 32,
                    (y > 32 ? y : 32) ];
    }})
	//},
		*/},
	QuickSearch : {

	Load: function() {
		var searchBox = $('quick-search-box')
		
		if (searchBox != null) {
			//Element.remove(searchBox)
			return
		}
		
		var html = 
		/*var left = window.innerWidth / 4 
		//) - 32
		var top = 32//(window.innerHeight / 2) - 64
		if (left < 32)
			left = 32

		/*if (top < 32)
			top = 32*/
		 '<div id="quick-search-box" class="finder-box" style="display: none; left:224px; top:32px;"><h4><a href="#" id="search-cancel" onclick="Finder.Hide(\'quick-search\'); return false;">✖</a>&nbsp;Quick Search</h4><br><form action="index.php?page=search" method="post" style="padding: 0; margin: 0px 106px 24px 16px;"><select name="type" class="main_search_select"><option value="posts" selected>Posts</option><option value="forum">Forum</option><option value="tags">Tags</option><option value="alias">Alias</option><option value="trac">Trac</option></select><input name="tags" type="text" style="width:320px; border-left:0px;" class="main_search_text" value=""><input type="submit" value="Search" id="search-submit"  class="main_search" value=""></form></div>'

		new Insertion.Bottom('header', html) //deprecated
		//Finder.Object.Arrange($('quick-search-box'),x,y)
		//new Draggable('quick-search-box');
		//Effect.Appear('quick-search-box',{to:0.9})
    	Element.bringToFront('quick-search-box');
		//Event.observe('search-cancel', 'click', Finder.bind("Cancel"), true)
		//Event.observe('note-help-' + this.id, 'click', this.bind("help"), true)
	}
	}//,
	/*Cancel: function(e) {
		this.Hide(e)
		Event.stop(e)
	}*//*,
  DragStart: function(e) {
    var node = e.element().nodeName
    if ((node != 'FORM' && node == 'INPUT') || node == 'A') {
      return
    }

    document.observe("mousemove", this.Drag.bindAsEventListener(this))
    document.observe("mouseup", this.DragStop.bindAsEventListener(this))
    document.observe("selectstart", function() {return false})

    this.searchBox = $('quick-search-box');
    this.cursorStartX = e.pointerX()
    this.cursorStartY = e.pointerY()
    this.searchStartX = this.searchBox.offsetLeft
    this.searchStartY = this.searchBox.offsetTop
    Element.bringToFront(this.searchBox.id);
    this.dragging = true
  },

  // Stop dragging the edit box
  DragStop: function(e) {
    document.stopObserving()

    this.cursorStartX = null
    this.cursorStartY = null
    this.searchStartX = null
    this.searchStartY = null
    this.dragging = false
  },

  // Update the edit box's position as it gets dragged
  Drag: function(e) {
    var left = this.searchStartX + e.pointerX() - this.cursorStartX
    var top = this.searchStartY + e.pointerY() - this.cursorStartY

	var bound

		bound = 32
		if (left < bound)
			left = bound

		bound = window.innerWidth - this.searchBox.clientWidth - 32
		if (left > bound)
			left = bound

		bound = 32
		if (top < bound)
			top = bound

    this.searchBox.style.left = left + 'px'
    this.searchBox.style.top = top + 'px'

    e.stop()
  }*/
}
//Added for Checksum Reporter and else.
Report = {
	header: '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head><title>Report @ ' + new Date() + '</title><link rel="stylesheet" type="text/css" media="all" href="default.css" title="default"><meta http-equiv="content-type" content="text/html; charset=utf-8"><meta name="author" content="Racozsoft Corporation"></head><body>',
	footer: '</body></html>',
	show: function(report_data) {
		var printer = window.open()
		printer.document.open("text/html")
		printer.document.write(this.header)
		printer.document.write($(report_data).innerHTML)
		printer.document.write(this.footer)
		printer.document.close()
	},
	print: function(print_data, show_notice) {
		if (confirm("Do you want to print this report?"))
		{
		if (show_notice == true)
			notice("Printing report...")
		var printer = window.open()
		printer.document.open("text/html")
		printer.document.write(this.header)
		printer.document.write($(print_data).innerHTML)
		printer.document.write(this.footer)
		printer.document.close()
		printer.print()
		printer.window.close()
		if (show_notice == true)
			notice("Report printed.")
		}
	}
}
//Page Effects
document.observe('dom:loaded', 
function () {
	document.body.setStyle("display:none")
	new Effect.Appear(document.body)
	//Finder.Load()
	//Finder.QuickLogin.Show()
}
)