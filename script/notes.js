/*note.js*/var Note = Class.create()
Note.zindex = 0
Note.counter = -1
Note.all = []
Note.display = true

Note.show = function() {
	for (var i=0; i<Note.all.length; ++i) {
		Note.all[i].bodyHide()
		Note.all[i].elements.box.appear({to:Note.all[i].elements.box.getOpacity()})
	}
}

Note.hide = function() {
	for (var i=0; i<Note.all.length; ++i) {
		Note.all[i].bodyHide()
		Note.all[i].elements.box.fade()
	}
}

Note.find = function(id) {
	for (var i = 0; i<Note.all.length; ++i) {
		if (Note.all[i].id == id) {
			return Note.all[i]
		}
	}
	return null
}

Note.toggle = function() {
	if (Note.display) {
		Note.hide()
		Note.display = false
	} else {
		Note.show()
		Note.display = true
	}
}

Note.updateNoteCount = function() {
	if (Note.all.length > 0) {
		var label = ""

		if (Note.all.length == 1)
			label = "note"
		else
			label = "notes"

		$('note-count').innerHTML = "This post has <a href=\"#\" onclick=\"document.location='index.php?page=history&type=page_notes&id=" + Note.post_id + "'\">" + Note.all.length + " " + label + "</a>"
	} else {
		$('note-count').innerHTML = ""
	}
};

Note.create = function() {
	var html = ''
	html += '<div class="note-box unsaved" style="width: 150px; height: 150px; '
	html += 'top: ' + ($('image').clientHeight / 2 - 75) + 'px; '
	html += 'left: ' + ($('image').clientWidth / 2 - 75) + 'px;" '
	html += 'id="note-box-' + Note.counter + '">'
	html += '<div class="note-corner" id="note-corner-' + Note.counter + '"></div>'
	html += '</div>'
	html += '<div class="note-body" title="Click to edit" id="note-body-' + Note.counter + '"></div>'
	new Insertion.Bottom('note-container', html)
	Note.all.push(new Note(Note.counter, true))
	Note.counter -= 1
};

Note.prototype = {
	// Necessary because addEventListener/removeEventListener don't play nice with
	// different instantiations of the same method.
	bind: function(method_name) {
		if (!this.bound_methods) {
			this.bound_methods = new Object()
		}

		if (!this.bound_methods[method_name]) {
			this.bound_methods[method_name] = this[method_name].bindAsEventListener(this)
		}

		return this.bound_methods[method_name]
	},

	initialize: function(id, is_new) {
		this.id = id
		this.is_new = is_new

		// get the elements
		this.elements = {
			box:		$('note-box-' + this.id),
			corner:		$('note-corner-' + this.id),
			body:		$('note-body-' + this.id),
			image:		$('image')
		}

		// store the data
		this.old = {
			left:		this.elements.box.offsetLeft,
			top:		this.elements.box.offsetTop,
			width:		this.elements.box.clientWidth,
			height:		this.elements.box.clientHeight,
			body:		this.elements.body.innerHTML
		}

		// reposition the box to be relative to the image

		this.elements.box.style.top = this.elements.box.offsetTop + "px"
		this.elements.box.style.left = this.elements.box.offsetLeft + "px"

		// Make the note translucent
		if (is_new) {
			this.elements.box.setOpacity(0.2)
		} else {
			this.elements.box.setOpacity(0.5)      
		}

		// attach the event listeners
		Event.observe(this.elements.box, "mousedown", this.bind("dragStart"), true)
		Event.observe(this.elements.box, "mouseout", this.bind("bodyHideTimer"), true)
		Event.observe(this.elements.box, "mouseover", this.bind("bodyShow"), true)
		Event.observe(this.elements.corner, "mousedown", this.bind("resizeStart"), true)
		Event.observe(this.elements.body, "mouseover", this.bind("bodyShow"), true)
		Event.observe(this.elements.body, "mouseout", this.bind("bodyHideTimer"), true)
		Event.observe(this.elements.body, "click", this.bind("showEditBox"), true)
	},

	textValue: function() {
		return this.old.body.replace(/(?:^\s+|\s+$)/, '')
	},

	hideEditBox: function(e) {
		var editBox = $('edit-box')

		if (editBox != null) {
			// redundant?
			//Event.stopObserving('edit-box', 'mousedown', this.bind("editDragStart"), true)
			Event.stopObserving('edit-box', "mousedown", $('edit-box').eventMouseDown)
			Event.stopObserving('note-save-' + this.id, 'click', this.bind("save"), true)
			Event.stopObserving('note-cancel-' + this.id, 'click', this.bind("cancel"), true)
			Event.stopObserving('note-remove-' + this.id, 'click', this.bind("remove"), true)
			Event.stopObserving('note-history-' + this.id, 'click', this.bind("history"), true)
			Event.stopObserving('note-help-' + this.id, 'click', this.bind("help"), true)
			Draggables.unregister('edit-box')
			Element.remove('edit-box')
		}

	},

	showEditBox: function(e) {
		//var editBox = $('edit-box')

		this.hideEditBox(e)

		var html = ''

		var top = this.elements.box.offsetTop - 14
		var left = this.elements.box.offsetLeft - 14
		var width = this.elements.box.clientWidth
		var height = this.elements.box.clientHeight
		
		html += '<div id="edit-box" style="opacity:0.9; position: absolute; visibility: visible; z-index: 128; background: #222; border: 1px solid #AAA; border-radius: 16px; box-shadow: 0px 0px 16px #000; padding: 12px;">'
		html += '<form onsubmit="return false;" style="padding: 0; margin: 0;">'
		html += '<textarea rows="7" id="edit-box-text" style="width:'+width+'px; height:'+height+'px; min-width:256px; min-height:128px; margin: 2px 2px 12px 2px; border: 0px; background: #222; resize:none;">' + this.textValue() + '</textarea>'
		html += '<center><input type="submit" style="border: 0px;" value="Save" name="save" id="note-save-' + this.id + '">'
		html += '<input type="submit" style="border: 0px;" value="Cancel" name="cancel" id="note-cancel-' + this.id + '">'
		html += '<input type="submit" style="border: 0px;" value="Remove" name="remove" id="note-remove-' + this.id + '">'
		html += '<input type="submit" style="border: 0px;" value="History" name="history" id="note-history-' + this.id + '">'
		html += '<input type="submit" style="border: 0px;" value="Help" name="help" id="note-help-' + this.id + '"></center>'
		html += '</form>'
		html += '</div>'

		new Insertion.Bottom('note-container', html)
		var editBox = $('edit-box')
		
if (left < ($('image').clientWidth - editBox.clientWidth))
{if(left < 0 ) left = 0} else{ left =($('image').clientWidth - editBox.clientWidth)}
if(top < ($('image').clientHeight - editBox.clientHeight)){if (top < 0 ) top = 0 } else{ top=($('image').clientHeight - editBox.clientHeight)}/*(top > 0 ? top : 0)*/

    	editBox.style.left = left + 'px'
    	editBox.style.top = top + 'px'

		this.a = new Draggable('edit-box',{snap: function(x, y) {return[ (x < ($('image').clientWidth - editBox.clientWidth)) ? (x > 0 ? x : 0 ) : ($('image').clientWidth - editBox.clientWidth),(y < ($('image').clientHeight - editBox.clientHeight)) ? (y > 0 ? y : 0 ) : ($('image').clientHeight - editBox.clientHeight)/*(y > 0 ? y : 0)*/]}});
		//Element.show('note-container')
		//Effect.Appear('edit-box',{to:0.9})
		Element.bringToFront('edit-box');
		Event.observe('edit-box', 'mousedown', this.bind("editDragStart"), true)
		Event.observe('note-save-' + this.id, 'click', this.bind("save"), true)
		Event.observe('note-cancel-' + this.id, 'click', this.bind("cancel"), true)
		Event.observe('note-remove-' + this.id, 'click', this.bind("remove"), true)
		Event.observe('note-history-' + this.id, 'click', this.bind("history"), true)
		Event.observe('note-help-' + this.id, 'click', this.bind("help"), true)
	},

	bodyShow: function(e) {
		if (this.dragging)
			return

		if (this.hideTimer) {
			clearTimeout(this.hideTimer)
			this.hideTimer = null
		}

		// hide the other notes
		if (Note.all) {
			for (var i=0; i<Note.all.length; ++i) {
				if (Note.all[i].id != this.id) {
					Note.all[i].bodyHide()
				}
			}
		}

		this.elements.box.style.zIndex = ++Note.zindex
		this.elements.body.style.zIndex = Note.zindex
		//Element.bringToFront(this.elements.box.id)
		//Element.bringToFront(this.elements.body.id)
		this.elements.body.style.top = (this.elements.box.offsetTop + this.elements.box.clientHeight + 5) + "px"
		this.elements.body.style.left = this.elements.box.offsetLeft + "px"
		this.elements.body.style.display = "block"
	},

	bodyHideTimer: function(e) {
		this.hideTimer = setTimeout(this.bind("bodyHide"), 250)
	},

	bodyHide: function(e) {
		this.elements.body.style.display = "none"
	},

	dragStart: function(e) {
		Event.observe(document.documentElement, 'mousemove', this.bind("drag"), true)
		Event.observe(document.documentElement, 'mouseup', this.bind("dragStop"), true)
		document.onselectstart = function() {return false}

		this.cursorStartX = Event.pointerX(e)
		this.cursorStartY = Event.pointerY(e)
		this.boxStartX = this.elements.box.offsetLeft
		this.boxStartY = this.elements.box.offsetTop
		this.dragging = true

		this.bodyHide()
	},

	dragStop: function(e) {
		Event.stopObserving(document.documentElement, 'mousemove', this.bind("drag"), true)
		Event.stopObserving(document.documentElement, 'mouseup', this.bind("dragStop"), true)
		document.onselectstart = function() {return true}

		this.cursorStartX = null
		this.cursorStartY = null
		this.boxStartX = null
		this.boxStartY = null
		this.dragging = false

		this.bodyShow()
	},

	drag: function(e) {
		var left = this.boxStartX + Event.pointerX(e) - this.cursorStartX
		var top = this.boxStartY + Event.pointerY(e) - this.cursorStartY
		var bound

		bound = 5
		if (left < bound)
			left = bound

		bound = this.elements.image.clientWidth - this.elements.box.clientWidth - 5
		if (left > bound)
			left = bound

		bound = 5
		if (top < bound)
			top = bound

		bound = this.elements.image.clientHeight - this.elements.box.clientHeight - 5
		if (top > bound)
			top = bound

		this.elements.box.style.left = left + 'px'
		this.elements.box.style.top = top + 'px'

		Event.stop(e)
	},

  // Start dragging the edit box
  editDragStart: function(e) {
    //if (Note.debug) {
    //  console.debug("Note#editDragStart (id=%d)", this.id)
    //}
    
    //var node = e.element().nodeName
    //if (node != 'FORM' && node != 'DIV') {
    //  return
    //}

    //document.observe("mousemove", this.editDrag.bindAsEventListener(this))
    //document.observe("mouseup", this.editDragStop.bindAsEventListener(this))
    //document.observe("selectstart", function() {return false})

    //this.elements.editBox = $('edit-box');
    //this.cursorStartX = e.pointerX()
    //this.cursorStartY = e.pointerY()
    //this.editStartX = this.elements.editBox.offsetLeft
    //this.editStartY = this.elements.editBox.offsetTop
    Element.bringToFront('edit-box');
    //this.dragging = true
  },

  // Stop dragging the edit box
  /*editDragStop: function(e) {
    if (Note.debug) {
      console.debug("Note#editDragStop (id=%d)", this.id)
    }
    document.stopObserving()

    this.cursorStartX = null
    this.cursorStartY = null
    this.editStartX = null
    this.editStartY = null
    this.dragging = false
  },

  // Update the edit box's position as it gets dragged
  editDrag: function(e) {
    /*var left = this.editStartX + e.pointerX() - this.cursorStartX
    var top = this.editStartY + e.pointerY() - this.cursorStartY
		if (left < 0)
			left = 0
		if (top < 0)
			top = 0*/
    /*var x = this.elements.editBox.style.left - 'px'// = left + 'px'
    var y = this.elements.editBox.style.top - 'px'// = top + 'px'
	
	[ (x < ($('note-container').clientWidth - $('edit-box').clientWidth)) ? (x > 0 ? x : 0 ) : ($('note-container').clientWidth - $('edit-box').clientWidth),(y > 0 ? y : 0)]
	
    this.elements.editBox.style.left = x + 'px'
    this.elements.editBox.style.top = y + 'px'
    e.stop()
  },*/

  // Start resizing the note
  resizeStart: function(e) {
    if (Note.debug) {
      console.debug("Note#resizeStart (id=%d)", this.id)
    }
    
    this.cursorStartX = e.pointerX()
    this.cursorStartY = e.pointerY()
    this.boxStartWidth = this.elements.box.clientWidth
    this.boxStartHeight = this.elements.box.clientHeight
    this.boxStartX = this.elements.box.offsetLeft
    this.boxStartY = this.elements.box.offsetTop
    this.boundsX = new ClipRange(10, this.elements.image.clientWidth - this.boxStartX - 5)
    this.boundsY = new ClipRange(10, this.elements.image.clientHeight - this.boxStartY - 5)
    this.dragging = true

    document.stopObserving()
    document.observe("mousemove", this.resize.bindAsEventListener(this))
    document.observe("mouseup", this.resizeStop.bindAsEventListener(this))
    
    e.stop()
    this.bodyHide()
  },

  // Stop resizing teh note
  resizeStop: function(e) {
    if (Note.debug) {
      console.debug("Note#resizeStop (id=%d)", this.id)
    }
    
    document.stopObserving()

    this.boxCursorStartX = null
    this.boxCursorStartY = null
    this.boxStartWidth = null
    this.boxStartHeight = null
    this.boxStartX = null
    this.boxStartY = null
    this.boundsX = null
    this.boundsY = null
    this.dragging = false

    e.stop()
  },

  // Update the note's dimensions as it gets resized
  resize: function(e) {
    var width = this.boxStartWidth + e.pointerX() - this.cursorStartX
    var height = this.boxStartHeight + e.pointerY() - this.cursorStartY
    width = this.boundsX.clip(width)
    height = this.boundsY.clip(height)

    this.elements.box.style.width = width + "px"
    this.elements.box.style.height = height + "px"
    var ratio = this.ratio()
    this.fullsize.width = width / ratio
    this.fullsize.height = height / ratio

    e.stop()
  },

	save: function(e) {
		this.old.left = this.elements.box.offsetLeft
		this.old.top = this.elements.box.offsetTop
		this.old.width = this.elements.box.clientWidth
		this.old.height = this.elements.box.clientHeight
		this.old.body = $('edit-box-text').value
		this.elements.body.innerHTML = this.textValue()

		this.hideEditBox(e)
		this.bodyHide()

		var params = []
		params.push("note%5Bx%5D=" + this.old.left)
		params.push("note%5By%5D=" + this.old.top)
		params.push("note%5Bwidth%5D=" + this.old.width)
		params.push("note%5Bheight%5D=" + this.old.height)
		params.push("note%5Bbody%5D=" + encodeURIComponent(this.old.body))
		params.push("note%5Bpost_id%5D=" + Note.post_id)
		
		notice("Saving note...")
		new Ajax.Request('./public/note_save.php?id=' + this.id, {
			asynchronous: true,
			method: 'get',
			parameters: params.join("&"),
			onComplete: function(req) {
				if (req.status == 200) {
					notice("Note saved")
					//var response = eval(req.responseText)
					var response = req.responseText.split(":");
					if (response[1] < 0) {
						var n = Note.find(response[1])
						n.id = response[0]
						n.elements.box.id = 'note-box-' + n.id
						n.elements.body.id = 'note-body-' + n.id
						n.elements.corner.id = 'note-corner-' + n.id
						n.is_new = false;
					}
				n.elements.box.setOpacity(0.5)
				n.elements.box.removeClassName('unsaved')
				} else {
					notice("Error: " + req.responseText)
					this.elements.box.addClassName('unsaved')
					//alert(req.status);
				}
			}
		})
		Event.stop(e)
	},

	cancel: function(e) {
		this.hideEditBox(e)
		this.bodyHide()

		this.elements.box.style.top = this.old.top + "px"
		this.elements.box.style.left = this.old.left + "px"
		this.elements.box.style.width = this.old.width + "px"
		this.elements.box.style.height = this.old.height + "px"
		this.elements.body.innerHTML = this.old.body

		Event.stop(e)
	},

	removeCleanup: function() {
		Element.remove(this.elements.box)
		Element.remove(this.elements.body)

		var allTemp = []
		for (i=0; i<Note.all.length; ++i) {
			if (Note.all[i].id != this.id) {
				allTemp.push(Note.all[i])
			}
		}

		Note.all = allTemp
		Note.updateNoteCount()
	},

	remove: function(e) {
		if (confirm('Are you sure you want to delete this note?'))
		{
		this.hideEditBox(e)
		this.bodyHide()
		if (this.is_new) {
			this.removeCleanup()
			notice("Note removed")
		} else {
			notice("Removing note...")

			new Ajax.Request('./public/remove.php?id=' + Note.post_id + "&note_id=" + this.id, {
				asynchronous: true,
				method: 'get',
				onComplete: function(req) {
					if (req.status == 403) {
						notice("Access denied")
					} else if (req.status == 500) {
						notice("Error: " + req.responseText)
					} else {
						Note.find(parseInt(req.responseText)).removeCleanup()
						notice("Note removed")
					}
				}
			})
		}
		Event.stop(e)
		}
	},

	history: function(e) {
		this.hideEditBox(e)

		if (this.is_new) {
			notice("This note has no history")
		} else {
			document.location='index.php?page=history&type=note&id=' + this.id + "&pid=" + Note.post_id;
		}
	},

	help: function(e) {
		this.hideEditBox(e)
		document.location='index.php?page=help&topic=notes';
	}

}
